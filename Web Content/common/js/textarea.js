        function processTextAreaField(startPos, partLength, partGap, numParts, textAreaName) {                 
                var text = document.forms["HATSForm"].elements[textAreaName].value;                 
                text = formatString(text, partLength);                 
                
                text = fixStringLength(text, partLength * numParts); 
                                                
                for (var i = 0; i < numParts; i++) { 
                        var segment = text.substring(i * partLength, (i * partLength) + partLength); 
                        //alert("!" + segment + "! , length: " + segment.length); 
                        var fieldStart = startPos + (i * partGap) + (i * partLength); 
                        var fieldName = "in_" + fieldStart + "_" + partLength; 
                        //alert(fieldName); 
                        document.forms["HATSForm"].elements[fieldName].value = segment; 
                } 
                
                return true; 
        } 
        
        function formatString(text, partLength) { 
                text = text.replace(/\r/g,''); // remove \r (only applicable in IE) 
                
                //alert("first \n" + text.indexOf('\n')); 
                //alert("first \r" + text.indexOf('\r')); 
                
                while (text.indexOf('\n') != -1) { 
                        var index = text.indexOf('\n'); 
                        //alert(index); 
                //        alert(text.substring(0, index)); 
                //alert(index); 
                        text = text.substring(0, index ) + fixStringLength("", partLength - (index % partLength)) + text.substring(index + 1, text.length); 
                }         
        
                return text; 
        } 
        
        // generates a string of the specified length (either by truncating or padding) 
        function fixStringLength(string, length) { 
        var oldString = string + ""; 
        //alert("fix: " + string + ", " + length + ", " + string.length); 
                if (string.length < length) { 
                        for (var i = string.length; i < length; i++) { 
                                string = string + " "; 
                        } 
                } else if (string.length > length) { 
                        string = string.substring(0, length); 
                } 
                
                //alert("fix: !" + oldString + "! new: !" + string + "! length: "+ string.length); 
                
                return string; 
        } 
        
        function limitText(limitField, limitCount, limitNum) {
        	//alert(limitField);
        	if (limitField.value.length > limitCount-1) {
        		limitField.value = limitField.value.substring(0, limitCount);
        	} 
        	//else {  		limitCount.value = limitNum - limitField.value.length;       	}
        }

        
        function updateLengthField(element, maxLength, partLength) {         
                var text = element.value;
                alert(text);
                text = formatString(text, partLength); 
                
                var lengthElement = element.form.elements[element.name + "_length"]; 
                
                if (text.length > maxLength) { 
                        lengthElement.style.color = "red"; 
                } else { 
                        lengthElement.style.color = "green"; 
                } 
                                
                lengthElement.value = text.length; 
        }