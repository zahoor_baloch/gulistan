//function to get XMLHTTPObject to process asynchronous calls.
function getXMLHttp()
{
  var xmlHttp

  try
  {
    //Firefox, Opera 8.0+, Safari
    xmlHttp = new XMLHttpRequest();
	if (xmlHttp.overrideMimeType){
    	xmlHttp.overrideMimeType('text/xml');        
    }	
  }
  catch(e)
  {
    //Internet Explorer
    try
    {
      xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(e)
    {
      try
      {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch(e)
      {
        alert("Your browser does not support AJAX!")
        return false;
      }
    }
  }
  return xmlHttp;
}

//function called by the front-end html page to process async call back.
function MakeRequest(divId, page)
{	
	var callToAction = page;	
	var response = "<div style='float: left; margin-left:150px; width: 200px;' ><img align='center' src='images/load.gif'/></div>";
	document.getElementById(divId).innerHTML = response;
  var xmlHttp = getXMLHttp();
 
  xmlHttp.onreadystatechange = function()
  {
    if(xmlHttp.readyState == 4)
    {
      HandleResponse(xmlHttp.responseText,divId);
    }
  }  
  xmlHttp.open("GET", callToAction, true); 
  xmlHttp.send(null);
}

function focusedMakeRequest(divId,page)
{	
	var callToAction = page;	
	var response = "<div style='float: left; margin-left:150px; width: 200px;' ><img align='center' src='images/load.gif'/></div>";
	document.getElementById(divId).innerHTML = response;
  var xmlHttp = getXMLHttp();
 
  xmlHttp.onreadystatechange = function()
  {
    if(xmlHttp.readyState == 4)
    {
      focusedHandleResponse(xmlHttp.responseText,divId,focus);
      
    }
  }  
  xmlHttp.open("GET", callToAction, true); 
  xmlHttp.send(null);
}
//function to render response returned by async call.
function focusedHandleResponse(response,id,focus)
{
  	document.getElementById(id).innerHTML = response;
	focus=document.getElementById('modelId').focus();
}
function HandleResponse(response,id,focus)
{
  	document.getElementById(id).innerHTML = response;
}

<!-- "ANY FOOL CAN WRITE CODE A COMPUTER UNDERSTANDS, A REAL CODER WRITES CODE WHICH IS READABLE BY HUMANS" -->