//Licensed Materials - Property of IBM
//
// Copyright IBM Corp. 2010, 2011  All Rights Reserved.

//.============================================================================
//.Function:  Load the JSON information and behaviors into the Widget
//.============================================================================









function bindJSONDataToTextBox(uLabel, uInputWidget, JSONData)
{
	if (isDijit(uInputWidget) && JSONData && JSONData.value)
	{
		var elemValue = JSONData.value; 
		if (elemValue.caption){
			uLabel.innerHTML = elemValue.caption;
		}
		//uInputDomNode represents the DOM node of uInputWidget
		var uInputDomNode = uInputWidget.domNode;
		//bind JSON data to the Widget DOM node so the data can be accessed through the Widget DOM node
		uInputDomNode.JSONData = JSONData;
		uInputDomNode.hatsDojoConnections = [];
		var hc = uInputDomNode.hatsDojoConnections;
		//uInput represents the textbox of uInputWidget and is used for event/attribute binding
		var uInput = uInputWidget.textbox;		
		dojo.attr(uInput,"name",getPosLengthString(uInputWidget)); 
		setDijitAttribute(uInputWidget, "value", rightTrimFromString(elemValue.text));
		uInput.defaultValue = getDijitAttribute(uInputWidget, "value");
		dojo.style(uInputDomNode,"width",elemValue.length+"em");
		if (!(CodePage==420 || CodePage==424 || CodePage==803)){ 
			hc.push(dojo.connect(uInput, "onfocus", function(evt) {
			  setFocusFieldIntoGlobal(this, formID);}));
		}
		hc.push(dojo.connect(uInputWidget, "onChange", function(evt) {
		  checkInput(this);}));
		hc.push(dojo.connect(uInputWidget, "onClick", function(evt) {
		  setCursorPosition(elemValue.startPos, formID);}));
		var isDBCSSession = isDBCSCodePage(CodePage);
		if (conntype == 5250 || conntype == 3270){ 
			if (conntype == 5250){ 
				if (!elemValue.isProtected && elemValue.hsrAttributes){
					if (elemValue.hsrAttributes.monocase){
						hc.push(dojo.connect(uInputWidget, "onKeyPress", "convertToUpperCase"));
						hc.push(dojo.connect(uInput, "onpaste", "convertToUpperCase"));
						hc.push(dojo.connect(uInput, "ondrop", "convertToUpperCase"));
						hc.push(dojo.connect(uInput, "onblur", "convertToUpperCase"));
						hc.push(dojo.connect(uInputWidget, "onClick", "convertToUpperCase"));
					}
					if (elemValue.hsrAttributes.rightFillBlank || 
						elemValue.hsrAttributes.rightFillZero ||
						elemValue.hsrAttributes.fieldExitRequired ){
						dojo.attr(uInput,"disableAutoAdvance","true");
					}
					dojo.attr(uInput,"autoEnter", (elemValue.hsrAttributes.autoEnter)?true:false);
					dojo.attr(uInput,"fieldExitRequired", (elemValue.hsrAttributes.fieldExitRequired)?true:false);
					dojo.attr(uInput,"required", (elemValue.hsrAttributes.required)?true:false);
					dojo.attr(uInput,"fillRequired", (elemValue.hsrAttributes.fillRequired)?true:false);
				}
			}
			if (!elemValue.isProtected && elemValue.hsrAttributes){	
				var isSBCSOnly = true;
				var isFieldTypeFound = false;
				var methodVar;
				var imeModeVar = "inactive";
				var maxLengthVar = elemValue.length;	
				if (conntype == 5250){ 
					if (elemValue.hsrAttributes.alpha){
						methodVar = "allowAlphabeticOnly";
					} else
					if (elemValue.hsrAttributes.digitsOnly){
						methodVar = "allowDigitsOnly";
					} else
					if (elemValue.hsrAttributes.signedNumeric){
						methodVar = "allowSignedNumeric";
					} else
					if (elemValue.hsrAttributes.numericOnly){
						methodVar = "allowNumericOnly";
					} else
					if (elemValue.hsrAttributes.keyboardInhibited){
						methodVar = "inhibitKeyboardEntry";
						hc.push(dojo.connect(uInputWidget, "onKeyUp", "inhibitKeyboardEntry"));
						hc.push(dojo.connect(uInput, "oncut", "inhibitKeyboardEntry"));
						imeModeVar = "disabled";
						isSBCSOnly = false;
					} else						
					if (isDBCSSession && elemValue.hsrAttributes.dbcsOnly){
						methodVar = "allowDBCSOnly5250";
						imeModeVar = "active";
						isSBCSOnly = false;
						if(showUnProtectedSosiAsSpace) {
							if(!eliminateMaxLengthForDBCSFields) {
								if(!(CodePage == 1390 || CodePage == 1399)){
									maxLengthVar = (elemValue.length/2) + 1;
								}
							} else {
								maxLengthVar = null;
								dojo.attr(uInput,"eliminateMaxlengthInIdeographicFields","true");
							}
							if(CodePage == 1390 || CodePage == 1399) {
								if (getCharacterCount(uInputWidget.value) == (elemValue.length/2)) {
									setDijitAttribute(uInputWidget, "value", getDijitAttribute(uInputWidget, "value") + " ");
								}
							} else {
								if (uInputWidget.value && uInputWidget.value.length == (elemValue.length/2)) {
									setDijitAttribute(uInputWidget, "value", getDijitAttribute(uInputWidget, "value") + " ");
								}
							}
						} else {
							setDijitAttribute(uInputWidget, "value", leftTrimFromString(getDijitAttribute(uInputWidget, "value")));
							if(!eliminateMaxLengthForDBCSFields) {
								if(!(CodePage == 1390 || CodePage == 1399)) {
									maxLengthVar = (elemValue.length/2)-1;
								}
							} else {
								maxLengthVar = null;
								dojo.attr(uInput,"eliminateMaxlengthInIdeographicFields","true");
							}
						}
					} else
					if (isDBCSSession && elemValue.hsrAttributes.dbcsPure) {
						methodVar = "allowDBCSPure";
						imeModeVar = "active";
						isSBCSOnly = false;
						if(!eliminateMaxLengthForDBCSFields) {
							if(!(CodePage == 1390 || CodePage == 1399)){
								maxLengthVar = elemValue.length/2;
							}
						} else {
							maxLengthVar = null;
							dojo.attr(uInput,"eliminateMaxlengthInIdeographicFields","true");
						}
					} else
					if (elemValue.hsrAttributes.unicode){
						isSBCSOnly = false;
						isFieldTypeFound = true;
						hc.push(dojo.connect(uInputWidget, "onKeyPress", "allowUnicodeOnly"));
						dojo.attr(uInput,"maxLength",(elemValue.length/2));
						if(eliminateMaxLengthForDBCSFields){
							hc.push(dojo.connect(uInput, "onfocus", "allowUnicodeOnly"));
						}
					} else if (isDBCSSession && elemValue.hsrAttributes.dbcsOpen){
						methodVar = "allowDataLengthChecking";
						imeModeVar = null;
						isSBCSOnly = false;
						hc.push(dojo.connect(uInputWidget, "onKeyUp", "allowDataLengthChecking"));
						hc.push(dojo.connect(uInputWidget, "onKeyDown", "allowDataLengthChecking"));							
						if(eliminateMaxLengthForDBCSFields){
							maxLengthVar = null;
							dojo.attr(uInput,"eliminateMaxlengthInIdeographicFields","true");
						}
					} else
					if (isDBCSSession && elemValue.hsrAttributes.dbcsEither){
						methodVar = "allowDBCSEither";
						imeModeVar = "active";
						isSBCSOnly = false;
						var isJFieldMode = false;
						if (uInputWidget.value && uInputWidget.value.length > 0){
							var tmpValue = removeSpaceAsSOSI(uInputWidget.value);
							if (tmpValue.length>0){
								isJFieldMode = isDBCSChar(tmpValue.charAt(0), enableDBCSSession, enableDBCSEuro, CodePage);
							}
						}
						if (isJFieldMode){
							if(showUnProtectedSosiAsSpace) {
								if(!eliminateMaxLengthForDBCSFields) {
									if(!(CodePage == 1390 || CodePage == 1399)){
										maxLengthVar = (elemValue.length/2)+1;
									}
								} else {
									maxLengthVar = null;
									dojo.attr(uInput,"eliminateMaxlengthInIdeographicFields","true");
								}
								if(CodePage == 1390 || CodePage == 1399){
									if (getCharacterCount(uInputWidget.value) == (elemValue.length/2)) {
										setDijitAttribute(uInputWidget, "value", getDijitAttribute(uInputWidget, "value") + " ");
									}
								} else {
									if (uInputWidget.value && uInputWidget.value.length == (elemValue.length/2)) {
										setDijitAttribute(uInputWidget, "value", getDijitAttribute(uInputWidget, "value") + " ");
									}
								}
							} else {
								if(!eliminateMaxLengthForDBCSFields) {
									maxLengthVar = (elemValue.length/2)-1;
								} else {
									maxLengthVar = null;
									dojo.attr(uInput,"eliminateMaxlengthInIdeographicFields","true");
								}
							}
						} else {
							if(eliminateMaxLengthForDBCSFields) {
								maxLengthVar = null;
								dojo.attr(uInput,"eliminateMaxlengthInIdeographicFields","true");
							}
							imeModeVar = "inactive";
						}
					} else
					if (isDBCSSession && elemValue.hsrAttributes.katakanaShift){
						methodVar = "allowKatakanaShift";
						imeModeVar = "active";
					}
				} else {
					if (elemValue.hsrAttributes.numeric){
						methodVar = "allowAlphaNumericShift";
					} else
					if(isDBCSSession && elemValue.hsrAttributes.dbcsOnly) {
						methodVar = "allowDBCSOnly3270";
						imeModeVar = "active";
						isSBCSOnly = false;
						if(!eliminateMaxLengthForDBCSFields) {
							if(!(CodePage == 1390 || CodePage == 1399)) {
								maxLengthVar = elemValue.length/2;
							}
						} else {
							maxLengthVar = null;
							dojo.attr(uInput,"eliminateMaxlengthInIdeographicFields","true");
						}
					} else
					if(isDBCSSession && elemValue.hsrAttributes.dbcsOpen) {
						methodVar = "allowDataLengthChecking";
						isSBCSOnly = false;
						hc.push(dojo.connect(uInputWidget, "onKeyUp", "allowDataLengthChecking"));
						hc.push(dojo.connect(uInputWidget, "onKeyDown", "allowDataLengthChecking"));
						if(eliminateMaxLengthForDBCSFields){
							maxLengthVar = null;
							dojo.attr(uInput,"eliminateMaxlengthInIdeographicFields","true");
						}
					}
				}
				if (methodVar) {
					hc.push(dojo.connect(uInputWidget, "onKeyPress", methodVar));
					hc.push(dojo.connect(uInput, "onpaste", methodVar));
					hc.push(dojo.connect(uInput, "ondrop", methodVar));
					hc.push(dojo.connect(uInput, "onblur", methodVar));
					hc.push(dojo.connect(uInputWidget, "onClick", methodVar));
					if (imeModeVar) {
						dojo.style(uInput,"imeMode",imeModeVar);
					}
					if (maxLengthVar) {
						dojo.attr(uInput,"maxLength",maxLengthVar);
					}
					if(eliminateMaxLengthForDBCSFields){
						hc.push(dojo.connect(uInput, "onfocus", methodVar));
					}
					isFieldTypeFound = true;
				} 	
				if(isDBCSSession && isSBCSOnly) {
					hc.push(dojo.connect(uInputWidget, "onKeyPress", "allowSBCSOnly"));
				}	
				if (isSBCSOnly && !isFieldTypeFound) {
					hc.push(dojo.connect(uInputWidget, "onKeyPress", "allowAlphaNumericShift"));
					hc.push(dojo.connect(uInput, "onpaste", "allowAlphaNumericShift"));
					hc.push(dojo.connect(uInput, "ondrop", "allowAlphaNumericShift"));
					hc.push(dojo.connect(uInput, "onblur", "allowAlphaNumericShift"));
					hc.push(dojo.connect(uInputWidget, "onClick", "allowAlphaNumericShift"));
					if(eliminateMaxLengthForDBCSFields){
						hc.push(dojo.connect(uInput, "onfocus", "allowAlphaNumericShift"));
					}
					dojo.style(uInput,"imeMode","inactive");
					dojo.attr(uInput,"maxLength",elemValue.length);
				}				
			}
		}
		if (!showUnProtectedSosiAsSpace){
			if (uInputWidget.value && uInputWidget.value.length > 0){
				setDijitAttribute(uInputWidget, "value", removeSpaceAsSOSI(uInputWidget.value));
			}
		}
	}
}

