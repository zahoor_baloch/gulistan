<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true" %>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*" %>
<%@ page import="java.util.Locale" %>
<%
    Locale locale = request.getLocale();
	HatsMsgs m = new HatsMsgs("runtime",locale);
%>
<html>
  <head>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><%= m.get("PRINT_TITLE") %></title>
  </head>
  <body>
  <center><%
  if (exception == null)
  { %>
  <h3><%= m.get("PRINT_NOT_SUPPORTED") %></h3><%
  } 
  else
  { %>
  <h3><%= exception.getClass().getName() %></h3>
  <pre><%= exception.getMessage() %></pre><%
  } %>
      <br>
	  <input type="button" value="<%= m.get("PRINT_CLOSE_WINDOW_CAPTION") %>" onclick="window.close()" >
  </center>
  </body>
</html>
