<%// out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<% //out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<%session.setAttribute("orderInquiry_Detail_MoreInfo_sv1",null); %>
<!-- Insert your HATS component tags here. -->

<!--ddd-->
<div class="container">

<div class="heading_bg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
			<td width="33%" class="topbar_headingtxtred1" colspan="3" align="center">  <HATS:Component
				col="3" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="78"
				componentSettings="" row="2" /></td>
		</tr>
</table>
</div>

<div class="clear4px"></div>

<div class="heading_bg" style="display:none;">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="26" align="center" valign="middle" class="tiltletxt">Type in complete or partial style and/or color description.</td>
    </tr>
  </table>
</div>

<div class="clear10px" style="display:none;"></div>


<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top3.gif"></td>
    <td background="../common/images/center_middle_top3.gif"></td>
    <td width="5" height="5" background="../common/images/right_top3.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle3.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="txt2">
      <tr>
        <td width="212" align="left" class="fillbar" style="padding:3px 10px 3px 3px;"><strong>Bill To:</strong></td>
        <td width="220" bgcolor="#fffcf5" class="fillbarnew" style="padding:3px; border-top: 1px solid #b99b75;">
        <HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="4" /></td>
		<td width="2"></td>
        <td align="left" class="fillbar" style="padding:3px 10px 3px 3px;"><strong>Ship To:</strong></td>
        <td width="220" bgcolor="#fffcf5" class="fillbarnew" style="padding:3px; border-top: 1px solid #b99b75;">
        <HATS:Component col="44" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="73"
						componentSettings="" row="4" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58"><HATS:Component col="2"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="5" /></td>
        <td width="2"></td>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58"><HATS:Component col="44"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="73"
						componentSettings="" row="5" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58"><HATS:Component col="2"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="6" /></td>
        <td width="2"></td>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58"><HATS:Component col="44"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="73"
						componentSettings="" row="6" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
					<td align="left" bgcolor="#fffcf5" class="fillbor58"
						style="padding: 3px;"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="trim:false|style:|labelStyleClass:HATSCAPTION|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="29"
						componentSettings="" row="7" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<HATS:Component col="33" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="37"
						componentSettings="" row="7" /></td>
					<td width="2"></td>
        <td>&nbsp;</td>
					<td align="left" bgcolor="#fffcf5" class="fillbor58"
						style="padding: 3px;"><HATS:Component col="44" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="trim:false|style:|labelStyleClass:HATSCAPTION|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="71"
						componentSettings="" row="7" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="7" /></td>
					<td width="2"></td>
				</tr>
      </table>
    </td>
    <td background="../common/images/right_middle3.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom3.gif"></td>
    <td background="../common/images/center_middle_bottom3.gif"></td>
    <td height="5" background="../common/images/right_bottom3.gif"></td>
  </tr>
  </table>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->

</div>


<!--Menu Start-->
<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top3.gif"></td>
    <td background="../common/images/center_middle_top3.gif"></td>
    <td width="5" height="5" background="../common/images/right_top3.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle3.gif"></td>
    <td align="center" valign="top" bgcolor="#fffcf5">
    <table width="100%" border="0" cellspacing="0" cellpadding="3" class="txt2 fillbor1">
      <tr>
        <td width="200" align="left" bgcolor="#d1c4a2"><strong>Ordered By :</strong></td>
        <td width="200"><HATS:Component col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="9" /></td>
        <td width="250" align="left" bgcolor="#d1c4a2"><strong>Order Date :</strong></td>
        <td><HATS:Component col="63" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="70"
						componentSettings="" row="9" /></td>
        </tr>
      <tr>
        <td align="left" bgcolor="#d1c4a2"><strong>P.O. Number :</strong></td>
        <td width="200"><HATS:Component col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="10" /></td>
        <td align="left" bgcolor="#d1c4a2"><strong>Request Date :</strong></td>
        <td width="200"><HATS:Component col="63" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="70"
						componentSettings="" row="10" /></td>
        </tr>
      <tr>
        <td align="left" bgcolor="#d1c4a2">&nbsp;</td>
        <td>&nbsp;</td>
        <td align="left" bgcolor="#d1c4a2"><strong>Ship Complete :</strong></td>
        <td><HATS:Component col="63" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="63"
						componentSettings="" row="11" /></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#d1c4a2"><strong>Order Comments :</strong></td>
        <td colspan="3"><HATS:Component col="20" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="12" /></td>
      </tr>
      </table>
    </td>
    <td background="../common/images/right_middle3.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom3.gif"></td>
    <td background="../common/images/center_middle_bottom3.gif"></td>
    <td height="5" background="../common/images/right_bottom3.gif"></td>
  </tr>
</table>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->
      
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td height="165" background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="top">
      
      <div style="vertical-align: top; overflow:auto; height:165px; border:solid 1px #CCC;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
<thead>
                                                        <tr class="oddRowdd">
                                                          <td width="50" align="left" style="padding: 3px;">Style </td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Desc</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Color</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Desc</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Type</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Roll #</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Length</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Sq Yds</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Lot #</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Seq</td>
                                                        </tr>
                                                        </thead>
                                                      <tr class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="6"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="15" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="35"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="37" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="49"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="51" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="67"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="69" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="74"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="15" /></td>
                                                      </tr>
                                                      <tr class="oddRowd">
                                                        <td align="left"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="6"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="16" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="35"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="37" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="49"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="51" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="67"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="69" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="74"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="16" /></td>
                                                      </tr>
													  <tr class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="6"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="17" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="35"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="37" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="49"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="51" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="67"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="69" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="74"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="17" /></td>
                                                      </tr>
                                                      <tr class="oddRowd">
                                                        <td align="left"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="6"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="18" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="35"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="37" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="49"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="51" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="67"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="69" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="74"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="18" /></td>
                                                      </tr>
                                                      <tr class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="6"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="19" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="35"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="37" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="49"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="51" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="67"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="69" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="74"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="19" /></td>
                                                      </tr>
                                                      <tr class="oddRowd">
                                                        <td align="left"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="6"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="20" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="35"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="37" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="49"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="51" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="67"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="69" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="74"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="20" /></td>
                                                      </tr>
                                                      <tr class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="6"
						componentSettings="" row="21" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="21" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="35"
						componentSettings="" row="21" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="37" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="21" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="49"
						componentSettings="" row="21" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="51" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="21" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="67"
						componentSettings="" row="21" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="69" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="74"
						componentSettings="" row="21" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="21" /></td>
                                                      </tr>
                                                      <tr class="oddRowd">
                                                        <td align="left"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="22" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="6"
						componentSettings="" row="22" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="22" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="22" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="22" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="22" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="22" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="35"
						componentSettings="" row="22" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="37" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="22" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="22" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="22" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="49"
						componentSettings="" row="22" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="51" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="22" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="22" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="22" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="67"
						componentSettings="" row="22" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="69" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="22" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="74"
						componentSettings="" row="22" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="22" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="22" /></td>
                                                      </tr>
                                                      <tr class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="23" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="6"
						componentSettings="" row="23" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="23" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="23" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="23" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="23" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="23" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="35"
						componentSettings="" row="23" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="37" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="23" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="23" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="23" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="49"
						componentSettings="" row="23" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="51" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="23" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="23" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="23" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="67"
						componentSettings="" row="23" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="69" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="23" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="74"
						componentSettings="" row="23" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="23" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="23" /></td>
                                                      </tr>
                </table>
            </div></td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
</table>
</div>
<!--Mene End-->

<div class="clear0px"></div>

	<div class="btnmain">
  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
		<td align="center">
	<table width="auto" border="0" cellspacing="5" cellpadding="0">
		<tr>
		<td style="border: none;">
		<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');">Back</a></div>
		</td>
		<td style="border: none;">
		<div class="allbtn"><a href="javascript:ms('[pf5]','hatsportletid');" title="More Info">More Info</a></div>
		</td>
		</tr>
   	</table>
   		</td>
        <td width="78" align="center">
		<div id="nextButton" class="next_btn"><a href="javascript:ms('[pagedn]','HATSForm');"></a></div>
		<div id="previousButton" class="prev_btn"><a href="javascript:ms('[pageup]','HATSForm');"></a></div>
		</td>
		</tr>
	</table>
	</div>
</div>
<!--ddd-->





<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<% //out.println("<!--blank.jsp"); %>
</body>
</html>
<% //out.println("-->"); %>