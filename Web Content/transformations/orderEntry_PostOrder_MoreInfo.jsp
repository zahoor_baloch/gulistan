<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<HATS:Form>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>

<input type="hidden" name="hatsgv_menuClicked" id="hatsgv_menuClicked"/>

<input type="hidden" name="in_742_1" id="in_742_1"/>
<input type="hidden" name="in_790_1" id="in_790_1"/>




<div class="container">
<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top3.gif"></td>
    <td background="../common/images/center_middle_top3.gif"></td>
    <td width="5" height="5" background="../common/images/right_top3.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle3.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="txt2">
      <tr>
        <td width="215" align="left" class="fillbar" style="padding:3px 10px 3px 3px;"><strong>Bill To:</strong></td>
        <td width="200" bgcolor="#fffcf5" class="fillbarnew" style="padding: 2px 3px 0px 3px; border-top: 1px solid #b99b75;">
        <HATS:Component col="10" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="39"
						componentSettings="" row="3" /></td>
        <td width="2"></td>
        <td align="left" class="fillbar" style="padding:3px 10px 3px 3px;"><strong>Ship To:</strong></td>
        <td width="254" bgcolor="#fffcf5" class="fillbarnew" style="padding: 2px 3px 0px 3px; border-top: 1px solid #b99b75;">
        <HATS:Component col="50" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="3" BIDIOpposite="false" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58" style="padding: 2px 3px 0px 3px"><HATS:Component col="10"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="39"
						componentSettings="" row="4" /></td>
        <td width="2"></td>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58" style="padding: 2px 3px 0px 3px">
        <HATS:Component col="50" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:field25|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:fieldtxt04_2|reverseVideoStyle:|usingMonospaceFont:false|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="79"
						componentSettings="" row="4" BIDIOpposite="false" />
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58" style="padding: 2px 3px 0px 3px"><HATS:Component col="10"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="39"
						componentSettings="" row="5" /></td>
        <td width="2"></td>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58" style="padding: 2px 3px 0px 3px">
        <HATS:Component col="50" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:field25|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="79"
						componentSettings="" row="5" BIDIOpposite="false" />
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58" style="padding: 2px 3px 0px 3px"><HATS:Component
						col="10" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="39"
						componentSettings="" row="6" /></td>
        <td width="2"></td>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58" style="padding: 2px 3px 0px 3px">
        <HATS:Component col="50" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:field25|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:fieldtxt04|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="79"
						componentSettings="" row="6" BIDIOpposite="false" />
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>

					<td align="right" bgcolor="#fffcf5" class="fillbor58"
						style="padding: 2px 3px 0px 3px;"><HATS:Component col="35"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="39"
						componentSettings="" row="7" /></td>
					<td width="2"></td>
        <td>&nbsp;</td>
					<td bgcolor="#fffcf5" class="fillbor58" style="padding:2px 0px 0px 3px;" align="right"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:field7|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:fieldtxt04|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="79"
						componentSettings="" row="7" BIDIOpposite="false" /></td>
				</tr>
      </table>
    </td>
    <td background="../common/images/right_middle3.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom3.gif"></td>
    <td background="../common/images/center_middle_bottom3.gif"></td>
    <td height="5" background="../common/images/right_bottom3.gif"></td>
  </tr>
  </table>

<div class="clear4px"></div>
<div class="linsprtd"></div>
<div class="clear4px"></div>
</div>


<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top3.gif"></td>
    <td background="../common/images/center_middle_top3.gif"></td>
    <td width="5" height="5" background="../common/images/right_top3.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle3.gif"></td>
    <td align="center" valign="top" bgcolor="#fffcf5">
    <table width="100%" border="0" cellspacing="0" cellpadding="1" class="txt2 fillbor1">
      <tr>
        <td align="left" bgcolor="#d1c4a2" style="padding: 0px 0px 0px 3px"><strong>Ordered By :</strong></td>
        <td width="200">
          <HATS:Component col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="26"
						componentSettings="" row="8" />
          </td>
        <td align="left" bgcolor="#d1c4a2" style="padding: 0px 0px 0px 3px"><strong> </strong></td>
					<td>        
								

        			</td>
        </tr>
      <tr>
        <td align="left" bgcolor="#d1c4a2" style="padding: 0px 0px 0px 3px"><strong>P.O. Number :</strong></td>
        <td width="200"><HATS:Component col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="31"
						componentSettings="" row="9" /></td>
        <td align="left" bgcolor="#d1c4a2" style="padding: 0px 0px 0px 3px"><strong>Via :</strong></td>
        <td width="200"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt7|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="76"
						componentSettings="" row="9" /></td>
        </tr>
      <tr>
        <td align="left" bgcolor="#d1c4a2" style="padding: 0px 0px 0px 3px"><strong>Ship Complete (Y/N) :</strong></td>
        <td><HATS:Component col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Yes=Y;No=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:HATSDROPDOWN|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="22"
						componentSettings="" row="10" /></td>
        <td align="left" bgcolor="#d1c4a2" style="padding: 0px 0px 0px 3px"><strong>Prepaid(P) or Collect(C) :</strong></td>
        <td><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Prepaid=P;Collect=C;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:HATSDROPDOWN|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="70"
						componentSettings="" row="10" /></td>
        </tr>
      <tr>
        <td align="left" bgcolor="#d1c4a2" style="padding: 0px 0px 0px 3px"><strong>Std Shipping Inst :</strong></td>
        <td><HATS:Component col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="71"
						componentSettings="" row="11" /></td>
        <td align="left" bgcolor="#d1c4a2">&nbsp;<strong>Requested
					Ship Date :</strong></td>
        <td><HATS:Component col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.CalendarWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="tableCellStyleClass:|trimSpacesOnInputs:true|stripUnderlinesOnInputs:false|tableRowStyleClass:|tableStyleClass:HATSTABLE|columnsPerRow:1|tableCaptionCellStyleClass:|labelStyleClass:HATSCAPTION|restrictMinDate:false|useInlineCalendar:false|inputFieldStyleClass:fieldtxt4|blinkStyle:font-style: italic|restrictMaxDate:false|captionSource:VALUE|launcherImage:calendar.gif|mapExtendedAttributes:false|patternLocaleSelected:|defaultValue:|rangeEnd:|launcherCaption:Date|underlineStyle:text-decoration: underline|preserveColors:false|readOnly:false|columnSeparatorStyle:border-width: 1px; border-style: solid|style:|pattern:MMddyy|patternLocale:BROWSER|caption:|eliminateMaxlengthInIdeographicFields:false|launcherButtonStyleClass:allbtn23|launcherType:BUTTON|rangeStart:|launcherLinkStyleClass:HATSLINK|reverseVideoStyle:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="28"
						componentSettings="" row="7" /></td>
        </tr>
      <tr>
        <td align="left" bgcolor="#d1c4a2" style="padding: 0px 0px 0px 3px"><strong>Order Comments :</strong></td>
        <td colspan="3"><HATS:Component col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxtfull|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="12" /><br>
						
						<HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxtfull|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="13" /></td>
        </tr>
      </table>
    </td>
    <td background="../common/images/right_middle3.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom3.gif"></td>
    <td background="../common/images/center_middle_bottom3.gif"></td>
    <td height="5" background="../common/images/right_bottom3.gif"></td>
  </tr>
</table>
      
      
      <div class="clear4px"></div>      
      <div class="linsprtd"></div>      
      <div class="clear4px"></div>
      
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td height="75" background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="top">
      
      <div style="border:solid 1px #CCC; overflow: auto; height: 90px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
<thead>
                                                        <tr class="oddRowdd">
                                                          <td width="50" align="left" style="padding: 3px;">Delete</td>
                                                          <td width="150" align="left" class="leftline2" style="padding: 3px;">Style </td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Desc</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Color</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Desc</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Type</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Width</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Length</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Sq Yds</td>
                                                          <td width="100" align="left" class="leftline2" style="padding: 3px;">Price</td>
                                                        </tr>
                                                        </thead>
                                                      <tr id="row_1" class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.CheckboxWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="style:|tableRowStyleClass:|tableStyleClass:HATSTABLE|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:D|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="16" />
                                                        </td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="8"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="10" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="22"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="24" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="28"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="30" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="37"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="42"
						componentSettings="" row="16" /></td>
						<td align="left" class="leftline"><HATS:Component col="44" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="48"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="50" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="57"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="59" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="66"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline">&nbsp;<HATS:Component
						col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="80"
						componentSettings="" row="16" /></td>
                                                      </tr>
						<tr id="row_2" class="evenRowd">
                                                        <td align="left"></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="10" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="22"
						componentSettings="" row="17" /><HATS:Component col="6"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="25"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="33" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="44"
						componentSettings="" row="17" /></td>
						<td class="leftline">&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="55"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"><HATS:Component
						col="57" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="66"
						componentSettings="" row="17" />&nbsp;</td>
                                                      </tr>
                        <tr id="row_3" class="evenRowd">
                                                        <td align="left"></td>
                                                        <td align="left" class="leftline"></td>
                                                        <td align="left" class="leftline"></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="29"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"><HATS:Component
						col="30" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="37"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"><HATS:Component
						col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="18" /></td>
						<td class="leftline">&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="43" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="48"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"><HATS:Component col="51"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="55"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="57" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="18" />&nbsp;<HATS:Component
						col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="18" /></td>
                                                      </tr>
                        <tr id="row_4">
                                                        <td align="left"></td>
                                                        <td align="left" class="leftline">&nbsp;</td>
                                                        <td align="left" class="leftline"></td>
                                                        <td align="left" class="leftline"></td>
                                                        <td align="left" class="leftline"></td>
                                                        <td align="left" class="leftline"></td>
                                                        <td class="leftline">&nbsp;</td>
                                                        <td align="left" class="leftline"></td>
                                                        <td align="left" class="leftline"></td>
                                                        <td align="left" class="leftline"></td>
                                                      </tr>                                                                                                                  	
                        <tr id="row_5" class="oddRowd">
                                                        <td align="left">
                                                        <HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.CheckboxWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="style:|tableRowStyleClass:|tableStyleClass:HATSTABLE|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:D|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="8"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="10" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="22"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="24" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="28"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="30" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="37"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="42"
						componentSettings="" row="19" /></td>
						<td align="left" class="leftline"><HATS:Component col="44" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="48"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="50" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="57"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="59" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="66"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline">&nbsp;<HATS:Component
						col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="80"
						componentSettings="" row="19" /></td>
                                                      </tr>
						<tr id="row_6" class="oddRowd">
                                                        <td align="left"></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="25"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="33" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;" ><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="44"
						componentSettings="" row="20" /></td>
						<td class="leftline">&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="55"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"><HATS:Component
						col="57" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="66"
						componentSettings="" row="20" />&nbsp;</td>
                                                      </tr>
                        <tr id="row_7" class="oddRowd">
                                                        <td align="left"></td>
                                                        <td align="left" class="leftline"></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"></td>
                                                        <td align="left" class="leftline" style="font-weight: lighter;"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="29"
						componentSettings="" row="21" /></td>
                                                        <td align="left" class="leftline""><HATS:Component
						col="31" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="36"
						componentSettings="" row="21" /></td>
                                                        <td align="left" class="leftline"  style="font-weight: lighter;"><HATS:Component
						col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="21" /></td>
						<td class="leftline">&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="43" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="48"
						componentSettings="" row="21" /></td>
                                                        <td align="left" class="leftline"  style="font-weight: lighter;"><HATS:Component
						col="51" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="55"
						componentSettings="" row="21" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="57" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="21" /></td>
                                                      </tr>                                                          
                </table>
            </div></td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
</table>

<div class="clear2px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="middle" class="topbar_headingtxtrad">
      &nbsp;<HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="24" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="24" />&nbsp;
      </td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
  
</table>

</div>

<div class="clear0px"></div>

<div class="btnmain">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table width="auto" border="0" cellspacing="5" cellpadding="0">
          <tr>
		<td style="border: none;">
		<div class="allbtn"><a href="javascript:ms('[pf3]','HATSForm');" title="Back">Back</a></div>
		</td>
		<td style="border: none;">
		<div class="allbtn"><a href="javascript:ms('[enter]','HATSForm');" title="Delete">Delete</a></div>
		</td>
		<td style="border: none;">
		<div class="allbtn"><a href="javascript:ms('[pf6]','HATSForm');" title="Post Order">Post Order</a></div>
		</td>
		<td style="border: none;">
		<div class="allbtn"><a href="javascript:ms('[pf2]','HATSForm');" title="Add Line">Add Line</a></div>
		</td>
		<td style="border: none;">
		<div class="allbtn"><a href="javascript:ms('[pf4]','HATSForm');" title="Ship to">Ship to</a></div>
		</td>
		<td style="border: none;">
		<div class="allbtn"><a href="javascript:ms('[pf12]','HATSForm');" title="Cancel">Cancel</a></div>
		</td>
		<td style="border: none;">
		<div class="allbtn"><a href="javascript:ms('[pf5]','HATSForm');" title="Detail">Detail</a></div>
		</td>
		</tr>        </table></td>
        <td width="78" align="center">
      <div id="nextButton" class="next_btn"><a href="javascript:ms('[pagedn]','HATSForm');"></a></div>
      <div id="previousButton" class="prev_btn"><a href="javascript:ms('[pageup]','HATSForm');"></a></div>
      </td>
    </tr>
  </table>
</div>
</div>


	<%
	String isPrevPage = "false";
	String postOrder_nextPage_moreInfo= ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("postOrder_nextPage_moreInfo", true).getString(0);

	if(session.getAttribute("postOrder_nextPage_moreInfo_sv") == null){
	session.setAttribute("postOrder_nextPage_moreInfo_sv",postOrder_nextPage_moreInfo);
	isPrevPage = "true";
	}
	else{
	if(session.getAttribute("postOrder_nextPage_moreInfo_sv").toString().equalsIgnoreCase(postOrder_nextPage_moreInfo)){
	isPrevPage = "true";
	}
	}


 	%>
	<script language="JavaScript">
	var isNextPage = "<%= postOrder_nextPage_moreInfo%>";
	var isPrevPage = "<%= isPrevPage%>";
	</script>

	<script language="JavaScript">
	if(typeof document.HATSForm.in_290_30 != 'undefined'){
	document.HATSForm.in_290_30.className = 'fieldtxt7';
	}
	if(typeof document.HATSForm.in_370_30 != 'undefined'){
	document.HATSForm.in_370_30.className = 'fieldtxt7';
	}
	if(typeof document.HATSForm.in_450_30 != 'undefined'){
	document.HATSForm.in_450_30.className = 'fieldtxt7';
	}
	if(typeof document.HATSForm.in_555_5 != 'undefined'){
	document.HATSForm.in_555_5.className = 'fieldtxt3';
	}
	</script>
	<HATS:HostKeypad />
</HATS:Form>

<HATS:OIA/>

</body>
</html>
