<% out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<% out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<input type="hidden" name="in_402_1" id="in_402_1"/>
<input type="hidden" name="in_482_1" id="in_482_1"/>
<input type="hidden" name="in_562_1" id="in_562_1"/>
<input type="hidden" name="in_642_1" id="in_642_1"/>
<input type="hidden" name="in_722_1" id="in_722_1"/>
<input type="hidden" name="in_802_1" id="in_802_1"/>
<input type="hidden" name="in_882_1" id="in_882_1"/>
<input type="hidden" name="in_962_1" id="in_962_1"/>
<input type="hidden" name="in_1042_1" id="in_1042_1"/>
<input type="hidden" name="in_1122_1" id="in_1122_1"/>
<input type="hidden" name="in_1202_1" id="in_1202_1"/>
<input type="hidden" name="in_1282_1" id="in_1282_1"/>
<input type="hidden" name="in_1362_1" id="in_1362_1"/>
<input type="hidden" name="in_1442_1" id="in_1442_1"/>
<input type="hidden" name="in_1522_1" id="in_1522_1"/>
<input type="hidden" name="in_1602_1" id="in_1602_1"/>
<!-- Insert your HATS component tags here. -->

<div class="heading_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" class="topbar_headingtxtred1">Customer Search 2</td>

    <td width="34%" align="center" class="topbar_headingtxt2">&nbsp;</td>
    <td width="33%" align="right" class="topbar_headingtxt3"></td>
  </tr>
</table>
</div>

<div class="clear5px"></div>

<div class="heading_bg" style="display:none;">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>

      <td height="26" align="center" valign="middle" class="tiltletxt"></td>
    </tr>
  </table>
</div>

<div class="clear10px" style="display:none;"></div>


<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>

    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
    <table width="auto" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
	<tr>
	<td bgcolor="#FFFFFF" class="txt1"><strong>Customer:</strong></td>
	<td>
	<HATS:Component col="12" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="41"
						componentSettings="" row="2" />
	</td>
	<td bgcolor="#FFFFFF" class="txt1"><strong>Type Number or Complete/Partial Name.</strong></td>
	<td bgcolor="#FFFFFF" style="display: none;"><img src="../common/images/Search-icon.png" width="24" height="24" /></td>
	<td align="center" bgcolor="#FFFFFF">
<table width="auto" border="0" cellspacing="5" cellpadding="0">
	<tr>
	<td style="border: none;">
	<div class="allbtn"><a title="Enter" href="javascript:ms('[enter]','hatsportletid');">Enter</a></div>
	</td>
	</tr>
</table>
	</td>
 	</tr>
 </table>

    </td>
    <td background="../common/images/right_middle4.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
  </tr>
</table>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top">
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    
    <td>
<div style="vertical-align: top; overflow-y:auto; overflow-x:hidden; height:300px; border:solid 1px #CCC;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td width="100%">

<HATS:Component type="com.ibm.hats.transform.components.VisualTableComponent" widget="gulistan_JPS02.widgets.CustomerSearchMore" row="4" col="1" erow="21" ecol="72" componentIdentifier="CustomerSearch2" applyTextReplacement="true"/>

</td>
		</tr>
	</table>
	</div>
    </td>
    </tr>
    </table>
      </td>
      
    <td background="../common/images/right_middle4.gif"></td>
  </tr>
  
  <tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
  </tr>
</table>
<div class="clear2px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle2.gif"></td>
    <td bgcolor="#ffffff" align="center" valign="middle" class="topbar_headingtxtrad">&nbsp;
      <HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="24" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="24" />&nbsp;
      </td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
  
</table>

</div>

<div class="clear"></div>

	<div class="btnmain">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="center">
	<table width="auto" border="0" cellspacing="5" cellpadding="0">
		<tr>
		<td>
		<div class="allbtn"><a title="Back" href="javascript:ms('[pf3]','hatsportletid');">Back</a></div>
		</td>
		<td>
		<div class="allbtn"><a title="More" href="javascript:ms('[pf5]','hatsportletid');">More</a></div>
		</td>
		</tr>
	</table>
		</td>

		</tr>
	</table>
	</div>

<HATS:HostKeypad />
</HATS:Form>


<!-- End of the HATS form. -->
<HATS:OIA/>
<% out.println("<!--blank.jsp"); %>
</body>
</html>
<% out.println("-->"); %>