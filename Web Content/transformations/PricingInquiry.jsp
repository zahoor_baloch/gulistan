<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java"%>
<%@ taglib uri="hats.tld" prefix="HATS"%>
<%@page import="com.ibm.hats.common.*"%>
<%@page import="com.ibm.hats.common.TransformInfo"%>
<%@page import="com.ibm.hats.common.CommonConstants"%>

<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink />
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<script language="JavaScript">

function incNext(){
var checkValue=<%=request.getParameter("incvalue")%>;

if(checkValue!=null){
document.getElementsByName("incvalue")[0].value=<%=request.getParameter("incvalue")%>;
document.getElementsByName("incvalue")[0].value++;
}
 }

function decPrev(){
var checkValue=<%=request.getParameter("incvalue")%>;
if(checkValue!=null){
document.getElementsByName("incvalue")[0].value=<%=request.getParameter("incvalue")%>;
document.getElementsByName("incvalue")[0].value--;
} 
 }
	
function zeroInc(){
document.getElementsByName("incvalue")[0].value=<%=request.getParameter("incvalue")%>;
document.getElementsByName("incvalue")[0].value=null;
}
</script>
 

<%
	String serverName = request.getServerName();
	int port = request.getLocalPort();
%>
<script language="JavaScript">

function excelGen(){

		var detailsTable=document.getElementById("detailsTable");
 		var port = "<%=port%>";
 		var content1 = "<TABLE BORDER=1>" ; 
	 content1 +="<TR>";
	 content1 += "<TD style='font-weight: bold;'  bgcolor='yellow'> Style Code</TD>";
	 content1 += "<TD style='font-weight: bold;'  bgcolor='yellow'> Style Description</TD>";
	 content1 += "<TD style='font-weight: bold;'  bgcolor='yellow'> Warehouse</TD>";
	 content1 += "<TD style='font-weight: bold;'  bgcolor='yellow'> Effect Date</TD>"; 
	 content1 += "<TD style='font-weight: bold;'  bgcolor='yellow'> Ending Date</TD>";
	 content1 += "<TD style='font-weight: bold;'  bgcolor='yellow'> Min Qty </TD>";
	 content1 += "<TD style='font-weight: bold;'  bgcolor='yellow'> Roll Price </TD>";
	 content1 += "<TD style='font-weight: bold;'  bgcolor='yellow'> Cut Price</TD>";
	 content1 += "<TD style='font-weight: bold;' bgcolor='yellow'> UM</TD>";
	 content1 += "</TR>";
	
	
	for ( var y=0;y<detailsTable.rows.length;y++) 
	{ 
	content1 +="<TR>";
	for ( var x=0; x<detailsTable.rows[y].cells.length; x++) 
	{ 
	if (y==0)
	{ content1 += "<TD>" + detailsTable.rows[y].cells[x].innerHTML + "</TD>"; 
	} 
	else 
	{ content1 += "<TD>" + detailsTable.rows[y].cells[x].innerHTML + "</TD>" ; 
	} 
	} 
	content1 += "</TR>"; 
	} 
	content1 += "</TABLE>" ;
	
	var serverName = "<%=serverName%>";  
	var pwin = window.open("","excel_genrato"); 
	pwin.document.write("<HTML><BODY onLoad='document.excelForm.submit();'><form  name='excelForm' method='post' ><input type='hidden' name='excelDataTA' id='excelDataTA'/> </form>");
	pwin.document.getElementById("excelDataTA").value = content1;
	pwin.document.pdfForm.action="http://"+serverName+":"+port+"/Gulistan_JPS02/ExcelExp";
	pwin.document.write( "</BODY></HTML>" ); 
    pwin.document.close();
    pwin.close();
	
	
 }
 
function excelPopUp()
{
var serverName = "<%=serverName%>";
var portAdd = "<%=port%>";
var urlPath="http://"+serverName+":"+portAdd+"/Gulistan_JPS02/Spreadsheet.jsp";
var popupwindow=window.open(urlPath, "popup", "width=1024;location=yes;menubar=yes;resizable=yes;scrollbars=yes;status=no;titlebar=no;toolbar=no;height:1068")

}

function pdfPopUp()
{
 sOptions = 'status=yes,menubar=yes,scrollbars=yes,resizable=yes,toolbar=yes';
   sOptions = sOptions + ',width=' + (screen.availWidth - 10).toString();
   sOptions = sOptions + ',height=' + (screen.availHeight - 122).toString();
   sOptions = sOptions + ',screenX=0,screenY=0,left=0,top=0';
var serverName = "<%=serverName%>";
var portAdd = "<%=port%>";
var urlPath="http://"+serverName+":"+portAdd+"/Gulistan_JPS02/pdfPopUp.jsp";
var popupwindow=window.open(urlPath, "popup",sOptions )

}
 function pdfGen(){

	var detailsTable=document.getElementById("detailsTable");
	var port = "<%=port%>";
 	var content1 = "";
 	var innerHTML=detailsTable.innerHTML;
	var browser=navigator.appName;
	for (var y =0;y<detailsTable.rows.length;y++) //y < 80 rows
	{ 
	for ( var x=0; x<detailsTable.rows[y].cells.length; x++) // every rows.cell length <tr>
	{ 
		if(browser=="Netscape"){
		innerHTML.replace(/<div(.)*?>|<\/div>/g,'');//works in firefox
		
		
		
		detailsTable.rows[y].cells[x].innerHTML.replace(/<div(.)*?>|<\/div>/g,'');
	if (y==0)
	{
	if(detailsTable.rows[y].cells[x].innerHTML==" " || detailsTable.rows[y].cells[x].innerHTML == "")
	{
		
		content1 +=  "*_" ; 
		
	}
	else
	{ 
	
	content1 +=  detailsTable.rows[y].cells[x].innerHTML.replace(/<div(.)*?>|<\/div>/g,'')+"_" ; 
		
	}
		
	} 
	else 
	{if(detailsTable.rows[y].cells[x].innerHTML==" " || detailsTable.rows[y].cells[x].innerHTML == "")
	{
		content1 +=  "*_" ; 
		
	}else
	{ 
	content1 +=  detailsTable.rows[y].cells[x].innerHTML.replace(/<div(.)*?>|<\/div>/g,'')+"_" ; 
		
	}
	}
	
	

	} 
	
	else if(browser=="Microsoft Internet Explorer"){
		//innerHTML.replace(/<div(.)*?>|<\/div>/g,'');//works in Explorer
		
		
		
		detailsTable.rows[y].cells[x].innerText;
	if (y==0)
	{
	if(detailsTable.rows[y].cells[x].innerText==" " || detailsTable.rows[y].cells[x].innerText == "")
	{
		
		content1 +=  "*_" ; 
		
	}
	else
	{ 
	
	content1 +=  detailsTable.rows[y].cells[x].innerText+"_" ; 
		
	}
		
	} 
	else 
	{if(detailsTable.rows[y].cells[x].innerText==" " || detailsTable.rows[y].cells[x].innerText == "")
	{
		content1 +=  "*_" ; 
		
	}else
	{ 
	content1 +=  detailsTable.rows[y].cells[x].innerText+"_" ; 
		
	}
	}
	
	

	}
	}
	
	}
	

	var serverName="<%=serverName%>";

  	var pwin =window.open("","pdf_generator"); 
	
	//pwin.document.open(); 
	
	pwin.document.write("<HTML><BODY onLoad='document.pdfForm.submit();'><form  name='pdfForm' method='post' ><input type='hidden' name='pdfData' id='pdfDataTA'/></form>");
	
	pwin.document.getElementById("pdfDataTA").value = content1;
	pwin.document.pdfForm.action="http://"+serverName+":"+port+"/Gulistan_JPS02/PdfExp";
	
	
	pwin.document.write( "</BODY></HTML>" ); 
    pwin.document.close();
    pwin.close();
 	//setTimeout( function () {pwin.close();},5000);
   


 
 }


 
 
 </script>



<HATS:Form>

	<input type="hidden" name="incvalue" id="incvalue" value=0 />

	<%
		try {

				String user = "";
				String pwd = "";
				if (request.getParameterNames().toString().length() != 0) {

					user = request.getParameter("in_100_10");
					pwd = request.getParameter("in_180_10");
				}

				if (session.getAttribute("gul_id") == null) {
					session.setAttribute("gul_id", user);
					session.setAttribute("gul_password", pwd);
				}

			}

			catch (Exception ex) {

			}
	%>
	<%
		session.setAttribute("inventoryInquiry_Details_sv1", null);
	%>
	<%
		session.setAttribute("orderInquiry_Details_nextPage_sv", null);
	%>


	<!-- Insert your HATS component tags here. -->

	<input type="hidden" name="in_162_1">
	<input type="hidden" name="in_242_1">
	<input type="hidden" name="in_322_1">
	<input type="hidden" name="in_402_1">
	<input type="hidden" name="in_482_1">
	<input type="hidden" name="in_562_1">
	<input type="hidden" name="in_642_1">
	<input type="hidden" name="in_722_1">
	<input type="hidden" name="in_802_1">
	<input type="hidden" name="in_882_1">
	<input type="hidden" name="in_962_1">
	<input type="hidden" name="in_1042_1">
	<input type="hidden" name="in_1122_1">
	<input type="hidden" name="in_1202_1">
	<input type="hidden" name="in_1282_1">
	<input type="hidden" name="in_1362_1">
	<input type="hidden" name="in_1442_1">
	<input type="hidden" name="in_1522_1">
	<input type="hidden" name="in_1602_1">
	<input type="hidden" name="in_1682_1">
	<input type="hidden" name="in_1762_1">
	<input type="hidden" name="in_1842_1">
	
	<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV" />
	<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV" />
	<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV" />
	<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked" />
	<input type="hidden" name="hatsgv_logoffClicked"
		id="hatsgv_logoffClicked" />




	<!--ddd-->
	<div class="container">
	<div class="heading_bg">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="33%" class="topbar_headingtxt1">&nbsp;</td>
			<td width="34%" align="center" class="topbar_headingtxtredd"><HATS:Component
				col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="66"
				componentSettings="" row="2" /></td>
			<td width="33%" align="right" class="topbar_headingtxt3">&nbsp;</td>
		</tr>
	</table>
	</div>

	<!--Menu Start-->
	<div class="menubgg"><!--line-->
	<table width="100%">
		<tr>
			<td height="1">
			<div class="line"></div>
			</td>
		</tr>
	</table>
	<!--line-->

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="5" height="5" background="../common/images/left_top4.gif"></td>
			<td background="../common/images/center_middle_top4.gif"></td>
			<td width="5" height="5" background="../common/images/right_top4.gif"></td>
		</tr>
		<tr>
			<td background="../common/images/left_middle4.gif"></td>
			<td align="center" valign="top">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>

					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr class="oddRowdd">
							<td width="70" align="center">Style Code</td>
							<td width="250" align="center" class="leftline2">Style
							Description</td>
							<td width="90" align="center" class="leftline2">Warehouse</td>
							<td width="70" align="center" class="leftline2">Effect Date</td>
							<td width="90" align="center" class="leftline2">Ending Date</td>
							<td width="90" align="center" class="leftline2">Min Qty</td>
							<td width="90" align="center" class="leftline2">Roll Price</td>
							<td width="70" align="center" class="leftline2">Cut Price</td>
							<td align="center" class="leftline2">UM</td>
						</tr>
					</table>
					<div
						style="vertical-align: top; overflow-y: auto; overflow-x: hidden; height: 360px; border: solid 1px #CCC;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="100%"><HATS:Component type="com.ibm.hats.transform.components.TableComponent" widget="gulistan_JPS02.widgets.PricingInquiry" row="5" col="2" erow="22" ecol="79" componentSettings="startCol:2|endRow:22|columnBreaks:8,29,38,47,56,62,69,76,79|endCol:79|excludeCols:|visualTableDefaultValues:false|numberOfTitleRows:0|startRow:5|includeEmptyRows:true|excludeRows:|" componentIdentifier="PricingInquiry" applyTextReplacement="true"/>
							</td>
						</tr>
					</table>
					</div>

					</td>
				</tr>
			</table>
			</td>

			<td background="../common/images/right_middle4.gif"></td>
		</tr>

		<tr>
			<td height="5" background="../common/images/left_bottom4.gif"></td>
			<td background="../common/images/center_middle_bottom4.gif"></td>
			<td height="5" background="../common/images/right_bottom4.gif"></td>
		</tr>
	</table>

	</div>
	<!--Mene End-->


	<div class="clear"></div>

	<div class="btnmain">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left">
			<div style="padding: 0 0 0 385px;">
			<table width="auto" border="0" cellspacing="5" cellpadding="0">
				<TR>
					<td>
					<div class="allbtn"><a title="Exit" id="Exit"
						href="javascript:zeroInc();ms('[pf3]','hatsportletid');">Exit</a></div>
					</td>


					<%
						if (request.getParameter("incvalue") == null
									|| Integer.parseInt(request.getParameter("incvalue")) == -1) {
					%>
					<%
						} else if (request.getParameter("incvalue") != null
									|| Integer.parseInt(request.getParameter("incvalue")) != -1) {
					%>
					<td>
					<div class="allbtn"><a title="Previous" id="previous"
						href="javascript:decPrev();ms('[pageup]','hatsportletid');">Previous</a></div>
					</td>
					<%
						}
					%>
					<%
						if (((TransformInfo) request
									.getAttribute(CommonConstants.REQ_TRANSFORMINFO))
									.getGlobalVariable("moreRecords", true).getString(0)
									.equals("+")) {
					%>
					<td>
					<div class="allbtn"><a title="Next" id="next"
						href="javascript:incNext();ms('[pagedn]','hatsportletid');">Next</a></div>
					</td>
					<%
						}
					%>

				</TR>
			</table>
			</div>
			</td>
			<td width="199" align="center">
			<div class="exportdata1">
			Export Data :
			<a style="cursor: pointer" onclick="ms('[pf2]','hatsportletid');" title="Email">
				<img src="../common/images/maifl.png" title="Email" width="28" height="28" border="0" align="absmiddle" />
			</a>
			<a style="cursor: pointer" onclick="pdfPopUp();">
				<img src="../common/images/pdf_icon_20x20.png" title="PDF" width="28" height="24" border="0" align="absmiddle" />
			</a>
			<a style="cursor: pointer" onclick="excelPopUp();">
				<img src="../common/images/excel.png" title="Excel" width="24" height="24" border="0" align="absmiddle" />
			</a>
			</div>
			</td>
		</tr>
	</table>
	</div>

	</div>


	<input type="hidden" name="testHiddenValue" id="testHiddenValue" />




</HATS:Form>

<HATS:OIA />

</body>
</html>
