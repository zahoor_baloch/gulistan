<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>


<HATS:Form>



<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV" />
	<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV" />
	<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV" />
	<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked" />
	<input type="hidden" name="hatsgv_logoffClicked"
		id="hatsgv_logoffClicked" />
	
<input type="hidden" name="hatsgv_menuClicked" id="hatsgv_menuClicked"/>

	<input type="hidden" name="in_242_1" />
	<input type="hidden" name="in_322_1" />
	<input type="hidden" name="in_402_1" />
	<input type="hidden" name="in_482_1" />
	<input type="hidden" name="in_562_1" />
	<input type="hidden" name="in_642_1" />	
	
	
	<!--<input type="hidden" name="in_482_1">
	<input type="hidden" name="in_562_1">
	<input type="hidden" name="in_642_1">
	<input type="hidden" name="in_722_1">
	<input type="hidden" name="in_802_1">
	<input type="hidden" name="in_882_1">
	<input type="hidden" name="in_962_1">
	<input type="hidden" name="in_1042_1">
	<input type="hidden" name="in_1122_1">
	<input type="hidden" name="in_1202_1">
	<input type="hidden" name="in_1282_1">
	<input type="hidden" name="in_1362_1">
	<input type="hidden" name="in_1442_1">
	<input type="hidden" name="in_1522_1">
	<input type="hidden" name="in_1602_1">
	<input type="hidden" name="in_1682_1">
		
	
	--><div class="container">

<div class="heading_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" class="topbar_headingtxt1">&nbsp;</td>
    <td width="34%" align="center" class="topbar_headingtxtredd">Sample Type Selection</td>
    <td width="33%" align="right" class="topbar_headingtxt3">&nbsp;</td>
  </tr>
</table>
</div>


<div class="clear4px"></div>
<div class="menubgg">

<div class="popupmain57">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center" valign="middle">
<table width="100%" height="150" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td background="../common/images/popup_top_left.png" width="16">&nbsp;</td>
	<td height="16" background="../common/images/popup_top_middle.png">&nbsp;</td>
	<td background="../common/images/popup_top_right.png" width="16">&nbsp;</td>
	</tr>
	<tr>
	<td background="../common/images/popup_left_middle.png">&nbsp;</td>
	<td bgcolor="#FFFFFF">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="5" height="5" background="../common/images/left_top2.gif"></td>
	<td background="../common/images/center_middle_top2.gif"></td>
	<td width="5" height="5" background="../common/images/right_top2.gif"></td>
	</tr>
    <tr>
	<td background="../common/images/left_middle2.gif"></td>
	<td align="center" valign="top" bgcolor="#d1c4a2">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<thead>
	<tr class="barhead">
	<td align="left">Type</td>
	<td align="left" class="leftline2">Type Description</td>
	</tr>
	</thead>
    <tr>
	<td height="1" colspan="3" align="left"></td>
	</tr>
	<tr class="evenRowbar" onclick="setCursorPosition(242, 'HATSForm');checkInput2('in_242_1', '/','hidden');ms('[enter]', 'HATSForm')">
	
	<td align="left" valign="top"> A </td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="8"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="4" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="27" componentSettings="" row="4" BIDIOpposite="false" /></td>
	</tr>
	<tr class="oddRowbar" onclick="setCursorPosition(322, 'HATSForm');checkInput2('in_322_1', '/','hidden');ms('[enter]', 'HATSForm')">
	<td align="left" valign="top"> C</td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="8"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="5" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="27" componentSettings="" row="5" BIDIOpposite="false" /></td>
	</tr>
    <tr class="evenRowbar" onclick="setCursorPosition(402, 'HATSForm');checkInput2('in_402_1', '/','hidden');ms('[enter]', 'HATSForm')">
	<td align="left" valign="top"> D </td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="8"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="6" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="27" componentSettings="" row="6" BIDIOpposite="false" /></td>
	</tr>
	<tr class="oddRowbar" onclick="setCursorPosition(482, 'HATSForm');checkInput2('in_482_1', '/','hidden');ms('[enter]', 'HATSForm')">
	<td align="left" valign="top">E</td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="8"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="7" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="27" componentSettings="" row="7" BIDIOpposite="false" /></td>
	</tr>
    <tr class="evenRowbar" onclick="setCursorPosition(562, 'HATSForm');checkInput2('in_562_1', '/','hidden');ms('[enter]', 'HATSForm')">
	<td align="left" valign="top">P</td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="8"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="8" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="27" componentSettings="" row="8" BIDIOpposite="false" /></td>
	</tr>
	<tr class="oddRowbar" onclick="setCursorPosition(642, 'HATSForm');checkInput2('in_642_1', '/','hidden');ms('[enter]', 'HATSForm')">
	<td align="left" valign="top">S</td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="8"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="9" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="27" componentSettings="" row="9" BIDIOpposite="false" /></td>
	</tr>
</table>            
	</td>
	<td background="../common/images/right_middle2.gif"></td>
	</tr>
	<tr>
	<td height="5" background="../common/images/left_bottom2.gif"></td>
	<td background="../common/images/center_middle_bottom2.gif"></td>
	<td height="5" background="../common/images/right_bottom2.gif"></td>
	</tr>
</table>
	</td>
	<td background="../common/images/popup_right_middle.png">&nbsp;</td>
	</tr>
	<tr>
	<td background="../common/images/popup_below_left.png" width="16">&nbsp;</td>
  <td height="16" background="../common/images/popup_below_middle.png">&nbsp;</td>
  <td background="../common/images/popup_below_right.png" width="16">&nbsp;</td>
  </tr>
</table>
	</td>
	</tr>
</table>
</div>

	<div class="clear5px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="5" height="5" background="../common/images/left_top2.gif"></td>
	<td background="../common/images/center_middle_top2.gif"></td>
	<td width="5" height="5" background="../common/images/right_top2.gif"></td>
	</tr>
	<tr>
	<td background="../common/images/left_middle2.gif"></td>
	<td align="center" valign="middle" class="topbar_headingtxtrad">&nbsp;
	<HATS:Component col="2" alternate=""
		widget="com.ibm.hats.transform.widgets.LabelWidget"
		alternateRenderingSet="" erow="24" textReplacement=""
		widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
		type="com.ibm.hats.transform.components.TextComponent" ecol="79"
		componentSettings="" row="24" />&nbsp;</td>
	<td background="../common/images/right_middle2.gif"></td>
	</tr>
	<tr>
	<td height="5" background="../common/images/left_bottom2.gif"></td>
	<td background="../common/images/center_middle_bottom2.gif"></td>
	<td height="5" background="../common/images/right_bottom2.gif"></td>
	</tr>

</table>
</div>

	<div class="clear"></div>

	<div class="btnmain">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center">
<table width="auto" border="0" cellspacing="5" cellpadding="0">
	<tr>	
	<td>
	<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');" title="Cancel">Cancel</a></div>
	</td>
	
	<td>
	<div class="allbtn"><a href="javascript:ms('[pf10]','hatsportletid');" title="Review Header">Review Header</a></div>
	</td>
	<%
		String postOrderKey = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("postOrderKey", true).getString(0).trim();
		if(postOrderKey.length()>0 && postOrderKey.equalsIgnoreCase("Post Order")){
	%>
	<td>
	<div class="allbtn"><a href="javascript:ms('[pf9]','hatsportletid');" title="Post Order">Post Order</a></div>
	</td>
	<td>
	<div class="allbtn"><a href="javascript:ms('[pf11]','hatsportletid');" title="Review Detail">Review Detail</a></div>
	</td>
	<% 	
		} 
	%>
	</tr>
</table>
	</td>
	</tr>
</table>
	</div>

</div>
</HATS:Form>
</body>
</html>