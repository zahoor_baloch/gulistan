<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java"%>
<%@ taglib uri="hats.tld" prefix="HATS"%>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink />

<script type="text/javascript">

function MM_openBrWindow(theURL,winName,features) { 
  window.open(theURL,winName,features);
}

</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<link rel="stylesheet" href="../common/css/menu2.css" type="text/css">
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>


<HATS:Form>
	<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV" />
	<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV" />
	<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV" />
	<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked" />
	<input type="hidden" name="hatsgv_logoffClicked"
		id="hatsgv_logoffClicked" />


	<%
		session.setAttribute("postOrder_nextPage_sv", null);
	%>
	<%
		session.setAttribute("postOrder_nextPage_moreInfo_sv", null);
	%>
	<input type="hidden" name="in_722_1">
	<input type="hidden" name="in_802_1">
	<input type="hidden" name="in_882_1">
	<input type="hidden" name="in_962_1">
	<input type="hidden" name="in_1042_1">
	<input type="hidden" name="in_1122_1">
	<input type="hidden" name="in_1202_1">
	<input type="hidden" name="in_1282_1">
	<input type="hidden" name="in_1362_1">
	<input type="hidden" name="in_1442_1">

	<div class="container">

	<div class="heading_bg">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" class="topbar_headingtxt1" style="display: none;">

			</td>

		</tr>

	</table>
	</div>

	<div class="clear4px"></div>

	<div class="popupmain2">
	<table width="100%" height="100%" border="0" cellspacing="0"
		cellpadding="0">
		<tr>
			<td align="center" valign="middle">
			<table width="100%" height="200" border="0" cellspacing="0"
				cellpadding="0">
				<tr>
					<td background="../common/images/popup_top_left.gif" width="16">&nbsp;</td>

					<td height="16" background="../common/images/popup_top_middle.gif">&nbsp;</td>
					<td background="../common/images/popup_top_right.gif" width="16">&nbsp;</td>
				</tr>
				<tr>
					<td background="../common/images/popup_left_middle.gif">&nbsp;</td>
					<td>
					<table width="100%" border="0" cellpadding="3" cellspacing="0"
						class="fillbor1">
						<tr>
							<td colspan="6" bgcolor="#d1c4a2" class="txt1"
								style="font-size: 14pt;"><strong>Please fill in
							the details</strong></td>

						</tr>
						<tr>
							<td colspan="6" bgcolor="#d1c4a2" class="txt1"><strong>Reserve
							Roll/Cut</strong></td>

						</tr>
						<tr>
							<td bgcolor="#d1c4a2" class="txt1"><strong>Roll:</strong></td>
							<td class="txt1"><HATS:Component col="13" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings=""
								type="com.ibm.hats.transform.components.TextComponent" ecol="20"
								componentSettings="" row="5" /></td>
							<td bgcolor="#d1c4a2" class="txt1"><strong>Length:</strong></td>
							<td class="txt1"><HATS:Component col="31" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings=""
								type="com.ibm.hats.transform.components.TextComponent" ecol="36"
								componentSettings="" row="5" /></td>
							<td bgcolor="#d1c4a2" class="txt1"><strong>Sq yds:</strong></td>

							<td class="txt1"><HATS:Component col="46" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings=""
								type="com.ibm.hats.transform.components.TextComponent" ecol="52"
								componentSettings="" row="5" /></td>
						</tr>
						<tr>
							<td bgcolor="#d1c4a2" class="txt1"><strong>Cut Lth:</strong></td>
							<td colspan="3" class="txt1">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td style="border: none;"><strong>Ft</strong><span
										class="red">*</span></td>
									<td style="border: none;"><HATS:Component col="19"
										alternate=""
										widget="com.ibm.hats.transform.widgets.FieldWidget"
										alternateRenderingSet="" erow="7" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
										type="com.ibm.hats.transform.components.FieldComponent"
										ecol="21" componentSettings="" row="7" /></td>
									<td style="border: none;"><strong>In</strong><span
										class="red">*</span></td>
									<td style="border: none;"><HATS:Component col="27"
										alternate=""
										widget="com.ibm.hats.transform.widgets.FieldWidget"
										alternateRenderingSet="" erow="7" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
										type="com.ibm.hats.transform.components.FieldComponent"
										ecol="28" componentSettings="" row="7" /></td>
								</tr>

							</table>





							</td>
							<td class="txt1" colspan="2"><strong> </strong><HATS:Component
								col="31" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="trim:true|style:font-weight: bold|labelStyleClass:HATSCAPTION|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="47"
								componentSettings="" row="7" /></td>
						</tr>
						<tr>
							<td bgcolor="#d1c4a2" class="txt1"><strong>Reserve
							Note:</strong></td>
							<td class="txt1" colspan="5"><HATS:Component col="21"
								alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
								alternateRenderingSet="" erow="9" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxtfull2|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
								type="com.ibm.hats.transform.components.FieldComponent"
								ecol="40" componentSettings="" row="9" /></td>
						</tr>
						<tr>
							<td colspan="6" class="topbar_headingtxtrad">&nbsp;<HATS:Component
								col="6" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings=""
								type="com.ibm.hats.transform.components.TextComponent" ecol="61"
								componentSettings="" row="11" />							
								&nbsp;&nbsp;<HATS:Component col="4" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="13" textReplacement=""
								widgetSettings=""
								type="com.ibm.hats.transform.components.TextComponent" ecol="59"
								componentSettings="" row="13" BIDIOpposite="false" />	
							</td>
						</tr>

						<tr>
							<td colspan="6" align="center" valign="middle">

							<table width="auto" border="0" cellspacing="0" cellpadding="0">
								<tr>

									<td style="border: none;" align="center">
									<div class="clear4px"></div>
									<div style="padding-left: 25px;">
									<table width="auto" border="0" cellspacing="5" cellpadding="0">
										<tr>
											<td style="border: none;">
											<div class="allbtn"><a style="cursor: pointer;"
												title="Back" onclick="ms('[pf3]','hatsportletid')">Back</a></div>
											</td>
											<td width="5" style="border: none;"></td>
											<td style="border: none;">
											<div class="allbtn"><a style="cursor: pointer;"
												title="Enter" onclick="ms('[enter]','hatsportletid')">Enter</a></div>
											</td>

										</tr>
									</table>
									</div>
									<div class="clear4px"></div>
									</td>
								</tr>
								<tr>
									<td class="indicator" align="center" style="border: none;">*
									Field requires numeric characters</td>
								</tr>
							</table>

							</td>
						</tr>

					</table>
					</td>
					<td background="../common/images/popup_right_middle.gif">&nbsp;</td>

				</tr>
				<tr>
					<td background="../common/images/popup_below_left.gif" width="16">&nbsp;</td>
					<td height="16"
						background="../common/images/popup_below_middle.gif">&nbsp;</td>
					<td background="../common/images/popup_below_right.gif" width="16">&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>

	</table>
	</div>

	<div class="clear5px"></div>
	</div>









</HATS:Form>

<HATS:OIA />

</body>
</html>
