<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>


<script language="JavaScript">

function incNext(){
var checkValue=<%=request.getParameter("incvalue_4")%>;

if(checkValue!=null){
document.getElementsByName("incvalue_4")[0].value=<%=request.getParameter("incvalue_4")%>;
document.getElementsByName("incvalue_4")[0].value++;
}

 }

function decPrev(){
var checkValue=<%=request.getParameter("incvalue_4")%>;
if(checkValue!=null){
document.getElementsByName("incvalue_4")[0].value=<%=request.getParameter("incvalue_4")%>;
document.getElementsByName("incvalue_4")[0].value--;
} 

 }
	
function zeroInc(){
document.getElementsByName("incvalue_4")[0].value=<%=request.getParameter("incvalue_4")%>;
document.getElementsByName("incvalue_4")[0].value=null;
}
</script>


<HATS:Form>

<input type="hidden" name="incvalue_4" id="incvalue_4" value=0 />

<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV" />
	<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV" />
	<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV" />
	<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked" />
	<input type="hidden" name="hatsgv_logoffClicked"
		id="hatsgv_logoffClicked" />

<input type="hidden" name="hatsgv_menuClicked" id="hatsgv_menuClicked"/>

	<input type="hidden" name="in_332_1">
	<input type="hidden" name="in_562_1">
	<!--<input type="hidden" name="in_642_1">
	<input type="hidden" name="in_722_1">
	<input type="hidden" name="in_802_1">
	<input type="hidden" name="in_882_1">
	<input type="hidden" name="in_962_1">
	<input type="hidden" name="in_1042_1">
	<input type="hidden" name="in_1122_1">
	<input type="hidden" name="in_1202_1">
	<input type="hidden" name="in_1282_1">
	<input type="hidden" name="in_1362_1">
	<input type="hidden" name="in_1442_1">
	<input type="hidden" name="in_1522_1">
	<input type="hidden" name="in_1602_1">
	<input type="hidden" name="in_1682_1">


--><div class="container" style="height: 500px">

<div class="heading_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td width="33%" class="topbar_headingtxt1">&nbsp;</td>
    <td width="34%" align="center" class="topbar_headingtxtredd">Detail Line Review</td>
    <td width="33%" align="right" class="topbar_headingtxt3">&nbsp;</td>
	</tr>
</table>
</div>

<!--line-->
	<table width="100%">
		<tr>
			<td height="1">
			<div class="line"></div>
			</td>
		</tr>
	</table>
	<!--line-->

<div class="menubgg">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="5" height="5" background="../common/images/left_top4.gif"></td>
	<td background="../common/images/center_middle_top4.gif"></td>
	<td width="5" height="5" background="../common/images/right_top4.gif"></td>
	</tr>
    <tr>
	<td background="../common/images/left_middle4.gif"></td>
	<td align="center" valign="top" bgcolor="#d1c4a2">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<thead>
	<tr class="barhead">
	<td align="left" valign="bottom">DEL</td>
	<td align="left" valign="bottom" class="leftline2">C1<br>
	  Cd</td>
	<td align="left" valign="bottom" class="leftline2">Style</td>
	<td align="left" valign="bottom" class="leftline2">Color</td>
	<td align="left" valign="bottom" class="leftline2">Size</td>
	<td align="left" valign="bottom" class="leftline2">Description</td>
	<td align="left" valign="bottom" class="leftline2">Color<br>
	  Desc</td>
	<td align="left" valign="bottom" class="leftline2">Order<br>
	  Price</td>
	<td align="left" valign="bottom" class="leftline2">Order<br>
	  Quantity</td>
	</tr>
	</thead>
    <tr>
	<td height="1" colspan="8" align="left"></td>
	</tr>
	<tr class="oddRowbar2" id="detailLine_row5">
	<td align="left" valign="top"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.CheckboxWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="style:|tableRowStyleClass:|tableStyleClass:HATSTABLE|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="5" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="5" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="7"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="5" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="13"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="5" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="19"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="5" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="28"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="5" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="49"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="56"
						componentSettings="" row="5" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="59"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="64"
						componentSettings="" row="5" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="68"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="5" /></td>
	</tr>
    <tr class="evenRowbar3" id="detailLine_row5_2">    
		<td align="left"><HATS:Component col="2" alternate=""
							widget="com.ibm.hats.transform.widgets.LabelWidget"
							alternateRenderingSet="" erow="6" textReplacement=""
							widgetSettings="trim:true|style:|labelStyleClass:|"
							type="com.ibm.hats.transform.components.TextComponent" ecol="14"
							componentSettings="" row="6" /></td>
		<td colspan="8" align="left"><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="69"
						componentSettings="" row="6" /></td>
									
	</tr>	
    <tr class="oddRowbar2" id="detailLine_row5_3">
    <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="7" /></td>
	<td colspan="4" align="left" valign="top" class="leftline"><HATS:Component col="6"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:HATSINPUT|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="25"
						componentSettings="" row="7" /></td>
	<td class="leftline" align="right"><HATS:Component col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="29"
						componentSettings="" row="7" /></td>
	<td colspan="3" align="left" valign="top" class="leftline"> <HATS:Component
						col="31" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:HATSINPUT|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="80"
						componentSettings="" row="7" /></td>
	</tr>
    <tr class="evenRowbar2" id="detailLine_row9" >
    <td align="left" valign="top"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.CheckboxWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="style:|tableRowStyleClass:|tableStyleClass:HATSTABLE|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="9" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="9" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="7"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="9" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="13"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="9" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="19"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="9" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="28"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="9" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="49"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="56"
						componentSettings="" row="9" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="59"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="64"
						componentSettings="" row="9" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="68"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="9" /></td>
	</tr>
    <tr class="evenRowbar3" id="detailLine_row9_2">
	<td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="15"
						componentSettings="" row="10" /></td>
	<td colspan="8" align="left"><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="69"
						componentSettings="" row="10" /></td>
	</tr>
    <tr class="oddRowbar2" id="detailLine_row5_3">
    <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="11" /></td>
	<td colspan="4" align="left" valign="top" class="leftline"><HATS:Component
						col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:HATSINPUT|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="25"
						componentSettings="" row="11" /></td>
	<td class="leftline" align="right"><HATS:Component col="27"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="29"
						componentSettings="" row="11" /></td>
	<td colspan="3" align="left" valign="top" class="leftline"> <HATS:Component
						col="31" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:HATSINPUT|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="80"
						componentSettings="" row="11" /></td>
	</tr>
	<tr class="evenRowbar2" id="detailLine_row13" >
	<td align="left" valign="top"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.CheckboxWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="style:|tableRowStyleClass:|tableStyleClass:HATSTABLE|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="13" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="13" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="7"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="13" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="13"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="13" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="19"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="13" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="28"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="13" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="49"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="56"
						componentSettings="" row="13" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="59"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="64"
						componentSettings="" row="13" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="68"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="13" /></td>
	</tr>
    <tr class="evenRowbar3" id="detailLine_row13_2">
	<td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="15"
						componentSettings="" row="14" /></td>
	<td colspan="8" align="left"><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="69"
						componentSettings="" row="14" /></td>
	</tr>
    <tr class="oddRowbar2" id="detailLine_row5_3">
    <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="15" /></td>
	<td colspan="4" align="left" valign="top" class="leftline"><HATS:Component
						col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:HATSINPUT|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="25"
						componentSettings="" row="15" /></td>
	<td class="leftline" align="right"><HATS:Component col="27"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="29"
						componentSettings="" row="15" /></td>
	<td colspan="3" align="left" valign="top" class="leftline"> <HATS:Component
						col="31" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:HATSINPUT|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="80"
						componentSettings="" row="15" /></td>
	</tr>
	<tr class="evenRowbar2" id="detailLine_row17" >
	<td align="left" valign="top"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.CheckboxWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="style:|tableRowStyleClass:|tableStyleClass:HATSTABLE|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="17" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="17" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="7"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="17" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="13"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="17" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="19"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="17" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="28"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="17" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="49"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="56"
						componentSettings="" row="17" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="59"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="64"
						componentSettings="" row="17" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="68"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="17" /></td>
	</tr>
    <tr class="evenRowbar3" id="detailLine_row17_2">
	<td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="15"
						componentSettings="" row="18" /></td>
	<td colspan="8" align="left"><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="69"
						componentSettings="" row="18" /></td>
	</tr>
    <tr class="oddRowbar2" id="detailLine_row5_3">
    <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="19" /></td>
	<td colspan="4" align="left" valign="top" class="leftline"><HATS:Component
						col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:HATSINPUT|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="25"
						componentSettings="" row="19" /></td>
	<td class="leftline" align="right"><HATS:Component col="27"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="29"
						componentSettings="" row="19" /></td>
	<td colspan="3" align="left" valign="top" class="leftline"> <HATS:Component
						col="31" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:HATSINPUT|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="80"
						componentSettings="" row="19" /></td>
	</tr>
</table>            
	</td>
	<td background="../common/images/right_middle4.gif"></td>
	</tr>
	<tr>
	<td height="5" background="../common/images/left_bottom4.gif"></td>
	<td background="../common/images/center_middle_bottom4.gif"></td>
	<td height="5" background="../common/images/right_bottom4.gif"></td>
	</tr>
</table>

	<div class="clear5px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="5" height="5" background="../common/images/left_top2.gif"></td>
	<td background="../common/images/center_middle_top2.gif"></td>
	<td width="5" height="5" background="../common/images/right_top2.gif"></td>
	</tr>
	<tr>
	<td background="../common/images/left_middle2.gif"></td>
	<td align="center" valign="middle" class="topbar_headingtxtrad">&nbsp;
	<HATS:Component col="2" alternate=""
		widget="com.ibm.hats.transform.widgets.LabelWidget"
		alternateRenderingSet="" erow="24" textReplacement=""
		widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
		type="com.ibm.hats.transform.components.TextComponent" ecol="79"
		componentSettings="" row="24" />&nbsp;</td>
	<td background="../common/images/right_middle2.gif"></td>
	</tr>
	<tr>
	<td height="5" background="../common/images/left_bottom2.gif"></td>
	<td background="../common/images/center_middle_bottom2.gif"></td>
	<td height="5" background="../common/images/right_bottom2.gif"></td>
	</tr>

</table>
</div>

	<div class="clear"></div>

	<div class="btnmain">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
	<table width="auto" border="0" cellspacing="5" cellpadding="0">
		<tr>
			<td align="left">
			<div style="padding: 0 0 0 225px;">
			<table border="0" cellspacing="5" cellpadding="0" align="CENTER">
				<TR>

					<td>
						<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');" title="Cancel">Cancel</a></div>
					</td>
					<td>
						<div class="allbtn"><a href="javascript:ms('[pf9]','hatsportletid');" title="Post Order">Post Order</a></div>
					</td>
					<td>
						<div class="allbtn"><a href="javascript:ms('[pf2]','hatsportletid');" title="Type Selection">Type Selection</a></div>
					</td>
					<td>
						<div class="allbtn"><a href="javascript:ms('[pf10]','hatsportletid');" title="Review Header">Review Header</a></div>
					</td>
					<td>
						<div class="allbtn"><a href="javascript:ms('[enter]','hatsportletid');" title="Delete">Delete</a></div>
					</td>
				</TR>
			</table>
			</div>
			</td>
			<td width="162" align="center">
			</td>
		</tr>
	</table>
        </td>
        <td width="78" align="center">
      <div id="nextButton" class="next_btn"><a href="javascript:ms('[pagedn]','HATSForm');"></a></div>
      <div id="previousButton" class="prev_btn"><a href="javascript:ms('[pageup]','HATSForm');"></a></div>
      </td>
    </tr>
  </table>
</div>

</div>








	<input type="hidden" name="testHiddenValue" id="testHiddenValue" />

	<%
	String detailLine_row5 =((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("detailLine_row5", true).getString(0).trim();
	System.out.println("detailLine_row5"+detailLine_row5); 
	String detailLine_row9 =((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("detailLine_row9", true).getString(0).trim();
	System.out.println("detailLine_row9"+detailLine_row9);
	String detailLine_row13 =((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("detailLine_row13", true).getString(0).trim();
	System.out.println("detailLine_row13"+detailLine_row13); 
	String detailLine_row17 =((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("detailLine_row17", true).getString(0).trim();
	System.out.println("detailLine_row17"+detailLine_row17);
	%>

	<script language="JavaScript">
	
	var detailLine_row5 = "<%= detailLine_row5%>";
	var detailLine_row9 = "<%= detailLine_row9%>";
	var detailLine_row13 = "<%= detailLine_row13%>";
	var detailLine_row17 = "<%= detailLine_row17%>";
	
	
	if(detailLine_row5 == ""){
	document.getElementById("detailLine_row5").style.display = 'none';
	document.getElementById("detailLine_row5_2").style.display = 'none';
	}
	if(detailLine_row9 == ""){
	document.getElementById("detailLine_row9").style.display = 'none';
	document.getElementById("detailLine_row9_2").style.display = 'none';
	}
	if(detailLine_row13 == ""){
	document.getElementById("detailLine_row13").style.display = 'none';
	document.getElementById("detailLine_row13_2").style.display = 'none';
	}
	if(detailLine_row17 == ""){
	document.getElementById("detailLine_row17").style.display = 'none';
	document.getElementById("detailLine_row17_2").style.display = 'none';
	}
	</script>

</HATS:Form>
</body>
</html>