<%// out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<%//out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<!-- Insert your HATS component tags here. -->


<div class="container">

<div class="heading_bg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="26" align="center" valign="middle" class="tiltletxt" style="text-transform: none;">Sample Order Entry Form</td>
    </tr>
  </table>
</div>

<div class="clear4px"></div>

<div class="menubgg">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="txt2">
      <tr>
        <td width="200" bgcolor="#fffcf5" class="fillbor58" style="border-top: 1px solid #b99b75;">
          <HATS:Component col="3" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="2" /></td>
        <td align="right" style="padding:3px 10px 3px 3px;">&nbsp;</td>
        <td rowspan="4" align="center" valign="top" style="padding:3px 10px 3px 3px;">
        <table width="auto" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
      
      <tr>
          <td class="txt1order2" width="100" ><strong>Special Notes:</strong></td>
          <td colspan="3" align="left">
          <HATS:Component col="17" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxtfullnewone_3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="76" componentSettings="" row="7" /></td>
          </tr>
        <tr>
          <td class="txt1order2"><strong>PO :</strong></td>
          <td align="left">
          <HATS:Component col="7" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="8" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="16" componentSettings="" row="8" /></td>
          <td class="txt1order2" width="70"><strong>Order By :</strong></td>
          <td align="left"><HATS:Component col="31" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="8" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="35" componentSettings="" row="8" /></td>
								
          </tr>
      <tr>

<td class="txt1order2" ><font align="left" style="white-space: nowrap">Ship to Phone Nbr:</font></td>
      <td align="left">
          <HATS:Component col="59" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="8" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="61" componentSettings="" row="8" /></td>
          
          <td align="left"><HATS:Component col="63" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="8" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="69" componentSettings="" row="8" /></td>
      </tr>
      </table>
        </td>
        <td width="60" bgcolor="#fffcf5" align="right" class="fillbar"><strong>Ship To:</strong></td>
        <td width="200" bgcolor="#fffcf5" class="fillbarnew">
        <HATS:Component col="36" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="65"
						componentSettings="" row="3" /></td>
      </tr>
      <tr>
        <td bgcolor="#fffcf5" class="fillbor58"><HATS:Component
						col="3" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="32"
						componentSettings="" row="3" /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58">
        <HATS:Component col="36" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="65"
						componentSettings="" row="4" /></td>
      </tr>
      <tr>
        <td bgcolor="#fffcf5" class="fillbor58"><HATS:Component
						col="3" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="32"
						componentSettings="" row="4" /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58">
        <HATS:Component col="36" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="65"
						componentSettings="" row="5" /></td>
      </tr>
      <tr>
        <td bgcolor="#fffcf5" class="fillbor58"><HATS:Component
						col="3" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="32"
						componentSettings="" row="5" /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td bgcolor="#fffcf5" class="fillbor58">
          <HATS:Component col="36" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="65"
						componentSettings="" row="6" /></td>
      </tr>
      </table>
    </td>
    <td background="../common/images/right_middle4.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
  </tr>
  </table>

</div>

<!--Menu Start-->
<div class="menubgg">
  
<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->
      
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
  </tr>
  <tr>
    <td height="75" background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top">
      
	<div style="vertical-align: top; overflow-y:auto; overflow-x:hidden; height:auto; border:solid 1px #CCC;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<thead>
	<tr class="oddRowdd">
	<td class="smpleord1" align="center">Style</td>
	<td align="center" class="leftline2 smpleord2">Style Description</td>
	<td align="center" class="leftline2 smpleord3">Unit</td>
	<td align="center" class="leftline2 smpleord4">Size</td>
	<td align="center" class="leftline2 smpleord5">Qty</td>
	<td align="center" class="leftline2 smpleord6">Colors</td>
	<td align="center" class="leftline2">Sidemark</td>                                                  
	</tr>
	</thead>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr id="row_1" onclick="setStyle(this.id);" onkeyup="setStyle(this.id);" class="evenRownew6" >
	<td  width="101" align="center"><HATS:Component col="2" alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="10" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field7|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="6" componentSettings="" row="10" BIDIOpposite="false" />
										</td>
	<td class="leftline smpleord2_2" align="center"><HATS:Component
												col="8" alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="10" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field15|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="27" componentSettings="" row="10" BIDIOpposite="false" />
										</td>
	<td  width="72" align="center" class="leftline"><HATS:Component
												col="29" alternate=""
												widget="com.ibm.hats.transform.widgets.DropdownWidget"
												alternateRenderingSet="" erow="10" textReplacement=""
												widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Card=C;Each=E;Set=S;DecBD=D;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field4|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="29" componentSettings="" row="10" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline smpleord4_2"><HATS:Component
												col="31" alternate=""
												widget="com.ibm.hats.transform.widgets.DropdownWidget"
												alternateRenderingSet="" erow="10" textReplacement=""
												widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:CARD=CARD;DECK BD=DECK BD;13.5x18=13.5x18;27x18=27x18;27x48=27x48|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field6|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="38" componentSettings="" row="10" BIDIOpposite="false" />
										</td>
	<td  width="52" align="center" class="leftline"><HATS:Component
												col="40" alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="10" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="43" componentSettings="" row="10" BIDIOpposite="false" />
										</td>
	<td  width="302" align="center" class="leftline"><HATS:Component
												col="45" alternate=""
												widget="com.ibm.hats.transform.widgets.FieldWidget"
												alternateRenderingSet="" erow="10" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:field3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
												type="com.ibm.hats.transform.components.FieldComponent"
												ecol="79" componentSettings="" row="10" BIDIOpposite="false" />
										</td>
	<td  align="center" class="leftline"><HATS:Component col="12"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="11" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field10|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="31" componentSettings="" row="11" BIDIOpposite="false" />
										</td>
	</tr>
	<tr id="row_2" onclick="setStyle(this.id);" onkeyup="setStyle(this.id);" class="oddRownew6">
	<td align="center" ><HATS:Component col="2" alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="12" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field7|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|trimCaptions:true|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|deselectValue:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSCHECKBOX|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|selectValue:D|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="6" componentSettings="" row="12" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="8"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="12" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field15|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="27" componentSettings="" row="12" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="29"
												alternate=""
												widget="com.ibm.hats.transform.widgets.DropdownWidget"
												alternateRenderingSet="" erow="12" textReplacement=""
												widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Card=C;Each=E;Set=S;DecBD=D;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field4|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="29" componentSettings="" row="12" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="31"
												alternate=""
												widget="com.ibm.hats.transform.widgets.DropdownWidget"
												alternateRenderingSet="" erow="12" textReplacement=""
												widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:CARD=CARD;DECK BD=DECK BD;13.5x18=13.5x18;27x18=27x18;27x48=27x48|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field6|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="38" componentSettings="" row="12" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="40"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="12" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="43" componentSettings="" row="12" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="45"
												alternate=""
												widget="com.ibm.hats.transform.widgets.FieldWidget"
												alternateRenderingSet="" erow="12" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:field3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
												type="com.ibm.hats.transform.components.FieldComponent"
												ecol="79" componentSettings="" row="12" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="12"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="13" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field10|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="31" componentSettings="" row="13" BIDIOpposite="false" />
										</td>
	</tr>
	<tr id="row_3" onclick="setStyle(this.id);" onkeyup="setStyle(this.id);" class="evenRownew6">
	<td align="center"><HATS:Component col="2" alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="14" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field7|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="6" componentSettings="" row="14" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="8"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="14" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field15|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="27" componentSettings="" row="14" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="29"
												alternate=""
												widget="com.ibm.hats.transform.widgets.DropdownWidget"
												alternateRenderingSet="" erow="14" textReplacement=""
												widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Card=C;Each=E;Set=S;DecBD=D;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field4|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="29" componentSettings="" row="14" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="31"
												alternate=""
												widget="com.ibm.hats.transform.widgets.DropdownWidget"
												alternateRenderingSet="" erow="14" textReplacement=""
												widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:CARD=CARD;DECK BD=DECK BD;13.5x18=13.5x18;27x18=27x18;27x48=27x48|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field6|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="38" componentSettings="" row="14" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="40"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="14" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="43" componentSettings="" row="14" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="45"
												alternate=""
												widget="com.ibm.hats.transform.widgets.FieldWidget"
												alternateRenderingSet="" erow="14" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:field3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
												type="com.ibm.hats.transform.components.FieldComponent"
												ecol="79" componentSettings="" row="14" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="12"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="15" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field10|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="31" componentSettings="" row="15" BIDIOpposite="false" />
										</td>
	</tr>
	<tr id="row_4" onclick="setStyle(this.id);" onkeyup="setStyle(this.id);" class="oddRownew6">
	<td align="center"><HATS:Component col="2" alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="16" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field7|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|trimCaptions:true|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|deselectValue:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSCHECKBOX|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|selectValue:D|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="6" componentSettings="" row="16" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="8"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="16" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field15|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="27" componentSettings="" row="16" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="29"
												alternate=""
												widget="com.ibm.hats.transform.widgets.DropdownWidget"
												alternateRenderingSet="" erow="16" textReplacement=""
												widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Card=C;Each=E;Set=S;DecBD=D;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field4|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="29" componentSettings="" row="16" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="31"
												alternate=""
												widget="com.ibm.hats.transform.widgets.DropdownWidget"
												alternateRenderingSet="" erow="16" textReplacement=""
												widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:CARD=CARD;DECK BD=DECK BD;13.5x18=13.5x18;27x18=27x18;27x48=27x48|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field6|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="38" componentSettings="" row="16" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="40"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="16" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="43" componentSettings="" row="16" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="45"
												alternate=""
												widget="com.ibm.hats.transform.widgets.FieldWidget"
												alternateRenderingSet="" erow="16" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:field3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
												type="com.ibm.hats.transform.components.FieldComponent"
												ecol="79" componentSettings="" row="16" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="12"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="17" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field10|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="31" componentSettings="" row="17" BIDIOpposite="false" />
										</td>
	</tr>                                                                                                                  	
	<tr id="row_5" onclick="setStyle(this.id);" onkeyup="setStyle(this.id);" class="evenRownew6">
	<td align="center"><HATS:Component col="2" alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="18" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field7|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="6" componentSettings="" row="18" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="8"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="18" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field15|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="27" componentSettings="" row="18" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="29"
												alternate=""
												widget="com.ibm.hats.transform.widgets.DropdownWidget"
												alternateRenderingSet="" erow="18" textReplacement=""
												widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Card=C;Each=E;Set=S;DecBD=D;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field4|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="29" componentSettings="" row="18" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="31"
												alternate=""
												widget="com.ibm.hats.transform.widgets.DropdownWidget"
												alternateRenderingSet="" erow="18" textReplacement=""
												widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:CARD=CARD;DECK BD=DECK BD;13.5x18=13.5x18;27x18=27x18;27x48=27x48|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field6|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="38" componentSettings="" row="18" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="40"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="18" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="43" componentSettings="" row="18" BIDIOpposite="false" />
										</td>
 	<td align="center" class="leftline"><HATS:Component col="45"
												alternate=""
												widget="com.ibm.hats.transform.widgets.FieldWidget"
												alternateRenderingSet="" erow="18" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:field3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
												type="com.ibm.hats.transform.components.FieldComponent"
												ecol="79" componentSettings="" row="18" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="12"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="19" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field10|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="31" componentSettings="" row="19" BIDIOpposite="false" />
										</td>
	</tr>
	<tr id="row_6" onclick="setStyle(this.id);" onkeyup="setStyle(this.id);" class="oddRownew6">
	<td align="center" ><HATS:Component col="2" alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="20" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field7|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|trimCaptions:true|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|deselectValue:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSCHECKBOX|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|selectValue:D|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="6" componentSettings="" row="20" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="8"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="20" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field15|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="27" componentSettings="" row="20" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="29"
												alternate=""
												widget="com.ibm.hats.transform.widgets.DropdownWidget"
												alternateRenderingSet="" erow="20" textReplacement=""
												widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Card=C;Each=E;Set=S;DecBD=D;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field4|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="29" componentSettings="" row="20" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="31"
												alternate=""
												widget="com.ibm.hats.transform.widgets.DropdownWidget"
												alternateRenderingSet="" erow="20" textReplacement=""
												widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:CARD=CARD;DECK BD=DECK BD;13.5x18=13.5x18;27x18=27x18;27x48=27x48|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field6|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="38" componentSettings="" row="20" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="40"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="20" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="43" componentSettings="" row="20" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="45"
												alternate=""
												widget="com.ibm.hats.transform.widgets.FieldWidget"
												alternateRenderingSet="" erow="20" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:field3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
												type="com.ibm.hats.transform.components.FieldComponent"
												ecol="79" componentSettings="" row="20" BIDIOpposite="false" />
										</td>
	<td align="center" class="leftline"><HATS:Component col="12"
												alternate=""
												widget="com.ibm.hats.transform.widgets.InputWidget"
												alternateRenderingSet="" erow="21" textReplacement=""
												widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:field10|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
												type="com.ibm.hats.transform.components.InputComponent"
												ecol="31" componentSettings="" row="21" BIDIOpposite="false" />
										</td>
	</tr>                                                            
	</table>
	</div>
	</td>
    <td background="../common/images/right_middle4.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
  </tr>
</table>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="middle" class="topbar_headingtxtrad">
      &nbsp;<HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="24" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="24" />&nbsp;
      </td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
  
</table>

</div>
<!--Mene End-->

<div class="clear20px"></div>

<div class="btnmain">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="center">
	<table width="auto" border="0" cellspacing="5" cellpadding="0">
		<tr>
		<td>
		<div class="allbtn"><a href="javascript:ms('[pf3]','HATSForm');" title="Back">Back</a></div>
		</td>
		<td>
		<div class="allbtn"><a href="javascript:ms('[pf6]','HATSForm');" title="Enter">Submit</a></div>
		</td>
		</tr>
	</table>
		</td>
		</tr>
	</table>
</div>



</div>
<!--ddd-->

<script language="JavaScript">
	var tar=0;//row_5
	
	
	function setStyle(event){
		
		if(tar!=0){
		
		document.getElementById(tar).style.backgroundColor="#f7f7f7";
		
		
		}
		tar=event;
		//alert(tar);
		if(event=="row_5"){
		
		
		
		document.getElementById(event).style.backgroundColor="#a08b5b";
		
			
			
		
		}
		
		else if(event=="row_1"){

		document.getElementById(event).style.backgroundColor="#a08b5b";
				}
		else if(event=="row_2"){
		
		document.getElementById(event).style.backgroundColor="#a08b5b";
		}
		else if(event=="row_3"){
		
		document.getElementById(event).style.backgroundColor="#a08b5b";
		}
		else if(event=="row_4"){
		
		document.getElementById(event).style.backgroundColor="#a08b5b";
		}
		else if(event=="row_6"){
		
		document.getElementById(event).style.backgroundColor="#a08b5b";
		}
		 
		 
}	




				

	
	
	
	</script>




	<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<%// out.println("<!--blank.jsp"); %>
</body>
</html>
<% //out.println("-->"); %>