<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<HATS:Form>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<%session.setAttribute("inventoryInquiry_Details_sv1",null); %>
<%session.setAttribute("orderInquiry_Details_nextPage_sv",null); %>


<input type="hidden" name="in_482_1">
<input type="hidden" name="in_562_1">
<input type="hidden" name="in_642_1">
<input type="hidden" name="in_722_1">
<input type="hidden" name="in_802_1">
<input type="hidden" name="in_882_1">
<input type="hidden" name="in_962_1">
<input type="hidden" name="in_1042_1">
<input type="hidden" name="in_1122_1">
<input type="hidden" name="in_1202_1">
<input type="hidden" name="in_1282_1">
<input type="hidden" name="in_1362_1">
<input type="hidden" name="in_1442_1">
<input type="hidden" name="in_1522_1">
<input type="hidden" name="in_1602_1">
<input type="hidden" name="in_1682_1">
	
	<%String orderEntry_ProductSearch_combo_more = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("orderEntry_ProductSearch_combo_more", true).getString(0); %>
	
	<!--ddd-->
<div class="container">
<div style="display: none;" class="heading_bg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" class="topbar_headingtxt1">
    <HATS:Component col="21"
				alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="1" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="35"
				componentSettings="" row="1" />
    </td>
    <td width="34%" align="center" class="topbar_headingtxtred">&nbsp;</td>
    <td width="33%" align="right" class="topbar_headingtxt3">
    <HATS:Component
				col="38" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="1" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="1" />
    </td>
  </tr>
</table>
</div>

<div class="clear"></div>

<div class="heading_bg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="26" align="center" valign="middle" class="tiltletxt" style="text-transform: none;">Enter style and color number or partial style/color description.</td>
    </tr>
  </table>
</div>

<div class="clear3px"></div>

<!--Menu Start-->
<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top3.gif"></td>
    <td background="../common/images/center_middle_top3.gif"></td>
    <td width="5" height="5" background="../common/images/right_top3.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle3.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
      <table width="auto" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
        <tr>
          <td bgcolor="#FFFFFF" class="txt1"><strong>Style# / Name :</strong></td>
          <td>
            <HATS:Component col="15" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="34"
						componentSettings="" row="4" />
          </td>
          <td bgcolor="#FFFFFF" class="txt1"><strong>Color# / Name :</strong></td>
          <td>
            <HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="72"
						componentSettings="" row="4" />
          </td>
          <td align="center" bgcolor="#FFFFFF">
		<table width="auto" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td style="border: none;">
			<div class="allbtn"><a title="Enter" href="javascript:ms('[enter]','hatsportletid');">Enter</a></div>
			</td>
			</tr>
		</table>
          </td>
          </tr>
      </table>
      
</td>
    <td background="../common/images/right_middle3.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom3.gif"></td>
    <td background="../common/images/center_middle_bottom3.gif"></td>
    <td height="5" background="../common/images/right_bottom3.gif"></td>
  </tr>
</table>
    
<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->
      
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top">
    
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      	<thead>
        <tr class="oddRowdd">
        <td width="150" align="left">Style</td>
        <td width="150" class="leftline" >Color</td>
        <td width="210"  class="leftline">BK</td>
        <td width="180"  class="leftline">Style Description</td>
        <td align="center" class="leftline">Color Description</td>
        </tr>
        </thead>
     </table>
     <div style="vertical-align: top; overflow-y:auto; overflow-x:hidden; height:285px; border:solid 1px #CCC; padding:0px;">
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td width="100%">
        <HATS:Component type="com.ibm.hats.transform.components.TableComponent" widget="gulistan_JPS02.widgets.SampleOrderStatus" row="7" col="2" erow="22" ecol="75" componentSettings="startCol:2|endRow:22|columnBreaks:3,11,17,25,31,53|endCol:75|excludeCols:|visualTableDefaultValues:false|startRow:7|numberOfTitleRows:0|excludeRows:|includeEmptyRows:true|" componentIdentifier="orderEntry_ProductSearch_locate_combo" applyTextReplacement="true"/>
        </td></tr>
      </table>
      
      </div>
      
      
      
      </td>
      
    <td background="../common/images/right_middle4.gif"></td>
  </tr>
  
  <tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
  </tr>
</table>
<div class="clear2px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="middle" class="topbar_headingtxtrad">&nbsp;
      <HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="24" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="24" />&nbsp;
      </td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
  
</table>
</div>
<!--Mene End-->

<div class="clear"></div>

<div class="btnmain">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
	<table width="auto" border="0" cellspacing="5" cellpadding="0">
		<tr>
		<td>
		<div class="allbtn"><a title="Back" href="javascript:ms('[pf3]','hatsportletid');">Back</a></div>
		</td>
		</tr>
	</table>
        </td>
        <td width="78" align="center">
      <div id="nextButton" class="next_btn"><a href="javascript:ms('[pagedn]','HATSForm');"></a></div>
      <div id="previousButton" class="prev_btn"><a href="javascript:ms('[pageup]','HATSForm');"></a></div>
      </td>
    </tr>
  </table>
</div>
</div>
<!--ddd-->




	<script language="JavaScript">
	
	var orderEntry_ProductSearch_combo_more = "<%= orderEntry_ProductSearch_combo_more.trim()%>";
	
	if(orderEntry_ProductSearch_combo_more != "+"){
	document.getElementById("nextButton").innerHTML = "";
	document.getElementById("nextButton").className = "next_btndisable";
	}
	/*if(isPrevPage == "true"){
	document.getElementById("previousButton").innerHTML = "";
	document.getElementById("previousButton").className = "prev_btndisable";
	}
	if(isLocatePage == "yes"){
	document.getElementById("previousButton").style.visibility = "hidden";
	document.getElementById("nextButton").style.visibility = "hidden";
	}*/
	
	
	</script>







</HATS:Form>


</body>
</html>