
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>


<!-- Insert your HATS component tags here. -->

<input type="hidden" name="in_482_1">
<input type="hidden" name="in_562_1">
<input type="hidden" name="in_642_1">
<input type="hidden" name="in_722_1">
<input type="hidden" name="in_802_1">
<input type="hidden" name="in_882_1">
<input type="hidden" name="in_962_1">
<input type="hidden" name="in_1042_1">
<input type="hidden" name="in_1122_1">
<input type="hidden" name="in_1202_1">
<input type="hidden" name="in_1282_1">
<input type="hidden" name="in_1362_1">
<input type="hidden" name="in_1442_1">
<input type="hidden" name="in_1522_1">
<input type="hidden" name="in_1602_1">
<input type="hidden" name="in_1682_1">
	
		<%String ENDPLUS=((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("EndPlus", true).getString(0); %>
	<!--ddd-->
<div class="container">

<div class="heading_bg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
			<td width="33%" class="topbar_headingtxtred1" colspan="3" align="center"><HATS:Component
				col="3" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="78"
				componentSettings="" row="2" /> &nbsp;</td>
		</tr>
</table>
</div>

<div class="clear4px"></div>

<!--Menu Start-->
<div class="menubgg">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top">
    
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      	<thead>
        <tr class="oddRowdd">
        
        <td align="center" class="padd1">Order<br />Number</td>
		<td align="center" class="leftline2 padd2">Customer<br />PO Number</td>
		<td align="center" class="leftline2 padd3">Order<br />Status</td>
		<td align="center" class="leftline2 padd4">Order<br />Date</td>
		<td align="center" class="leftline2">Ship to Address</td>
		</tr>
        </thead>
     </table>
     <div style="vertical-align: top; overflow-y:auto; overflow-x:hidden; height:315px; border:solid 1px #CCC; padding:0px;">
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td width="100%">
        <HATS:Component
						type="com.ibm.hats.transform.components.TableComponent"
						widget="gulistan_JPS02.widgets.SampleOrderStatusCombination_2"
						row="5" col="2" erow="22" ecol="79"
						componentSettings="startCol:2|endRow:22|columnBreaks:4,8,14,28,40,50|endCol:79|excludeCols:|visualTableDefaultValues:false|startRow:5|numberOfTitleRows:0|excludeRows:|includeEmptyRows:true|"
						componentIdentifier="SampleCombination"
						applyTextReplacement="true" alternate="" alternateRenderingSet=""
						textReplacement="" widgetSettings="" />
        </td></tr>
      </table>
      
      </div>
      
      
      
      </td>
      
    <td background="../common/images/right_middle4.gif"></td>
  </tr>
  
  <tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
  </tr>
</table>
<div class="clear5px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="5" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="middle" class="topbar_headingtxtrad">&nbsp;
      <HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="24" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="24" />&nbsp;
      </td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="5" background="../common/images/right_bottom2.gif"></td>
  </tr>
  
</table>
</div>
<!--Mene End-->

<div class="clear"></div>

	<div class="btnmain">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="center" style="border: none;">
	<table width="auto" border="0" cellspacing="5" cellpadding="0">
		<tr>
		<td style="border: none;">
		<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');">Back</a></div>
		</td>
		</tr>
	</table>
	</td>
		<td width="78" align="center">
	<div id="nextButton" class="next_btn"><a href="javascript:ms('[pagedn]','HATSForm');"></a></div>
	<div id="previousButton" class="prev_btn"><a href="javascript:ms('[pageup]','HATSForm');"></a></div>
		</td>
		</tr>
	</table>
	</div>
	
</div>
<!--ddd-->




	<script language="JavaScript">
	
	var ENDPLUS = "<%= ENDPLUS.trim()%>";
	
	if(ENDPLUS != "+"){
	document.getElementById("nextButton").innerHTML = "";
	document.getElementById("nextButton").className = "next_btndisable";
	}
	/*if(isPrevPage == "true"){
	document.getElementById("previousButton").innerHTML = "";
	document.getElementById("previousButton").className = "prev_btndisable";
	}
	if(isLocatePage == "yes"){
	document.getElementById("previousButton").style.visibility = "hidden";
	document.getElementById("nextButton").style.visibility = "hidden";
	}*/
	
	
	</script>







	<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<%// out.println("<!--blank.jsp"); %>
</body>
</html>
<% //out.println("-->"); %>