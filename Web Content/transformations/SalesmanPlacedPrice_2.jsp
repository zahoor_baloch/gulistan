<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java"%>
<%@ taglib uri="hats.tld" prefix="HATS"%>
<%@page import="com.ibm.hats.common.*"%>
<%@page import="com.ibm.hats.common.TransformInfo"%>
<%@page import="com.ibm.hats.common.CommonConstants"%>

<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink />
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<script language="JavaScript">

</script>
<HATS:Form>
	
	<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV" />
	<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV" />
	<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV" />
	<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked" />
	<input type="hidden" name="hatsgv_logoffClicked"
		id="hatsgv_logoffClicked" />

	<!--ddd-->
	<div class="container">
	<div class="heading_bg">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="33%" class="topbar_headingtxt1">&nbsp;</td>
			<td width="34%" align="center" class="topbar_headingtxtredd"><HATS:Component
				col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="66"
				componentSettings="" row="2" /></td>
			<td width="33%" align="right" class="topbar_headingtxt3">&nbsp;</td>
		</tr>
	</table>
	</div>

	<!--Menu Start-->
	<div class="menubgg"><!--line-->
	<table width="100%">
		<tr>
			<td height="1">
			<div class="line"></div>
			</td>
		</tr>
	</table>
	<!--line-->

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="5" height="5" background="../common/images/left_top4.gif"></td>
			<td background="../common/images/center_middle_top4.gif"></td>
			<td width="5" height="5" background="../common/images/right_top4.gif"></td>
		</tr>
		<tr>
			<td background="../common/images/left_middle4.gif"></td>
			<td align="center" valign="top">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>

					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr class="oddRowdd">
							<td width="70" align="center">Style Code</td>
							<td width="250" align="center" class="leftline2">Style
							Description</td>
							<td width="90" align="center" class="leftline2">Warehouse</td>
							<td width="70" align="center" class="leftline2">Effect Date</td>
							<td width="90" align="center" class="leftline2">Ending Date</td>
							<td width="90" align="center" class="leftline2">Min Qty</td>
							<td width="90" align="center" class="leftline2">Roll Price</td>
							<td width="70" align="center" class="leftline2">Cut Price</td>
							<td align="center" class="leftline2">UM</td>
						</tr>
					</table>
					<div
						style="vertical-align: top; overflow-y: auto; overflow-x: hidden; height: 360px; border: solid 1px #CCC;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="100%"><HATS:Component type="com.ibm.hats.transform.components.TableComponent" widget="gulistan_JPS02.widgets.PricingInquiry" row="5" col="2" erow="22" ecol="79" componentSettings="startCol:2|endRow:22|columnBreaks:8,29,38,47,56,62,69,76,79|endCol:79|excludeCols:|visualTableDefaultValues:false|numberOfTitleRows:0|startRow:5|includeEmptyRows:true|excludeRows:|" componentIdentifier="PricingInquiry" applyTextReplacement="true"/>
							</td>
						</tr>
					</table>
					</div>

					</td>
				</tr>
			</table>
			</td>

			<td background="../common/images/right_middle4.gif"></td>
		</tr>

		<tr>
			<td height="5" background="../common/images/left_bottom4.gif"></td>
			<td background="../common/images/center_middle_bottom4.gif"></td>
			<td height="5" background="../common/images/right_bottom4.gif"></td>
		</tr>
	</table>

	</div>
	<!--Mene End-->


	<div class="clear"></div>

	<div class="btnmain">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left">
			<div style="padding: 0 0 0 385px;">
			<table width="auto" border="0" cellspacing="5" cellpadding="0">
				<TR>
					<td>
					<div class="allbtn"><a title="Exit" id="Exit"
						href="javascript:zeroInc();ms('[pf3]','hatsportletid');">Exit</a></div>
					</td>


					<%
						if (request.getParameter("incvalue") == null
									|| Integer.parseInt(request.getParameter("incvalue")) == -1) {
					%>
					<%
						} else if (request.getParameter("incvalue") != null
									|| Integer.parseInt(request.getParameter("incvalue")) != -1) {
					%>
					<td>
					<div class="allbtn"><a title="Previous" id="previous"
						href="javascript:decPrev();ms('[pageup]','hatsportletid');">Previous</a></div>
					</td>
					<%
						}
					%>
					<%
						if (((TransformInfo) request
									.getAttribute(CommonConstants.REQ_TRANSFORMINFO))
									.getGlobalVariable("moreRecords", true).getString(0)
									.equals("+")) {
					%>
					<td>
					<div class="allbtn"><a title="Next" id="next"
						href="javascript:incNext();ms('[pagedn]','hatsportletid');">Next</a></div>
					</td>
					<%
						}
					%>

				</TR>
			</table>
			</div>
			</td>
			<td width="199" align="center">
			<div style="border: 1px solid #8b794f; margin: 0 4px 0 0;">
			<table width="auto" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="10" bgcolor="#d1c4a2">&nbsp;</td>
					<td bgcolor="#d1c4a2">Export Data : &nbsp;&nbsp;</td>
					<td bgcolor="#d1c4a2"><a
						href="javascript:ms('[pf2]','hatsportletid');" title="Email"><img
						src="../common/images/maifl.png" title="Email" width="28"
						height="28" border="0" align="absmiddle" /></a>&nbsp;&nbsp;</td>
					<td bgcolor="#d1c4a2"><a style="cursor: pointer"
						onclick="pdfPopUp();"><img
						src="../common/images/pdf_icon_20x20.png" title="PDF" width="28"
						height="24" border="0" align="absmiddle" /></a>&nbsp;&nbsp;</td>
					<td bgcolor="#d1c4a2"><a style="cursor: pointer"
						onclick="excelPopUp();"><img src="../common/images/excel.png"
						title="Excel" width="24" height="24" border="0" align="absmiddle" /></a></td>
					<td width="10" bgcolor="#d1c4a2">&nbsp;</td>
				</tr>
			</table>
			</div>
			</td>
		</tr>
	</table>
	</div>

	</div>


	<input type="hidden" name="testHiddenValue" id="testHiddenValue" />




</HATS:Form>

<HATS:OIA />

</body>
</html>
