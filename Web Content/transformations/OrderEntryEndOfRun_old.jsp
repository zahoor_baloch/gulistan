
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>


<HATS:Form>

<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPage_cancelGV" id="hatsgv_menuPage_cancelGV"/>
<input type="hidden" name="hatsgv_menuField_cancelGV" id="hatsgv_menuField_cancelGV"/>


	<input type="hidden" name="in_482_1">
	<input type="hidden" name="in_562_1">
	<input type="hidden" name="in_642_1">
	<input type="hidden" name="in_722_1">
	<input type="hidden" name="in_802_1">
	<input type="hidden" name="in_882_1">
	<input type="hidden" name="in_962_1">
	<input type="hidden" name="in_1042_1">
	<input type="hidden" name="in_1122_1">
	<input type="hidden" name="in_1202_1">
	<input type="hidden" name="in_1282_1">
	<input type="hidden" name="in_1362_1">
	<input type="hidden" name="in_1442_1">
	<input type="hidden" name="in_1522_1">
	<input type="hidden" name="in_1602_1">
	<input type="hidden" name="in_1682_1">



<input type="hidden" name="in_373_1" id="in_373_1"/>


	<div class="container">
<div class="menubgg">
<div class="heading_bg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" class="topbar_headingtxt1"> </td>
    <td width="34%" align="center" class="topbar_headingtxtred"> </td>
    <td width="33%" align="right" class="topbar_headingtxt3"></td>
  </tr>

</table>
</div>

<div class="clear4px"></div>

<div class="popupmain">
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center" valign="middle">
      <table width="100%" height="200" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td background="../common/images/popup_top_left.gif" width="16">&nbsp;</td>

      <td height="16" background="../common/images/popup_top_middle.gif">&nbsp;</td>
      <td background="../common/images/popup_top_right.gif" width="16">&nbsp;</td>
    </tr>
    <tr>
      <td background="../common/images/popup_left_middle.gif">&nbsp;</td>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="fillbor1">
        <tr>
          <td align="center" class="txtpopred" style="padding: 5px;">Order Entry End Of Run</td>

        </tr>
        <tr>
          <td align="center" bgcolor="#f7f7f7" class="txtpopred" style="padding: 5px;"> There is currently an unposted order in the system.<br>
       Do you wish to cancel this order?            </td>
        </tr>
        <tr>
          <td height="5" align="center" style="border:none;"></td>
        </tr>

        <tr>
          <td align="center" style="padding: 5px;">
          <table width="auto" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="border:none;">
            <div id="popupbtn">
             <div align="center">
              <ul>
                <li><a href ="javascript:setCursorPosition(373, 'HATSForm');checkInput2('in_373_1', 'Y','hidden');ms('[enter]', 'HATSForm')" title="Exit"><span>YES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></li>

              </ul>             
             </div>
            </div>
            </td>
          </tr>
        </table>
     
        <div class="clear10px"></div>
        <table width="auto" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="border:none;">
            <div id="popupbtn">
             <div align="center">
              <ul>
                <li id="normalCance"><a href ="javascript:setCursorPosition(373, 'HATSForm');checkInput2('in_373_1', 'N','hidden');ms('[enter]', 'HATSForm')" title="Exit"><span>NO</span></a></li>              	          
               </ul>			       
             </div>
            </div>
            </td>
          </tr>
        </table>
        
        </tr>
        <tr>
          <td height="5" align="center" style="border:none;"></td>
        </tr>
        <tr>
          <td style="visibility:hidden;  font-size: 12px; padding: 5px;">
          (*Clicking the Cancel Order button will return you to the Dashboard page)
          </td>
          
          </tr>
      </table></td>
      <td background="../common/images/popup_right_middle.gif">&nbsp;</td>
    </tr>
    <tr>
      <td background="../common/images/popup_below_left.gif" width="16">&nbsp;</td>

      <td height="16" background="../common/images/popup_below_middle.gif">&nbsp;</td>
      <td background="../common/images/popup_below_right.gif" width="16">&nbsp;</td>
    </tr>
  </table>
      </td>
    </tr>
    
  </table>
</div>


<div class="clear5px"></div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="5" height="5" background="../common/images/left_top2.gif"></td>
	<td background="../common/images/center_middle_top2.gif"></td>
	<td width="5" height="5" background="../common/images/right_top2.gif"></td>
	</tr>
	<tr>
	<td background="../common/images/left_middle2.gif"></td>
	<td align="center" valign="middle" class="topbar_headingtxtrad">&nbsp;
	<HATS:Component col="2" alternate=""
		widget="com.ibm.hats.transform.widgets.LabelWidget"
		alternateRenderingSet="" erow="24" textReplacement=""
		widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
		type="com.ibm.hats.transform.components.TextComponent" ecol="79"
		componentSettings="" row="24" />&nbsp;</td>
	<td background="../common/images/right_middle2.gif"></td>
	</tr>
	<tr>
	<td height="5" background="../common/images/left_bottom2.gif"></td>
	<td background="../common/images/center_middle_bottom2.gif"></td>
	<td height="5" background="../common/images/right_bottom2.gif"></td>
	</tr>

</table>
<div class="clear5px"></div>




	<div class="btnmain">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center">
<table width="auto" border="0" cellspacing="5" cellpadding="0">
	<tr>
	<td>
	<div class="allbtn"><a href="javascript:ms('[pf2]','hatsportletid');" title="Return to Processing">Return to Processing</a></div>
	</td>
	
	
	
	
	</tr>
</table>
	</td>
	</tr>
</table>
	</div>
	</div>
</div>



	<HATS:Component col="53" alternate=""
		widget="com.ibm.hats.transform.widgets.FieldWidget"
		alternateRenderingSet="" erow="5" textReplacement="" widgetSettings=""
		type="com.ibm.hats.transform.components.FieldComponent" ecol="53"
		componentSettings="" row="5" />



	<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>

</body>
</html>
