<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<HATS:Form>


<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPage_cancelGV" id="hatsgv_menuPage_cancelGV"/>
<input type="hidden" name="hatsgv_menuField_cancelGV" id="hatsgv_menuField_cancelGV"/>

<input type="hidden" name="hatsgv_menuClicked" id="hatsgv_menuClicked"/>

	<!--<input type="hidden" name="in_482_1">
	<input type="hidden" name="in_562_1">
	<input type="hidden" name="in_642_1">
	<input type="hidden" name="in_722_1">
	<input type="hidden" name="in_802_1">
	<input type="hidden" name="in_882_1">
	<input type="hidden" name="in_962_1">
	<input type="hidden" name="in_1042_1">
	<input type="hidden" name="in_1122_1">
	<input type="hidden" name="in_1202_1">
	<input type="hidden" name="in_1282_1">
	<input type="hidden" name="in_1362_1">
	<input type="hidden" name="in_1442_1">
	<input type="hidden" name="in_1522_1">
	<input type="hidden" name="in_1602_1">
	<input type="hidden" name="in_1682_1">


--><div class="container">

<div class="heading_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td width="33%" class="topbar_headingtxt1">&nbsp;</td>
    <td width="34%" align="center" class="topbar_headingtxtredd">Sample Type Selection</td>
    <td width="33%" align="right" class="topbar_headingtxt3">&nbsp;</td>
	</tr>
</table>
</div>

<div class="clear4px"></div>

<div class="menubgg">
<div class="popupmain57">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center" valign="middle">
<table width="100%" height="150" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td background="../common/images/popup_top_left.png" width="16">&nbsp;</td>
	<td height="16" background="../common/images/popup_top_middle.png">&nbsp;</td>
	<td background="../common/images/popup_top_right.png" width="16">&nbsp;</td>
	</tr>
	<tr>
	<td background="../common/images/popup_left_middle.png">&nbsp;</td>
	<td bgcolor="#FFFFFF">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="5" height="5" background="../common/images/left_top2.gif"></td>
	<td background="../common/images/center_middle_top2.gif"></td>
	<td width="5" height="5" background="../common/images/right_top2.gif"></td>
	</tr>
    <tr>
	<td background="../common/images/left_middle2.gif"></td>
	<td align="center" valign="top" bgcolor="#d1c4a2">
<table width="auto" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
	<tr>
    <td colspan="2" align="center" bgcolor="#FFFFFF" class="txt4"><strong>Order Post</strong></td>
    </tr>
	<tr height="50">
	
	<td class="txt1"><HATS:Component col="5" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="6" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="44" componentSettings="" row="6" /></td>
	</tr>
	<tr>
	<td colspan="2" align="center" bgcolor="#FFFFFF">
	<div class="btnmain">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
	<td align="center" style="border:none;">
<table width="auto" border="0" cellspacing="5" cellpadding="0">
    <tr>
	<td style="border:none;">
	<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');" title="OK">OK</a></div>
	</td>
	</tr>
</table>
	</td>
	</tr>
</table>
	</div>
	</td>
	</tr>
</table>            
	</td>
	<td background="../common/images/right_middle2.gif"></td>
	</tr>
	<tr>
	<td height="5" background="../common/images/left_bottom2.gif"></td>
	<td background="../common/images/center_middle_bottom2.gif"></td>
	<td height="5" background="../common/images/right_bottom2.gif"></td>
	</tr>
</table>
	</td>
	<td background="../common/images/popup_right_middle.png">&nbsp;</td>
	</tr>
	<tr>
	<td background="../common/images/popup_below_left.png" width="16">&nbsp;</td>
	<td height="16" background="../common/images/popup_below_middle.png">&nbsp;</td>
	<td background="../common/images/popup_below_right.png" width="16">&nbsp;</td>
	</tr>
</table>
	</td>
	</tr>
</table>
</div>

</div>

</div>
</HATS:Form>
</body>
</html>