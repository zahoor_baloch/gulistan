<%// out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<% //out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
	<%session.setAttribute("postOrder_nextPage_sv",null); %>
	<%session.setAttribute("postOrder_nextPage_moreInfo_sv",null); %>
<!-- Insert your HATS component tags here. -->


<%session.setAttribute("orderInquiry_ProductSearch_locate_sv1",null); %>

<!--ddd-->
<div class="container">
<div class="topbar_heading">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" class="topbar_headingtxt1"><HATS:Component col="21"
				alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="1" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="35"
				componentSettings="" row="1" /></td>
    <td width="34%" align="center" class="topbar_headingtxt2">&nbsp;</td>
    <td width="33%" align="right" class="topbar_headingtxt3"><HATS:Component
				col="38" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="1" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="58"
				componentSettings="" row="1" /></td>
  </tr>
</table>
</div>

<div class="clear5px"></div>

<div class="heading_bg" style="display:none;">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="26" align="center" valign="middle" class="tiltletxt"></td>
    </tr>
  </table>
</div>

<div class="clear10px" style="display:none;"></div>

<!--Menu Start-->
<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top3.gif"></td>
    <td background="../common/images/center_middle_top3.gif"></td>
    <td width="5" height="5" background="../common/images/right_top3.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle3.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
      <table width="auto" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
        <tr>
          <td bgcolor="#FFFFFF" class="txt1"><strong>Style :</strong></td>
          <td valign="middle">
            <HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="23"
						componentSettings="" row="4" />
          </td>
          <td bgcolor="#FFFFFF" class="txt1"><strong>Color :</strong></td>
          <td valign="middle">
            <HATS:Component col="34" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="38"
						componentSettings="" row="4" />
          </td>
          <td bgcolor="#FFFFFF" class="txt1"><strong>Backing Codes (Bk) :</strong></td>
          <td valign="middle"><label>
           <HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Action Back=AB;Unitary=UN|captionSource:VALUE|sharedGVs:false|style:background-color: #ececec|useHints:true|dropdownStyleClass:HATSDROPDOWN|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="47"
						componentSettings="" row="4" />
          </label></td>
          <td bgcolor="#FFFFFF"><img onmouseover="this.style.cursor ='hand'"; onmouseout="this.style.cursor = 'auto'"; onclick="ms('[pf4]','hatsportletid');" src="../common/images/Search-icon.png" width="24" height="24" /></td>
          <td width="80" align="center" bgcolor="#FFFFFF">
          <table width="auto" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="3" height="24" background="../common/images/left_img_btn.gif" style="border:none;"></td>
                  <td background="../common/images/middle_img_btn.gif" style="background-repeat:repeat-x; border:none;">
                    <div class="allbtn"><a href="javascript:ms('[enter]','hatsportletid');">Enter</a></div>
                    </td>
                  <td width="3" background="../common/images/right_img_btn.gif" style="border:none;"></td>
                  </tr>
                </table>
          </td>
        </tr>
       
  
  
      </table>
      
      </td>
    <td background="../common/images/right_middle3.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom3.gif"></td>
    <td background="../common/images/center_middle_bottom3.gif"></td>
    <td height="5" background="../common/images/right_bottom3.gif"></td>
  </tr>
</table>
    
      <div class="clear5px"></div>
      
      <div class="linsprtd"></div>
      
      <div class="clear5px"></div>
      
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td height="317" background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="top">
      &nbsp;
      </td>
    <td align="right" background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
  
</table>

<div class="clear2px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="middle" class="topbar_headingtxtrad" id="msgArea">
      &nbsp;<HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="24" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="24" />&nbsp;
      </td>
      <script language="JavaScript">
		var msgArea = document.getElementById("msgArea").innerHTML;
		if(msgArea.search("ORDER") != -1 && msgArea.search("POSTED") != -1 && msgArea.search("GULISTAN") != -1 ){
		//alert("Found");
		document.getElementById("msgArea").innerHTML = "&nbsp;";
		}
	  </script>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
  
</table>
</div>
<!--Mene End-->

<div class="clear1px"></div>

<div class="btnmain">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table width="auto" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td>
              <table width="auto" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="3" height="24" background="../common/images/left_img_btn.gif"></td>
                  <td background="../common/images/middle_img_btn.gif" style="background-repeat:repeat-x">
                    <div class="bttntxt1"><a href="javascript:ms('[pf24]','hatsportletid');">Back</a></div>
                    </td>
                  <td width="3" background="../common/images/right_img_btn.gif"></td>
                  </tr>
                  
                </table>
            </td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</div>

<!--ddd-->

	<%String orderPostedNumber = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("orderPostedNumber", true).getString(0); %>

	<script language="JavaScript">
	
	var orderPostedNumber = "<%= orderPostedNumber.trim()%>";
	var onlyNums = /[^0-9]/g;
	if (onlyNums.test(orderPostedNumber)) { 
	//alert("numbers only"); 
	}
	if(orderPostedNumber != "" && !onlyNums.test(orderPostedNumber)){
	alert("Order "+ orderPostedNumber +" Posted to Gulistan File");
	}
	
	</script>

	


	<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<% //out.println("<!--blank.jsp"); %>
</body>
</html>
<% //out.println("-->"); %>