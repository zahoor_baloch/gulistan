<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<HATS:Form>


<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV" />
	<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV" />
	<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV" />
	<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked" />
	<input type="hidden" name="hatsgv_logoffClicked"
		id="hatsgv_logoffClicked" />


	<input type="hidden" name="hatsgv_menuClicked" id="hatsgv_menuClicked"/>

	<!--<input type="hidden" name="in_482_1">
	<input type="hidden" name="in_562_1">
	<input type="hidden" name="in_642_1">
	<input type="hidden" name="in_722_1">
	<input type="hidden" name="in_802_1">
	<input type="hidden" name="in_882_1">
	<input type="hidden" name="in_962_1">
	<input type="hidden" name="in_1042_1">
	<input type="hidden" name="in_1122_1">
	<input type="hidden" name="in_1202_1">
	<input type="hidden" name="in_1282_1">
	<input type="hidden" name="in_1362_1">
	<input type="hidden" name="in_1442_1">
	<input type="hidden" name="in_1522_1">
	<input type="hidden" name="in_1602_1">
	<input type="hidden" name="in_1682_1">


--><div class="container">

<div class="heading_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td width="33%" class="topbar_headingtxt1"><HATS:Component col="24"
				alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="33"
				componentSettings="" row="2" /></td>
    <td width="34%" align="center" class="topbar_headingtxtredd">Inventory Selection</td>
    <td width="33%" align="right" class="topbar_headingtxt3"><HATS:Component
				col="36" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="65"
				componentSettings="" row="2" /></td>
	</tr>
</table>
</div>

<div class="clear4px"></div>

<div class="menubgg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
	</tr>
	<tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
<table width="auto" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
	<tr>
	<td class="txt1order2">Class:</td>
	<td align="left"><HATS:Component col="12" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="3" />&nbsp;&nbsp;&nbsp;</td>
	<td class="txt1order2">Pattern:</td>
	<td align="left"><HATS:Component col="12" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="4" />&nbsp;&nbsp;&nbsp;</td>
	<td class="txt1order2">Color:</td>
	<td align="left"><HATS:Component col="12" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="5" />&nbsp;&nbsp;&nbsp;</td>
    <td class="txt1order2">Size:</td>
	<td align="left"><HATS:Component col="12" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="6" /></td>
	</tr>
</table>
	</td>
	<td background="../common/images/right_middle4.gif"></td>
	</tr>
	<tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
	</tr>
</table>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:0 10px 0 0;">
	<tr>
	<td width="5" height="5" background="../common/images/left_top4.gif"></td>
	<td background="../common/images/center_middle_top4.gif"></td>
	<td width="5" height="5" background="../common/images/right_top4.gif"></td>
	</tr>
    <tr>
	<td background="../common/images/left_middle4.gif"></td>
	<td align="center" valign="top" bgcolor="#d1c4a2">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<thead>
	<tr class="barhead">
	<td align="left">CO</td>
	<td align="left" class="leftline2">Pattern</td>
	<td align="left" class="leftline2">Color</td>
	<td align="left" class="leftline2">Size</td>
	<td align="left" class="leftline2">Class</td>
	</tr>
	</thead>
    <tr>
	<td height="1" colspan="4" align="left"></td>
	</tr>
	<tr class="evenRowbar" height="23">
	<td align="left" valign="top"><HATS:Component col="3" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="9" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="3"
								componentSettings="" row="9" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="7"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="9" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="11"
								componentSettings="" row="9" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="15"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="9" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="19"
								componentSettings="" row="9" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="23"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="9" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="30"
								componentSettings="" row="9" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="35"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="9" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="36"
								componentSettings="" row="9" /></td>
	</tr>
</table>            
	</td>
	<td background="../common/images/right_middle4.gif"></td>
	</tr>
	<tr>
	<td height="5" background="../common/images/left_bottom4.gif"></td>
	<td background="../common/images/center_middle_bottom4.gif"></td>
	<td height="5" background="../common/images/right_bottom4.gif"></td>
	</tr>
</table>
    </td>
    <td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="5" height="5" background="../common/images/left_top4.gif"></td>
	<td background="../common/images/center_middle_top4.gif"></td>
	<td width="5" height="5" background="../common/images/right_top4.gif"></td>
	</tr>
    <tr>
	<td background="../common/images/left_middle4.gif"></td>
	<td align="center" valign="top" bgcolor="#d1c4a2">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<thead>
	<tr class="barhead">
	<td align="left">Unit Price</td>
	<td align="left" class="leftline2">Quantity Ordered</td>
	</tr>
	</thead>
    <tr>
	<td height="1" colspan="2" align="left"></td>
	</tr>
	<tr class="oddRowbar">
	<td align="left" valign="top"><HATS:Component col="44" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="48" componentSettings="" row="5" /><HATS:Component
								col="44" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="48" componentSettings="" row="5" /><HATS:Component
								col="44" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="48" componentSettings="" row="5" /><HATS:Component
								col="44" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="48" componentSettings="" row="5" /><HATS:Component
								col="44" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="48" componentSettings="" row="5" /><HATS:Component col="3"
								alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent" ecol="8"
								componentSettings="" row="14" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="21"
								alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="27" componentSettings="" row="14" /></td>
	</tr>
</table>            
	</td>
	<td background="../common/images/right_middle4.gif"></td>
	</tr>
	<tr>
	<td height="5" background="../common/images/left_bottom4.gif"></td>
	<td background="../common/images/center_middle_bottom4.gif"></td>
	<td height="5" background="../common/images/right_bottom4.gif"></td>
	</tr>
</table>
    </td>
	</tr>
</table>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="5" height="5" background="../common/images/left_top4.gif"></td>
	<td background="../common/images/center_middle_top4.gif"></td>
	<td width="5" height="5" background="../common/images/right_top4.gif"></td>
	</tr>
    <tr>
	<td background="../common/images/left_middle4.gif"></td>
	<td align="center" valign="top" bgcolor="#d1c4a2">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<thead>
	<tr class="barhead">
	<td align="left">Sidemarks</td>
	<td align="left" class="leftline2">Line Instructions</td>
	</tr>
	</thead>
    <tr>
	<td height="1" colspan="2" align="left"></td>
	</tr>
	<tr class="oddRowbar">
	<td align="left" valign="top"><HATS:Component col="3" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="22"
						componentSettings="" row="18" /></td>
	<td align="left" valign="top" class="leftline"><HATS:Component col="26"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt9|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="75"
						componentSettings="" row="18" /></td>
	</tr>
</table>            
	</td>
	<td background="../common/images/right_middle4.gif"></td>
	</tr>
	<tr>
	<td height="5" background="../common/images/left_bottom4.gif"></td>
	<td background="../common/images/center_middle_bottom4.gif"></td>
	<td height="5" background="../common/images/right_bottom4.gif"></td>
	</tr>
</table>

	<div class="clear5px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="5" height="5" background="../common/images/left_top2.gif"></td>
	<td background="../common/images/center_middle_top2.gif"></td>
	<td width="5" height="5" background="../common/images/right_top2.gif"></td>
	</tr>
	<tr>
	<td background="../common/images/left_middle2.gif"></td>
	<td align="center" valign="middle" class="topbar_headingtxtrad">&nbsp;
	<HATS:Component col="2" alternate=""
		widget="com.ibm.hats.transform.widgets.LabelWidget"
		alternateRenderingSet="" erow="24" textReplacement=""
		widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
		type="com.ibm.hats.transform.components.TextComponent" ecol="79"
		componentSettings="" row="24" />&nbsp;</td>
	<td background="../common/images/right_middle2.gif"></td>
	</tr>
	<tr>
	<td height="5" background="../common/images/left_bottom2.gif"></td>
	<td background="../common/images/center_middle_bottom2.gif"></td>
	<td height="5" background="../common/images/right_bottom2.gif"></td>
	</tr>

</table>
</div>

	<div class="clear"></div>

	<div class="btnmain">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center">
<table width="auto" border="0" cellspacing="5" cellpadding="0">
	<tr>
	<td>
	<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');" title="Cancel">Cancel</a></div>
	</td>
	<td>
	<div class="allbtn"><a href="javascript:ms('[pf2]','hatsportletid');" title="Type Selection">Type Selection</a></div>
	</td>
	<td>
	<div class="allbtn"><a href="javascript:ms('[enter]','hatsportletid');" title="Enter">Enter</a></div>
	</td>
	</tr>
</table>
	</td>
	</tr>
</table>
	</div>

</div>
</HATS:Form>
</body>
</html>