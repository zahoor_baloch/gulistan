<% out.println("<!--default.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>

<body>
<% out.println("-->"); %>   
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
</script>

<script type="text/javascript">
    portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
</script>

<script type="text/javascript">
    PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<div align="center">

<HATS:Form>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<table>
  <tr>
    <td>
      <HATS:DefaultRendering/>
    </td>
  </tr>
  <tr>
    <td align="center">
      <HATS:HostKeypad />
    </td>
  </tr>
  <tr>
   <td align="center">
      <HATS:OIA/>
   </td>
  </tr>
</table>

</HATS:Form>

</div>
<% out.println("<!--default.jsp"); %>
</body>
</html>
<% out.println("-->"); %>