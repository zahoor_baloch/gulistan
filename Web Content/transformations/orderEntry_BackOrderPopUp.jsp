<%// out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>

<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>

<link rel="stylesheet" href="../common/css/menu2.css" type="text/css">
</head>
<body>
<% //out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<!-- Insert your HATS component tags here. -->

	<%session.setAttribute("postOrder_nextPage_sv",null); %>
	<%session.setAttribute("postOrder_nextPage_moreInfo_sv",null); %>
	<input type="hidden" name="in_722_1">
	<input type="hidden" name="in_802_1">
	<input type="hidden" name="in_882_1">
	<input type="hidden" name="in_962_1">
	<input type="hidden" name="in_1042_1">
	<input type="hidden" name="in_1122_1">
	<input type="hidden" name="in_1202_1">
	<input type="hidden" name="in_1282_1">
	<input type="hidden" name="in_1362_1">
	<input type="hidden" name="in_1442_1">
	<!--ddd-->
	<div class="container">
	
	<div class="heading_bg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr>
 <td align="center" class="topbar_headingtxt1">
 
 </td>
 
 </tr>

</table>
</div>

<div class="clear4px"></div>

<div class="popupmain2">
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center" valign="middle">
      <table width="100%" height="200" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td background="../common/images/popup_top_left.gif" width="16">&nbsp;</td>

      <td height="16" background="../common/images/popup_top_middle.gif">&nbsp;</td>
      <td background="../common/images/popup_top_right.gif" width="16">&nbsp;</td>
    </tr>
    <tr>
      <td background="../common/images/popup_left_middle.gif">&nbsp;</td>
      <td><table width="100%" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
      <tr>
          <td colspan="6" bgcolor="#d1c4a2" class="txt1" style="font-size: 14pt;"><strong>Please fill in the details to complete the order</strong></td>

          </tr>
        <tr>
          <td colspan="6" bgcolor="#d1c4a2" class="txt1"><strong>Back Order Selections </strong></td>

          </tr>
        
        <tr>
          <td bgcolor="#d1c4a2" class="txt1"><strong>Cut Lth:</strong></td>
          <td colspan="3" class="txt1">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td style="border: none;"><strong>Ft</strong><span class="red">*</span></td>
          <td style="border: none;"><HATS:Component col="19"
										alternate=""
										widget="com.ibm.hats.transform.widgets.FieldWidget"
										alternateRenderingSet="" erow="5" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
										type="com.ibm.hats.transform.components.FieldComponent"
										ecol="21" componentSettings="" row="5" /></td>
          <td style="border: none;"><strong>In</strong><span class="red">*</span></td>
          <td style="border: none;"><HATS:Component col="27"
										alternate=""
										widget="com.ibm.hats.transform.widgets.FieldWidget"
										alternateRenderingSet="" erow="5" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
										type="com.ibm.hats.transform.components.FieldComponent"
										ecol="28" componentSettings="" row="5" /></td>
          </tr>
          </table>
           
          
            
           

           </td>
          <td bgcolor="#d1c4a2" class="txt1"><strong>Blank =</strong></td>
          <td class="txt1"><HATS:Component col="38" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings=""
								type="com.ibm.hats.transform.components.TextComponent" ecol="48"
								componentSettings="" row="5" /></td>
        </tr>
        <tr>
          <td bgcolor="#d1c4a2" class="txt1"><strong>S/m:</strong></td>
          <td colspan="5" class="txt1">

          <HATS:Component col="13" alternate=""
								widget="com.ibm.hats.transform.widgets.FieldWidget"
								alternateRenderingSet="" erow="6" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxtfull2|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
								type="com.ibm.hats.transform.components.FieldComponent"
								ecol="32" componentSettings="" row="6" />
          </td>
          </tr>
        <tr>
          <td bgcolor="#d1c4a2" class="txt1"><strong>Same Dyelot(Y/N) :</strong></td>
          <td colspan="5" class="txt1">
          <HATS:Component col="25" alternate=""
								widget="com.ibm.hats.transform.widgets.DropdownWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Yes=Y;No=N;|captionSource:VALUE|sharedGVs:false|style:background-color: #ececec|useHints:true|dropdownStyleClass:HATSDROPDOWN|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="25" componentSettings="" row="7" />
          </td>
          </tr>
          
          <tr>
          <td bgcolor="#d1c4a2" class="txt1"><strong>Special Price :</strong></td>
          <td class="txt1"><HATS:Component col="22"
								alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|protectedLinkStyleClass:HATSPROTLINK|underlineStyle:text-decoration: underline|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
								type="com.ibm.hats.transform.components.FieldComponent"
								ecol="26" componentSettings="" row="7" />
          
          </td>
          <td bgcolor="#d1c4a2" class="txt1"><strong>Price Comment :</strong></td>
          
          <td colspan="5" class="txt1" align="left">
          <HATS:Component col="44" alternate=""
								widget="com.ibm.hats.transform.widgets.FieldWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|protectedLinkStyleClass:HATSPROTLINK|underlineStyle:text-decoration: underline|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
								type="com.ibm.hats.transform.components.FieldComponent"
								ecol="63" componentSettings="" row="7" />
          
          </td>
          </tr>
         <tr>
          <td bgcolor="#d1c4a2" class="txt1"><strong>Rolls to Back Order :</strong></td>
          <td colspan="5" class="txt1">
          <HATS:Component col="28" alternate=""
								widget="com.ibm.hats.transform.widgets.FieldWidget"
								alternateRenderingSet="" erow="8" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt2|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
								type="com.ibm.hats.transform.components.FieldComponent"
								ecol="29" componentSettings="" row="8" />
          </td>
          </tr> 
        <tr>
          <td bgcolor="#d1c4a2" class="txt1"><strong>Required For</strong></td>
          <td colspan="5" class="txt1">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td style="border: none;" width="50"><strong style="white-space: nowrap;">Min Ft</strong><span class="red2">*</span></td>
          <td style="border: none;" width="150"><HATS:Component col="28" alternate=""
											widget="com.ibm.hats.transform.widgets.FieldWidget"
											alternateRenderingSet="" erow="9" textReplacement=""
											widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
											type="com.ibm.hats.transform.components.FieldComponent"
											ecol="30" componentSettings="" row="9" /></td>
          <td style="border: none;" width="20"><strong>In</strong><span class="red">*</span></td>
          <td style="border: none;"><HATS:Component col="35" alternate=""
											widget="com.ibm.hats.transform.widgets.FieldWidget"
											alternateRenderingSet="" erow="9" textReplacement=""
											widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
											type="com.ibm.hats.transform.components.FieldComponent"
											ecol="36" componentSettings="" row="9" /></td>
          </tr>
          </table>
          
            

            
            
          </td>
          </tr>
        <tr>
          <td bgcolor="#d1c4a2" class="txt1"><strong>Balance Roll</strong></td>
          <td colspan="5" class="txt1">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td style="border: none;" width="50"><strong style="white-space: nowrap;">Max Ft</strong><span class="red">*</span></td>
          <td style="border: none;" width="150"><HATS:Component col="28" alternate=""
											widget="com.ibm.hats.transform.widgets.FieldWidget"
											alternateRenderingSet="" erow="10" textReplacement=""
											widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
											type="com.ibm.hats.transform.components.FieldComponent"
											ecol="30" componentSettings="" row="10" /></td>
          <td style="border: none;" width="20"><strong>In</strong><span class="red">*</span></td>
          <td style="border: none;"><HATS:Component col="35" alternate=""
											widget="com.ibm.hats.transform.widgets.FieldWidget"
											alternateRenderingSet="" erow="10" textReplacement=""
											widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt3|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
											type="com.ibm.hats.transform.components.FieldComponent"
											ecol="36" componentSettings="" row="10" /></td>
          </tr>
          </table>
           
            

            
            
          </td>
          </tr>
          <tr>
          <td colspan="6" class="topbar_headingtxtrad">&nbsp;<HATS:Component
								col="3" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings=""
								type="com.ibm.hats.transform.components.TextComponent" ecol="54"
								componentSettings="" row="11" />
			<HATS:Component col="4" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="13" textReplacement=""
								widgetSettings=""
								type="com.ibm.hats.transform.components.TextComponent" ecol="65"
								componentSettings="" row="13" BIDIOpposite="false" />
		</td>
          </tr>
        <tr>
          <td colspan="6" align="center" valign="middle">
          <div class="indicator">* Numeric Characters Only</div>
          <table width="auto" border="0" cellspacing="0" cellpadding="0">
          <tr>

            <td style="border:none;">
            <div class="clear4px"></div>
            
            <div id="vista_toolbar" style="padding-right: 120px;">
             <div align="center">
              <ul>
                <li><a style="cursor: pointer;" title="Back" onclick="ms('[pf3]','hatsportletid')"><span>Back</span></a></li>
                <li><a style="cursor: pointer;" title="Enter" onclick="ms('[enter]','hatsportletid')"><span>Enter</span></a></li>
              </ul>             
             </div>

            </div>
            <div class="clear4px"></div>
            </td>
          </tr>
        </table>
          </td>
          </tr>
          
        </table></td>
      <td background="../common/images/popup_right_middle.gif">&nbsp;</td>

    </tr>
    <tr>
      <td background="../common/images/popup_below_left.gif" width="16">&nbsp;</td>
      <td height="16" background="../common/images/popup_below_middle.gif">&nbsp;</td>
      <td background="../common/images/popup_below_right.gif" width="16">&nbsp;</td>
    </tr>
  </table>
      </td>
    </tr>

  </table>
</div>

<div class="clear5px"></div>
</div>

	






	<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<% //out.println("<!--blank.jsp"); %>
</body>
</html>
<% //out.println("-->"); %>