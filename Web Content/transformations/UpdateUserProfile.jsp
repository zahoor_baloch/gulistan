<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<HATS:Form>
<input type="hidden" name="in_162_1">
<input type="hidden" name="in_242_1">
<input type="hidden" name="in_322_1">
<input type="hidden" name="in_402_1">
<input type="hidden" name="in_482_1">
<input type="hidden" name="in_562_1">
<input type="hidden" name="in_642_1" id="in_642_1">
<input type="hidden" name="in_722_1" id="in_722_1">
<input type="hidden" name="in_802_1" id="in_802_1">
<input type="hidden" name="in_882_1" id="in_882_1">
<input type="hidden" name="in_962_1" id="in_962_1">
<input type="hidden" name="in_1042_1" id="in_1042_1">
<input type="hidden" name="in_1122_1" id="in_1122_1">
<input type="hidden" name="in_1202_1" id="in_1202_1">
<input type="hidden" name="in_1282_1" id="in_1282_1">
<input type="hidden" name="in_1362_1"id="in_1362_1">
<input type="hidden" name="in_1442_1" id="in_1442_1">
<input type="hidden" name="in_1522_1" id="in_1522_1">
<input type="hidden" name="in_1602_1" id="in_1602_1">
<input type="hidden" name="in_1682_1" id="in_1682_1">
<input type="hidden" name="in_1762_1" id="in_1762_1">
<input type="hidden" name="in_1842_1">
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>




	
<div class="container">

<div class="heading_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" class="topbar_headingtxt1"></td>
    <td width="34%" align="center" class="topbar_headingtxtredd">Update User Profile </td>
    <td width="33%" align="right" class="topbar_headingtxt3"></td>
  </tr>
</table>
</div>

<div class="clear4px"></div>

<div class="menubgg">
      
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top">
      
      
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
		<tr class="oddRowdd">
		<td width="170" align="center"><div class="linepad_01head">User</div></td>
		<td width="170" align="center" class="leftline2">Password</td>
		<td width="300" align="center" class="leftline2">Contact Name</td>
		<td align="center" class="leftline2">Setup Date</td>
		</tr>
		</thead>
		</table>
	<div style="vertical-align: top; overflow-y:auto; overflow-x:hidden; height:330px; border:solid 1px #CCC;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td height="1" colspan="4" align="left"></td>
		</tr>
		<tr class="evenRownew5" onclick ="setCursorPosition(322, 'HATSForm');checkInput2('in_322_1', '/','hidden');ms('[enter]', 'HATSForm')">
		<td width="171" align="center"><div class="linepad_01"><HATS:Component
						col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="5" /></div></td>
		<td width="172" align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="5" /></div></td>
		<td width="302" align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="5" /></div></td>
		<td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="5" /></div></td>
		</tr>
		<tr class="oddRownew5" onclick ="setCursorPosition(402, 'HATSForm');checkInput2('in_402_1', '/','hidden');ms('[enter]', 'HATSForm')">
		<td align="center"><div class="linepad_01"><HATS:Component
						col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="6" /></div></td>
		<td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="6" /></div></td>
		<td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="6" /></div></td>
		<td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="6" /></div></td>
		</tr>
		<tr class="evenRownew5" onclick ="setCursorPosition(482, 'HATSForm');checkInput2('in_482_1', '/','hidden');ms('[enter]', 'HATSForm')">
		<td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="7" /></div></td>
		<td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="7" /></div></td>
		<td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="7" /></div></td>
		<td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="7" /></div></td>
		</tr>
		<tr class="oddRownew5" onclick ="setCursorPosition(562, 'HATSForm');checkInput2('in_562_1', '/','hidden');ms('[enter]', 'HATSForm')">
		  <td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="8" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="8" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="8" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="8" /></div></td>
		  </tr>
		  <tr class="evenRownew5" onclick ="setCursorPosition(642, 'HATSForm');checkInput2('in_642_1', '/','hidden');ms('[enter]', 'HATSForm')">
		<td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="9" /></div></td>
		<td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="9" /></div></td>
		<td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="9" /></div></td>
		<td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="9" /></div></td>
		</tr>
		<tr class="oddRownew5" onclick ="setCursorPosition(722, 'HATSForm');checkInput2('in_722_1', '/','hidden');ms('[enter]', 'HATSForm')">
		  <td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="10" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="10" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="10" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="10" /></div></td>
		  </tr>
		  <tr class="evenRownew5" onclick ="setCursorPosition(802, 'HATSForm');checkInput2('in_802_1', '/','hidden');ms('[enter]', 'HATSForm')">
		<td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="11" /></div></td>
		<td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="11" /></div></td>
		<td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="11" /></div></td>
		<td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="11" /></div></td>
		</tr>
		<tr class="oddRownew5" onclick ="setCursorPosition(882, 'HATSForm');checkInput2('in_882_1', '/','hidden');ms('[enter]', 'HATSForm')">
		  <td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="12" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="12" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="12" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="12" /></div></td>
		  </tr>
		  <tr class="evenRownew5" onclick ="setCursorPosition(962, 'HATSForm');checkInput2('in_962_1', '/','hidden');ms('[enter]', 'HATSForm')">
		<td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="13" /></div></td>
		<td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="13" /></div></td>
		<td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="13" /></div></td>
		<td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="13" /></div></td>
		</tr>
		<tr class="oddRownew5" onclick ="setCursorPosition(1042, 'HATSForm');checkInput2('in_1042_1', '/','hidden');ms('[enter]', 'HATSForm')">
		  <td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="14" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="14" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="14" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="14" /></div></td>
		  </tr>
		  		  <tr class="evenRownew5" onclick ="setCursorPosition(1122, 'HATSForm');checkInput2('in_1122_1', '/','hidden');ms('[enter]', 'HATSForm')">
		<td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="15" /></div></td>
		<td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="15" /></div></td>
		<td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="15" /></div></td>
		<td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="15" /></div></td>
		</tr>
		<tr class="oddRownew5" onclick ="setCursorPosition(1202, 'HATSForm');checkInput2('in_1202_1', '/','hidden');ms('[enter]', 'HATSForm')">
		  <td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="16" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="16" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="16" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="16" /></div></td>
		  </tr>
		  		  		  <tr class="evenRownew5" onclick ="setCursorPosition(1282, 'HATSForm');checkInput2('in_1282_1', '/','hidden');ms('[enter]', 'HATSForm')">
		<td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="17" /></div></td>
		<td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="17" /></div></td>
		<td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="17" /></div></td>
		<td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="17" /></div></td>
		</tr>
				<tr class="oddRownew5" onclick ="setCursorPosition(1362, 'HATSForm');checkInput2('in_1362_1', '/','hidden');ms('[enter]', 'HATSForm')">
		  <td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="18" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="18" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="18" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="18" /></div></td>
		  </tr>
		  <tr class="evenRownew5" onclick ="setCursorPosition(1442, 'HATSForm');checkInput2('in_1442_1', '/','hidden');ms('[enter]', 'HATSForm')">
		<td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="19" /></div></td>
		<td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="19" /></div></td>
		<td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="52"
						componentSettings="" row="19" /></div></td>
		<td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="19" /></div></td>
		</tr>
		<tr  class="oddRownew5" onclick ="setCursorPosition(1522, 'HATSForm');checkInput2('in_1522_1', '/','hidden');ms('[enter]', 'HATSForm')">
		  <td align="center"><div class="linepad_01"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="13"
						componentSettings="" row="20" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_01"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="25"
						componentSettings="" row="20" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_02"><HATS:Component
						col="27" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="51"
						componentSettings="" row="20" /></div></td>
		  <td align="center" class="leftline"><div class="linepad_03"><HATS:Component
						col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="20" /></div></td>
		  </tr>
		  
		</table>
	</div>
      
	</td>
	<td background="../common/images/right_middle4.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
  </tr>
</table>
</div>

<div class="clear"></div>

<div class="btnmain">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="center">
	<table width="auto" border="0" cellspacing="5" cellpadding="0">
		<tr>
		<td>
		<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid')">Back</a></div>
		</td>

		</tr>
	</table>
		</td>
		</tr>
	</table>
</div>

</div>
 
</HATS:Form>

</body>
</html>