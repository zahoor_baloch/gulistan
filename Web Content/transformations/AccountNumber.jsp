<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java"%>
<%@ taglib uri="hats.tld" prefix="HATS"%>
<HTML>
<HEAD>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink />
</HEAD>
<BODY>
<SCRIPT type="text/javascript" src="../common/env.js">
</SCRIPT>

<SCRIPT type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</SCRIPT>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<SCRIPT type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</SCRIPT>

<SCRIPT type="text/javascript" src="../common/HatsJS.js">
</SCRIPT>


<HATS:Form>
	<SCRIPT language="JavaScript" src="../common/js/textarea.js"
		type="text/javascript"></SCRIPT>


	<INPUT type="hidden" name="in_162_1">
	<INPUT type="hidden" name="in_242_1">
	<INPUT type="hidden" name="in_322_1">
	<INPUT type="hidden" name="in_402_1">
	<INPUT type="hidden" name="in_482_1">
	<INPUT type="hidden" name="in_562_1">
	<INPUT type="hidden" name="in_642_1">
	<INPUT type="hidden" name="in_722_1">
	<INPUT type="hidden" name="in_802_1">
	<INPUT type="hidden" name="in_882_1">
	<INPUT type="hidden" name="in_962_1">
	<INPUT type="hidden" name="in_1042_1">
	<INPUT type="hidden" name="in_1122_1">
	<INPUT type="hidden" name="in_1202_1">
	<INPUT type="hidden" name="in_1282_1">
	<INPUT type="hidden" name="in_1362_1">
	<INPUT type="hidden" name="in_1442_1">
	<INPUT type="hidden" name="in_1522_1">
	<INPUT type="hidden" name="in_1602_1">
	<INPUT type="hidden" name="in_1682_1">
	<INPUT type="hidden" name="in_1762_1">
	<INPUT type="hidden" name="in_1842_1">
	<INPUT type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV" />
	<INPUT type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV" />
	<INPUT type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV" />
	<INPUT type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked" />
	<INPUT type="hidden" name="hatsgv_logoffClicked"
		id="hatsgv_logoffClicked" />

<input type="hidden" name="hatsgv_menuClicked" id="hatsgv_menuClicked"/>

	<DIV class="container">
	<DIV class="clear5px"></DIV>

	
	<DIV class="detailstxtnew">Please fill in the form below to get registered.</DIV>
	

	<DIV class="clear5px"></DIV>

	<DIV class="menubgg">
	<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD width="5" height="5" background="../common/images/left_top4.gif"></TD>
			<TD background="../common/images/center_middle_top4.gif"></TD>
			<TD width="5" height="5" background="../common/images/right_top4.gif"></TD>
		</TR>
		<TR>
			<TD background="../common/images/left_middle4.gif"></TD>
			<TD align="center" valign="top" bgcolor="#d1c4a2">
			<TABLE width="100%" border="0" cellspacing="0" cellpadding="2"
				class="txt2">
				<TR>
					<TD align="center" valign="top" style="padding: 3px 10px 3px 3px;">
					<TABLE width="auto" border="0" cellpadding="3" cellspacing="0"
						class="fillbor1">

						<TR>
							<TD class="txt1order2"><STRONG>Your Gulistan
							Account Number :</STRONG></TD>
							<TD width="250" align="left"><STRONG><HATS:Component
								col="31" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="36"
								componentSettings="" row="5" /></STRONG></TD>
						</TR>
						<TR>
							<TD class="txt1order2"><STRONG>Your Zip Code :</STRONG></TD>
							<TD align="left"><STRONG><HATS:Component col="31"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="6" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="36"
								componentSettings="" row="6" /></STRONG></TD>
						</TR>
					</TABLE>
					</TD>
				</TR>
			</TABLE>
			</TD>
			<TD background="../common/images/right_middle4.gif"></TD>
		</TR>
		<TR>
			<TD height="5" background="../common/images/left_bottom4.gif"></TD>
			<TD background="../common/images/center_middle_bottom4.gif"></TD>
			<TD height="5" background="../common/images/right_bottom4.gif"></TD>
		</TR>
	</TABLE>




	<TABLE width="100%">
		<TR>
			<TD height="1">
			<DIV class="line"></DIV>
			</TD>
		</TR>
	</TABLE>


	<DIV class="txt1orderreq">A confirmation email will be sent to 	the provided email address.</DIV>

	
	<TABLE width="100%">
		<TR>
			<TD height="1">
			<DIV class="line"></DIV>
			</TD>
		</TR>
	</TABLE>
	

	<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD width="5" height="5" background="../common/images/left_top4.gif"></TD>
			<TD background="../common/images/center_middle_top4.gif"></TD>
			<TD width="5" height="5" background="../common/images/right_top4.gif"></TD>
		</TR>
		<TR>
			<TD background="../common/images/left_middle4.gif"></TD>
			<TD align="center" valign="top" bgcolor="#d1c4a2">
			<DIV
				style="vertical-align: top; overflow-y: auto; overflow-x: hidden; height: auto;">
			<TABLE width="100%" border="0" cellpadding="3" cellspacing="0"
				class="fillbor1">
				<TR>
					<TD width="30%" class="txt1order2">Contact Name :</TD>
					<TD align="left" bgcolor="#d1c4a2"><SPAN class="srick"><HATS:Component
						col="20" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="10" /></SPAN> <HATS:Component col="22"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:plorderfieldemail|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="61"
						componentSettings="" row="10" /></TD>
				</TR>
				<TR>
					<TD class="txt1order2">Email Address :</TD>
					<TD align="left" bgcolor="#d1c4a2"><SPAN class="srick"><HATS:Component
						col="20" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="11" /></SPAN> <HATS:Component col="22"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:plorderfieldemail|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="71"
						componentSettings="" row="11" /></TD>
				</TR>
				<TR>
					<TD class="txt1order2">Phone Number :</TD>
					<TD align="left" valign="middle" bgcolor="#d1c4a2"><SPAN
						class="srick"><HATS:Component col="20" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="12" /></SPAN> <HATS:Component col="22"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:plorderfieldphone|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:3|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:true|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="25"
						componentSettings="" row="12" /> <HATS:Component col="28"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:plorderfieldphone2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:7|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:true|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="35"
						componentSettings="" row="12" /></TD>
				</TR>
				<TR>
					<TD class="txt1order2">Your Web Site :</TD>
					<TD align="left" bgcolor="#d1c4a2"><SPAN class="srick"></SPAN>
					<HATS:Component col="22" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:plorderfieldemail|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="71"
						componentSettings="" row="13" /></TD>
				</TR>
			</TABLE>
			</DIV>
			</TD>
			<TD background="../common/images/right_middle4.gif"></TD>
		</TR>
		<TR>
			<TD height="5" background="../common/images/left_bottom4.gif"></TD>
			<TD background="../common/images/center_middle_bottom4.gif"></TD>
			<TD height="5" background="../common/images/right_bottom4.gif"></TD>
		</TR>
	</TABLE>

	
	<TABLE width="100%">
		<TR>
			<TD height="1">
			<DIV class="line"></DIV>
			</TD>
		</TR>
	</TABLE>
	
	<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD width="5" height="5" background="../common/images/left_top4.gif"></TD>
			<TD background="../common/images/center_middle_top4.gif"></TD>
			<TD width="5" height="5" background="../common/images/right_top4.gif"></TD>
		</TR>
		<TR>
			<TD background="../common/images/left_middle4.gif"></TD>
			<TD align="center" valign="top" bgcolor="#d1c4a2">
			<DIV
				style="vertical-align: top; overflow-y: auto; overflow-x: hidden; height: auto;">
			<TABLE width="100%" border="0" cellpadding="3" cellspacing="0"
				class="fillbor1">
				<TR>
					<TD width="30%" class="txt1order2">User ID :</TD>
					<TD align="left" bgcolor="#d1c4a2" width="205"><SPAN
						class="srick"><HATS:Component col="20" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="15" /></SPAN> <HATS:Component col="22"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:plorderfieldphone2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="31"
						componentSettings="" row="15" /></TD>
					<TD width="459"><HATS:Component col="33" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:color: red; font-weight: bold|labelStyleClass:HATSCAPTION|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="80"
						componentSettings="" row="15" /></TD>

				</TR>
				<TR>
					<TD class="txt1order2">Password :</TD>
					<TD align="left" bgcolor="#d1c4a2" width="205"><SPAN
						class="srick"><HATS:Component col="20" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="16" /></SPAN> <HATS:Component col="22"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:plorderfieldphone2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="31"
						componentSettings="" row="16" /></TD>
					<TD width="459"><HATS:Component col="33" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:color: red; font-weight: bold|labelStyleClass:HATSCAPTION|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="80"
						componentSettings="" row="16" /></TD>
				</TR>
				<TR>
					<TD class="txt1order2">Confirm Password :</TD>
					<TD align="left" bgcolor="#d1c4a2" width="205"><SPAN
						class="srick"><HATS:Component col="20" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="20"
						componentSettings="" row="17" /></SPAN> <HATS:Component col="22"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:plorderfieldphone2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="31"
						componentSettings="" row="17" /></TD>
				</TR>
			</TABLE>
			</DIV>
			</TD>
			<TD background="../common/images/right_middle4.gif"></TD>
		</TR>
		<TR>
			<TD height="5" background="../common/images/left_bottom4.gif"></TD>
			<TD background="../common/images/center_middle_bottom4.gif"></TD>
			<TD height="5" background="../common/images/right_bottom4.gif"></TD>
		</TR>
	</TABLE>

	
	<TABLE width="100%">
		<TR>
			<TD height="1">
			<DIV class="line"></DIV>
			</TD>
		</TR>
	</TABLE>
	

	<DIV class="txt1orderreq"><SPAN class="txt1orderreq2"><HATS:Component
		col="8" alternate=""
		widget="com.ibm.hats.transform.widgets.LabelWidget"
		alternateRenderingSet="" erow="20" textReplacement=""
		widgetSettings="trim:true|style:txt1orderreq2|labelStyleClass:HATSCAPTION|"
		type="com.ibm.hats.transform.components.TextComponent" ecol="50"
		componentSettings="" row="20" /></SPAN></DIV>

	
	<TABLE width="100%">
		<TR>
			<TD height="1">
			<DIV class="line"></DIV>
			</TD>
		</TR>
	</TABLE>
</DIV>

	<DIV class="clear"></DIV>

	<DIV class="btnmain">
	<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD align="center">
			<TABLE width="auto" border="0" cellspacing="5" cellpadding="0">
				<TR>
					<TD>
					<DIV class="allbtn"><A
						href="javascript:ms('[pf3]','hatsportletid');" title="Back">Back</A></DIV>
					</TD>
					<TD>
					<DIV class="allbtn"><A
						href="javascript:ms('[enter]','hatsportletid');" title="Enter">

					Enter</A></DIV>
					</TD>
				</TR>
			</TABLE>
			</TD>
		</TR>
	</TABLE>
	</DIV>
	</DIV>



	<SCRIPT language="JavaScript" type="text/javascript">
	var limitNum=40;
	function limittext(){
	var fld=document.getElementsByName("in_742_40");
	
	if(fld.value.length > limitNum){
	fld.value=fld.value.substring(0, limitNum);
	
	}else {
limitCount.value = limitNum - limitField.value.length;
}
}
	
	
	
	
	</SCRIPT>


</HATS:Form>

</BODY>
</HTML>