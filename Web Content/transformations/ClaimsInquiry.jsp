<%// out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<% //out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
<%
try{

String user="";
String pwd="";
if(request.getParameterNames().toString().length()!=0){
	
	user=request.getParameter("in_100_10");
	pwd=request.getParameter("in_180_10");
}

if(session.getAttribute("gul_id")==null){
	session.setAttribute("gul_id",user);
	session.setAttribute("gul_password",pwd);
}


}

catch(Exception ex){

}


 %>
<%session.setAttribute("inventoryInquiry_Details_sv1",null); %>
<%session.setAttribute("orderInquiry_Details_nextPage_sv",null); %>

	<% String isNextPage= ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("isNextPage", true).getString(0); %>
	
	<%
		
	 %>
	<!-- Insert your HATS component tags here. -->

<input type="hidden" name="in_162_1">
<input type="hidden" name="in_242_1">
<input type="hidden" name="in_322_1">
<input type="hidden" name="in_402_1">
<input type="hidden" name="in_482_1">
<input type="hidden" name="in_562_1">
<input type="hidden" name="in_642_1">
<input type="hidden" name="in_722_1">
<input type="hidden" name="in_802_1">
<input type="hidden" name="in_882_1">
<input type="hidden" name="in_962_1">
<input type="hidden" name="in_1042_1">
<input type="hidden" name="in_1122_1">
<input type="hidden" name="in_1202_1">
<input type="hidden" name="in_1282_1">
<input type="hidden" name="in_1362_1">
<input type="hidden" name="in_1442_1">
<input type="hidden" name="in_1522_1">
<input type="hidden" name="in_1602_1">
<input type="hidden" name="in_1682_1">
<input type="hidden" name="in_1762_1">
<input type="hidden" name="in_1842_1">
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>

	
	

	<!--ddd-->
<div class="container">
<div class="heading_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" class="topbar_headingtxt1">&nbsp;</td>
    <td width="34%" align="center" class="topbar_headingtxtredd">Claims Inquiry </td>
    <td width="33%" align="right" class="topbar_headingtxt3">&nbsp;</td>
  </tr>
</table>
</div>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>

<div class="menubgg">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top">
      
      
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
     <thead>
     <tr class="oddRowdd">
     <td width="70" align="center">Claim No.</td>
     <td width="100" align="center" class="leftline2">Seq No.</td>
     <td width="100" align="center" class="leftline2">Date</td>
     <td width="100" align="center" class="leftline2">Reference</td>
     <td width="90" align="center" class="leftline2">Inv No.</td>
     <td width="110" align="center" class="leftline2">Type</td>
     <td width="110" align="center" class="leftline2">Reason</td>
     <td width="100" align="center" class="leftline2">Amount</td>
     <td align="center" class="leftline2">Status</td>
      </tr>
      </thead>
	</table>
	<div style="vertical-align: top; overflow-y:auto; overflow-x:hidden; height:362px; border:solid 1px #CCC;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td height="1" colspan="9" align="left"><HATS:Component type="com.ibm.hats.transform.components.TableComponent" widget="gulistan_JPS02.widgets.ClaimsInquiry" row="3" col="2" erow="22" ecol="80" componentSettings="startCol:2|endRow:22|columnBreaks:3,11,17,26,36,44,53,62,71|endCol:80|excludeCols:|visualTableDefaultValues:false|startRow:3|numberOfTitleRows:0|excludeRows:|includeEmptyRows:true|" componentIdentifier="ClaimsInquiry" applyTextReplacement="true"/>
		</td>
		</tr>

        </table>
	</div>
      
      </td>
    <td background="../common/images/right_middle4.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
  </tr>
</table>
<div class="clear"></div>
</div>
<!--Mene End-->
<%
String serverName=request.getServerName();
System.out.println("This is the Server Name...."+ serverName);
 %>
 
 <script language="JavaScript">

 function excelGen(){

var detailsTable=document.getElementById("detailsTable");

 var content1 = "<TABLE>" ; 
	
	
	 content1 +="<TR>";
	
	 
	 
	 content1 += "<TD style='font-weight: bold;'  bgcolor='yellow'> Style Code </TD>";
	 content1 += "<TD style='font-weight: bold;'  > Style Description </TD>";
	 content1 += "<TD style='font-weight: bold;'  > Warehouse </TD>";
	 content1 += "<TD style='font-weight: bold;'  > Effect Date </TD>"; 
	 
	 content1 += "<TD style='font-weight: bold;'  > Ending Date </TD>";
	 content1 += "<TD style='font-weight: bold;'  > Min Qty </TD>";
	 content1 += "<TD style='font-weight: bold;'  > Roll Price </TD>";
	content1 += "<TD style='font-weight: bold;'  > Cut Price  </TD>";
	content1 += "<TD style='font-weight: bold;' > UM	</TD>";
	
	 
	 
	content1 += "</TR>";
	
	
	for ( var y=0;y<detailsTable.rows.length;y++) 
	{ content1 +="<TR>";
	for ( var x=0; x<detailsTable.rows(y).cells.length; x++) 
	{ 
	if (y==0)
	{ content1 += "<TD style='font-weight: bold;'  " + detailsTable.rows(y).cells(x).innerText + "</TD>"; 
	} 
	else 
	{ content1 += "<TD>" + detailsTable.rows(y).cells(x).innerText + "</TD>" ; 
	} 
	} 
	content1 += "</TR>"; 
	} 
	content1 += "</TABLE>" ;

    var serverName = "<%= serverName%>";  
	
	var submit_form_content = "<form method='post' action='http://"+serverName+":9080/Gulistan_JPS02/Spreadsheet.jsp' name='RC_Form' id='RC_Form'>";
	 
	
	var content = "<HTML>" + "<BODY onLoad='document.RC_Form.submit();'>" + submit_form_content + "<DIV style='display: none;'><Textarea rows='82' cols='9' id='SpreadsheetTextArea' name='SpreadsheetTextArea'>" + content1 +"</Textarea></DIV>" + "</form>" ;
	 
	var pwin = window.open( "" , "excel_generator" , "width=100;height=100"); 
	pwin.document.open(); 
	pwin.document.write(content);

 
	pwin.document.write( "</BODY></HTML>" ); 
	
	
    pwin.document.close();

    setTimeout( function () {pwin.close();}, 100);
 
 }
 
 function pdfGen(){
//var headingsTable= document.getElementById("headingsTable");
var detailsTable=document.getElementById("detailsTable");
//var heading1=document.getElementById("heading1");
//var heading2=document.getElementById("heading2");
//var heading3=document.getElementById("heading3");
//var heading4=document.getElementById("heading4");
//var heading5=document.getElementById("heading5");
//var heading6=document.getElementById("heading6");
//var heading7=document.getElementById("heading7"); 
 var content1 = "<TABLE>" ; 
	
	
	 content1 +="<TR>";
	
	 content1 += "<TD style='font-weight: bold;'  bgcolor='blue'> Style Code </TD>";
	 content1 += "<TD style='font-weight: bold;'  > Style Description </TD>";
	 content1 += "<TD style='font-weight: bold;'  > Warehouse </TD>";
	 content1 += "<TD style='font-weight: bold;'  > Effect Date </TD>"; 
	 
	 content1 += "<TD style='font-weight: bold;'  > Ending Date </TD>";
	 content1 += "<TD style='font-weight: bold;'  > Min Qty </TD>";
	 content1 += "<TD style='font-weight: bold;'  > Roll Price </TD>";
	content1 += "<TD style='font-weight: bold;'  > Cut Price  </TD>";
	content1 += "<TD style='font-weight: bold;' > UM	</TD>";
	
	 
	 
	content1 += "</TR>";
	
	
	for ( var y=0;y<detailsTable.rows.length;y++) 
	{ content1 +="<TR>";
	for ( var x=0; x<detailsTable.rows(y).cells.length; x++) 
	{ 
	if (y==0)
	{ content1 += "<TD style='font-weight: bold;' " + detailsTable.rows(y).cells(x).innerText + "</TD>"; 
	} 
	else 
	{ content1 += "<TD>" + detailsTable.rows(y).cells(x).innerText + "</TD>" ; 
	} 
	} 
	content1 += "</TR>"; 
	} 
	content1 += "</TABLE>" ;

    var serverName = "<%= serverName%>";  
	
	var submit_form_content = "<form method='post' action='http://"+serverName+":9080/Gulistan_JPS02/pdf1.jsp' name='RC_Form' id='RC_Form'>";
	 
	
	var content = "<HTML>" + "<BODY onLoad='document.RC_Form.submit();'>" + submit_form_content + "<DIV style='display: none;'><Textarea rows='82' cols='9' id='PdfTextArea' name='PdfTextArea'>" + content1 +"</Textarea></DIV>" + "</form>" ;
	 
	var pwin = window.open( "" , "pdf_generator" , "width=100;height=100"); 
	pwin.document.open(); 
	pwin.document.write(content);

 
	pwin.document.write( "</BODY></HTML>" ); 
	
	
    pwin.document.close();

    setTimeout( function () {pwin.close();}, 100);
 
 }
 
 
 
 </script>
<div class="clear"></div>

<div class="btnmain">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center"">
<table width="auto" border="0" cellspacing="5" cellpadding="0">
		<tr>
		<td>
		<div class="allbtn"><a title="Previous" href="javascript:ms('[pageup]','hatsportletid');">Previous</a></div>
		</td>
		<td>
		<div class="allbtn"><a title="Exit" href="javascript:ms('[pf3]','hatsportletid');">Exit</a></div>
		</td>
		<td>
		<div class="allbtn"><a href="javascript:ms('[enter]','HATSForm');" title="Enter">Enter</a></div>
		</td>
		<%
			if(isNextPage.equals("+"))
			{
			
		 %>
		<td>
		<div class="allbtn"><a title="Next" href="javascript:ms('[pagedn]','hatsportletid');">Next</a></div>
		</td>
		<%
		}
		else
		{
		
		%>
		<td>
		<div class="disabledButton"></div>
		</td>
		<%} %>
		</tr>
		
	</table>

	</td>
	</tr>
</table>
</div>

</div>
<!--ddd-->
	
	

	 
	

	


	<script language="JavaScript">
	
	
	//alert(navigator.appName);
	//alert(navigator.appCodeName);
	//alert(navigator.appVersion);
	//alert(navigator.userAgent);
	//document.getElementById("homebtn").style.display="none";
	
	</script>
	<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<% //out.println("<!--blank.jsp"); %>
</body>
</html>
<%// out.println("-->"); %>