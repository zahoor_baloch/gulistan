<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<!-- Start of the HATS form. -->
<HATS:Form>

<!-- Insert your HATS component tags here. -->
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV" />
	<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV" />
	<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV" />
	<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked" />
	<input type="hidden" name="hatsgv_logoffClicked"
		id="hatsgv_logoffClicked" />


<div class="container">

<div class="heading_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" class="topbar_headingtxt1">&nbsp;</td>
    <td width="34%" align="center" class="topbar_headingtxtredd"><HATS:Component
				col="30" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="1" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="51"
				componentSettings="" row="1" /></td>
    <td width="33%" align="right" class="topbar_headingtxt3">&nbsp;</td>
  </tr>
</table>
</div>



<!--Menu Start-->
<div class="menubgg">

<table width="100%">
		<tr>
			<td height="1">
			<div class="line"></div>
			</td>
		</tr>
	</table>
      
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
	</tr>
	<tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="newtable" style="border-collapse:collapse;">
    <tr><!--
    <th width="100" valign="bottom">&nbsp;</th>
    --><th valign="bottom">Wh<br>Cd</th>
    <th valign="bottom">Cust#<br>Nbr.</th>
    <th valign="bottom">Customer Name</th>
    <th valign="bottom">&nbsp;</th>
    </tr>
    <tr class="evenRowtable" onclick ="setCursorPosition(722, 'HATSForm');checkInput2('in_722_1', '1','hidden');ms('[enter]', 'HATSForm')">
   <td align="center"><input type="hidden" name="in_722_1" id="in_722_1">&nbsp;<HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="10" /></td>
    <td align="center"><HATS:Component col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="10" /></td>
    <td align="center"><HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="10" /></td>
    <td align="center"><HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="75"
						componentSettings="" row="10" BIDIOpposite="false" /></td>
    </tr>
    <tr class="oddRowtable" onclick ="setCursorPosition(802, 'HATSForm');checkInput2('in_802_1', '1','hidden');ms('[enter]', 'HATSForm')">
    <td align="center"><input type="hidden" name="in_802_1" id="in_802_1">&nbsp;<HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="11" /></td>
    <td align="center"><HATS:Component col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="11" /></td>
    <td align="center"><HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="11" /></td>
    <td align="center"><HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="75"
						componentSettings="" row="11" BIDIOpposite="false" /></td>
	</tr>
   <tr class="evenRowtable" onclick ="setCursorPosition(882, 'HATSForm');checkInput2('in_882_1', '1','hidden');ms('[enter]', 'HATSForm')">
    <td align="center"><input type="hidden" name="in_882_1" id="in_882_1">&nbsp;<HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="12" /></td>
    <td align="center"><HATS:Component col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="12" /></td>
    <td align="center"><HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="12" /></td>
    <td align="center"><HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="75"
						componentSettings="" row="12" BIDIOpposite="false" /></td>
    </tr>
    <tr class="oddRowtable" onclick ="setCursorPosition(962, 'HATSForm');checkInput2('in_962_1', '1','hidden');ms('[enter]', 'HATSForm')">
    <td align="center"><input type="hidden" name="in_962_1" id="in_962_1">&nbsp;<HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="13" /></td>
    <td align="center"><HATS:Component col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="13" /></td>
    <td align="center"><HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="13" /></td>
    <td align="center"><HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="75"
						componentSettings="" row="13" BIDIOpposite="false" /></td>
	</tr>
    <tr class="evenRowtable" onclick ="setCursorPosition(1042, 'HATSForm');checkInput2('in_1042_1', '1','hidden');ms('[enter]', 'HATSForm')">
    <td align="center"><input type="hidden" name="in_1042_1" id="in_1042_1">&nbsp;<HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="14" /></td>
    <td align="center"><HATS:Component col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="14" /></td>
    <td align="center"><HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="14" /></td>
    <td align="center"><HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="75"
						componentSettings="" row="14" BIDIOpposite="false" /></td>
    </tr>
    <tr class="oddRowtable" onclick ="setCursorPosition(1122, 'HATSForm');checkInput2('in_1122_1', '1','hidden');ms('[enter]', 'HATSForm')">
    <td align="center"><input type="hidden" name="in_1122_1" id="in_1122_1">&nbsp;<HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="15" /></td>
    <td align="center"><HATS:Component col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="15" /></td>
    <td align="center"><HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="15" /></td>
    <td align="center"><HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="75"
						componentSettings="" row="15" BIDIOpposite="false" /></td>
	</tr>
    <tr class="evenRowtable" onclick ="setCursorPosition(1202, 'HATSForm');checkInput2('in_1202_1', '1','hidden');ms('[enter]', 'HATSForm')">
   <td align="center"><input type="hidden" name="in_1202_1" id="in_1202_1">&nbsp;<HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="16" /></td>
    <td align="center"><HATS:Component col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="16" /></td>
    <td align="center"><HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="16" /></td>
    <td align="center"><HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="75"
						componentSettings="" row="16" BIDIOpposite="false" /></td>
    </tr>
    <tr class="oddRowtable" onclick ="setCursorPosition(1282, 'HATSForm');checkInput2('in_1282_1', '1','hidden');ms('[enter]', 'HATSForm')">
    <td align="center"><input type="hidden" name="in_1282_1" id="in_1282_1">&nbsp;<HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="17" /></td>
    <td align="center"><HATS:Component col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="17" /></td>
    <td align="center"><HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="17" /></td>
    <td align="center"><HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="75"
						componentSettings="" row="17" BIDIOpposite="false" /></td>
	</tr>
	<tr class="evenRowtable" onclick ="setCursorPosition(1362, 'HATSForm');checkInput2('in_1362_1', '1','hidden');ms('[enter]', 'HATSForm')">
    <td align="center"><input type="hidden" name="in_1362_1" id="in_1362_1">&nbsp;<HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="18" /></td>
    <td align="center"><HATS:Component col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="18" /></td>
    <td align="center"><HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="18" /></td>
    <td align="center"><HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="75"
						componentSettings="" row="18" BIDIOpposite="false" /></td>
    </tr>
    <tr class="oddRowtable" onclick ="setCursorPosition(1442, 'HATSForm');checkInput2('in_1442_1', '1','hidden');ms('[enter]', 'HATSForm')">
    <td align="center"><input type="hidden" name="in_1442_1" id="in_1442_1">&nbsp;<HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="19" /></td>
    <td align="center"><HATS:Component col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="19" /></td>
    <td align="center"><HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="19" /></td>
    <td align="center"><HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="75"
						componentSettings="" row="19" BIDIOpposite="false" /></td>
	</tr>
	<tr class="evenRowtable" onclick ="setCursorPosition(1552, 'HATSForm');checkInput2('in_1552_1', '1','hidden');ms('[enter]', 'HATSForm')">
    <td align="center"><input type="hidden" name="in_1552_1" id="in_1552_1">&nbsp;<HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="20" /></td>
    <td align="center"><HATS:Component col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="20" /></td>
    <td align="center"><HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="20" /></td>
    <td align="center"><HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="75"
						componentSettings="" row="20" BIDIOpposite="false" /></td>
    </tr>
    <tr class="oddRowtable" onclick ="setCursorPosition(1602, 'HATSForm');checkInput2('in_1602_1', '1','hidden');ms('[enter]', 'HATSForm')">
    <td align="center"><input type="hidden" name="in_1602_1" id="in_1602_1">&nbsp;<HATS:Component col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="5"
						componentSettings="" row="21" /></td>
    <td align="center"><HATS:Component col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="21" /></td>
    <td align="center"><HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="21" /></td>
    <td align="center"><HATS:Component col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="75"
						componentSettings="" row="21" BIDIOpposite="false" /></td>
	</tr>
</table>    
    </td>
    <td background="../common/images/right_middle4.gif"></td>
	</tr>
	<tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
	</tr>
</table>

<!--Mene End-->

<!-- Error Msg Start -->
<div class="clear5px"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody><tr>
			<td width="5" height="5" background="../common/images/left_top2.gif"></td>
			<td background="../common/images/center_middle_top2.gif"></td>
			<td width="5" height="5" background="../common/images/right_top2.gif"></td>
		</tr>
		<tr>
			<td background="../common/images/left_middle2.gif"></td>			
			<td align="center" valign="middle" class="topbar_headingtxtrad">&nbsp;
				<!--dynamicCapable-->

<span class="HATSCAPTION"></span>&nbsp;
			</td>				
			<td background="../common/images/right_middle2.gif"></td>
		</tr>
		<tr>
			<td height="5" background="../common/images/left_bottom2.gif"></td>
			<td background="../common/images/center_middle_bottom2.gif"></td>
			<td height="5" background="../common/images/right_bottom2.gif"></td>
		</tr>

	</tbody></table>

<!-- Error Msg end -->

</div>
<div class="clear"></div>

<div class="btnmain">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="center">
	<table width="auto" border="0" cellspacing="5" cellpadding="0">
		<tr>
		<td>
		<div class="allbtn"><a title="Exit"
						href="javascript:ms('[pf3]','hatsportletid');">Exit</a></div>
		</td>
		</tr>
	</table>
		</td>
		</tr>
	</table>
</div>

</div>



</HATS:Form>
<!-- End of the HATS form. -->
</body>
</html>