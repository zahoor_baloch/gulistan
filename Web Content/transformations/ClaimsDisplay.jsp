<% out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<% out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
	<input type="hidden" name="in_162_1">
<input type="hidden" name="in_242_1">
<input type="hidden" name="in_322_1">
<input type="hidden" name="in_402_1">
<input type="hidden" name="in_482_1">
<input type="hidden" name="in_562_1">
<input type="hidden" name="in_642_1">
<input type="hidden" name="in_722_1">
<input type="hidden" name="in_802_1">
<input type="hidden" name="in_882_1">
<input type="hidden" name="in_962_1">
<input type="hidden" name="in_1042_1">
<input type="hidden" name="in_1122_1">
<input type="hidden" name="in_1202_1">
<input type="hidden" name="in_1282_1">
<input type="hidden" name="in_1362_1">
<input type="hidden" name="in_1442_1">
<input type="hidden" name="in_1522_1">
<input type="hidden" name="in_1602_1">
<input type="hidden" name="in_1682_1">
<input type="hidden" name="in_1762_1">
<input type="hidden" name="in_1842_1">
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<!-- Insert your HATS component tags here. -->

<div class="container">

<div class="heading_bg">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td width="33%" class="topbar_headingtxt1">&nbsp;</td>
		<td width="34%" align="center" class="topbar_headingtxtredd">Claims Display</td>
		<td width="33%" align="right" class="topbar_headingtxt3">&nbsp;</td>
		</tr>
	</table>
</div>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->

<div class="menubgg">

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td width="5" height="5" background="../common/images/left_top4.gif"></td>
		<td background="../common/images/center_middle_top4.gif"></td>
		<td width="5" height="5" background="../common/images/right_top4.gif"></td>
		</tr>
		<tr>
		<td background="../common/images/left_middle4.gif"></td>
		<td align="center" valign="top" bgcolor="#d1c4a2">
	<div style="vertical-align: top; overflow-y:auto; overflow-x:hidden; height:auto;">
	<table width="100%" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
		<tr>
        <td width="20%" class="txt1order2">Customer# :</td>
        <td colspan="3" align="left" bgcolor="#d1c4a2"><HATS:Component
						col="13" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="53"
						componentSettings="" row="2" /></td>
       
        </tr>
		<tr>
        <td class="txt1order2">Invoice# :</td>
        <td colspan="3" align="left" bgcolor="#d1c4a2"><HATS:Component
						col="15" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="29"
						componentSettings="" row="3" /></td>
        </tr>
		<tr>
        <td class="txt1order2">Mill Claim# :</td>
        <td width="25%" align="left" bgcolor="#d1c4a2"><HATS:Component
						col="15" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="24"
						componentSettings="" row="4" /></td>
        <td width="6%" class="txt1order2">Seq# :</td>
        <td align="left" bgcolor="#d1c4a2"><HATS:Component col="32"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="44"
						componentSettings="" row="4" /></td>
        </tr>
		<tr>
        <td class="txt1order2">Cust Claim# :</td>
        <td colspan="3" align="left" bgcolor="#d1c4a2"><HATS:Component
						col="15" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="33"
						componentSettings="" row="5" /></td>
        </tr>
		<tr>
        <td class="txt1order2">Status :</td>
        <td colspan="3" align="left" bgcolor="#d1c4a2"><HATS:Component
						col="10" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="42"
						componentSettings="" row="6" /></td>
        </tr>
		<tr>
        <td class="txt1order2">Type :</td>
        <td colspan="3" align="left" bgcolor="#d1c4a2"><HATS:Component
						col="10" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="7" /></td>
        </tr>
		<tr>
        <td class="txt1order2">Reason :</td>
        <td colspan="3" align="left" bgcolor="#d1c4a2"><HATS:Component
						col="10" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="8" /></td>
        </tr>
	</table>
	</div>
		</td>
		<td background="../common/images/right_middle4.gif"></td>
		</tr>
		<tr>
		<td height="5" background="../common/images/left_bottom4.gif"></td>
		<td background="../common/images/center_middle_bottom4.gif"></td>
		<td height="5" background="../common/images/right_bottom4.gif"></td>
	</tr>
	</table>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td width="5" height="5" background="../common/images/left_top4.gif"></td>
		<td background="../common/images/center_middle_top4.gif"></td>
		<td width="5" height="5" background="../common/images/right_top4.gif"></td>
		</tr>
		<tr>
		<td background="../common/images/left_middle4.gif"></td>
		<td align="center" valign="top">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="oddRowdd">
		<td width="180" align="center"> Roll</td>
		<td width="180" align="center" class="leftline2">Unit Price</td>
		<td width="180" align="center" class="leftline2">Total Amt</td>
		<td width="180" align="center" class="leftline2">Style</td>
		<td align="center" class="leftline2">Color</td>
		</tr>
        <tr><td colspan="5" height="1"></td></tr>
		<tr class="evenRownew6">
		<td align="center">&nbsp;<HATS:Component col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="10" /></td>
		<td align="center" class="leftline"><HATS:Component col="45"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="54"
						componentSettings="" row="10" />&nbsp;</td>
		<td align="center" class="leftline"><HATS:Component col="68"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="76"
						componentSettings="" row="10" />&nbsp;</td>
		<td align="center" class="leftline"><HATS:Component col="9"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="34"
						componentSettings="" row="11" />&nbsp;</td>
		<td align="center" class="leftline">&nbsp;<HATS:Component col="43"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="76"
						componentSettings="" row="11" /></td>
		</tr>
	</table>
		</td>
		<td background="../common/images/right_middle4.gif"></td>
		</tr>
		<tr>
		<td height="5" background="../common/images/left_bottom4.gif"></td>
		<td background="../common/images/center_middle_bottom4.gif"></td>
		<td height="5" background="../common/images/right_bottom4.gif"></td>
		</tr>
	</table>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td width="5" height="5" background="../common/images/left_top4.gif"></td>
		<td background="../common/images/center_middle_top4.gif"></td>
		<td width="5" height="5" background="../common/images/right_top4.gif"></td>
		</tr>
		<tr>
		<td background="../common/images/left_middle4.gif"></td>
		<td align="center" valign="top" bgcolor="#d1c4a2">
	<div style="vertical-align: top; overflow-y:auto; overflow-x:hidden; height:auto;">
	<table width="100%" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
		<tr>
		<td width="20%" class="txt1order2">Quantity :</td>
		<td width="31" colspan="3" align="left" bgcolor="#d1c4a2">&nbsp;<HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="25"
						componentSettings="" row="13" /></td>
		</tr>
      <tr>
        <td class="txt1order2">Flat Amt :</td>
        <td colspan="3" align="left" bgcolor="#d1c4a2">&nbsp;<HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="24"
						componentSettings="" row="14" /></td>
      </tr>
      </table>
      </div>
      </td>
    <td background="../common/images/right_middle4.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
  </tr>
</table>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
    <div style="vertical-align: top; overflow-y:auto; overflow-x:hidden; height:auto;">
    <table width="100%" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
      
      <tr>
        <td width="20%" class="txt1order2">R/A Number :</td>
        <td width="31" align="left" bgcolor="#d1c4a2"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="25"
						componentSettings="" row="18" /></td>
        <td width="31" align="left" bgcolor="#d1c4a2"><HATS:Component
						col="26" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="34"
						componentSettings="" row="18" /></td>
        <td colspan="5" align="left" bgcolor="#d1c4a2">&nbsp;</td>
        </tr>
      <tr>
        <td class="txt1order2">Returned Ft :</td>
        <td align="left" bgcolor="#d1c4a2"><HATS:Component col="16"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="21"
						componentSettings="" row="19" /></td>
        <td align="left" class="txt1order2">Width :</td>
        <td width="31" align="left" bgcolor="#d1c4a2"><HATS:Component
						col="30" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="35"
						componentSettings="" row="19" /></td>
        <td width="31" align="left" class="txt1order2">SY :</td>
        <td width="31" align="left" bgcolor="#d1c4a2"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="54"
						componentSettings="" row="19" /></td>
        <td width="31" align="left" class="txt1order2">Waste SY :</td>
        <td width="31" align="left" bgcolor="#d1c4a2">&nbsp;<HATS:Component
						col="67" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="19" /></td>
      </tr>
      </table>
      </div>
      </td>
    <td background="../common/images/right_middle4.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
  </tr>
</table>

</div>

<div class="clear"></div>

<div class="btnmain">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="left">
		  <div style="padding:0 0 0 450px;">
		    <table width="auto" border="0" cellspacing="5" cellpadding="20">
		      <tr>
		        <td>
		          <div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');" title="Back">Back</a></div>
		          </td>
		        </tr>
		      </table>
		    </div>
		  </td>
		</tr>
	</table>
</div>

</div>

<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<% out.println("<!--blank.jsp"); %>
</body>
</html>
<% out.println("-->"); %>