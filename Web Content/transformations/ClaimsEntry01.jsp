<% out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<% out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<!-- Start of the HATS form. -->
<HATS:Form>
	<script language="JavaScript" src="../common/js/textarea.js"></script>
	<!-- Insert your HATS component tags here. -->
	
	<input type="hidden" name="in_162_1">
<input type="hidden" name="in_242_1">
<input type="hidden" name="in_322_1">
<input type="hidden" name="in_402_1">
<input type="hidden" name="in_482_1">
<input type="hidden" name="in_562_1">
<input type="hidden" name="in_642_1">
<input type="hidden" name="in_722_1">
<input type="hidden" name="in_802_1">
<input type="hidden" name="in_882_1">
<input type="hidden" name="in_962_1">
<input type="hidden" name="in_1042_1">
<input type="hidden" name="in_1122_1">
<input type="hidden" name="in_1202_1">
<input type="hidden" name="in_1282_1">
<input type="hidden" name="in_1362_1">
<input type="hidden" name="in_1442_1">
<input type="hidden" name="in_1522_1">
<input type="hidden" name="in_1602_1">
<input type="hidden" name="in_1682_1">
<input type="hidden" name="in_1762_1">
<input type="hidden" name="in_1842_1">
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>

<div class="container">
<div class="heading_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" class="topbar_headingtxtredd">Please fill out form and Submit to enter Claims request</td>
  </tr>
</table>
</div>

<div class="clear4px"></div>

<div class="menubgg">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="left" valign="top" bgcolor="#d1c4a2">
    <table width="100%" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
      
      <tr>
        <td width="200" align="right" class="txt1order2"><strong>Contact Name :</strong></td>
        <td colspan="5" align="left">
        <HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxtfullnewone1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="46"
						componentSettings="" row="2" /></td>
        </tr>
      <tr>
        <td align="right" class="txt1order2"><strong>Email :</strong></td>
        <td colspan="5" align="left"><HATS:Component
						col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxtfullnewone2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="66"
						componentSettings="" row="3" /></td>
        </tr>
      <tr>
        <td align="right" class="txt1order2"><strong>Consumer Name :</strong></td>
        <td colspan="5" align="left"><HATS:Component col="17"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxtfullnewone2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="66"
						componentSettings="" row="4" /></td>
      </tr>
      <tr>
        <td align="right" class="txt1order2"><strong>Address :</strong></td>
        <td colspan="3" align="left"><HATS:Component col="17"
						alternate="" widget="gulistan_JPS02.widgets.TextAreaWidget1"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="style:width:400px; color:#000; height:15px; border:1px solid #a08b5b; overflow:visible; background:#fff url(../common/images/contactus-fieldrpt.gif) repeat-x center bottom;|"

						type="com.ibm.hats.transform.components.FieldComponent" ecol="66"
						componentSettings="" row="5" />
          </td>
        <td align="left" class="txt1order2">Zip :</td>
        <td align="left"><HATS:Component col="69" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="7" /></td>
      </tr>
      <tr>
        <td align="right" class="txt1order2"><strong>Phone :</strong></td>
        <td colspan="5" align="left"><HATS:Component col="17"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="26"
						componentSettings="" row="8" /></td>
        </tr>
      <tr>
        <td align="right" class="txt1order2">Invoice Number :</td>
        <td colspan="2" align="left"><HATS:Component col="18"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="24"
						componentSettings="" row="9" /></td>
        <td align="left" class="txt1order2">Invoice Date :</td>
        <td colspan="2" align="left"><HATS:Component col="42"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="51"
						componentSettings="" row="9" /></td>
        </tr>
      <tr>
        <td align="right" class="txt1order2">Roll :</td>
        <td align="left"><HATS:Component col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="18"
						componentSettings="" row="10" /></td>
        <td width="150" class="txt1order2">Defective Length :</td>
        <td align="left"><HATS:Component col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="50"
						componentSettings="" row="10" /></td>
        <td width="110" align="left" class="txt1order2">Installed Date :</td>
        <td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.CalendarWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="tableCellStyleClass:|trimSpacesOnInputs:true|stripUnderlinesOnInputs:false|tableRowStyleClass:|tableStyleClass:HATSTABLE|columnsPerRow:1|tableCaptionCellStyleClass:|labelStyleClass:HATSCAPTION|restrictMinDate:false|useInlineCalendar:false|inputFieldStyleClass:fieldtxt4|blinkStyle:font-style: italic|restrictMaxDate:false|captionSource:VALUE|launcherImage:calendar.gif|mapExtendedAttributes:false|patternLocaleSelected:|defaultValue:|rangeEnd:|launcherCaption:Date|underlineStyle:text-decoration: underline|preserveColors:false|readOnly:false|columnSeparatorStyle:border-width: 1px; border-style: solid|style:|pattern:MMddyyyy|patternLocale:BROWSER|caption:|eliminateMaxlengthInIdeographicFields:false|launcherButtonStyleClass:allbtn23|launcherType:BUTTON|rangeStart:|launcherLinkStyleClass:HATSLINK|reverseVideoStyle:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="79"
						componentSettings="" row="10" /></td>
      </tr>
      <tr>
        <td align="right" class="txt1order2">Style/Name :</td>
        <td colspan="2" align="left"><HATS:Component col="15"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="19"
						componentSettings="" row="11" /><HATS:Component col="21"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="40"
						componentSettings="" row="11" /></td>
        <td align="left" class="txt1order2">Color/Name :</td>
        <td colspan="2" align="left"><HATS:Component col="55"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="59"
						componentSettings="" row="11" /><HATS:Component col="61"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="80"
						componentSettings="" row="11" /></td>
      </tr>
      <tr>
        <td align="right" class="txt1order2">Reason for Claim :</td>
        <td colspan="5" align="left"><HATS:Component col="3"
						alternate="" widget="gulistan_JPS02.widgets.TextAreaWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="style:width:700px; color:#000; height:40px; border:1px solid #a08b5b; overflow:visible; background:#fff url(../common/images/contactus-fieldrpt.gif) repeat-x center bottom;|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="77"
						componentSettings="" row="13" /></td>
        </tr>
      <tr>
        <td align="right" class="txt1order2">Inspector Name :</td>
        <td colspan="2" align="left"><HATS:Component col="19"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt7|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="48"
						componentSettings="" row="18" /></td>
        <td align="left" class="txt1order2">Inspected Date :</td>
        <td colspan="2" align="left"><HATS:Component col="68"
						alternate=""
						widget="com.ibm.hats.transform.widgets.CalendarWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="tableCellStyleClass:|trimSpacesOnInputs:true|stripUnderlinesOnInputs:false|tableRowStyleClass:|tableStyleClass:HATSTABLE|columnsPerRow:1|tableCaptionCellStyleClass:|labelStyleClass:HATSCAPTION|restrictMinDate:false|useInlineCalendar:false|inputFieldStyleClass:fieldtxt4|blinkStyle:font-style: italic|restrictMaxDate:false|captionSource:VALUE|launcherImage:calendar.gif|mapExtendedAttributes:false|patternLocaleSelected:|defaultValue:|rangeEnd:|launcherCaption:Date|underlineStyle:text-decoration: underline|preserveColors:false|readOnly:false|columnSeparatorStyle:border-width: 1px; border-style: solid|style:|pattern:MMddyyyy|patternLocale:BROWSER|caption:|eliminateMaxlengthInIdeographicFields:false|launcherButtonStyleClass:allbtn23|launcherType:BUTTON|rangeStart:|launcherLinkStyleClass:HATSLINK|reverseVideoStyle:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="77"
						componentSettings="" row="18" /></td>
      </tr>
      <tr>
        <td align="right" class="txt1order2">Amount for Carpet :</td>
        <td colspan="2" align="left"><HATS:Component col="27"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="36"
						componentSettings="" row="19" /></td>
        <td colspan="3" align="left" class="txt1order2">&nbsp;</td>
        </tr>
      <tr>
        <td align="right" class="txt1order2">Freight :</td>
        <td colspan="2" align="left"><HATS:Component col="27"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="36"
						componentSettings="" row="20" /></td>
        <td colspan="3" rowspan="3" align="left" class="txt1order3">
        Please send supporting documents to :<br />
          claims@gulistan.com or fax-(800)649-0259<br />
          
          </td>
        </tr>
      <tr>
        <td align="right" class="txt1order2">Labor :</td>
        <td colspan="2" align="left"><HATS:Component col="27"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="36"
						componentSettings="" row="21" /></td>
        </tr>
      <tr>
        <td align="right" class="txt1order2">Total amount requested :</td>
        <td colspan="2" align="left"><HATS:Component col="27"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="22" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="36"
						componentSettings="" row="22" /></td>
        </tr>
      </table>
      
      </td>
    <td background="../common/images/right_middle4.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
  </tr>
</table>
</div>

<div class="clear"></div>

<div class="btnmain">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="center">
	<table width="auto" border="0" cellspacing="5" cellpadding="0">
		<tr>
		<td>
		<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');">Back</a></div>
		</td>
		<td>
		<div class="allbtn"><a href="javascript:ms('[pf6]','hatsportletid');">Submit</a></div>
		</td>
		</tr>
	</table>
		</td>
		</tr>
	</table>
</div>
</div>





<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<% out.println("<!--blank.jsp"); %>
</body>
</html>
<% out.println("-->"); %>