<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java"%>
<%@ taglib uri="hats.tld" prefix="HATS"%>
<%@page import="com.ibm.hats.common.*"%><HTML>
<HEAD>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink />
</HEAD>
<BODY>

<SCRIPT type="text/javascript" src="../common/env.js">
</SCRIPT>

<SCRIPT type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</SCRIPT>

<SCRIPT type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</SCRIPT>

<SCRIPT type="text/javascript" src="../common/HatsJS.js">
</SCRIPT>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<HATS:Form>
	<% String isPlus= ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("isPlus", true).getString(0); %>
	<INPUT type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV" />
	<INPUT type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV" />
	<INPUT type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV" />
	<INPUT type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked" />
	<INPUT type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked" />
	
	<input type="hidden" name="hatsgv_menuClicked" id="hatsgv_menuClicked"/>
	
	<INPUT type="hidden" name="in_482_1">
	<INPUT type="hidden" name="in_562_1">
	<INPUT type="hidden" name="in_642_1">
	<INPUT type="hidden" name="in_722_1">
	<INPUT type="hidden" name="in_802_1">
	<INPUT type="hidden" name="in_882_1">
	<INPUT type="hidden" name="in_962_1">
	<INPUT type="hidden" name="in_1042_1">
	<INPUT type="hidden" name="in_1122_1">
	<INPUT type="hidden" name="in_1202_1">
	<INPUT type="hidden" name="in_1282_1">
	<INPUT type="hidden" name="in_1362_1">
	<INPUT type="hidden" name="in_1442_1">
	<INPUT type="hidden" name="in_1522_1">
	<INPUT type="hidden" name="in_1602_1">
	<INPUT type="hidden" name="in_1682_1">

	<%String ENDPLUS=((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("EndPlus", true).getString(0); %>

	<DIV class="container">

	<DIV class="heading_bg">
	<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD width="33%" class="topbar_headingtxt1"></TD>
			<TD width="34%" align="center" class="topbar_headingtxtred1"><HATS:Component
				col="36" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="1" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="50"
				componentSettings="" row="1" /></TD>
			<TD width="33%" align="right" class="topbar_headingtxt3"><HATS:Component
				col="34" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="62"
				componentSettings="" row="2" /></TD>
		</TR>
	</TABLE>
	</DIV>

	<DIV class="clear4px"></DIV>


	<DIV class="menubgg">

	<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD width="5" height="5" background="../common/images/left_top4.gif"></TD>
			<TD background="../common/images/center_middle_top4.gif"></TD>
			<TD width="5" height="5" background="../common/images/right_top4.gif"></TD>
		</TR>
		<TR>
			<TD background="../common/images/left_middle4.gif"></TD>
			<TD align="center" valign="top" bgcolor="#d1c4a2">
			<TABLE width="100%" border="0" cellspacing="0" cellpadding="2"
				class="txt2">
				<TR>
					<TD width="250" bgcolor="#fffcf5" class="fillbor58"
						style="padding: 3px; border-top: 1px solid #b99b75;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="7"
						componentSettings="" row="2" /></TD>
					<TD align="right" style="padding: 3px 10px 3px 3px;">&nbsp;</TD>
					<TD width="250" bgcolor="#fffcf5" class="fillbor58"
						style="padding: 3px; border-top: 1px solid #b99b75;">&nbsp;</TD>
				</TR>
				<TR>
					<TD bgcolor="#fffcf5" class="fillbor58" style="padding: 3px;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="3" /></TD>
					<TD>&nbsp;</TD>
					<TD bgcolor="#fffcf5" class="fillbor58" style="padding: 3px;"><STRONG><HATS:Component
						col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="49"
						componentSettings="" row="3" /></STRONG> <HATS:Component col="51"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="80"
						componentSettings="" row="3" /></TD>
				</TR>
				<TR>
					<TD bgcolor="#fffcf5" class="fillbor58" style="padding: 3px;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="4" /></TD>
					<TD>&nbsp;</TD>
					<TD bgcolor="#fffcf5" class="fillbor58" style="padding: 3px;"><STRONG><HATS:Component
						col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="49"
						componentSettings="" row="4" /></STRONG> <HATS:Component col="51"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="80"
						componentSettings="" row="4" /></TD>
				</TR>
				<TR>
					<TD bgcolor="#fffcf5" class="fillbor58" style="padding: 3px;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="5" /></TD>
					<TD>&nbsp;</TD>
					<TD bgcolor="#fffcf5" class="fillbor58" style="padding: 3px;"><STRONG><HATS:Component
						col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="49"
						componentSettings="" row="5" /></STRONG> <HATS:Component col="51"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="66"
						componentSettings="" row="5" /></TD>
				</TR>
				<TR>
					<TD bgcolor="#fffcf5" class="fillbor58" style="padding: 3px;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="6" /></TD>
					<TD>&nbsp;</TD>
					<TD bgcolor="#fffcf5" class="fillbor58" style="padding: 3px;">&nbsp;</TD>
				</TR>
			</TABLE>
			</TD>
			<TD background="../common/images/right_middle4.gif"></TD>
		</TR>
		<TR>
			<TD height="5" background="../common/images/left_bottom4.gif"></TD>
			<TD background="../common/images/center_middle_bottom4.gif"></TD>
			<TD height="5" background="../common/images/right_bottom4.gif"></TD>
		</TR>
	</TABLE>

	<!--line-->
	<TABLE width="100%">
		<TR>
			<TD height="1">
			<DIV class="line"></DIV>
			</TD>
		</TR>
	</TABLE>
	<!--line-->

	<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD width="5" height="5" background="../common/images/left_top4.gif"></TD>
			<TD background="../common/images/center_middle_top4.gif"></TD>
			<TD width="5" height="5" background="../common/images/right_top4.gif"></TD>
		</TR>
		<TR>
			<TD background="../common/images/left_middle4.gif"></TD>
			<TD align="center" valign="top">


			<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
				<THEAD>
					<TR class="oddRowdd">
						<TD width="120" align="center">INVOICE<BR />
						NUMBER</TD>
						<TD width="120" align="center" class="leftline2">INV<BR />
						DATE</TD>
						<TD width="150" align="center" class="leftline2">CUST<BR />
						REFNO</TD>
						<TD width="120" align="center" class="leftline2">DUE<BR />
						DATE</TD>
						<TD width="200" align="center" class="leftline2">INVOICE<BR />
						AMOUNT</TD>
						<TD align="center" class="leftline2">OPEN/<BR />
						NET</TD>
					</TR>
				</THEAD>
			</TABLE>
			<DIV
				style="vertical-align: top; overflow-y: auto; overflow-x: hidden; height: 195px; border: solid 1px #CCC; padding: 0px;">
			<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
				<TR>
					<TD width="100%"><HATS:Component type="com.ibm.hats.transform.components.TableComponent" widget="gulistan_JPS02.widgets.AccountSummaryWidget" row="11" col="2" erow="22" ecol="69" componentSettings="startCol:2|endRow:22|columnBreaks:14,24,35,45,58,72|endCol:78|excludeCols:|visualTableDefaultValues:false|startRow:11|numberOfTitleRows:0|excludeRows:|includeEmptyRows:true|" componentIdentifier="AccountSummaryCombination" applyTextReplacement="true"/></TD>
				</TR>
			</TABLE>

			</DIV>



			</TD>

			<TD background="../common/images/right_middle4.gif"></TD>
		</TR>

		<TR>
			<TD height="5" background="../common/images/left_bottom4.gif"></TD>
			<TD background="../common/images/center_middle_bottom4.gif"></TD>
			<TD height="5" background="../common/images/right_bottom4.gif"></TD>
		</TR>
	</TABLE>
	<DIV class="clear2px"></DIV>

	<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD width="6" height="5" background="../common/images/left_top2.gif"></TD>
			<TD background="../common/images/center_middle_top2.gif"></TD>
			<TD width="6" height="5" background="../common/images/right_top2.gif"></TD>
		</TR>
		<TR>
			<TD background="../common/images/left_middle2.gif"></TD>
			<TD bgcolor="#ffffff" align="center" valign="middle"
				class="topbar_headingtxtrad">&nbsp; <HATS:Component col="2"
				alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="24" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="24" />&nbsp;</TD>
			<TD background="../common/images/right_middle2.gif"></TD>
		</TR>
		<TR>
			<TD height="8" background="../common/images/left_bottom2.gif"></TD>
			<TD background="../common/images/center_middle_bottom2.gif"></TD>
			<TD height="8" background="../common/images/right_bottom2.gif"></TD>
		</TR>

	</TABLE>
	</DIV>
	<!--Mene End-->

	<DIV class="clear"></DIV>

	<DIV class="btnmain">
	<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD align="center">
			<TABLE width="auto" border="0" cellspacing="5" cellpadding="0">
				<TR>

					<TD>
					<DIV class="allbtn"><A title="Back"
						href="javascript:ms('[pf3]','hatsportletid');">Back</A></DIV>
					</TD>
					<TD>
					<DIV class="allbtn"><A
						href="javascript:ms('[enter]','HATSForm');" title="Enter">Enter</A></DIV>
					</TD>

				</TR>
			</TABLE>
			</TD>
			<TD width="78" align="center">
			<DIV id="nextButton" class="next_btn"><A
				href="javascript:ms('[pagedn]','HATSForm');"></A></DIV>
			<DIV id="previousButton" class="prev_btn"><A
				href="javascript:ms('[pageup]','HATSForm');"></A></DIV>
			</TD>
		</TR>
	</TABLE>
	</DIV>
	</DIV>
	<!--ddd-->




	<SCRIPT language="JavaScript" type="text/javascript">
	
	var ENDPLUS = "<%= ENDPLUS.trim()%>";
	
	if(ENDPLUS != "+"){
	document.getElementById("nextButton").innerHTML = "";
	document.getElementById("nextButton").className = "next_btndisable";
	}
	
	
	</SCRIPT>







	
</HATS:Form>

<HATS:OIA />

</BODY>
</HTML>
