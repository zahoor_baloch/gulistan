<%// //out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="java.net.URL"%>
<%@page import="java.io.File"%>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <meta http-equiv="Pragma" content="no-cache">
 <meta http-equiv="Cache-Control" content="no-cache">
<HATS:VCTStylesLink/>
</head>
<body>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>

	
<!-- Insert your HATS component tags here. -->
<input type="hidden" name="in_528_1">

<!--ddd-->
<div class="container">
<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-store" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   
 	
	URL url=this.getClass().getClassLoader().getResource("common");				
		
		String path1_1 = url.getPath().substring(0, url.getPath().lastIndexOf("common"))+"PdfFile/";
		String path2_1 = url.getPath().substring(0, url.getPath().lastIndexOf("common"))+"excelFile/";
				
		File pdfFile=new File(path1_1);
		File excelFile=new File(path2_1);
					
		boolean isPdfFileCreated = pdfFile.mkdir();
		boolean isExcelFileCreated = excelFile.mkdir();	
		
		url=this.getClass().getClassLoader().getResource("PdfFile");
		
		System.out.println("isPdfFileCreated:::"+isPdfFileCreated+".........isExcelFileCreated:::"+isExcelFileCreated+"..........url:::"+url);
 
%>


<div class="clear50px"></div>

<!--Login Start-->
<div class="login_bg">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="225">
      <div class="clear40px"></div>
      <div class="logintxt">User Id :</div>
      </td>
    <td>
    <div class="clear40px"></div>
    <HATS:Component col="20" alternate=""
				widget="com.ibm.hats.transform.widgets.FieldWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:logginfield|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
				type="com.ibm.hats.transform.components.FieldComponent" ecol="29"
				componentSettings="" row="2" />
    </td>
  </tr>
  
  <tr>
    <td width="200">
      <div class="clear20px"></div>
      <div class="logintxt">Password :</div>
      </td>
    <td>
    <div class="clear20px"></div>
    <HATS:Component col="20" alternate=""
				widget="com.ibm.hats.transform.widgets.FieldWidget"
				alternateRenderingSet="" erow="3" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:logginfield|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
				type="com.ibm.hats.transform.components.FieldComponent" ecol="29"
				componentSettings="" row="3" />
    </td>
  </tr>
  <tr>
      <td align="right" colspan="2">
      <div class="clear7px"></div>
      <div class="errorlogin">
      &nbsp;
      <HATS:Component col="32" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="58"
				componentSettings="" row="2" />
	  &nbsp;
	  </div>
	  
      <div class="clear23px"></div>
      <div class="clickbtnmain">
      <div class="clickherebtn"><input name="" type="submit"  value="Login &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/></div>
      </div>
      
      </td>
    </tr>
</table>

</div>
<!--Login End-->

<div class="clear20px"></div>

<!--Details Start-->
<div class="detailsmain">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="8" height="8" class="left_top"></td>
    <td class="center_middle_top">
    <div class="detailstxtheading">Details</div>
    </td>
    <td width="8" height="8" class="right_top"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle.gif"></td>
    <td>
    <div class="clear10px"></div>
    <div class="detailstxt">If you are not signed up for Online Services and would like to register, please click <a href="javascript:setCursorPosition(528, 'HATSForm');checkInput2('in_528_1', 'X','hidden');ms('[enter]', 'HATSForm')" >here</a> to register.</div>
    <div class="clear10px"></div>
    </td>
    <td background="../common/images/right_middle.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom.gif"></td>
    <td background="../common/images/center_middle_bottom.gif"></td>
    <td height="8" background="../common/images/right_bottom.gif"></td>
  </tr>
</table>
</div>
<!--Details End-->

<div class="clear70px"></div>

<div class="btnmain">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table width="auto" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="3" height="24" background="../common/images/left_img_btn.gif" style="display: none;"></td>
            <td background="../common/images/middle_img_btn.gif" style="background-repeat:repeat-x">
            <div class="bttntxt1" style="display: none;"><a href="javascript:ms('[pf3]','hatsportletid');">Exit</a></div>
            
            </td>
            <td width="3" background="../common/images/right_img_btn.gif" style="display: none;"></td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</div>
<!--ddd-->




	<script language="JavaScript">
	document.getElementsByName('in_100_10')[0].focus();
	</script>



	<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<% //out.println("<!--blank.jsp"); %>
</body>
</html>
<% //out.println("-->"); %>