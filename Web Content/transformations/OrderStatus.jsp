<%// out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<% //out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<%session.setAttribute("orderInquirySearchDetails_sv2",null); %>
<!-- Insert your HATS component tags here. -->

<!--ddd-->
<div class="container">
<div class="heading_bg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
			<td width="33%" class="topbar_headingtxtred1" colspan="3" align="center"><HATS:Component
				col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="77"
				componentSettings="" row="2" /> &nbsp;</td>
		</tr>
</table>
</div>

<div class="clear4px"></div>

<!--Menu Start-->
<div class="menubgg">
  <div class="popupmain57">
    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="middle">
          <table width="100%" height="150" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td background="../common/images/popup_top_left.png" width="16">&nbsp;</td>
              <td height="16" background="../common/images/popup_top_middle.png">&nbsp;</td>
              <td background="../common/images/popup_top_right.png" width="16">&nbsp;</td>
              </tr>
            <tr>
              <td background="../common/images/popup_left_middle.png">&nbsp;</td>
              <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5" height="5" background="../common/images/left_top4.gif"></td>
                  <td background="../common/images/center_middle_top4.gif"></td>
                  <td width="5" height="5" background="../common/images/right_top4.gif"></td>
                  </tr>
                <tr>
                  <td background="../common/images/left_middle4.gif"></td>
                  <td align="center" valign="top" bgcolor="#d1c4a2">
                    <table width="auto" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
                      <tr>
                        <td colspan="2" align="center" bgcolor="#FFFFFF" class="txt4"><strong>Key Order Number or PO Number</strong></td>
                        </tr>
                      <tr>
                        <td bgcolor="#d1c4a2" class="txt1"><strong>Gulistan Order Number:</strong></td>
                        <td><HATS:Component col="44" alternate=""
										widget="com.ibm.hats.transform.widgets.FieldWidget"
										alternateRenderingSet="" erow="6" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt6|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
										type="com.ibm.hats.transform.components.FieldComponent"
										ecol="48" componentSettings="" row="6" /></td>
                        </tr>
                      <tr>
                        <td valign="top" bgcolor="#d1c4a2" class="txt1"><strong>Customer P.O. Number:</strong></td>
                        <td align="right">
                          <HATS:Component col="44" alternate=""
										widget="com.ibm.hats.transform.widgets.FieldWidget"
										alternateRenderingSet="" erow="8" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt6|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
										type="com.ibm.hats.transform.components.FieldComponent"
										ecol="53" componentSettings="" row="8" />
									<div class="clear5px"></div>

<div class="btnmain">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="right" style="border:none;">
	<table width="auto" border="0" cellspacing="5" cellpadding="0">
		<tr>
		<td style="border:none;">
		<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');" title="Back">Back</a></div>
		</td>
        <td width="5" style="border:none;"></td>
		<td style="border:none;">
		<div class="allbtn"><a href="javascript:ms('[enter]','hatsportletid');" title="Search">Search</a></div>
		</td>
		</tr>
	</table>
		</td>
		</tr>
	</table>
</div>
                          </td>
                        </tr>
                      <tr>
                        <td colspan="2" align="center" bgcolor="#FFFFFF" class="txt4"><strong>For a List of all orders leave blank</strong></td>
                        
                        </tr>
                        <tr><td colspan="2" align="center" valign="middle" class="topbar_headingtxtrad"><HATS:Component col="2" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="12" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="76" componentSettings="" row="12" />&nbsp;</td></tr>
                      </table>
                    
                    </td>
                  <td background="../common/images/right_middle4.gif"></td>
                  </tr>
                <tr>
                  <td height="5" background="../common/images/left_bottom4.gif"></td>
                  <td background="../common/images/center_middle_bottom4.gif"></td>
                  <td height="5" background="../common/images/right_bottom4.gif"></td>
                  </tr>
                </table></td>
              <td background="../common/images/popup_right_middle.png">&nbsp;</td>
              </tr>
            <tr>
              <td background="../common/images/popup_below_left.png" width="16">&nbsp;</td>
              <td height="16" background="../common/images/popup_below_middle.png">&nbsp;</td>
              <td background="../common/images/popup_below_right.png" width="16">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
</div>

</div>
<!--Mene End-->

<div class="clear5px"></div>


</div>
<!--ddd-->





<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<% //out.println("<!--blank.jsp"); %>
</body>
</html>
<%// out.println("-->"); %>