<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<HATS:Form>

<div class="container">

<div class="PlaceOrderbg">

<div class="popupmain57">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center" valign="middle">
<table width="100%" height="150" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td background="../common/images/popup_top_left.png" width="16">&nbsp;</td>
	<td height="16" background="../common/images/popup_top_middle.png">&nbsp;</td>
	<td background="../common/images/popup_top_right.png" width="16">&nbsp;</td>
	</tr>
	<tr>
	<td background="../common/images/popup_left_middle.png">&nbsp;</td>
	<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="5" height="5" background="../common/images/left_top2.gif"></td>
	<td background="../common/images/center_middle_top2.gif"></td>
	<td width="5" height="5" background="../common/images/right_top2.gif"></td>
	</tr>
	<tr>
	<td background="../common/images/left_middle2.gif"></td>
	<td align="center" valign="top" bgcolor="#d1c4a2">
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
	<tr>
	<td colspan="2" align="center" bgcolor="#FFFFFF" class="txt4"><strong>Sample Order Entry Form</strong></td>
	</tr>
	<tr>
	<td align="right" bgcolor="#d1c4a2" class="txtplace">Your Sample request has been submitted :</td>
	<td align="left" style="font-size:14px;"><HATS:Component col="47"
										alternate=""
										widget="com.ibm.hats.transform.widgets.FieldWidget"
										alternateRenderingSet="" erow="17" textReplacement=""
										widgetSettings=""
										type="com.ibm.hats.transform.components.FieldComponent"
										ecol="52" componentSettings="" row="17" /></td>
	</tr>
	<tr>
	<td colspan="2" align="center" bgcolor="#FFFFFF">
                          
	<div class="btnmain">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
	<td align="center" style="border:none;">
<table width="auto" border="0" cellspacing="0" cellpadding="0">
    <tr>
	<td style="border:none;">
	<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');" title="Ok">Ok</a></div>
	</td>
	</tr>
</table>
	</td>
	</tr>
</table>
	</div>
                          
	</td>
	</tr>
</table>
                    
	</td>
	<td background="../common/images/right_middle2.gif"></td>
	</tr>
	<tr>
	<td height="5" background="../common/images/left_bottom2.gif"></td>
	<td background="../common/images/center_middle_bottom2.gif"></td>
	<td height="5" background="../common/images/right_bottom2.gif"></td>
	</tr>
</table>
	</td>
	<td background="../common/images/popup_right_middle.png">&nbsp;</td>
	</tr>
	<tr>
	<td background="../common/images/popup_below_left.png" width="16">&nbsp;</td>
	<td height="16" background="../common/images/popup_below_middle.png">&nbsp;</td>
	<td background="../common/images/popup_below_right.png" width="16">&nbsp;</td>
	</tr>
</table>
	</td>
	</tr>
</table>
</div>

</div>

</div>

</HATS:Form>

</body>
</html>