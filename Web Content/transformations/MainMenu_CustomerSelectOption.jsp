<%// out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<% //out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<input type="hidden" name="in_344_1" id="in_344_1"/>
<%session.setAttribute("orderInquirySearchDetails_sv2",null); %>
<!-- Insert your HATS component tags here. -->

<div class="container">
<div class="clear5px"></div>
<div class="heading_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" class="topbar_headingtxt1">Customer 009244</td>

    <td width="34%" align="center" class="topbar_headingtxt2">&nbsp;</td>
    <td width="33%" align="right" class="topbar_headingtxt3">Test/Example Customer</td>
  </tr>
</table>
</div>

<div class="clear4px"></div>


<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>

    <td width="5" height="5" background="../common/images/left_top3.gif"></td>
    <td background="../common/images/center_middle_top3.gif"></td>
    <td width="5" height="5" background="../common/images/right_top3.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle3.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
    <table width="auto" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
        <tr>

          <td bgcolor="#FFFFFF" class="txt1"><strong>Customer Number :</strong></td>
          <td>
           <HATS:Component col="31" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="36"
						componentSettings="" row="3" />
          </td>
          <td align="center" bgcolor="#FFFFFF">
            <div id="vista_toolbar2">
              <div align="center">
              <ul>

                 <li><a href="javascript:ms('[enter]','hatsportletid');" title="Enter"><span>Enter</span></a></li>
              </ul>   
              </div>
              </div>
          </td>
          <td style="display: none; color: red;" bgcolor="white">ghfhjgjghjgjhgjhd</td>
        </tr>
        </table>
    </td>
    <td background="../common/images/right_middle3.gif"></td>

  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom3.gif"></td>
    <td background="../common/images/center_middle_bottom3.gif"></td>
    <td height="5" background="../common/images/right_bottom3.gif"></td>
  </tr>
</table>

<div class="clear4px"></div>

<div class="linsprtd"></div>

</div>


<div class="clear4px"></div>

<!--Menu Start-->
<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>

  </tr>
  <tr>
    <td background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="top">
    	<table width="auto" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <td>
          <div id="popupbtn">
          <div align="center">

          <ul>
             <li><a href="javascript:setCursorPosition(344, 'HATSForm');checkInput2('in_344_1', 'X','hidden');ms('[enter]', 'HATSForm')" title="Search by Name"><span>Search by Name</span></a></li>
          </ul>   
          </div>
          </div>
        </td>
        </tr>
		</table>
      </td>

    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
</table>
      
      
      <div class="clear5px"></div>

      
      <div class="linsprtd"></div>
      
      <div class="clear5px"></div>
      
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>

    <td height="310" background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="top">&nbsp;</td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>

</table>
</div>
<!--Mene End-->

<div class="clear0px"></div>

<div class="btnmain">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table width="auto" border="0" cellspacing="5" cellpadding="0">
          <tr>

            <td>
              <div id="vista_toolbar">
              <div align="center">
              <ul>
                 <li><a href="javascript:ms('macrorun_customerSelect_logoff','hatsportletid');" title="Back"><span>Back</span></a></li>
              </ul>   
              </div>
              </div>
            </td>

            </tr>
        </table></td>
    </tr>
  </table>
</div>
</div>






<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<% //out.println("<!--blank.jsp"); %>
</body>
</html>
<% //out.println("-->"); %>