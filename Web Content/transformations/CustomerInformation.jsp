<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<HATS:Form>

<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV" />
	<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV" />
	<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV" />
	<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked" />
	<input type="hidden" name="hatsgv_logoffClicked"
		id="hatsgv_logoffClicked" />

<input type="hidden" name="hatsgv_menuClicked" id="hatsgv_menuClicked"/>

	<!--<input type="hidden" name="in_482_1">
	<input type="hidden" name="in_562_1">
	<input type="hidden" name="in_642_1">
	<input type="hidden" name="in_722_1">
	<input type="hidden" name="in_802_1">
	<input type="hidden" name="in_882_1">
	<input type="hidden" name="in_962_1">
	<input type="hidden" name="in_1042_1">
	<input type="hidden" name="in_1122_1">
	<input type="hidden" name="in_1202_1">
	<input type="hidden" name="in_1282_1">
	<input type="hidden" name="in_1362_1">
	<input type="hidden" name="in_1442_1">
	<input type="hidden" name="in_1522_1">
	<input type="hidden" name="in_1602_1">
	<input type="hidden" name="in_1682_1">

--><div class="container">

<div class="heading_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" class="topbar_headingtxt1">&nbsp;</td>
    <td width="34%" align="center" class="topbar_headingtxtredd">Customer Information</td>
    <td width="33%" align="right" class="topbar_headingtxt3"><HATS:Component
				col="22" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="51"
				componentSettings="" row="2" /></td>
  </tr>
</table>
</div>



<div class="menubgg">
<!--line-->
	<table width="100%">
		<tr>
			<td height="1">
			<div class="line"></div>
			</td>
		</tr>
	</table>
	<!--line-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
	</tr>
	<tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="txt2">
	<tr>
    <td align="left" class="fillbar" bgcolor="#fffcf5"><strong>Sold To:</strong></td>
    <td width="200" align="left" class="fillbarnew" style="padding:2px 0px 0px 3px;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="16"
						componentSettings="" row="4" /></td>
    <td width="10"></td>
    <td width="330" align="center"><strong><HATS:Component col="34"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="3" /></strong></td>
    <td width="10"></td>
    <td align="left" bgcolor="#fffcf5" class="fillbar"><strong>Ship To:</strong></td>
    <td width="200" align="left" class="fillbarnew" style="padding:2px 0px 0px 3px;"><HATS:Component
						col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="60"
						componentSettings="" row="4" /></td>
	</tr>
	<tr>
    <td>&nbsp;</td>
    <td align="left" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="5" /></td>
    <td width="10"></td>
    <td width="330" align="center"><strong><HATS:Component col="34"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="4" /></strong></td>
    <td width="10"></td>
    <td>&nbsp;</td>
    <td align="left" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><HATS:Component
						col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:fieldtxt4|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="75"
						componentSettings="" row="5" /></td>
	</tr>
	<tr>
    <td>&nbsp;</td>
    <td align="left" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="6" /></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td align="left" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><HATS:Component
						col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="75"
						componentSettings="" row="6" /></td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	<td align="left" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="7" /></td>
	<td></td>
	<td align="center" valign="bottom">&nbsp;</td>
	<td></td>
	<td>&nbsp;</td>
	<td align="left" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><HATS:Component
						col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="75"
						componentSettings="" row="7" /></td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	<td align="left" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="8" /></td>
	<td></td>
	<td align="center" valign="bottom">&nbsp;</td>
	<td></td>
	<td>&nbsp;</td>
	<td align="left" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><HATS:Component
						col="46" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="75"
						componentSettings="" row="8" /></td>
	</tr>
    <tr>
	<td>&nbsp;</td>
	<td align="right" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><HATS:Component
						col="20" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="31"
						componentSettings="" row="9" /></td>
	<td></td>
	<td align="center" valign="bottom">&nbsp;</td>
	<td></td>
	<td>&nbsp;</td>
	<td align="right" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><HATS:Component
						col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="76"
						componentSettings="" row="9" /></td>
	</tr>
    <tr>
	<td>&nbsp;</td>
	<td align="left" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;">&nbsp;</td>
	<td></td>
	<td align="center" valign="bottom">&nbsp;</td>
	<td></td>
	<td>&nbsp;</td>
	<td align="left" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><strong>State Code:</strong> <HATS:Component
						col="58" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="59"
						componentSettings="" row="10" /></td>
	</tr>
    <tr>
	<td>&nbsp;</td>
	<td align="left" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><strong>Telephone:</strong> <HATS:Component
						col="13" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="25"
						componentSettings="" row="11" /></td>
	<td></td>
	<td align="center" valign="bottom"><strong><HATS:Component col="34"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="11" BIDIOpposite="false" /></strong></td>
	<td></td>
	<td>&nbsp;</td>
	<td align="left" bgcolor="#fffcf5" class="fillbor58" style="padding:3px;"><strong>Telephone:</strong> <HATS:Component
						col="57" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|style:|renderProtectedTextAsLinks:false|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="69"
						componentSettings="" row="11" /></td>
	</tr>
</table>
    </td>
    <td background="../common/images/right_middle4.gif"></td>
	</tr>
	<tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
	</tr>
</table>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td width="5" height="5" background="../common/images/left_top4.gif"></td>
    <td background="../common/images/center_middle_top4.gif"></td>
    <td width="5" height="5" background="../common/images/right_top4.gif"></td>
	</tr>
	<tr>
    <td background="../common/images/left_middle4.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
<table width="auto" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
      
	<tr>
	<td class="txt1order2">Coupon$:</td>
	<td align="left">
	<HATS:Component col="11" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:25=25;50=50;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:HATSDROPDOWN|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="12"
						componentSettings="" row="13" />
	</td>
	<td class="txt1order2">Qty:</td>
	<td align="left"><HATS:Component col="20" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="23"
						componentSettings="" row="13" /></td>
	<td class="txt1order2">P.O.#:</td>
	<td align="left"><HATS:Component col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="48"
						componentSettings="" row="13" /></td>
	<td class="txt1order2">SHIP COMPLETE:</td>
	<td align="left"><HATS:Component col="68" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="68"
						componentSettings="" row="13" BIDIOpposite="false" /></td>
	</tr>
</table>
      
	</td>
    <td background="../common/images/right_middle4.gif"></td>
	</tr>
	<tr>
    <td height="5" background="../common/images/left_bottom4.gif"></td>
    <td background="../common/images/center_middle_bottom4.gif"></td>
    <td height="5" background="../common/images/right_bottom4.gif"></td>
	</tr>
</table>

	<div class="clear5px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="5" height="5" background="../common/images/left_top2.gif"></td>
	<td background="../common/images/center_middle_top2.gif"></td>
	<td width="5" height="5" background="../common/images/right_top2.gif"></td>
	</tr>
	<tr>
	<td background="../common/images/left_middle2.gif"></td>
	<td align="center" valign="middle" class="topbar_headingtxtrad">&nbsp;
	<HATS:Component col="2" alternate=""
		widget="com.ibm.hats.transform.widgets.LabelWidget"
		alternateRenderingSet="" erow="24" textReplacement=""
		widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
		type="com.ibm.hats.transform.components.TextComponent" ecol="79"
		componentSettings="" row="24" />&nbsp;</td>
	<td background="../common/images/right_middle2.gif"></td>
	</tr>
	<tr>
	<td height="5" background="../common/images/left_bottom2.gif"></td>
	<td background="../common/images/center_middle_bottom2.gif"></td>
	<td height="5" background="../common/images/right_bottom2.gif"></td>
	</tr>

</table>
</div>

	<div class="clear"></div>

	<div class="btnmain">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center">
<table width="auto" border="0" cellspacing="5" cellpadding="0">
	<tr>
	<td>
	<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');" title="Exit">Exit</a></div>
	</td>
	<td>
	<div class="allbtn"><a href="javascript:ms('[enter]','hatsportletid');" title="Enter">Enter</a></div>
	</td>
	</tr>
</table>
	</td>
	</tr>
</table>
	</div>

</div>
</HATS:Form>

</body>
</html>