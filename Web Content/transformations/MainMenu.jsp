<%// out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<%// out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
<%session.setAttribute("inventoryInquiry_Details_sv1",null); %>
<%session.setAttribute("orderInquiry_ProductSearch_locate_sv1",null); %>
<%session.setAttribute("orderInquiry_Details_nextPage_sv",null); %>

<!-- Insert your HATS component tags here. -->
<input type="hidden" name="in_403_1">
<input type="hidden" name="in_483_1">
<input type="hidden" name="in_563_1">
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<div class="clear1px"></div>

<!--ddd-->
<div class="container">

<div class="heading_bg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" class="topbar_headingtxt1">
    <HATS:Component
				col="3" alternate=""
				widget="com.ibm.hats.transform.widgets.FieldWidget"
				alternateRenderingSet="" erow="3" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.FieldComponent" ecol="11"
				componentSettings="" row="3" />
    </td>
    <td width="34%" align="center" class="topbar_headingtxtred">
    <HATS:Component
				col="8" alternate=""
				widget="com.ibm.hats.transform.widgets.FieldWidget"
				alternateRenderingSet="" erow="1" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.FieldComponent" ecol="29"
				componentSettings="" row="1" />
    </td>
    <td width="33%" align="right" class="topbar_headingtxt3">
    <HATS:Component
				col="14" alternate=""
				widget="com.ibm.hats.transform.widgets.FieldWidget"
				alternateRenderingSet="" erow="3" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.FieldComponent" ecol="35"
				componentSettings="" row="3" />
    </td>
  </tr>
</table>
</div>

<div class="clear4px"></div>

<!--Menu Start-->
<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td height="370" background="../common/images/left_middle2.gif"></td>
    <td valign="top">
      <table width="100%" border="0" cellspacing="10" cellpadding="0">
        <tr>
          <td align="center">
            <div class="menubtn"><a href="javascript:setCursorPosition(403, 'HATSForm');checkInput2('in_403_1', 'X','hidden');ms('[enter]', 'HATSForm')">Select Order Entry</a></div>
          </td>
        </tr>
        <tr>
          <td align="center">
            <div class="menubtn"><a href="javascript:setCursorPosition(483, 'HATSForm');checkInput2('in_483_1', 'X','hidden');ms('[enter]', 'HATSForm')">Select Inventory Inquiry</a></div>
          </td>
        </tr>
        <tr>
          <td align="center">
            <div class="menubtn"><a href="javascript:setCursorPosition(563, 'HATSForm');checkInput2('in_563_1', 'X','hidden');ms('[enter]', 'HATSForm')">Select Order Inquiry</a></div>
          </td>
        </tr>
      </table></td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
  <tr>
  </tr>
</table>

</div>
<!--Mene End-->

<div class="clear10px"></div>
 
<div class="btnmain">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table width="auto" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="3" height="24" background="../common/images/left_img_btn.gif"></td>
            <td background="../common/images/middle_img_btn.gif" style="background-repeat:repeat-x">
            <div class="bttntxt1"><a href="javascript:ms('[pf3]','hatsportletid');">Back</a></div>
            </td>
            <td width="3" background="../common/images/right_img_btn.gif"></td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
 
</div>



<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<%// out.println("<!--blank.jsp"); %>
</body>
</html>
<% //out.println("-->"); %>