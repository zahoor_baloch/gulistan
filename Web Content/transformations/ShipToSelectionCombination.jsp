<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java"%>
<%@ taglib uri="hats.tld" prefix="HATS"%>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink />
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<%
	String serverName = request.getServerName();
	int port = request.getLocalPort();
%>

<script type="text/javascript">

function pdfPopUp()
{
 sOptions = 'status=yes,menubar=yes,scrollbars=yes,resizable=yes,toolbar=yes';
   sOptions = sOptions + ',width=' + (screen.availWidth - 10).toString();
   sOptions = sOptions + ',height=' + (screen.availHeight - 122).toString();
   sOptions = sOptions + ',screenX=0,screenY=0,left=0,top=0';
var serverName = "<%=serverName%>";
var portAdd = "<%=port%>";
var urlPath="http://"+serverName+":"+portAdd+"/Gulistan_JPS02/ShipTopdfPopUp.jsp";
var popupwindow=window.open(urlPath, "popup",sOptions );

}

function excelPopUp()
{
var serverName = "<%=serverName%>";
var portAdd = "<%=port%>";
var urlPath="http://"+serverName+":"+portAdd+"/Gulistan_JPS02/ShipToSpreadsheet.jsp";
var popupwindow=window.open(urlPath, "popup", "width=1024;location=yes;menubar=yes;resizable=yes;scrollbars=yes;status=no;titlebar=no;toolbar=no;height:1068");

}
</script>


<HATS:Form>
	<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV" />
	<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV" />
	<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV" />
	<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked" />
	<input type="hidden" name="hatsgv_logoffClicked"
		id="hatsgv_logoffClicked" />

<input type="hidden" name="hatsgv_menuClicked" id="hatsgv_menuClicked"/>

	<!--<input type="hidden" name="in_482_1">
	<input type="hidden" name="in_562_1">
	<input type="hidden" name="in_642_1">
	<input type="hidden" name="in_722_1">
	<input type="hidden" name="in_802_1">
	<input type="hidden" name="in_882_1">
	<input type="hidden" name="in_962_1">
	<input type="hidden" name="in_1042_1">
	<input type="hidden" name="in_1122_1">
	<input type="hidden" name="in_1202_1">
	<input type="hidden" name="in_1282_1">
	<input type="hidden" name="in_1362_1">
	<input type="hidden" name="in_1442_1">
	<input type="hidden" name="in_1522_1">
	<input type="hidden" name="in_1602_1">
	<input type="hidden" name="in_1682_1">



	--><div class="container">
	

		<div class="heading_bg">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="33%" class="topbar_headingtxtred1" colspan="3" align="center"><HATS:Component
					col="32" alternate=""
					widget="com.ibm.hats.transform.widgets.LabelWidget"
					alternateRenderingSet="" erow="1" textReplacement=""
					widgetSettings=""
					type="com.ibm.hats.transform.components.TextComponent" ecol="48"
					componentSettings="" row="1" /> &nbsp;</td>
			</tr>
		</table>
		</div>
	
		<div class="menubgg">
	
		<!--line-->
		<table width="100%">
			<tr>
				<td height="1">
				<div class="line"></div>
				</td>
			</tr>
		</table>
		<!--line-->
	
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="5" height="5" background="../common/images/left_top4.gif"></td>
				<td background="../common/images/center_middle_top4.gif"></td>
				<td width="5" height="5" background="../common/images/right_top4.gif"></td>
			</tr>
			<tr>
				<td background="../common/images/left_middle4.gif"></td>
				<td align="center" valign="top">
	
	
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr class="oddRowdd">
							<td class="shiptocom1" align="center">
							<div class="leftlinepad8top">Customer Num</div>
							</td>
							<td align="center" class="leftline2 shiptocom2">
							<div class="leftlinepad9top">Customer Name</div>
							</td>
							<td align="center" class="leftline2">
							<div class="leftlinepad7top">City/State</div>
							</td>
	
						</tr>
					</thead>
				</table>
				<div
					style="vertical-align: top; overflow-y: auto; overflow-x: hidden; height: 330px; border: solid 1px #CCC; padding: 0px;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%"><HATS:Component type="com.ibm.hats.transform.components.TableComponent" widget="gulistan_JPS02.widgets.ShipToSelectionWidget" row="4" col="4" erow="19" ecol="79" componentSettings="startCol:4|endRow:19|columnBreaks:18,50,79|endCol:79|excludeCols:|visualTableDefaultValues:false|numberOfTitleRows:0|startRow:4|includeEmptyRows:true|excludeRows:|" componentIdentifier="ShipToSelectionCombination" applyTextReplacement="true"/></td>
					</tr>
				</table>
	
				</div>
	
	
	
				</td>
	
				<td background="../common/images/right_middle4.gif"></td>
			</tr>
	
			<tr>
				<td height="5" background="../common/images/left_bottom4.gif"></td>
				<td background="../common/images/center_middle_bottom4.gif"></td>
				<td height="5" background="../common/images/right_bottom4.gif"></td>
			</tr>
		</table>
	<div class="clear5px"></div>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="5" height="5" background="../common/images/left_top2.gif"></td>
			<td background="../common/images/center_middle_top2.gif"></td>
			<td width="5" height="5" background="../common/images/right_top2.gif"></td>
		</tr>
		<tr>
			<td background="../common/images/left_middle2.gif"></td>			
			<td align="center" valign="middle" class="topbar_headingtxtrad">&nbsp;
				<HATS:Component col="2" alternate=""
					widget="com.ibm.hats.transform.widgets.LabelWidget"
					alternateRenderingSet="" erow="24" textReplacement=""
					widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
					type="com.ibm.hats.transform.components.TextComponent" ecol="79"
					componentSettings="" row="24" />&nbsp;
			</td>				
			<td background="../common/images/right_middle2.gif"></td>
		</tr>
		<tr>
			<td height="5" background="../common/images/left_bottom2.gif"></td>
			<td background="../common/images/center_middle_bottom2.gif"></td>
			<td height="5" background="../common/images/right_bottom2.gif"></td>
		</tr>

	</table>
	</div>
	
	
		<div class="clear"></div>
	
		<div class="btnmain">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" style="border: none;">
				<div>
				<table width="auto" border="0" cellspacing="5" cellpadding="0">
					<tr>
						<td style="border: none;">
							<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');">Back</a></div>
						</td>
						<td style="border: none;">
							<div class="allbtn"><a href="javascript:ms('[enter]','hatsportletid');">NEW SHIP TO</a></div>
						</td>
					</tr>
				</table>
				</div>
				</td>
				
				<!--<td width="162" align="center">
				<div style="border: 1px solid #8b794f; margin: 0 4px 0 0;">
				<table width="auto" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="10" bgcolor="#d1c4a2">&nbsp;</td>
						<td bgcolor="#d1c4a2">Export Data : &nbsp;&nbsp;</td>
						<td bgcolor="#d1c4a2"><a style="cursor: pointer"
							onclick="pdfPopUp();"><img
							src="../common/images/pdf_icon_20x20.png" title="PDF" width="24"
							height="24" border="0" align="absmiddle" /></a>&nbsp;&nbsp;</td>
						<td bgcolor="#d1c4a2"><a style="cursor: pointer"
							onclick="excelPopUp();"><img
							src="../common/images/excel.png" title="Excel" width="24"
							height="24" border="0" align="absmiddle" /></a></td>
						<td width="10" bgcolor="#d1c4a2">&nbsp;</td>
					</tr>
				</table>
				</div>
				</td>
			--></tr>
		</table>
		
		</div>

	</div>



</HATS:Form>


</body>
</html>
