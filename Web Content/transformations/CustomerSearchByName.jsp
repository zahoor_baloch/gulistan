<%//out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<% //out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<input type="hidden" name="in_402_1" id="in_402_1"/>
<input type="hidden" name="in_482_1" id="in_482_1"/>
<input type="hidden" name="in_562_1" id="in_562_1"/>
<input type="hidden" name="in_642_1" id="in_642_1"/>
<input type="hidden" name="in_722_1" id="in_722_1"/>
<input type="hidden" name="in_802_1" id="in_802_1"/>
<input type="hidden" name="in_882_1" id="in_882_1"/>
<input type="hidden" name="in_962_1" id="in_962_1"/>
<input type="hidden" name="in_1042_1" id="in_1042_1"/>
<input type="hidden" name="in_1122_1" id="in_1122_1"/>
<input type="hidden" name="in_1202_1" id="in_1202_1"/>
<input type="hidden" name="in_1282_1" id="in_1282_1"/>
<input type="hidden" name="in_1362_1" id="in_1362_1"/>
<input type="hidden" name="in_1442_1" id="in_1442_1"/>
<input type="hidden" name="in_1522_1" id="in_1522_1"/>
<input type="hidden" name="in_1602_1" id="in_1602_1"/>

<!-- Insert your HATS component tags here. -->

<!--ddd-->
<div class="container">
<div class="heading_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" class="topbar_headingtxtred1">Customer Search</td>

    <td width="34%" align="center" class="topbar_headingtxt2">&nbsp;</td>
    <td width="33%" align="right" class="topbar_headingtxt3"></td>
  </tr>
</table>
</div>

<div class="clear5px"></div>

<div class="heading_bg" style="display:none;">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>

      <td height="26" align="center" valign="middle" class="tiltletxt"></td>
    </tr>
  </table>
</div>

<div class="clear10px" style="display:none;"></div>


<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>

    <td width="5" height="5" background="../common/images/left_top3.gif"></td>
    <td background="../common/images/center_middle_top3.gif"></td>
    <td width="5" height="5" background="../common/images/right_top3.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle3.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
    <table width="auto" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
        <tr>

          <td bgcolor="#FFFFFF" class="txt1"><strong>Customer:</strong></td>
          <td>
            <HATS:Component col="12" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fieldtxt5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="41"
						componentSettings="" row="2" />
            </td>
          <td bgcolor="#FFFFFF" class="txt1"><strong>Type Number or Complete/Partial Name.</strong></td>
          <td bgcolor="#FFFFFF" style="display: none;"><img src="../common/images/Search-icon.png" width="24" height="24" /></td>
          <td align="center" bgcolor="#FFFFFF">
            <div id="vista_toolbar2">

              <div align="center">
              <ul>
                 <li><a href="javascript:ms('[enter]','hatsportletid');" title="Enter"><span>Enter</span></a></li>
              </ul>   
              </div>
              </div>
            </td>
        </tr>
        </table>

    </td>
    <td background="../common/images/right_middle3.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom3.gif"></td>
    <td background="../common/images/center_middle_bottom3.gif"></td>
    <td height="5" background="../common/images/right_bottom3.gif"></td>
  </tr>
</table>

<div class="clear5px"></div>

<div class="linsprtd"></div>

</div>

<div class="clear5px"></div>

<!--Menu Start-->
<div class="menubgg">
      
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>

    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td height="330" background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="top">
      
      <div style="vertical-align: top; overflow:auto; height:330px; border:solid 1px #CCC;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <thead>

                                                        <tr class="oddRowdd">
                                                          <td width="50" align="right" style="padding: 3px 10px 3px 3px;">Cust</td>
                                                          <td width="200" align="left" class="leftline2" style="padding: 3px 3px 3px 10px;">Address</td>
                                                          <td align="left" style="padding: 3px;"></td>
                                                          </tr>
                                                        </thead>
                                                      <tr class="evenRowd" id="record_1" onclick ="setCursorPosition(402, 'HATSForm');checkInput2('in_402_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='evenRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="6" /></td>

                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="6" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="6" /></td>
                                                        </tr>
                                                      <tr class="oddRowd" id="record_2" onclick ="setCursorPosition(482, 'HATSForm');checkInput2('in_482_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='oddRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="7" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="7" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="7" /></td>

                                                        </tr>
                                                      <tr class="evenRowd" id="record_3" onclick ="setCursorPosition(562, 'HATSForm');checkInput2('in_562_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='evenRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="8" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="8" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="8" /></td>
                                                        </tr>
                                                      <tr class="oddRowd" id="record_4" onclick ="setCursorPosition(642, 'HATSForm');checkInput2('in_642_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='oddRowd'">

                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="9" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="9" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="9" /></td>
                                                        </tr>
                                                      <tr class="evenRowd" id="record_5" onclick ="setCursorPosition(722, 'HATSForm');checkInput2('in_722_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='evenRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="10" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="10" /></td>

                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="10" /></td>
                                                        </tr>
                                                      <tr class="oddRowd" id="record_6" onclick ="setCursorPosition(802, 'HATSForm');checkInput2('in_802_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='oddRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="11" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="11" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="11" /></td>
                                                        </tr>

                                                      <tr class="evenRowd" id="record_7" onclick ="setCursorPosition(882, 'HATSForm');checkInput2('in_882_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='evenRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="12" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="12" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="12" /></td>
                                                        </tr>
                                                      <tr class="oddRowd" id="record_8" onclick ="setCursorPosition(962, 'HATSForm');checkInput2('in_962_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='oddRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="13" /></td>

                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="13" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="13" /></td>
                                                        </tr>
                                                      <tr class="evenRowd" id="record_9" onclick ="setCursorPosition(1042, 'HATSForm');checkInput2('in_1042_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='evenRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="14" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="14" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="14" /></td>

                                                        </tr>
                                                      <tr class="oddRowd" id="record_10" onclick ="setCursorPosition(1122, 'HATSForm');checkInput2('in_1122_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='oddRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="15" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="15" /></td>
                                                      </tr>
                                                      <tr class="evenRowd" id="record_11" onclick ="setCursorPosition(1202, 'HATSForm');checkInput2('in_1202_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='evenRowd'">

                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="16" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="16" /></td>
                                                      </tr>
                                                      <tr class="oddRowd" id="record_12" onclick ="setCursorPosition(1282, 'HATSForm');checkInput2('in_1282_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='oddRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="17" /></td>

                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="17" /></td>
                                                      </tr>
                                                      <tr class="evenRowd" id="record_13" onclick ="setCursorPosition(1362, 'HATSForm');checkInput2('in_1362_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='evenRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="18" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="18" /></td>
                                                      </tr>

                                                      <tr class="oddRowd" id="record_14" onclick ="setCursorPosition(1442, 'HATSForm');checkInput2('in_1442_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='oddRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="19" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="19" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="19" /></td>
                                                      </tr>
                                                      
                                                      <tr class="oddRowd" id="record_15" onclick ="setCursorPosition(1522, 'HATSForm');checkInput2('in_1522_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='oddRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="20" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="20" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="20" /></td>
                                                      </tr>
                                                      
                                                      <tr class="oddRowd" id="record_16" onclick ="setCursorPosition(1602, 'HATSForm');checkInput2('in_1602_1', 'X','hidden');ms('[enter]', 'HATSForm')" onMouseOver="this.className='tableRowClickEffect1d'" onMouseOut="this.className='oddRowd'">
                                                        <td align="right" style="padding-right:10px;"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="14"
						componentSettings="" row="21" /></td>
                                                        <td align="left" class="leftline" style="padding-left:10px;"><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="45"
						componentSettings="" row="21" /></td>
                                                        <td align="left"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="78"
						componentSettings="" row="21" /></td>
                                                      </tr>
                </table>
                                                    </div>

      
      </td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>

    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
</table>

<div class="clear2px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="middle" class="topbar_headingtxtrad">
      &nbsp;<HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="24" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="24" />&nbsp;
      </td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
  
</table>

</div>
<!--Menu End-->



<div class="btnmain">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">

    <tr>
      <td align="center">
        <table width="auto" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td>
              <div id="vista_toolbar">
              <div align="center">
              <ul>
                 <li><a href="javascript:ms('[pf3]','hatsportletid');" title="Back"><span>Back</span></a></li>

                 <li><a href="javascript:ms('[pf5]','hatsportletid');" title="More"><span>More</span></a></li>
              </ul>   
              </div>
              </div>
            </td>
            </tr>
        </table></td>
    </tr>
  </table>

</div>
</div>
<!--ddd-->


	<%
	//fetching global variables for checking empty rows
	
	String custSel_record_1 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_1", true).getString(0); 
	String custSel_record_2 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_2", true).getString(0);
	String custSel_record_3 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_3", true).getString(0);
	String custSel_record_4 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_4", true).getString(0);
	String custSel_record_5 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_5", true).getString(0);
	String custSel_record_6 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_6", true).getString(0);
	String custSel_record_7 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_7", true).getString(0);
	String custSel_record_8 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_8", true).getString(0);
	String custSel_record_9 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_9", true).getString(0);
	String custSel_record_10 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_10", true).getString(0);
	String custSel_record_11 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_11", true).getString(0);
	String custSel_record_12 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_12", true).getString(0);
	String custSel_record_13 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_13", true).getString(0);
	String custSel_record_14 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_14", true).getString(0);
	String custSel_record_15 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_15", true).getString(0);
	String custSel_record_16 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("custSel_record_16", true).getString(0);
	
	
	
	
	%>

	<script language="JavaScript">
	
	var custSel_record_1 = "<%= custSel_record_1.trim()%>";
	var custSel_record_2 = "<%= custSel_record_2.trim()%>";
	var custSel_record_3 = "<%= custSel_record_3.trim()%>";
	var custSel_record_4 = "<%= custSel_record_4.trim()%>";
	var custSel_record_5 = "<%= custSel_record_5.trim()%>";
	var custSel_record_6 = "<%= custSel_record_6.trim()%>";
	var custSel_record_7 = "<%= custSel_record_7.trim()%>";
	var custSel_record_8 = "<%= custSel_record_8.trim()%>";
	var custSel_record_9 = "<%= custSel_record_9.trim()%>";
	var custSel_record_10 = "<%= custSel_record_10.trim()%>";
	var custSel_record_11 = "<%= custSel_record_11.trim()%>";
	var custSel_record_12 = "<%= custSel_record_12.trim()%>";
	var custSel_record_13 = "<%= custSel_record_13.trim()%>";
	var custSel_record_14 = "<%= custSel_record_14.trim()%>";
	var custSel_record_15 = "<%= custSel_record_15.trim()%>";
	var custSel_record_16 = "<%= custSel_record_16.trim()%>";
	
	if(custSel_record_1 == ""){
	document.getElementById('record_1').style.display = 'none';
	}
	if(custSel_record_2 == ""){
	document.getElementById('record_2').style.display = 'none';
	}
	if(custSel_record_3 == ""){
	document.getElementById('record_3').style.display = 'none';
	}
	if(custSel_record_4 == ""){
	document.getElementById('record_4').style.display = 'none';
	}
	if(custSel_record_5 == ""){
	document.getElementById('record_5').style.display = 'none';
	}
	if(custSel_record_6 == ""){
	document.getElementById('record_6').style.display = 'none';
	}
	if(custSel_record_7 == ""){
	document.getElementById('record_7').style.display = 'none';
	}
	if(custSel_record_8 == ""){
	document.getElementById('record_8').style.display = 'none';
	}
	if(custSel_record_9 == ""){
	document.getElementById('record_9').style.display = 'none';
	}
	if(custSel_record_10 == ""){
	document.getElementById('record_10').style.display = 'none';
	}
	if(custSel_record_11 == ""){
	document.getElementById('record_11').style.display = 'none';
	}
	if(custSel_record_12 == ""){
	document.getElementById('record_12').style.display = 'none';
	}
	if(custSel_record_13 == ""){
	document.getElementById('record_13').style.display = 'none';
	}
	if(custSel_record_14 == ""){
	document.getElementById('record_14').style.display = 'none';
	}
	if(custSel_record_15 == ""){
	document.getElementById('record_15').style.display = 'none';
	}
	if(custSel_record_16 == ""){
	document.getElementById('record_16').style.display = 'none';
	}
	</script>

	<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<% //out.println("<!--blank.jsp"); %>
</body>
</html>
<% //out.println("-->"); %>