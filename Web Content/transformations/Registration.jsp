<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>

<HATS:Form>

	
<input type="hidden" name="in_528_1">


<div class="container">


<div class="clear46px"></div>

<!--Details Start-->
<div class="detailsmainnew">
	<div class="detailstxtnew">
	Thank you for your interest in Gulistan's online services.  Please enter your account number and zip code to continue.
	</div>
</div>
<!--Details End-->

<div class="clear20px"></div>

<!--Login Start-->
<div class="login_bgnew">

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="center" valign="top">
	<table width="350" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td width="130" height="83">
		<div class="clear30px"></div>
		<div class="logintxt">Your Gulistan&nbsp;&nbsp; Account Number :</div>
		</td>
        <td height="83">
        <div class="clear40px"></div>
        <HATS:Component col="31" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:logginfield|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="37"
						componentSettings="" row="5" />
        </td>
		</tr>
		<tr>
        
        <td height="23">
		<div class="logintxt">Your Zip Code :</div>
		</td>
        <td height="23">
        <HATS:Component col="31" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:logginfield|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="36"
						componentSettings="" row="6" />
        </td>
		</tr>
		<tr><td height="20" colspan="2"><div class="errortxtred09"><HATS:Component
							col="39" alternate=""
							widget="com.ibm.hats.transform.widgets.LabelWidget"
							alternateRenderingSet="" erow="5" textReplacement=""
							widgetSettings="trim:true|style:|labelStyleClass:|"
							type="com.ibm.hats.transform.components.TextComponent" ecol="69"
							componentSettings="" row="5" /></div></td></tr>
        <tr><td height="20" colspan="2"><div class="errortxtred09"><HATS:Component
						col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="76"
						componentSettings="" row="6" /></div></td></tr>
		<tr>
		<td colspan="2">
		<div class="clear20px"></div>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="left">&nbsp;
		
		</td>
		<td align="right">
		<div class="clickbtnmainnew">
		<div class="clickherebtn"><input name="" type="submit" value="Enter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" /></div>
		</div>
		</td>
		</tr>
	</table>
    	</td>
        </tr>
	</table>
		</td>
		</tr>
	</table>
</div>
<!--Login End-->

<div class="clear"></div>

<div class="btnmain">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center">
<table width="auto" border="0" cellspacing="5" cellpadding="0">
	<tr>
	<td>
	<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');" title="Back">Back</a></div>
	</td>
	</tr>
</table>
	</td>
	</tr>
</table>
</div>


</div>

<script language="JavaScript">
	document.getElementsByName('in_100_10')[0].focus();
</script>

</HATS:Form>

</body>
</html>