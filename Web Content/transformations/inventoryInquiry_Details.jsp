<%// out.println("<!--blank.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<% //out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<SCRIPT type="text/javascript" src="../common/gulistan.js">
</SCRIPT>
<!-- Start of the HATS form. -->
<HATS:Form>
<input type="hidden" name="hatsgv_homeClicked" id="hatsgv_homeClicked"/>
<input type="hidden" name="hatsgv_logoffClicked" id="hatsgv_logoffClicked"/>
<input type="hidden" name="hatsgv_menuItemGV" id="hatsgv_menuItemGV"/>
<input type="hidden" name="hatsgv_menuPageGV" id="hatsgv_menuPageGV"/>
<input type="hidden" name="hatsgv_menuFieldGV" id="hatsgv_menuFieldGV"/>
<%session.setAttribute("orderInquiry_ProductSearch_locate_sv1",null); %>
<!-- Insert your HATS component tags here. -->

<!--ddd-->
<div class="container">

<div class="heading_bg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
			<td width="33%" class="topbar_headingtxtred" colspan="3" align="center"><HATS:Component
				col="3" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings=""
				type="com.ibm.hats.transform.components.TextComponent" ecol="78"
				componentSettings="" row="2" /></td>
		</tr>
</table>
</div>



<div class="clear5px"></div>

<div class="heading_bg" style="display:none;">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="26" align="center" valign="middle" class="tiltletxt">Type in complete or partial style and/or color description.</td>
    </tr>
  </table>
</div>

<div class="clear10px" style="display:none;"></div>


<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top3.gif"></td>
    <td background="../common/images/center_middle_top3.gif"></td>
    <td width="5" height="5" background="../common/images/right_top3.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle3.gif"></td>
    <td align="center" valign="top" bgcolor="#d1c4a2">
    <table width="auto" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
        <tr>
          <td bgcolor="#FFFFFF" class="txt1"><strong>Style :</strong></td>
          <td valign="middle">
            <HATS:Component col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt4|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="18"
						componentSettings="" row="4" BIDIOpposite="false" />
          </td>
          <td bgcolor="#FFFFFF" class="txt1"><strong>Color :</strong></td>
          <td valign="middle">
            <HATS:Component col="29" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:fieldtxt4|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="33"
						componentSettings="" row="4" BIDIOpposite="false" />
          </td>
          <td bgcolor="#FFFFFF" class="txt1"><strong>Backing Codes (Bk) :</strong></td>
          <td valign="middle">
           <HATS:Component col="41" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="useGlobalVariable:false|autoSubmitOnSelect:false|eliminateMaxlengthInIdeographicFields:false|submitButtonCaption:Submit|dataModeCEPValue:*|readOnly:false|overrideSize:false|sharedGVs:false|trimSpacesOnInputs:false|useHints:true|inputFieldStyleClass:HATSINPUT|size:1|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|dropdownStyleClass:HATSDROPDOWN|underlineStyle:text-decoration: underline|captionVariable:|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showSubmitButton:false|checkboxStyleClass:HATSINPUT|columnsPerRow:1|captionType:DESCRIPTION|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|valueVariable:|autoSubmitKey:[enter]|stripUnderlinesOnInputs:false|caption:|optionStyleClass:HATSOPTION|stringListItems:Action Back=AB;Unitary=UN|tableCellStyleClass:|style:background-color: #ececec|useString:true|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="42"
						componentSettings="" row="4" />
          </td>
          <td bgcolor="#FFFFFF" class="txt1"><strong>Width :</strong></td>
          <td valign="middle">
           &nbsp;<HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="54"
						componentSettings="" row="4" />&nbsp;&nbsp;
          </td>
          <td bgcolor="#FFFFFF" class="txt1"> <strong>Whse(M/L/S):</strong></td>
          <td valign="middle">
           &nbsp;<HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="autoSubmitOnSelect:false|useGlobalVariable:false|eliminateMaxlengthInIdeographicFields:false|submitButtonCaption:Submit|dataModeCEPValue:*|readOnly:false|overrideSize:false|sharedGVs:false|trimSpacesOnInputs:false|useHints:true|inputFieldStyleClass:HATSINPUT|size:1|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|dropdownStyleClass:HATSDROPDOWN|underlineStyle:text-decoration: underline|captionVariable:|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showSubmitButton:false|checkboxStyleClass:HATSINPUT|columnsPerRow:1|captionType:DESCRIPTION|preserveColors:false|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|valueVariable:|autoSubmitKey:[enter]|stripUnderlinesOnInputs:false|caption:|optionStyleClass:HATSOPTION|stringListItems:Action Back=AB;Unitary=UN|tableCellStyleClass:|style:background-color: #ececec|useString:true|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="4" BIDIOpposite="false" />&nbsp;&nbsp;
          </td>
          
          <td bgcolor="#FFFFFF"><img onmouseover="this.style.cursor ='hand'"; onmouseout="this.style.cursor = 'auto'"; onclick="ms('[pf4]','hatsportletid');" src="../common/images/Search-icon.png" width="24" height="24" /></td>
          <td align="center" bgcolor="#FFFFFF">
          <div class="allbtn"><a title="Enter" href="javascript:ms('[enter]','hatsportletid');">Enter</a></div>
          
          </td>
        </tr>
        </table>
    </td>
    <td background="../common/images/right_middle3.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom3.gif"></td>
    <td background="../common/images/center_middle_bottom3.gif"></td>
    <td height="5" background="../common/images/right_bottom3.gif"></td>
  </tr>
</table>

<div class="clear5px"></div>

<div class="linsprtd"></div>

</div>


<div class="clear5px"></div>

<!--Menu Start-->
<div class="menubgg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5" height="5" background="../common/images/left_top3.gif"></td>
    <td background="../common/images/center_middle_top3.gif"></td>
    <td width="5" height="5" background="../common/images/right_top3.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle3.gif"></td>
    <td align="center" valign="top">
      <table width="100%" border="0" cellpadding="3" cellspacing="0" class="fillbor1">
        <tr>
          <td bgcolor="#d1c4a2" class="txt1"><strong>Style</strong></td>
          <td class="txt1"><HATS:Component col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="27"
						componentSettings="" row="9" /></td>
          <td bgcolor="#d1c4a2" class="txt1"><strong>Color</strong></td>
          <td class="txt1"><HATS:Component col="37" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="56"
						componentSettings="" row="9" /></td>
          <td bgcolor="#d1c4a2" class="txt1"><strong>Whse</strong></td>
          <td class="txt1"><HATS:Component col="65" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="9" /></td>
        </tr>
        </table>
      
      </td>
    <td background="../common/images/right_middle3.gif"></td>
  </tr>
  <tr>
    <td height="5" background="../common/images/left_bottom3.gif"></td>
    <td background="../common/images/center_middle_bottom3.gif"></td>
    <td height="5" background="../common/images/right_bottom3.gif"></td>
  </tr>
</table>
      
      
      <div class="clear5px"></div>
      
      <div class="linsprtd"></div>
      
      <div class="clear5px"></div>
      
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td height="218" background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="top">
      
     <div class="detailsmainew2">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td width="8" height="8" background="../common/images/left_top.gif"></td>
					    <td background="../common/images/center_middle_top.gif"></td>
					    <td width="8" height="8" background="../common/images/right_top.gif"></td>
					  </tr>
					  <tr>
					    <td background="../common/images/left_middle.gif"></td>
					    <td align="left">
					    <div class="clear"></div>
					    
					    <div id="topbtn22">
				      	<div align="center">
				      	<ul>
				      	<li><a><HATS:Component col="5" alternate=""
											widget="com.ibm.hats.transform.widgets.LabelWidget"
											alternateRenderingSet="" erow="19" textReplacement=""
											widgetSettings="trim:true|style:|labelStyleClass:|"
											type="com.ibm.hats.transform.components.TextComponent" ecol="46"
											componentSettings="" row="19" /></a></li>
				      	<li><a><HATS:Component col="5" alternate=""
											widget="com.ibm.hats.transform.widgets.LabelWidget"
											alternateRenderingSet="" erow="20" textReplacement=""
											widgetSettings="trim:true|style:|labelStyleClass:|"
											type="com.ibm.hats.transform.components.TextComponent" ecol="46"
											componentSettings="" row="20" /></a></li>
				      	<li><a><HATS:Component col="54" alternate=""
											widget="com.ibm.hats.transform.widgets.LabelWidget"
											alternateRenderingSet="" erow="19" textReplacement=""
											widgetSettings="trim:true|style:|labelStyleClass:|"
											type="com.ibm.hats.transform.components.TextComponent" ecol="77"
											componentSettings="" row="19" /></a></li>
				      	<li><span style="color: black;"><HATS:Component col="50"
											alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
											alternateRenderingSet="" erow="20" textReplacement=""
											widgetSettings="trim:true|style:|labelStyleClass:|"
											type="com.ibm.hats.transform.components.TextComponent" ecol="79"
											componentSettings="" row="20" /></span></li>
				      	</ul>
				      	</div>
				      	</div>
					    
					    <div class="clear"></div>
					    </td>
					    <td background="../common/images/right_middle.gif"></td>
					  </tr>
					  <tr>
					    <td height="8" background="../common/images/left_bottom.gif"></td>
					    <td background="../common/images/center_middle_bottom.gif"></td>
					    <td height="8" background="../common/images/right_bottom.gif"></td>
					  </tr>
					</table>
	  </div>
      
      <div class="clear3px"></div>
      
      <div style="vertical-align: top; overflow:auto; height:216px; border:solid 1px #CCC;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <thead>
                                                        <tr class="oddRowdd">
                                                          <td width="180" align="left" style="padding: 3px;">Roll #</td>
                                                          <td width="180" align="left" class="leftline2" style="padding: 3px;">Length</td>
                                                          <td width="180" align="left" class="leftline2" style="padding: 3px;">Sq Yds</td>
                                                          <td width="180" align="left" class="leftline2" style="padding: 3px;">Lot #</td>
                                                          <td width="180" align="left" class="leftline2" style="padding: 3px;">Seq</td>
                                                          <td align="left" class="leftline2" style="padding: 3px;">B</td>
                                                          </tr>
                                                        </thead>
                                                      <tr class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="12" /> &nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="12" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="12" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="33"
						componentSettings="" row="12" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="34" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="37"
						componentSettings="" row="12" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="39"
						componentSettings="" row="12" /></td>
                                                        </tr>
                                                      <tr class="oddRowd">
                                                        <td align="left"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="13" /> &nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="13" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="13" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="33"
						componentSettings="" row="13" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="34" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="37"
						componentSettings="" row="13" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="39"
						componentSettings="" row="13" /></td>
                                                        </tr>
                                                      <tr class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="14" /> &nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="14" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="14" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="33"
						componentSettings="" row="14" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="34" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="37"
						componentSettings="" row="14" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="39"
						componentSettings="" row="14" /></td>
                                                        </tr>
                                                      <tr class="oddRowd">
                                                        <td align="left"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="15" /> &nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="33"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="34" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="37"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="39"
						componentSettings="" row="15" /></td>
                                                        </tr>
                                                      <tr class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="16" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="33"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="34" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="37"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="39"
						componentSettings="" row="16" /></td>
                                                        </tr>
                                                      <tr class="oddRowd">
                                                        <td align="left"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="17" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="33"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="34" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="37"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="39"
						componentSettings="" row="17" /></td>
                                                        </tr>
                                                      <tr class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="12"
						componentSettings="" row="18" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="14" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="33"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="34" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="37"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="39" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="39"
						componentSettings="" row="18" /></td>
                                                        </tr>
                                                      <tr class="oddRowd">
                                                        <td align="left"><HATS:Component
						col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="52"
						componentSettings="" row="12" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="54" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="59"
						componentSettings="" row="12" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="61" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="66"
						componentSettings="" row="12" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="68" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="73"
						componentSettings="" row="12" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="77"
						componentSettings="" row="12" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="79" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="12" /></td>
                                                        </tr>
                                                      <tr class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="52"
						componentSettings="" row="13" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="54" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="59"
						componentSettings="" row="13" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="61" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="66"
						componentSettings="" row="13" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="68" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="73"
						componentSettings="" row="13" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="77"
						componentSettings="" row="13" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="79" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="13" /></td>
                                                        </tr>
                                                      <tr class="oddRowd">
                                                        <td align="left"><HATS:Component
						col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="52"
						componentSettings="" row="14" />&nbsp;</td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="54" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="59"
						componentSettings="" row="14" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="61" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="66"
						componentSettings="" row="14" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="68" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="73"
						componentSettings="" row="14" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="77"
						componentSettings="" row="14" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="79" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="14" /></td>
                                                      </tr>
                                                      <tr class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="52"
						componentSettings="" row="15" />&nbsp; </td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="54" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="59"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="61" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="66"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="68" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="73"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="77"
						componentSettings="" row="15" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="79" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="15" /></td>
                                                      </tr>
                                                      <tr class="oddRowd">
                                                        <td align="left"><HATS:Component
						col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="52"
						componentSettings="" row="16" />&nbsp; </td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="54" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="59"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="62" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="67"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="68" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="73"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="77"
						componentSettings="" row="16" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="79" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="16" /></td>
                                                      </tr>
                                                      <tr class="evenRowd">
                                                        <td align="left"><HATS:Component
						col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="52"
						componentSettings="" row="17" />&nbsp; </td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="54" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="59"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="61" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="66"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="68" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="73"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="77"
						componentSettings="" row="17" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="79" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="17" /></td>
                                                      </tr>
                                                      <tr class="oddRowd">
                                                        <td align="left"><HATS:Component
						col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="52"
						componentSettings="" row="18" />&nbsp; </td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="54" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="59"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="61" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="66"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="68" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="73"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="77"
						componentSettings="" row="18" /></td>
                                                        <td align="left" class="leftline"><HATS:Component
						col="79" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="18" /></td>
                                                      </tr>
                </table>
                                                    </div>
      
      </td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
 
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
</table>

<div class="clear2px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" height="5" background="../common/images/left_top2.gif"></td>
    <td background="../common/images/center_middle_top2.gif"></td>
    <td width="6" height="5" background="../common/images/right_top2.gif"></td>
  </tr>
  <tr>
    <td background="../common/images/left_middle2.gif"></td>
    <td align="center" valign="middle" class="topbar_headingtxtrad">&nbsp;
      <HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="21" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:HATSCAPTION|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="21" />&nbsp;
      </td>
    <td background="../common/images/right_middle2.gif"></td>
  </tr>
  <tr>
    <td height="8" background="../common/images/left_bottom2.gif"></td>
    <td background="../common/images/center_middle_bottom2.gif"></td>
    <td height="8" background="../common/images/right_bottom2.gif"></td>
  </tr>
  
</table>

</div>
<!--Mene End-->

<div class="clear0px"></div>
	<div class="btnmain">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="center" style="border: none;">
	<table width="auto" border="0" cellspacing="5" cellpadding="0">
		<tr>
		<td style="border: none;">
		<div class="allbtn"><a href="javascript:ms('[pf3]','hatsportletid');" title="Exit">Exit</a></div>
		</td>
		</tr>
	</table>
	</td>
		<td width="78" align="center">
	<div id="nextButton" class="next_btn"><a href="javascript:ms('[pagedn]','HATSForm');"></a></div>
	<div id="previousButton" class="prev_btn"><a href="javascript:ms('[pageup]','HATSForm');"></a></div>
		</td>
		</tr>
	</table>
	</div>

</div>
<!--ddd-->

	
	<%
	String isNextPage02 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("isNextPage02", true).getString(0);
 	//session.setAttribute("inventoryInquiry_Details_sv1",null); 
	String inventoryInquiry_noRecords = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("inventoryInquiry_noRecords", true).getString(0);
	String inventoryInquiry_Details_st1 = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("inventoryInquiry_Details_st1", true).getString(0); 
 	String isPrevPage = "false";
 	if(session.getAttribute("inventoryInquiry_Details_sv1") == null){
	session.setAttribute("inventoryInquiry_Details_sv1",inventoryInquiry_Details_st1);
	isPrevPage = "true";
	//System.out.println("Null check true, value = "+inventoryInquiry_Details_st1);
	}
	else{
	if(session.getAttribute("inventoryInquiry_Details_sv1").toString().equalsIgnoreCase(inventoryInquiry_Details_st1)){
	isPrevPage = "true";
	//System.out.println("Null check false, value = "+inventoryInquiry_Details_st1);
	}
	}
 	%>
	<script language="JavaScript">
	var isNextPage = "<%= isNextPage02%>";
	var isPrevPage = "<%= isPrevPage%>";
	var inventoryInquiry_noRecords = "<%= inventoryInquiry_noRecords%>";
	if(isNextPage != "+"){
	document.getElementById("nextButton").innerHTML = "";
	document.getElementById("nextButton").className = "next_btndisable";
	}
	if(isPrevPage == "true" || inventoryInquiry_noRecords == "INVALID STYLE/COLOR/BACKING/WAREHOUSE - TRY F4=PRODUCT ALPHA SEARCH"){
	document.getElementById("previousButton").innerHTML = "";
	document.getElementById("previousButton").className = "prev_btndisable";
	}
	//alert(isPrevPage);
	//if(isLocatePage == "yes"){
	//document.getElementById("previousButton").style.visibility = "hidden";
	//document.getElementById("nextButton").style.visibility = "hidden";
	//}
	</script>

	<HATS:HostKeypad />
</HATS:Form>
<!-- End of the HATS form. -->
<HATS:OIA/>
<% //out.println("<!--blank.jsp"); %>
</body>
</html>
<% //out.println("-->"); %>