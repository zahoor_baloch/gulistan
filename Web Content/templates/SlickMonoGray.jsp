<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de -->
<HTML LANG="en">


<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=1.6,0,04/07/28,16:52:05  -->
<HEAD>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<LINK rel="stylesheet" href="../common/stylesheets/monochrometheme.css" type="text/css">
<LINK rel="stylesheet" href="../common/stylesheets/slickWhiteBackground.css" type="text/css">
<LINK rel="stylesheet" href="../common/stylesheets/reverseVideoMono.css" type="text/css">
</HEAD>
<BODY>
<table width="100%">
	<TBODY>
		<tr>
			<td><span class="TOP_TITLE">My Company</span></td>
			<td width="70%">
			<table width="100%" class="TEMPLATE_TOPBAR">
				<TBODY>
					<tr>
						<td height="40" valign="bottom" align="right"><span
							class="TOP_SUBTITLE">innovative technologies designed for the future</span></td>
					</tr>
				</TBODY>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" width="100%">
			<table width="100%">
				<TBODY>
					<tr>
						<td valign="top" width="20%">
						<table width="100%" class="TEMPLATE_SIDEBAR">
							<TBODY>
							  <!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
                                <!-- 
								<TR>
								<TD>
									<a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a>
  								</TD>
  								</TR>
  								-->
								<TR>
									<TD class="TEMPLATE_LINKCELL"><SPAN style="font-size: 1em;">Information
									on My Company:</SPAN></TD>
								</TR>
								<TR>
									<TD class="TEMPLATE_LINKCELL"><A CLASS="TEMPLATE_LINK"
										href="http://www.ibm.com">My Company Home Page</A></TD>
								</TR>
								<TR>
									<TD class="TEMPLATE_LINKCELL"><A CLASS="TEMPLATE_LINK"
										href="http://www.ibm.com">My Company Map</A></TD>
								</TR>
								<TR>
									<TD class="TEMPLATE_LINKCELL"><A CLASS="TEMPLATE_LINK"
										href="http://www.ibm.com">My Company Employees</A></TD>
								</TR>
								<TR>
									<TD class="TEMPLATE_LINKCELL"><A CLASS="TEMPLATE_LINK"
										href="http://www.ibm.com">Jobs at My Company</A></TD>
								</TR>
								<TR>
									<TD class="TEMPLATE_LINKCELL"><A CLASS="TEMPLATE_LINK"
										href="http://www.ibm.com">My Company Articles</A></TD>
								</TR>
								<TR>
									<TD class="TEMPLATE_LINKCELL"><A CLASS="TEMPLATE_LINK"
										href="http://www.ibm.com">Support</A></TD>
								</TR>
								<TR>
									<TD class="TEMPLATE_LINKCELL">
									<HR>
									</td>
								</TR>
								<tr>
									<td>
									<DIV align="center"><HATS:ApplicationKeypad settings="layout:vertical" /></DIV>
									</td>
								</tr>
							</TBODY>
						</table>
						</td>
						<td width="70%" valign="top">
						<a name="navskip"></a>
						<HATS:Transform skipBody="true">
							<P>Host screen transformation will be shown here</P>
						</HATS:Transform></td>
					</tr>
				</TBODY>
			</table>
			</td>
		</tr>

	</TBODY>
</table>
</BODY>
</HTML>
