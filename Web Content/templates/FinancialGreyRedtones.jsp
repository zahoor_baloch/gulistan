<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=%I%,%S%,%E%,%U%  -->


<HEAD>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<LINK rel="stylesheet" href="../common/stylesheets/graytheme.css" type="text/css">
<LINK rel="stylesheet" href="../common/stylesheets/reverseVideoGray.css" type="text/css">
<!-- embedded stylesheet to coordinate button and link colors -->
<STYLE type="text/css">

A.TOPBAR:link {color:white; font-size: 1em;}
A.TOPBAR:active {color:white; font-size: 1em;}
A.TOPBAR:visited {color:white; font-size: 1em;}

A.ApplicationKeyLink {
	color: white;
}

A.HATSLINK:link, A.HATSLINK:visited, A.HostKeyLink:link, A.HostKeyLink:visited {
	color: #841412;
}

input.ApplicationButton, select.ApplicationDropDown {
	width: 15em;
}

.COMPANYTITLE{
    color: #FFFFFF;
    font-size: x-large;
    font-style: italic;
}
.COMPANYSLOGAN{
    color: #FFFFFF;
    font-size: large;
    font-style: italic;
}


</STYLE>
</HEAD>
<BODY>
<TABLE width="100%" cellpadding="4" cellspacing="0" bgcolor="#841412" border="0">
    <TBODY>
    <TR>
    <TD>
	<TABLE width="100%" cellpadding="0" cellspacing="0" bgcolor="#841412" border="0" >
	<TBODY>
        <TR>        
            <TD nowrap="nowrap" colspan="2" align="left" bgcolor="#841412">
            <span CLASS="COMPANYTITLE">
            My Company</span>
            <BR>
            <span CLASS="COMPANYSLOGAN">
            Comprehensive and reliable financial data</span>
            </TD>
            <TD >
            <IMG src="../common/images/Financial1TopBackground.jpg" width="100%" height="80" alt="Financial1TopBackground.jpg" align="right"></TD>
        </TR>
     </TBODY>
     </TABLE>
     </TD>
     </TR>
     <TR>
     <TD>
	 <TABLE width="100%" cellpadding="0" cellspacing="0" bgcolor="#841412" border="0" >
	 <TBODY>
        <TR >          
            <TD align="left" height="24" valign="bottom" bgcolor="#616161">
            <!--Remove the comments from the following line if you wish a link to go directly to the transformation-->
            <!-- <a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a> -->
            </TD>            
            <TD  width="100%">
            <TABLE background="../common/images/Financial1ButtonBackground.jpg" width="100%" border="0" cellpadding="0" cellspacing="0">
                <TBODY>
                    <TR>
                        <TD width="60" background="../common/images/Financial1ButtonBackgroundGray.jpg" nowrap="nowrap"></TD>
                        <TD valign="middle" align="left" background="../common/images/Financial1ButtonBackgroundGray.jpg"><IMG src="../common/images/Financial1ButtonStart.jpg" alt="Financial1ButtonStart.jpg" border="0" width="14" height="24" align="right" hspace="0" vspace="0"></TD>
                        <TD align="center" background="../common/images/Financial1ButtonBackground.jpg" nowrap="nowrap"><A CLASS="TOPBAR" href="http://www.ibm.com">Home</A></TD>
                        <TD width="14" background="../common/images/Financial1ButtonBackgroundGray.jpg"><IMG src="../common/images/Financial1ButtonEnd.jpg" alt="Financial1ButtonEnd.jpg" align="left" border="0" width="14" height="24" hspace="0" vspace="0"></TD>
                        <TD width="60" background="../common/images/Financial1ButtonBackgroundGray.jpg"></TD>
                        <TD width="14" background="../common/images/Financial1ButtonBackgroundGray.jpg"><IMG src="../common/images/Financial1ButtonStart.jpg" alt="Financial1ButtonStart.jpg" border="0" width="14" height="24" align="right" hspace="0" vspace="0"></TD>
                        <TD align="center" nowrap="nowrap"><A CLASS="TOPBAR" href="http://www.ibm.com">Links</A></TD>
                        <TD background="../common/images/Financial1ButtonBackgroundGray.jpg"><IMG width="14" src="../common/images/Financial1ButtonEnd.jpg" alt="Financial1ButtonEnd.jpg" align="left" border="0" height="24" hspace="0" vspace="0"></TD>
                        <TD width="40" background="../common/images/Financial1ButtonBackgroundGray.jpg"></TD>
                        <TD width="14" background="../common/images/Financial1ButtonBackgroundGray.jpg"><IMG src="../common/images/Financial1ButtonStart.jpg" alt="Financial1ButtonStart.jpg" border="0" width="14" height="24" align="right" hspace="0" vspace="0"></TD>
                        <TD align="center" nowrap="nowrap"><A CLASS="TOPBAR" href="http://www.ibm.com">About My Company</A></TD>
                        <TD width="14" background="../common/images/Financial1ButtonBackgroundGray.jpg"><IMG src="../common/images/Financial1ButtonEnd.jpg" alt="Financial1ButtonEnd" align="left" border="0" width="14" height="24" hspace="0" vspace="0"></TD>
                        <TD width="40" background="../common/images/Financial1ButtonBackgroundGray.jpg"></TD>
                        <TD valign="middle" align="left" width="14" height="24" background="../common/images/Financial1ButtonBackgroundGray.jpg"><IMG width="14" src="../common/images/Financial1ButtonStart.jpg" alt="Financial1ButtonStart.jpg" border="0" height="24" align="right" hspace="0" vspace="0"></TD>
                        <TD align="center" nowrap="nowrap"><A CLASS="TOPBAR" href="http://www.ibm.com">Stock Quote</A></TD>
                        <TD width="14" background="../common/images/Financial1ButtonBackgroundGray.jpg"><IMG src="../common/images/Financial1ButtonEnd.jpg" alt="Financial1ButtonEnd.jpg" align="left" border="0" width="14" height="24" hspace="0" vspace="0"></TD>
                        <TD width="60" background="../common/images/Financial1ButtonBackgroundGray.jpg"></TD>
                    </TR>
                </TBODY>
            </TABLE>
            </TD>
        </TR>
        <TR bgcolor="616161">
            <TD height="100%" align="center">
            <DIV align="center">
            <TABLE height="100%" cellpadding="3" cellspacing="3" background="../common/images/Financial1LeftBackground.jpg" border="0" >
                <TBODY>
                <TR>
                <TD>
                <DIV align="center">
                <TABLE height="100%" border="0" cellpadding="0" cellspacing="0">
                <TBODY>
                    <TR>
                        <TD nowrap="nowrap" width="132" height="25" align="center" background="../common/images/Financial1LeftButton.jpg" width="14" height="24">
                        <A CLASS="TOPBAR" href="http://www.ibm.com">Home Page</A></TD>
                    </TR>
                    <TR>                    
                        <TD nowrap="nowrap" width="132" height="25" align="center" background="../common/images/Financial1LeftButton.jpg">
                        <A CLASS="TOPBAR" href="http://www.ibm.com">Financial Data</A></TD>                       
                    </TR>
                    <TR>                   
                        <TD nowrap="nowrap" width="132" height="25" align="center" background="../common/images/Financial1LeftButton.jpg">
                        <A CLASS="TOPBAR" href="http://www.ibm.com">Human Resources</A></TD>                        
                    </TR>
                    <TR>                   
                        <TD nowrap="nowrap" width="132" height="25" align="center" background="../common/images/Financial1LeftButton.jpg">
                        <A CLASS="TOPBAR" href="http://www.ibm.com">Employees</A></TD>                       
                    </TR>
                    <TR>                    
                        <TD nowrap="nowrap" width="132" height="25" align="center" background="../common/images/Financial1LeftButton.jpg">
                        <A CLASS="TOPBAR" href="http://www.ibm.com">Job Opportunities</A></TD>                        
                    </TR>
                    <TR>                    
                        <TD nowrap="nowrap" width="132" height="25" align="center" background="../common/images/Financial1LeftButton.jpg">
                        <A CLASS="TOPBAR" href="http://www.ibm.com">Location</A>
                        </TD>                        
                    </TR>
                    </TBODY>
                    </TABLE>   
                    </DIV>                                     
                    </TD>
                    </TR>
                    <TR>
					<TD height="20">&nbsp;</TD>
                    </TR>
					<TR>
					<TD>
					<DIV align="center">
               		<TABLE height="100%" border="0" cellpadding="0" cellspacing="0">
               		<TBODY>
               		<TR>
               	       <TD colspan="2"><HATS:ApplicationKeypad/></TD>
                    </TR>
                    <TR>
                        <TD height="60"></TD>
                    </TR>
                    </TBODY>
                    </TABLE>
                    </DIV>                    
                    </TD>
                    </TR>                    
                </TBODY>
            </TABLE>
            </DIV>
            </TD>
            <TD  width="100%" height="100%" bgcolor="#dddddd" valign="top">
            <a name="navskip"></a>
 			<HATS:Transform skipBody="true">
    		<DIV align="center"><P> Host screen transformation will be shown here </P></DIV>
 			</HATS:Transform>
			</TD>
        </TR>

    </TBODY>
</TABLE>
</TD>
</TR>
</TBODY>
</TABLE>
</BODY>
</HTML>
