<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<html lang="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>

<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=1.6,0,04/07/28,16:52:09  -->
<head>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<link href="../common/stylesheets/medical.css" rel="stylesheet" type="text/css">
<!--[if lt IE 7]>
<style type="text/css">
input.RHBLANK,input.RHBLUE,input.RHGREEN,input.RHRED,input.RHCYAN,
input.RHWHITE,input.RHMAGENTA,input.RHBROWN,input.RHYELLOW,input.RHGRAY 
{ background-image: none !important; }
</style>
<![endif]-->
</head>

<body style="background-color: #241A18;	color: 574d3a;">
<table id="m_wrapper">
<tr><td colspan="2" id="m_logo"><div class="m_COMPANYTITLE">My Company</div></td></tr>
	<tr>
		<td id="m_lvl1nav">
			<ul id="m_lvl1nav_list">
				<li class="m_lvl1nav_item"><a href="http://www.ibm.com/software/awdtools/hats/" >Main product</a></li>
				<li class="m_lvl1nav_item"><a href="http://www.ibm.com" >Additional products</a></li>
				<li class="m_lvl1nav_item"><a href="http://www.ibm.com/developerworks/downloads/ws/whats/" >Downloads</a></li>
				<li class="m_lvl1nav_item"><a href="http://www.ibm.com/software/awdtools/hats/support/" >Support</a></li>
				<li class="m_lvl1nav_item m_lvl1nav_active_item"><a href="javascript:;">Site</a></li>
		    </ul>
		</td>
		<td id="m_content_wrapper">
		    <div id="m_lvl2nav" >
				<ul id="m_lvl2nav_list">
					<li class="m_lvl2nav_item m_lvl2nav_active_item"><a href="javascript:;">My HATS Application</a></li>
					<li class="m_lvl2nav_item"><a href="http://www.ibm.com/software/awdtools/hats/">Home Page</a></li>
					<li class="m_lvl2nav_item"><a href="http://www.ibm.com">Employee portal</a></li>
					<li class="m_lvl2nav_item"><a href="http://www.ibm.com">Jobs</a></li>
					<li class="m_lvl2nav_item"><a href="http://www.ibm.com/software/awdtools/hats/library">Articles</a></li>
			    </ul>
			</div>
			
			<div id="m_content">
			 	<div>
					<HATS:Transform skipBody="true">
				   		 <P> Host screen transformation will be shown here </P>
				 	</HATS:Transform>
				</div>			    
				<div class="appKeypad"> <HATS:ApplicationKeypad settings="layout:horizontal"/></div>				
			</div>  <!-- END m_content -->
		    
		</td>
	</tr>
</table>
</body>
</html>
