<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<html lang="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>

<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=1.6,0,04/07/28,16:52:09  -->
<head>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<link href="../common/stylesheets/transport.css" rel="stylesheet" type="text/css">
<!--[if lt IE 7]>
<style type="text/css">
input.RHBLANK,input.RHBLUE,input.RHGREEN,input.RHRED,input.RHCYAN,
input.RHWHITE,input.RHMAGENTA,input.RHBROWN,input.RHYELLOW,input.RHGRAY 
{ background-image: none !important; }
</style>
<![endif]-->
<!--[if IE]> 
<style type="text/css">
.t_lvl1nav_item { display: inline !important; margin-left: 4px;}
</style>
<![endif]-->
<script type="text/javascript" src="../common/scripts/transport.js" ></script>
</head>

<body>

<table id="t_wrapper" cellspacing="0"> 

<tr><td id="t_logo"><div class="t_COMPANYTITLE">My Company</div></td></tr>
<tr><td>
<div id="t_lvl1nav" >
	<ul id="t_lvl1nav_list">
<li class="t_lvl1nav_item" id="t_lvl1nav_activeitem">
        	<a href="javascript:;" >Products</a>
        	<ul>
            	<li><a href="http://www.ibm.com/software/awdtools/hats/" >Main product</a></li>
            	<li><a href="http://www.ibm.com" >Additional products</a></li>
            	<li><a href="http://www.ibm.com/developerworks/downloads/ws/whats/" >Downloads</a></li>
            	<li><a href="http://www.ibm.com/software/awdtools/hats/support/" >Support</a></li>
            </ul>
        </li>
    	<li class="t_lvl1nav_item">
        	<a href="javascript:;" >Company Links</a>
        	<ul>
            	<li><a href="http://www.ibm.com/software/awdtools/hats/" >Home page</a></li>
				<li><a href="http://www.ibm.com" >About us</a></li>
            	<li><a href="http://www.ibm.com" >Employee portal</a></li>
            	<li><a href="http://www.ibm.com" >Jobs</a></li>
				<li><a href="http://www.ibm.com/software/awdtools/hats/library" >Articles</a></li>
				<li><a href="http://www.ibm.com" >Site map</a></li>
            </ul>
        </li>
    </ul>
</div>
</td></tr>
<tr><td id="t_content" align="center">
<div> 
    <div>
	<HATS:Transform skipBody="true">
    <P> Host screen transformation will be shown here </P>
 </HATS:Transform>
	</div>
	<div class="appKeypad"> <HATS:ApplicationKeypad settings="layout:horizontal"/></div>
    </div>

</td></tr>
</table>
</body>
</html>
