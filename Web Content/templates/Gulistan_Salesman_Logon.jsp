    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->

<%@page import="javax.servlet.jsp.tagext.TryCatchFinally"%><html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=1.18,0,04/07/28,16:51:40  -->

<head>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<meta name="GENERATOR" content="IBM WebSphere Studio" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<link rel="stylesheet" href="../common/css/style.css" type="text/css" />
<link rel="stylesheet" href="../common/css/menu2.css" type="text/css" />
<script type="text/javascript">
	
	var arrayOfRolloverClasses = new Array();
	var arrayOfClickClasses = new Array();
	var activeRow = false;
	var activeRowClickArray = new Array();
	
	function highlightTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(this!=activeRow){
			this.setAttribute('origCl',this.className);
			this.origCl = this.className;
		}
		this.className = arrayOfRolloverClasses[tableObj.id];
		
		activeRow = this;
		
	}
	
	function clickOnTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;		
		
		if(activeRowClickArray[tableObj.id] && this!=activeRowClickArray[tableObj.id]){
			activeRowClickArray[tableObj.id].className='';
		}
		this.className = arrayOfClickClasses[tableObj.id];
		
		activeRowClickArray[tableObj.id] = this;
				
	}
	
	function resetRowStyle()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(activeRowClickArray[tableObj.id] && this==activeRowClickArray[tableObj.id]){
			this.className = arrayOfClickClasses[tableObj.id];
			return;	
		}
		
		var origCl = this.getAttribute('origCl');
		if(!origCl)origCl = this.origCl;
		this.className=origCl;
		
	}
		
	function addTableRolloverEffect(tableId,whichClass,whichClassOnClick)
	{
		arrayOfRolloverClasses[tableId] = whichClass;
		arrayOfClickClasses[tableId] = whichClassOnClick;
		
		var tableObj = document.getElementById(tableId);
		var tBody = tableObj.getElementsByTagName('TBODY');
		if(tBody){
			var rows = tBody[0].getElementsByTagName('TR');
		}else{
			var rows = tableObj.getElementsByTagName('TR');
		}
		for(var no=0;no<rows.length;no++){
			rows[no].onmouseover = highlightTableRow;
			rows[no].onmouseout = resetRowStyle;
			
			if(whichClassOnClick){
				rows[no].onclick = clickOnTableRow;	
			}
		}
		
	}
	

function show(object,val) { 
document.getElementById(object).style.visibility = val; 
} 


	</script>

		<script language="JavaScript">
		//to disable the back button
		
		window.history.forward(1);

		</script>

<script language="javascript">
	
	function cursorWait()
{
    
   document.getElementById("topcontent").style.display ='none';
    document.getElementById("loadingimgtop").style.display ='block';
    document.getElementById("loadingimg").style.display ='block';
    
}
	function ChangeToHourGlass()
{
    document.body.style.cursor ="wait";			
}

	function ChangeToNormal()
{
    document.body.style.cursor ="default";
}


</script>
</head> 

<body onbeforeunload="cursorWait();"  onload="loadSubmit();">


<div class="gradimg">

<div class="main">


<div class="maininner">
<!--Header Start-->
<div class="header">
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td valign="bottom" align="right">
      <div class="topmenumain2">
        <table width="auto" border="0" cellspacing="0" cellpadding="0">
          <tr>
            
            <td align="right" valign="bottom"><div class="signoffbtn3"><a onclick='ms("macrorun_SalesManSignOff","hatsportletid")' title="Signoff"></a></div></td>
          </tr>
        </table>							
      </div>
      </td>
    </tr>
  </table>
</div>
<!--Header End-->

<div class="topbarline"></div>

<!--ddd-->
<a name="navskip"></a>
 <div id="topcontent" style="display: block;">
 <HATS:Transform skipBody="true">
 
    <P> Host screen transformation will be shown here </P>
 </HATS:Transform>
 </div>
<!--ddd-->		
						<div id="loadingimgtop" style="display: none;">
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
						<tr>
						<td align="center" valign="middle">
						
						<div style="visibility:hidden;" id="progress">
						<img id="progress_image" style="padding-left:5px;padding-top:5px;" 
						src="../common/images/ajax-loader.gif"></img>
						</div>
						
						<script language="JavaScript">

function loadSubmit() {

ProgressImage = document.getElementById('progress_image');
document.getElementById("progress").style.visibility = "visible";
setTimeout(function(){ProgressImage.src = ProgressImage.src},100);
return true;

}


</script>



		 				
						
						
						</td>
						</tr>
						</table>
						</div>

</div>
</div>

<div class="bottom_bg"></div>


<!--Footer Start-->
<div class="footerbg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center" valign="middle">
        <table width="auto" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="bottom">
            <div class="footertxt">&copy; Copyright 2010</div>
            </td>
            <td>
            <div class="culistan_footer"><img src="../common/images/culistan_footer.gif" width="72" height="30" /></div>
            </td>
            <td valign="bottom">
            <div class="footertxt">All rights reserved.</div>
            </td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
<!--Footer End-->

</div>

</body>
</html>