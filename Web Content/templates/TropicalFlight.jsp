<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=%I%,%S%,%E%,%U%  -->
<HEAD>
  <TITLE><HATS:Util type="applicationName" /></TITLE> <HATS:Util type="baseHref" />
  <META name="GENERATOR" content="IBM WebSphere Studio">
  <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- See the User's and Administration Guide for information on using templates and stylesheets -->
  <!-- Global Style Sheet -->
  <LINK rel="stylesheet" href="../common/stylesheets/tantheme.css" type="text/css">
  <LINK rel="stylesheet" href="../common/stylesheets/reverseVideoTan.css" type="text/css">
  <!-- embedded stylesheet to coordinate button and link colors -->
  <STYLE TYPE="text/css">
      A.BOTTOMBAR:link {color:black}
      A.BOTTOMBAR:active {color:black}
      A.BOTTOMBAR:visited {color:black}
      
      table.ApplicationKeypad
      { 
          background: inherit; 
          white-space: nowrap;    
      }
      
      td.ApplicationKeypad
      {
          background: inherit; 
          white-space: nowrap;    
      }
      
      input.HostButton {
        background-color: #C29130;
        color: white;
        white-space: pre;
      }
      
      input.HostPFKey {
        background-color: #CC6633;
        color: white;
        font-size: .8em;  
        white-space: pre;
      }
      
      input.ApplicationButton {
        background-color: #CC6633;
        color: white;
        white-space: pre;
        width: 15em;
      }
      
      select.ApplicationDropDown {
        width: 15em;
      }

      table.HostKeypad {
        background-color: #E3C993;
      }
      
      .COMPANYTITLE {
        color:  #A5110C;
        font-size: xx-large;
        font-style: italic;
      }
      
       .COMPANYSLOGAN {
        color:  #A5110C;
        font-size: large;
        font-style: italic;
      }
      
      .LINKSHEADER {
        color:  #A5110C;;
        font-size: large;
      }
  </STYLE>
</HEAD>
  <BODY>
  <TABLE width="100%" cellpadding="0" cellspacing="0" border="0" >
    <TBODY>
      <TR>
        <TD colspan="3" align="left"  background="../common/images/SunsetFlight.jpg" height="112" valign="top">
          <TABLE>
            <TBODY>
            <TR>
              <TD>
                <IMG SRC="../common/images/TropicalPhoto.jpg" width="136" height="112" alt="TropicalPhoto.jpg" >
              </TD>
              <TD>
                <SPAN class="COMPANYTITLE">
                &nbsp;&nbsp;&nbsp;My Company
                </SPAN>
                <BR>
                <SPAN class="COMPANYSLOGAN">
                &nbsp;&nbsp;&nbsp;&nbsp;My Company Slogan
                </SPAN>
              </TD>
            </TR>
            </TBODY>
          </TABLE>
        </TD>
      </TR>
    <TR>
      <TD background="../common/images/AmberToRustVertical.jpg"  width="220" height="100%" align="center" valign="top">
        <DIV align="center">
          <TABLE height="100%" cellpadding="3" cellspacing="3" border="0" width="100%">
            <TBODY>
            <!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
            <!-- 
            <TR>
				<TD>
				<a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a>
  				</TD>
  			</TR>
  			-->
            <TR>
              <TD nowrap="nowrap" height="500" valign="top" width="260">
                <BR>
                <BR>
                <STRONG>
                <SPAN CLASS="LINKSHEADER">My Company Links</SPAN> <BR>
                <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Home Page</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Map</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Employees</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Jobs at My Company</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Articles</A> <BR>
                <BR>
                <SPAN CLASS="LINKSHEADER">My Products</SPAN> <BR>
                <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Main Product</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Additional Products</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Downloads</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Support</A> <BR>
                </STRONG>
                &nbsp; <BR>
                &nbsp; <BR>
                <HATS:ApplicationKeypad/>
              </TD>
            </TR>
            </TBODY>
          </TABLE>
        </DIV>
      </TD>
      <TD width="100%" height="100%" valign="top" align="center">
      <a name="navskip"></a>
        <HATS:Transform skipBody="true">
        <DIV align="center">
          <P>
          Host screen transformation will be shown here
          </P>
        </DIV>
        </HATS:Transform>
      </TD>
    </TR>
    </TBODY>
  </TABLE>
  </BODY>
</HTML>
