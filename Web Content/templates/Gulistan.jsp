<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->

<%@page import="javax.servlet.jsp.tagext.TryCatchFinally"%><html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%>
<%@page import="java.util.ArrayList"%>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=1.18,0,04/07/28,16:51:40  -->

<head>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<meta name="GENERATOR" content="IBM WebSphere Studio" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link id="style" rel="stylesheet" href="../common/css/style.css" type="text/css" />
<link id="menu2" href="../common/css/menu2.css" rel="stylesheet" type="text/css" />
<link id="ddsmoothmenu" rel="stylesheet" type="text/css" href="../common/css/ddsmoothmenu.css" />

<script>
var isiPad = navigator.userAgent.match(/iPad/i) != null;

if(isiPad ){
document.getElementById('style').href = '../common/css/styleIPAD.css';
document.getElementById('menu2').href = '../common/css/menu2IPAD.css';
document.getElementById('ddsmoothmenu').href = '../common/css/ddsmoothmenuIPDAD.css';
}

</script>
<script type="text/javascript">
	
	var arrayOfRolloverClasses = new Array();
	var arrayOfClickClasses = new Array();
	var activeRow = false;
	var activeRowClickArray = new Array();
	
	function highlightTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(this!=activeRow){
			this.setAttribute('origCl',this.className);
			this.origCl = this.className;
		}
		this.className = arrayOfRolloverClasses[tableObj.id];
		
		activeRow = this;
		
	}
	
	function clickOnTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;		
		
		if(activeRowClickArray[tableObj.id] && this!=activeRowClickArray[tableObj.id]){
			activeRowClickArray[tableObj.id].className='';
		}
		this.className = arrayOfClickClasses[tableObj.id];
		
		activeRowClickArray[tableObj.id] = this;
				
	}
	
	function resetRowStyle()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(activeRowClickArray[tableObj.id] && this==activeRowClickArray[tableObj.id]){
			this.className = arrayOfClickClasses[tableObj.id];
			return;	
		}
		
		var origCl = this.getAttribute('origCl');
		if(!origCl)origCl = this.origCl;
		this.className=origCl;
		
	}
		
	function addTableRolloverEffect(tableId,whichClass,whichClassOnClick)
	{
		arrayOfRolloverClasses[tableId] = whichClass;
		arrayOfClickClasses[tableId] = whichClassOnClick;
		
		var tableObj = document.getElementById(tableId);
		var tBody = tableObj.getElementsByTagName('TBODY');
		if(tBody){
			var rows = tBody[0].getElementsByTagName('TR');
		}else{
			var rows = tableObj.getElementsByTagName('TR');
		}
		for(var no=0;no<rows.length;no++){
			rows[no].onmouseover = highlightTableRow;
			rows[no].onmouseout = resetRowStyle;
			
			if(whichClassOnClick){
				rows[no].onclick = clickOnTableRow;	
			}
		}
		
	}
	

function show(object,val) { 
document.getElementById(object).style.visibility = val; 
} 


	</script>

		<script language="JavaScript">
		//to disable the back button
		
		window.history.forward(1);

		</script>









<script type="text/javascript" src="../common/js/jquery.min.js"></script>
<script type="text/javascript" src="../common/js/ddsmoothmenu.js"></script>

<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>

<script language="JavaScript">
function selectMenu(field, page){
//alert("page = "+page+", field = "+field);
var pageVal = "";
var pageNum = parseInt(page);
//alert("pageNum"+pageNum);
if(pageNum == 0){
//alert("this is if condition");
pageVal = "[tab][up]";
}
else{
for(i=0; i<pageNum; i++){
pageVal+= "[pagedn]";
}

//document.getElementById("hatsgv_menuItemGV").value = item;

}

document.getElementById("hatsgv_menuPageGV").value = pageVal;
if(field == "323"){
//alert("Gotcha!!");
//document.getElementById("hatsgv_menuFieldGV").value = "[tab][up]";
}

else if(field == "403"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab]";
}

else if(field == "483"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab]";
}
else if(field == "563"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab]";
}
else if(field == "643"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab]";
}
else if(field == "723"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab]";
}
else if(field == "803"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab][tab]";
}
else if(field == "883"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab][tab][tab]";
}
else if(field == "963"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab][tab][tab][tab]";
}
else if(field == "1043"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab][tab][tab][tab][tab]";
}
else if(field == "1123"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab][tab][tab][tab][tab][tab]";
}
else if(field == "1203"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab]";
}
else if(field == "1283"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab]";
}
else if(field == "1363"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab]";
}
else if(field == "1443"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab]";
}
else if(field == "1523"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab]";
}
else if(field == "1603"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab]";
}
else if(field == "1683"){
//alert("Gotcha!!");
document.getElementById("hatsgv_menuFieldGV").value = "[tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab][tab]";
}
//alert(pageVal);
//alert(document.getElementById("hatsgv_menuPageGV").value);
//alert(document.getElementById("hatsgv_menuFieldGV").value); 
//alert('test in gulistan.jsp:'+field);
ms('macrorun_selectMenu','hatsportletid');

}
</script>

<script language="javascript">
	
	function cursorWait()
{
    //window.onbeforeunload=ChangeToHourGlass();
    document.getElementById("topcontent").style.display ='none';
    document.getElementById("loadingimgtop").style.display ='block';
    //document.getElementById("loadingimg").style.display ='block';
    
}
	function ChangeToHourGlass()
{
    document.body.style.cursor ="wait";			
}

	function ChangeToNormal()
{
    document.body.style.cursor ="default";
}
	
</script>

</head>

<% String addPrice= ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("addPrice", true).getString(0).trim(); %>
<%
		System.out.println("addPrice:............."+addPrice);
		if(addPrice.equalsIgnoreCase("CSF440_PROMPT11111"))
		{
				
 %>
 <body onload="loadSubmit();">
 <%
 }
 else
 {}
  %>
<body onbeforeunload="cursorWait();" onload="loadSubmit();">
	<%
	//}
	 %>

	<%
	//try{
	//Orders Samples Financials Pricing Claims Documents User Admin
	
	ArrayList orders = new ArrayList();
	ArrayList samples = new ArrayList();
	ArrayList financials = new ArrayList();
	ArrayList pricing = new ArrayList();
	ArrayList claims = new ArrayList();
	ArrayList documents = new ArrayList();
	ArrayList userAdmin = new ArrayList();
	String temp_category = "";
	String temp_item = "";
	String temp_page = "";
	String temp_field = "";
	int mainMenuCategory_size = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("mainMenuCategory", true).size(); 
	int mainMenuItemsList_size = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("mainMenuItemsList", true).size(); 
	int mainMenuPage_size = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("mainMenuPage", true).size();
	int mainMenuField_size = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("mainMenuField", true).size(); 
	
	for(int i=0; i<mainMenuCategory_size; i++){
	temp_category = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("mainMenuCategory", true).getString(i);
	temp_item = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("mainMenuItemsList", true).getString(i);
	temp_page = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("mainMenuPage", true).getString(i);
	temp_field = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("mainMenuField", true).getString(i);
	//System.out.print(((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("mainMenuCategory", true).getString(i)+"  ");
	//System.out.print(((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("mainMenuItemsList", true).getString(i)+"  ");
	//System.out.print(((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("mainMenuPage", true).getString(i)+"  ");
	//System.out.println(((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("mainMenuField", true).getString(i)+"  ");
	if(temp_category.trim().equalsIgnoreCase("Orders")){
	//System.out.println("temp_orders = "+temp_orders);
	orders.add(temp_item+"__"+temp_page+"___"+temp_field);
	}
	
	else if(temp_category.trim().equalsIgnoreCase("Samples")){
	samples.add(temp_item+"__"+temp_page+"___"+temp_field);
	}
	
	else if(temp_category.trim().equalsIgnoreCase("Financials")){
	financials.add(temp_item+"__"+temp_page+"___"+temp_field);
	}
	
	else if(temp_category.trim().equalsIgnoreCase("Pricing")){
	pricing.add(temp_item+"__"+temp_page+"___"+temp_field);
	}
	
	else if(temp_category.trim().equalsIgnoreCase("Claims")){
	claims.add(temp_item+"__"+temp_page+"___"+temp_field);
	}
	
	else if(temp_category.trim().equalsIgnoreCase("Documents")){
	documents.add(temp_item+"__"+temp_page+"___"+temp_field);
	}
	
	else if(temp_category.trim().equalsIgnoreCase("User Admin")){
	userAdmin.add(temp_item+"__"+temp_page+"___"+temp_field);
	}
	
	
	}
	//System.out.println("orders = "+orders); 
	//System.out.println("samples = "+samples);
	//System.out.println("financials = "+financials);
	//System.out.println("pricing = "+pricing);
	//System.out.println("claims = "+claims);
	//System.out.println("documents = "+documents);
	//System.out.println("userAdmin = "+userAdmin);
	
	//}
	//catch(Exception ex){
	//System.out.println("Exception caught : "+ex.getMessage());
	//}
	
	%>
<div class="gradimg">

<div class="main">


<div class="maininner">
<!--Header Start-->
<div class="header">
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td valign="bottom" align="right">
      <div class="topmenumain2">
        <table width="auto" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" valign="bottom"><div class="homebtn3"><a onclick="javascript:homeClicked();ms('macrorun_HOMEMACRO','hatsportletid')" title="Home"></a></div></td>
            <td width="3"></td>
			
            <td align="right" valign="bottom"><div class="signoffbtn3"><a onclick="javascript:logoffClicked();ms('macrorun_SignOffMacro02','hatsportletid')" title="Signoff"></a></div></td>
        
         
          </tr>
        </table>							
      </div>
      </td>
    </tr>
  </table>
</div>
<!--Header End-->

<div class="topbarline"></div>
<div class="clear1px"></div>

<div class="heading_bgd" >
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td background="../common/images/menuleftimg.gif" style="background-repeat:no-repeat;" width="8"></td>
      <td>
      <div class="menumainbg">

        <div id="smoothmenu1" class="ddsmoothmenu">
        <ul>
        <li><a style='cursor: pointer;'>Orders</a>
          <ul>
          <%
          for(int i=0; i<orders.size(); i++){
		  out.println("<li><a style='cursor: pointer;' onclick=javascript:whichElement(event);menuClicked();selectMenu("+orders.get(i).toString().substring(43)+","+orders.get(i).toString().substring(39,40)+")>"+orders.get(i).toString().substring(0,37)+"</a></li>");
          
          }
          %>
          </ul>
        </li>
        <li><a style='cursor: pointer;'>Samples</a>
          <ul>
         <%
          for(int i=0; i<samples.size(); i++){
          out.println("<li><a style='cursor: pointer;' onclick=javascript:whichElement(event);menuClicked();selectMenu("+samples.get(i).toString().substring(43)+","+samples.get(i).toString().substring(39,40)+")>"+samples.get(i).toString().substring(0,37)+"</a></li>");
          }
          %>
          </ul>
        </li>
        <li><a style='cursor: pointer;'>Financials</a>
          <ul>
          <%
          for(int i=0; i<financials.size(); i++){
          out.println("<li><a style='cursor: pointer;' onclick=javascript:whichElement(event);menuClicked();selectMenu("+financials.get(i).toString().substring(43)+","+financials.get(i).toString().substring(39,40)+")>"+financials.get(i).toString().substring(0,37)+"</a></li>");
          }
          %>
          </ul>
        </li>
        <li><a style='cursor: pointer;'>Pricing</a>
          <ul>
          <%
          for(int i=0; i<pricing.size(); i++){
          out.println("<li><a style='cursor: pointer;' onclick=javascript:whichElement(event);menuClicked();selectMenu("+pricing.get(i).toString().substring(43)+","+pricing.get(i).toString().substring(39,40)+")>"+pricing.get(i).toString().substring(0,37)+"</a></li>");
          }
          %>
          </ul>
        </li>
        <li><a style='cursor: pointer;'>Claims</a>
          <ul>
          <%
          for(int i=0; i<claims.size(); i++){
          out.println("<li><a style='cursor: pointer;' onclick=javascript:whichElement(event);menuClicked();selectMenu("+claims.get(i).toString().substring(43)+","+claims.get(i).toString().substring(39,40)+")>"+claims.get(i).toString().substring(0,37)+"</a></li>");
          }
          %>
          </ul>
        </li>
        <li><a style='cursor: pointer;'>Documents</a>
          <ul>
          <%
          for(int i=0; i<documents.size(); i++){
          out.println("<li><a style='cursor: pointer;' onclick=javascript:whichElement(event);menuClicked();selectMenu("+documents.get(i).toString().substring(43)+","+documents.get(i).toString().substring(39,40)+")>"+documents.get(i).toString().substring(0,37)+"</a></li>");
          }
          %>
          </ul>
        </li>
        <li><a style="border:none;cursor: pointer;">User Admin</a>
          <ul>
          <%
          for(int i=0; i<userAdmin.size(); i++){
          out.println("<li><a style='cursor: pointer;' onclick=javascript:whichElement(event);menuClicked();selectMenu("+userAdmin.get(i).toString().substring(43)+","+userAdmin.get(i).toString().substring(39,40)+")>"+userAdmin.get(i).toString().substring(0,37)+"</a></li>");
          }
          %>
          </ul>
        </li>
        </ul>
        <br style="clear: left" />
        </div>
        
        
        </div>
      </td>
      <td background="../common/images/menurightimg.gif" style="background-repeat:no-repeat;" width="6"></td>
    </tr>
    <tr>
      <td colspan="3" height="1"></td>
      </tr>
    <tr>
      <td colspan="3" background="../common/images/menushadow.gif" height="5" style="background-repeat:no-repeat;"></td>
      </tr>
  </table>
</div>

<!--ddd-->
<div class="clear4px"></div>
<div id="topcontent">


 <HATS:Transform skipBody="true">
 
    <p> Host screen transformation will be shown here </p>
	
</HATS:Transform>
 </div>
<!--ddd-->
					<div id="loadingimgtop" style="display: none;">
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
						<tr>
						<td align="center" valign="middle">
						
						<div style="visibility:hidden;" id="progress">
						<img id="progress_image" style="padding-left:5px;padding-top:5px;" 
						src="../common/images/ajax-loader.gif"></img>
						</div>
						
						<script language="JavaScript">

function loadSubmit() {

ProgressImage = document.getElementById('progress_image');
document.getElementById("progress").style.visibility = "visible";
setTimeout(function(){ProgressImage.src = ProgressImage.src},100);
return true;

}


</script>



 				
						</td>
						</tr>
						</table>
						</div>
</div>
</div>

<div class="bottom_bg"></div>


<!--Footer Start-->
<div class="footerbg">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center" valign="middle">
        <table width="auto" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="bottom">
            <div class="footertxt">&copy; Copyright 2010</div>
            </td>
            <td>
            <div class="culistan_footer"><img src="../common/images/culistan_footer.gif" width="72" height="30" /></div>
            </td>
            <td valign="bottom">
            <div class="footertxt">All rights reserved.</div>
            </td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>


</div>

</body>
</html>