<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*" %>
<% 
	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
%>
<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
 
<head>
<title><%=msgs.get("KEY_MENU_TITLE")%></title>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/adminconsole.css" type="text/css" />
</head>


<body class="navtree">
<script type="text/javascript" language="JavaScript" src="scripts/aptree.js">
</script>

<script type="text/javascript" language="JavaScript1.2">

setShowExpanders(true);
setExpandDepth(1);
setKeepState(false);
setShowHealth(false);
setInTable(false);
setTargetFrame("content");
openFolder = "images/open_folder.gif";
closedFolder = "images/closed_folder.gif";
plusIcon = "images/lplus.gif";
minusIcon = "images/lminus.gif";
blankIcon = "images/blank20.gif";

</script>










<script type="text/javascript" language="JavaScript1.2">
admin_domain = addRoot('images/Domain.gif', '','');

gettingstartedcategory = addItem(admin_domain, "images/onepix.gif", "<%= msgs.get("KEY_GETTING_STARTED") %>", "");
managementscope = addItem(gettingstartedcategory, "images/onepix.gif", "<%= msgs.get("KEY_MANAGEMENT_SCOPE") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_SCOPE)%>");


connmanagementcategory = addItem(admin_domain, "images/onepix.gif", "<%= msgs.get("KEY_CONNECTION_MANAGEMENT") %> ", "");
hostconnections = addItem(connmanagementcategory,"images/onepix.gif","<%= msgs.get("KEY_HOST_CONNECTIONS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_HOST_CONNECTIONS)%>");
databaseconnections = addItem(connmanagementcategory,"images/onepix.gif","<%= msgs.get("KEY_DB_CONNECTIONS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_DB_CONNECTIONS)%>");
viewconnpools = addItem(connmanagementcategory, "images/onepix.gif", "<%= msgs.get("KEY_CONN_POOLS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_VIEW_CONNPOOLS)%>");
viewpooldefs  = addItem(connmanagementcategory, "images/onepix.gif", "<%= msgs.get("KEY_POOL_DEFS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_VIEW_POOLDEFS)%>");


usermanagementcategory = addItem(admin_domain, "images/onepix.gif", "<%= msgs.get("KEY_USER_MANAGEMENT") %>", "");
viewuserlists = addItem(usermanagementcategory, "images/onepix.gif", "<%= msgs.get("KEY_USER_LISTS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_USER_LISTS)%>");


licensemanagementcategory = addItem(admin_domain, "images/onepix.gif", "<%= msgs.get("KEY_LICENSE_MANAGEMENT") %>", "");
viewlicenseinfo = addItem(licensemanagementcategory, "images/onepix.gif", "<%= msgs.get("KEY_LICENSE_USAGE") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_LICENSE_USAGE)%>");
setlicenseoptions = addItem(licensemanagementcategory, "images/onepix.gif", "<%= msgs.get("KEY_SET_LICENSE_OPTIONS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_SET_LICENSE_OPTIONS)%>");
<%  /**if (bean.isOperator() || bean.isAdministrator()) { %>
securitycategory = addItem(admin_domain, "images/onepix.gif", "<%= msgs.get("KEY_SECURITY") %>", "");
applicationpasswords = addItem(securitycategory, "images/onepix.gif", "<%= msgs.get("KEY_APPLICATION_PASSWORDS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_APPLICATION_PASSWORDS)%>");
<% } */ %>
troubleshootingcategory = addItem(admin_domain, "images/onepix.gif", "<%= msgs.get("KEY_TROUBLESHOOTING") %>", "");
viewLog = addItem(troubleshootingcategory, "images/onepix.gif", "<%= msgs.get("KEY_VIEW_LOG") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_VIEW_LOG)%>");
setlogoptions = addItem(troubleshootingcategory, "images/onepix.gif", "<%= msgs.get("KEY_SET_LOG_OPTIONS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_SET_LOG_OPTIONS)%>");
settraceoptions  = addItem(troubleshootingcategory, "images/onepix.gif", "<%= msgs.get("KEY_SET_TRACE_OPTIONS") %>","<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_SET_TRACE_OPTIONS)%>");


</script>


<script type="text/javascript" language="JavaScript1.2">
initialize();
</script>

</body>
</html>
