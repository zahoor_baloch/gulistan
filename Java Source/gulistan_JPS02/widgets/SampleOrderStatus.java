package gulistan_JPS02.widgets;


import java.util.ArrayList;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;

import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.FieldComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;









import com.ibm.hats.transform.elements.InputComponentElement;
import com.ibm.hats.transform.elements.FieldRowComponentElement;



import com.ibm.hats.transform.widgets.FieldWidget;

/**
 * @param args @Author Juzer 
 */
public class SampleOrderStatus extends Widget implements HTMLRenderer {

	//static Vector exceptionLabels = ExceptionLabelLoader.getExceptionLabels();
	public SampleOrderStatus(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
	}

	public StringBuffer drawHTML() {
		// TODO Auto-generated method stub
		StringBuffer buffer = new StringBuffer(256);
		
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
				contextAttributes, settings);
	
		
	
		//buffer.append("<DIV ID=\"scrollDiv1\" STYLE=\"overflow:auto; HEIGHT:310px; WIDTH:100% \"> ");
        
	
		buffer.append("<TABLE ID='detailsTable' CLASS=\"HATSTABLE\" CELLSPACING=0 CELLPADDING=0 WIDTH=\"100%\" BORDER=0> ");
				
		
	
		buffer.append("<TBODY> ");
		int cRow=0;
		int cCol=0;
		String styleDesc = "";
		String colorDesc = "";
		int len =0;
		int startPos =0;
		int sid =0;
		String cell2 ="";
		String cell3 ="";
		String cell4 ="";
		ComponentElement[] elements = this.getComponentElements();
    	int noOfComponents = elements.length;
    	//System.out.println("noOfComponents= "+noOfComponents);
    	//noOfComponents= 1
    	for (int compNo=0; compNo<noOfComponents; compNo++){
    		
    		
			TableComponentElement rcComp = (TableComponentElement)elements[compNo];
			//rcComp.addCols(cells);
			
			TableCellComponentElement rccells[][] = rcComp.getCells();

			System.out.println("rccells= "+rccells.length);
			//rccells= 36
			String oddEven = "oddRowd";
			
		try {
			for ( cRow=0; cRow<rccells.length; cRow++){//0<36.length , ++
				//System.out.println("rccells["+cRow+"][2] = X__" +rccells[cRow][2].getPreviewText().trim()+"__X");
			if(!rccells[cRow][2].getPreviewText().trim().equals("")){
				//System.out.println("rccells["+cRow+"][2] is non empty");
				if (oddEven.equalsIgnoreCase("evenRowd")) {
					oddEven="oddRowd";
				} else {
					oddEven="evenRowd";	
				}

			
				StringBuffer rowBuffer = new StringBuffer(256);
				len 			= rccells[cRow][0].getLength();
				sid 			= rccells[cRow][0].getScreenId();
				startPos 		= rccells[cRow][0].getStartPos();
			
				rowBuffer.append("<input type='hidden' name='in_"+startPos+"_1_"+sid+"' id='in_"+startPos+"_1_"+sid+"'>");
				rowBuffer.append("<TR id=\"row_"+cRow+"\" onclick =\"setCursorPosition("+startPos+", 'HATSForm');checkInput2('in_"+startPos+"_1_"+sid+"', '/','hidden');ms('[enter]', 'HATSForm')\" class=\""+oddEven+"\" onMouseOver=\"this.className='tableRowClickEffect1d'\" onMouseOut=\"this.className='"+oddEven+"'\">");
				
				
				System.out.println("rccells[cRow]= "+rccells[0].length);
				
				
				
				if(rccells[cRow][2].getLength() == 20 && rccells[cRow].length!=12)
				{
					rccells[cRow][2].setLength(8);
					rccells[cRow][3].setLength(12);
					
					rccells[cRow][4].setLength(10);
				
					rccells[cRow][5].setLength(4);
					
					if (rccells[cRow].length == 7){
					rccells[cRow][6].setLength(4);
					}
					
					              }
				
				else if(rccells[cRow].length==12){
					
					
					
				}
				
								
							
								styleDesc = rccells[cRow][5].getPreviewText();
								cell2 = rccells[cRow][3].getPreviewText();
								cell3 = rccells[cRow][4].getPreviewText();
								cell4 = rccells[cRow][4].getPreviewText();
								
								if (rccells[cRow].length <= 7){
									
									if(rccells[cRow].length == 7){
										colorDesc = rccells[cRow][6].getPreviewText();
									}
									else if(rccells[cRow].length == 6){
										
										cell2="";
										cell3 =rccells[cRow][3].getPreviewText();
										styleDesc = rccells[cRow][4].getPreviewText();
										colorDesc = rccells[cRow][5].getPreviewText();
									} 
									
									
									
								}
								
								else if(rccells[cRow].length > 7 && rccells[cRow].length <= 11){
									
								
									
									if(rccells[cRow].length == 8){
										
										colorDesc = rccells[cRow][6].getPreviewText()+""+
										rccells[cRow][7].getPreviewText();
									}
									else if(rccells[cRow].length == 9){
										
										colorDesc = rccells[cRow][6].getPreviewText()+""+
										rccells[cRow][7].getPreviewText()+""+rccells[cRow][8].getPreviewText();
									}
									else if (rccells[cRow].length == 10){
										colorDesc = rccells[cRow][6].getPreviewText()+""+
										rccells[cRow][7].getPreviewText()+""+rccells[cRow][8].getPreviewText()
										+""+rccells[cRow][9].getPreviewText();
									}
									else if(rccells[cRow].length == 11){
										colorDesc = rccells[cRow][6].getPreviewText()+""+
										rccells[cRow][7].getPreviewText()+""+rccells[cRow][8].getPreviewText()
										+""+rccells[cRow][9].getPreviewText()+""+rccells[cRow][10].getPreviewText();
										cell2 = "";
										cell3 =rccells[cRow][3].getPreviewText()+""+rccells[cRow][4].getPreviewText();
									}
									
									System.out.println(styleDesc+"Champoooooo......"+colorDesc);
									
									
								}
								else if (rccells[cRow].length > 11){
									colorDesc = rccells[cRow][6].getPreviewText()+""+
									rccells[cRow][7].getPreviewText()+""+rccells[cRow][8].getPreviewText()
									+""+rccells[cRow][9].getPreviewText()+""+rccells[cRow][10].getPreviewText()
									+""+rccells[cRow][11].getPreviewText();//+""+rccells[cRow][12].getPreviewText();
									System.out.println(styleDesc+"Champoooooo......"+colorDesc);
									if(rccells[cRow].length == 12){
										cell2 =rccells[cRow][3].getPreviewText()+""+rccells[cRow][4].getPreviewText();
										cell3=rccells[cRow][5].getPreviewText();
										styleDesc = rccells[cRow][6].getPreviewText();
										colorDesc = rccells[cRow][7].getPreviewText()+""+rccells[cRow][8].getPreviewText()
										+""+rccells[cRow][9].getPreviewText()+""+rccells[cRow][10].getPreviewText()
										+""+rccells[cRow][11].getPreviewText();//+""+rccells[cRow][12].getPreviewText();
									}
								}
								//System.out.println("Silvaaaaaa............."+rccells[cRow][12].getPreviewText());
								//System.out.println(styleDesc+"Champoooooo......"+colorDesc+"and cell 5="+rccells[cRow][6].getPreviewText());
								
								rowBuffer.append("<TD width ='151' class='leftline'>");
								rowBuffer.append(rccells[cRow][2].getPreviewText());
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD width ='152' class='leftline'> ");
								//rowBuffer.append("<div class='leftlinepad5'>");
								rowBuffer.append(cell2);
								
								//rowBuffer.append("</div>");
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD width ='212' class='leftline' >");
								//rowBuffer.append("<div class='leftlinepad7'>");
								rowBuffer.append(cell3);
								//rowBuffer.append("</div>");
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD width ='182' class='leftline' >");
								//rowBuffer.append("<div class='leftlinepad6'>");
								rowBuffer.append(styleDesc);
								//rowBuffer.append("</div>");
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD class='leftline' >");
								//rowBuffer.append("<div class='leftlinepad9'>");
								rowBuffer.append(colorDesc);
								//rowBuffer.append("</div>");
								rowBuffer.append("</TD>");
							
							
				
					
						rowBuffer.append(rccells[cRow][cCol].getPreviewText());
			
					
					rowBuffer.append("</TR> ");
					
					buffer.append(rowBuffer);
					}
			
			}
    	}
		catch (Exception e) {
			System.out.println("Sample Order Exception = "+e.getMessage());
		}
		}
				
		
    	
		buffer.append("</TBODY> ");
		
	
		buffer.append("</TABLE> ");
		buffer.append("<script> function doPage(myName){" +
				"alert('hello');"+
				"document.getElementById('myName').value='X';"+
				//"checkBoxCheck(pageName);"+
				//"document.getElementById(pageName).value='20';"+
				"ms('[enter]', 'HATSForm');"+
				"}</script>");
        
	
		//buffer.append("</DIV>");
			
		return (buffer);
	}
	
	
	public int getPropertyPageCount() {
		// TODO Auto-generated method stub
		return (1);
	}

	public Vector getCustomProperties(int iPageNumber, Properties properties,
			ResourceBundle bundle) {
		// TODO Auto-generated method stub
		// Vector v = new Vector();
		// Fill vector with com.ibm.hats.common.HCustomProperty objects.
		// See the API reference for more information.
		// return(v);
		return (null);
	}

	public Properties getDefaultValues(int iPageNumber) {
		// TODO Auto-generated method stub
		return (super.getDefaultValues(iPageNumber));
	}

}