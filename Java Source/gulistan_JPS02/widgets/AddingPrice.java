package gulistan_JPS02.widgets;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;
import jxl.Workbook;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class AddingPrice extends Widget implements HTMLRenderer {

	
	
	public AddingPrice(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
		
	}
	

	public StringBuffer drawHTML() {
		
		StringBuffer buffer = new StringBuffer(256);
		StringBuffer dataBuffer=new StringBuffer();
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
		contextAttributes, settings);
		
		buffer.append("<TABLE ID='detailsTable' CLASS=\"HATSTABLE\" CELLSPACING=0 CELLPADDING=0 WIDTH=\"100%\" BORDER=0> ");
		buffer.append("<TBODY> ");
		int cRow = 0;
		int startPos = 0;
		int sid = 0;
		
		
		ComponentElement[] elements = this.getComponentElements();
		
    	int noOfComponents = elements.length;
    	for (int compNo=0; compNo<noOfComponents; compNo++){
    		TableComponentElement rcComp = (TableComponentElement)elements[compNo];
			TableCellComponentElement rccells[][] = rcComp.getCells();
			String oddEven = "oddRowd";
			
		try {
			for ( cRow=0; cRow<rccells.length; cRow++){
				
			if(!rccells[cRow][2].getPreviewText().trim().equals("")){
				
				if (oddEven.equalsIgnoreCase("evenRowd")) {
					oddEven="oddRowd";
				} else {
					oddEven="evenRowd";	
				}

				
				StringBuffer rowBuffer = new StringBuffer(256);
				sid 			= rccells[cRow][0].getScreenId();
				startPos 		= rccells[cRow][0].getStartPos();
				
				
				rowBuffer.append("<input type='hidden' name='in_"+startPos+"_1_"+sid+"' id='in_"+startPos+"_1_"+sid+"'>");
				rowBuffer.append("<TR id=\"row_"+cRow+"\" onclick =\"setCursorPosition("+startPos+", 'HATSForm');checkInput2('in_"+startPos+"_1_"+sid+"', '/','hidden');ms('[enter]', 'HATSForm')\" class=\""+oddEven+"\" onMouseOver=\"this.className='tableRowClickEffect1d'\" onMouseOut=\"this.className='"+oddEven+"'\">");
				if (rccells[cRow][0].getPreviewText() != null) {
					
					rowBuffer.append("<TD width ='71' align='left'>");
					rowBuffer.append(" "+rccells[cRow][0].getPreviewText()+" ");
					System.out.println("rccells[cRow][0]"+rccells[cRow][0].getPreviewText());
					dataBuffer.append(rccells[cRow][0].getPreviewText());
					dataBuffer.append("_");
					
					}
					else
					{
						rowBuffer.append("<TD width ='71' align='left'>");
						rowBuffer.append("");
						dataBuffer.append("");
						dataBuffer.append("_");
					
					}
					if (rccells[cRow][1].getPreviewText() != null) {
					
					rowBuffer.append("			"+rccells[cRow][1].getPreviewText()+"		");
					dataBuffer.append(rccells[cRow][1].getPreviewText());
					dataBuffer.append("_");
					
					rowBuffer.append("</TD>");
					}
					else
					{
					
						rowBuffer.append("");
						dataBuffer.append("");
						dataBuffer.append("");
						
						rowBuffer.append("</TD>");
					}
					if (rccells[cRow][2].getPreviewText() != null) {
					rowBuffer.append("<TD width ='97' class='leftline' align='left'>");
					rowBuffer.append(" "+rccells[cRow][2].getPreviewText()+" ");
					dataBuffer.append(rccells[cRow][2].getPreviewText());
					dataBuffer.append("_");
					
					}
					else
					{
						rowBuffer.append("<TD width ='97' class='leftline' align='left'>");
						rowBuffer.append("");
						dataBuffer.append("");
						dataBuffer.append("");
						
					}
					if (rccells[cRow][3].getPreviewText() != null) {
					
			
					rowBuffer.append("			"+rccells[cRow][3].getPreviewText()+"		");
					dataBuffer.append(rccells[cRow][3].getPreviewText());
					dataBuffer.append("_");
					
					rowBuffer.append("</TD>");
					}
					else
					{
					
						rowBuffer.append("");
						dataBuffer.append("");
						dataBuffer.append("");
						
						rowBuffer.append("</TD>");
					}
					if (rccells[cRow][4].getPreviewText() != null) {
					rowBuffer.append("<TD width ='97' class='leftline' align='left'>");
					rowBuffer.append(" "+rccells[cRow][4].getPreviewText()+" ");
					dataBuffer.append(rccells[cRow][4].getPreviewText());
					dataBuffer.append("_");
				
					}
					else
					{
						rowBuffer.append("<TD width ='97' class='leftline' align='left'>");
						rowBuffer.append("");
						dataBuffer.append("");
						dataBuffer.append("");
					
					}
					if (rccells[cRow][5].getPreviewText() != null) {
					
					rowBuffer.append("			"+rccells[cRow][5].getPreviewText()+"		");
					dataBuffer.append(rccells[cRow][5].getPreviewText());
					dataBuffer.append("_");
					rowBuffer.append("</TD>");
					}
					else
					{
						
						rowBuffer.append("");
						dataBuffer.append("");
						dataBuffer.append("");
						rowBuffer.append("</TD>");
					}
		
				rowBuffer.append("</TR> ");
				buffer.append(rowBuffer);
				}

			}
    	}
		catch (Exception e) {
			System.out.println("Invoice Inquiry Exception = "+e.getMessage());
		}
		}
				
    	
    	
		buffer.append("</TBODY> ");
		
	
		buffer.append("</TABLE> ");
		
		buffer.append("<script> function doPage(myName){" + "alert('hello');"
				+ "document.getElementById('myName').value='X';" +
				// "checkBoxCheck(pageName);"+
				// "document.getElementById(pageName).value='20';"+
				"ms('[enter]', 'HATSForm');" + "}</script>");
		
			
			
		return (buffer);
	}
	
	
	
	public int getPropertyPageCount() {
		// TODO Auto-generated method stub
		return (1);
	}

	public Vector getCustomProperties(int iPageNumber, Properties properties,
			ResourceBundle bundle) {

		
		
		return (null);
	}

	public Properties getDefaultValues(int iPageNumber) {

		return (super.getDefaultValues(iPageNumber));
	}


	
	
	
	
	
	

}
