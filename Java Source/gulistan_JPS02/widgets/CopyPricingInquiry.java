package gulistan_JPS02.widgets;

import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;

import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;

public class CopyPricingInquiry extends Widget implements HTMLRenderer {

	public CopyPricingInquiry(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
		// TODO Auto-generated constructor stub
	}
	

	public StringBuffer drawHTML() {
		// TODO Auto-generated method stub
		StringBuffer buffer = new StringBuffer(256);
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
				contextAttributes, settings);

		buffer.append("<TABLE ID='detailsTable' CLASS=\"HATSTABLE\" CELLSPACING=0 CELLPADDING=0 WIDTH=\"100%\" BORDER=0> ");

		buffer.append("<TBODY>");
													
		int cRow = 0;
		int cCol = 0;
		String styleDesc = "";
		String colorDesc = "";
		int len = 0;
		int startPos = 0;
		int sid = 0;
		String cell0 = "";
		String cell1 = "";
		String cell2 = "";
		String cell3 = "";

		String cell4 = "";
		String cell5 = "";
		String cell6 = "";
		String cell7 = "";
		String cell8 = "";
		String cell9 = "";

		ComponentElement[] elements = this.getComponentElements();
		
		

		int noOfComponents = elements.length;
		// System.out.println("noOfComponents= "+noOfComponents);
		// noOfComponents= 1
		for (int compNo = 0; compNo < noOfComponents; compNo++) {

			TableComponentElement rcComp = (TableComponentElement) elements[compNo];

			TableCellComponentElement rccells[][] = rcComp.getCells();

			System.out.println("rccells:..... " + rccells.length);
			// rccells= 90
			String oddEven = "oddRowd";

			try {
				for (cRow = 0; cRow < rccells.length; cRow++) {
					// for ( cRow=0; cRow<50; cRow++){
					System.out.println("rccells[" + cRow + "][2] = X__"
							+ rccells[cRow][2].getPreviewText().trim() + "__X");
					if (!rccells[cRow][1].getPreviewText().trim().equals("")) {
						if (oddEven.equalsIgnoreCase("evenRowd")) {
							oddEven = "oddRowd";
						} else {
							oddEven = "evenRowd";
						}

						StringBuffer rowBuffer = new StringBuffer(256);
						len = rccells[cRow][0].getLength();
						sid = rccells[cRow][0].getScreenId();
						startPos = rccells[cRow][0].getStartPos();

						rowBuffer.append("<input type='hidden' name='in_"
								+ startPos + "_1_" + sid + "' id='in_"
								+ startPos + "_1_" + sid + "'>");
						rowBuffer
								.append("<TR id=\"row_"
										+ cRow
										+ "\" class=\""
										+ oddEven
										+ "\" onMouseOver=\"this.className='tableRowClickEffect1d'\" onMouseOut=\"this.className='"
										+ oddEven + "'\">");

						System.out.println("rccells[cRow]= "+ rccells[cRow].length);
						if ((rccells[cRow][1].getLength() == 21 && rccells[cRow][3].getLength() == 20)
								&& (rccells[cRow][4].getPreviewText()
										.equals(" "))) {
							System.out.println("If block");

							cell0 = rccells[cRow][0].getPreviewText();
							cell1 = rccells[cRow][1].getPreviewText();
							cell2 = rccells[cRow][2].getPreviewText();
							cell3 = rccells[cRow][3].getPreviewText()
									.substring(0, 9);
							cell4 = rccells[cRow][3].getPreviewText()
									.substring(10, 20);
							cell5 = rccells[cRow][4].getPreviewText();
							cell6 = rccells[cRow][5].getPreviewText();
							cell7 = rccells[cRow][6].getPreviewText();
							cell8 = rccells[cRow][7].getPreviewText();
						} else if ((rccells[cRow][1].getLength() == 21 || rccells[cRow][3]
								.getLength() == 20)
								&& (rccells[cRow][3].getLength() != 25)
								&& (!rccells[cRow][4].getPreviewText().equals(
										" "))
								&& (rccells[cRow][4].getLength() == 7) ) {
							System.out.println("else If block 1");
							cell0 = rccells[cRow][0].getPreviewText();
							cell1 = rccells[cRow][1].getPreviewText();
							cell2 = rccells[cRow][2].getPreviewText();
							cell3 = rccells[cRow][3].getPreviewText();
							cell4 = rccells[cRow][4].getPreviewText()
									.substring(0, 7);
							//System.out.println("cell4:..."+cell4);
							cell5 = rccells[cRow][4].getPreviewText();
									
							//System.out.println("cell5:..."+cell5);
							cell6 = rccells[cRow][5].getPreviewText();
							//System.out.println("cell6:..."+cell6);
							cell7 = rccells[cRow][6].getPreviewText();
							//System.out.println("cell7:..."+cell7);
							cell8 = rccells[cRow][7].getPreviewText();
							//System.out.println("cell8:..."+cell8);
						} else if ((rccells[cRow][1].getLength() == 21 || rccells[cRow][3]
								.getLength() == 20)
								&& (rccells[cRow][3].getLength() == 25)
								&& (!rccells[cRow][4].getPreviewText().equals(
										" "))
								&& (rccells[cRow][4].getLength() == 7)) {
							System.out.println("else If block 2");
							cell0 = rccells[cRow][0].getPreviewText();
							cell1 = rccells[cRow][1].getPreviewText();
							cell2 = rccells[cRow][2].getPreviewText();
							cell3 = rccells[cRow][3].getPreviewText()
									.substring(0, 7);
							cell4 = rccells[cRow][3].getPreviewText()
									.substring(8, 15);
							cell5 = rccells[cRow][3].getPreviewText()
									.substring(16, 25);
							cell6 = rccells[cRow][4].getPreviewText();
							cell7 = rccells[cRow][5].getPreviewText();
							cell8 = rccells[cRow][6].getPreviewText();
						} else if ((rccells[cRow][1].getLength() == 21 && rccells[cRow][3]
								.getLength() == 20)
								&& (!rccells[cRow][4].getPreviewText().equals(
										" "))
								&& (rccells[cRow][4].getLength() != 7)) {
							System.out.println("else If block 3");
							cell0 = rccells[cRow][0].getPreviewText();
							cell1 = rccells[cRow][1].getPreviewText();
							cell2 = rccells[cRow][2].getPreviewText();
							cell3 = rccells[cRow][3].getPreviewText()
									.substring(0, 10);
							cell4 = rccells[cRow][3].getPreviewText()
									.substring(11, 20);
							cell5 = rccells[cRow][4].getPreviewText()
									.substring(0, 6);
							cell6 = rccells[cRow][5].getPreviewText();
							cell7 = rccells[cRow][6].getPreviewText();
							cell8 = rccells[cRow][7].getPreviewText();
						}

						else if ((rccells[cRow][1].getLength() == 21 && rccells[cRow][3]
								.getLength() == 9)
								&& (!rccells[cRow][4].getPreviewText().equals(
										" "))
								&& (rccells[cRow][4].getLength() != 7)) {
							System.out.println("else If block 4");
							cell0 = rccells[cRow][0].getPreviewText();
							cell1 = rccells[cRow][1].getPreviewText();
							cell2 = rccells[cRow][2].getPreviewText();
							cell3 = rccells[cRow][3].getPreviewText();
							// cell4=rccells[cRow][3].getPreviewText().substring(0,
							// 8);
							cell4 = rccells[cRow][4].getPreviewText();
							cell5 = rccells[cRow][4].getPreviewText()
									.substring(9, 16);
							cell6 = rccells[cRow][5].getPreviewText();
							cell7 = rccells[cRow][6].getPreviewText();
							cell8 = rccells[cRow][7].getPreviewText();
							System.out.println("for else-if4 cell3 = " + cell3);
							System.out.println("cell2 = " + cell2);
							System.out.println("cell3 = " + cell3);
							System.out.println("cell4 = " + cell4);
							System.out
									.println("rccells[cRow][3].getPreviewText() = "
											+ rccells[cRow][3].getPreviewText());
							System.out
									.println("rccells[cRow][4].getPreviewText() = "
											+ rccells[cRow][4].getPreviewText());
							System.out.println("cell5 = " + cell5);
							System.out.println("cell6 = " + cell6);
						} else if ((rccells[cRow][2].getLength() == 3 || rccells[cRow][2]
								.getLength() == 4)
								&& (rccells[cRow][4].getPreviewText()
										.equals(" "))) {
							System.out.println("else If block 5");
							cell0 = rccells[cRow][0].getPreviewText();
							cell1 = rccells[cRow][1].getPreviewText() + ""
									+ rccells[cRow][2].getPreviewText();
							cell2 = rccells[cRow][3].getPreviewText()
									.substring(0, 10);
							cell3 = rccells[cRow][4].getPreviewText()
									.substring(0, 11);
							cell4 = rccells[cRow][4].getPreviewText()
									.substring(12, 20);
							cell5 = rccells[cRow][4].getPreviewText()
									.substring(21, 25);
							cell6 = rccells[cRow][5].getPreviewText();
							cell7 = rccells[cRow][6].getPreviewText();
							cell8 = rccells[cRow][7].getPreviewText();
						} else if ((rccells[cRow][2].getLength() == 3 || rccells[cRow][2]
								.getLength() == 4)
								&& (!rccells[cRow][4].getPreviewText().equals(
										" "))
								&& (rccells[cRow][4].getLength() != 25)) {

							System.out.println("else If block 6");
							cell0 = rccells[cRow][0].getPreviewText();
							cell1 = rccells[cRow][1].getPreviewText() + ""
									+ rccells[cRow][2].getPreviewText();
							cell2 = rccells[cRow][3].getPreviewText()
									.substring(0, 10);
							cell3 = rccells[cRow][4].getPreviewText();

							// cell4=rccells[cRow][4].getPreviewText().substring(12,
							// 20);
							// cell5=rccells[cRow][4].getPreviewText().substring(21,
							// 25);
							cell4 = rccells[cRow][5].getPreviewText()
									.substring(0, 7);
							cell5 = rccells[cRow][5].getPreviewText()
									.substring(8, 14);
							cell6 = rccells[cRow][6].getPreviewText();
							cell7 = rccells[cRow][7].getPreviewText();
							cell8 = rccells[cRow][8].getPreviewText();

						}

						else if ((rccells[cRow][2].getLength() == 3 || rccells[cRow][2]
								.getLength() == 4)
								&& (!rccells[cRow][4].getPreviewText().equals(
										" "))
								&& (rccells[cRow][4].getLength() == 25)) {
							System.out.println("else If block 7");
							cell0 = rccells[cRow][0].getPreviewText();
							cell1 = rccells[cRow][1].getPreviewText() + ""
									+ rccells[cRow][2].getPreviewText();
							cell2 = rccells[cRow][3].getPreviewText()
									.substring(0, 10);
							cell3 = rccells[cRow][4].getPreviewText()
									.substring(0, 7);
							cell4 = rccells[cRow][4].getPreviewText()
									.substring(8, 15);
							cell5 = rccells[cRow][4].getPreviewText()
									.substring(16, 25);
							cell6 = rccells[cRow][5].getPreviewText();
							cell7 = rccells[cRow][6].getPreviewText();
							cell8 = rccells[cRow][7].getPreviewText();

						} else if (rccells[cRow][3].getLength() == 25) {
							System.out.println("else If block 8");
							cell0 = rccells[cRow][0].getPreviewText();
							//System.out.println("cell0:..."+cell0);
							cell1 = rccells[cRow][1].getPreviewText();
							//System.out.println("cell1:..."+cell1);
							cell2 = rccells[cRow][2].getPreviewText();
							//System.out.println("cell2:..."+cell2);
							cell3 = rccells[cRow][3].getPreviewText()
									.substring(0, 10);
							//System.out.println("cell3:..."+cell3);
							cell4 = rccells[cRow][3].getPreviewText()
									.substring(11, 20);
							//System.out.println("cell4:..."+cell4);
							cell5 = rccells[cRow][3].getPreviewText()
									.substring(21, 25);
							//System.out.println("cell5:..."+cell5);
							cell6 = rccells[cRow][4].getPreviewText();
							//System.out.println("cell6:..."+cell6);
							cell7 = rccells[cRow][5].getPreviewText();
							//System.out.println("cell7:..."+cell7);
							cell8 = rccells[cRow][6].getPreviewText();
							//System.out.println("cell8:..."+cell8);

						}

						rowBuffer.append("<TD width ='71' align='center'>");

						rowBuffer.append(cell0);

						rowBuffer.append("</TD>");
						rowBuffer
								.append("<TD width ='251' class='leftline' align='center'>");
						rowBuffer.append("<div  class='leftlinepad'>");
						rowBuffer.append(cell1);
						rowBuffer.append("</div>");
						rowBuffer.append("</TD>");
						rowBuffer
								.append("<TD width ='93' class='leftline' align='center'>");

						rowBuffer.append(cell2);

						rowBuffer.append("</TD>");
						rowBuffer
								.append("<TD width ='71' class='leftline' align='center'>");
						rowBuffer.append("<div class='linepad11'>");
						rowBuffer.append(cell3);
						rowBuffer.append("</div>");
						rowBuffer.append("</TD>");
						rowBuffer
								.append("<TD width ='93' class='leftline' align='center'>");

						rowBuffer.append(cell4);

						rowBuffer.append("</TD>");
						rowBuffer
								.append("<TD width ='91' class='leftline' align='center'>");

						rowBuffer.append(cell5);

						rowBuffer.append("</TD>");
						rowBuffer
								.append("<TD width ='92' class='leftline' align='center'>");
						rowBuffer.append("<div  class='linepad10'>");
						rowBuffer.append(cell6);
						rowBuffer.append("</div>");
						rowBuffer.append("</TD>");
						rowBuffer
								.append("<TD width ='73' class='leftline' align='center'>");
						rowBuffer.append("<div  class='linepad9'>");
						rowBuffer.append(cell7);
						rowBuffer.append("</div>");
						rowBuffer.append("</TD>");
						rowBuffer
								.append("<TD class='leftline' align='center'>");
						rowBuffer.append("<div  class='linepad8'>");
						rowBuffer.append(cell8);
						rowBuffer.append("</div>");
						rowBuffer.append("</TD>");

						rowBuffer.append("</TR> ");

						buffer.append(rowBuffer);
					}
				}
			}

			catch (Exception e) {
				System.out.println("Price Inquiry Exception = ");
				e.printStackTrace();
			}
		}

		buffer.append("</TBODY> ");

		buffer.append("</TABLE> ");
		buffer.append("<script> function doPage(myName){" + "alert('hello');"
				+ "document.getElementById('myName').value='X';" +
				// "checkBoxCheck(pageName);"+
				// "document.getElementById(pageName).value='20';"+
				"ms('[enter]', 'HATSForm');" + "}</script>");
		System.out.println("Total Number of rows are:.......:"+cRow);
		return (buffer);
		
	}
	

	public int getPropertyPageCount() {
		// TODO Auto-generated method stub
		return (1);
	}

	public Vector getCustomProperties(int iPageNumber, Properties properties,
			ResourceBundle bundle) {

		// TODO Auto-generated method stub
		// Vector v = new Vector();
		// Fill vector with com.ibm.hats.common.HCustomProperty objects.
		// See the API reference for more information.
		// return(v);
		return (null);
	}

	public Properties getDefaultValues(int iPageNumber) {
		// TODO Auto-generated method stub
		return (super.getDefaultValues(iPageNumber));
	}

}
