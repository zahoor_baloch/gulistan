package gulistan_JPS02.widgets;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;
import jxl.Workbook;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class PendingPrices extends Widget implements HTMLRenderer {

	
	
	public PendingPrices(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
		
	}
	

	public StringBuffer drawHTML() {
		
		StringBuffer buffer = new StringBuffer(256);
		StringBuffer dataBuffer=new StringBuffer();
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
		contextAttributes, settings);
		
		buffer.append("<TABLE ID='detailsTable' CLASS=\"HATSTABLE\" CELLSPACING=0 CELLPADDING=0 WIDTH=\"100%\" BORDER=0> ");
		buffer.append("<TBODY> ");
		int cRow = 0;
		int startPos = 0;
		int sid = 0;
		
		
		ComponentElement[] elements = this.getComponentElements();
		
    	int noOfComponents = elements.length;
    	for (int compNo=0; compNo<noOfComponents; compNo++){
    		TableComponentElement rcComp = (TableComponentElement)elements[compNo];
			TableCellComponentElement rccells[][] = rcComp.getCells();
			String oddEven = "oddRowd";
			
		try {
			for ( cRow=0; cRow<rccells.length; cRow++){
				
			if(!rccells[cRow][2].getPreviewText().trim().equals("")){
				
				if (oddEven.equalsIgnoreCase("evenRowd")) {
					oddEven="oddRowd";
				} else {
					oddEven="evenRowd";	
				}

				
				StringBuffer rowBuffer = new StringBuffer(256);
				sid 			= rccells[cRow][0].getScreenId();
				startPos 		= rccells[cRow][0].getStartPos();
				
				
				rowBuffer.append("<input type='hidden' name='in_"+startPos+"_1_"+sid+"' id='in_"+startPos+"_1_"+sid+"'>");
				rowBuffer.append("<TR id=\"row_"+cRow+"\" onclick =\"setCursorPosition("+startPos+", 'HATSForm');checkInput2('in_"+startPos+"_1_"+sid+"', '/','hidden');ms('[enter]', 'HATSForm')\" class=\""+oddEven+"\" onMouseOver=\"this.className='tableRowClickEffect1d'\" onMouseOut=\"this.className='"+oddEven+"'\">");
				if (rccells[cRow][0].getPreviewText() != null) {
				rowBuffer.append("<TD width ='71' align='center'>");
				rowBuffer.append(rccells[cRow][0].getPreviewText());
				dataBuffer.append(rccells[cRow][0].getPreviewText());
				dataBuffer.append("_");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='71' align='center'>");
					rowBuffer.append("");
					dataBuffer.append("");
					dataBuffer.append("_");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][1].getPreviewText() != null) {
				rowBuffer.append("<TD width ='251' class='leftline' align='center'>");
				rowBuffer.append("<div  class='leftlinepad'>");
				rowBuffer.append(rccells[cRow][1].getPreviewText());
				dataBuffer.append(rccells[cRow][1].getPreviewText());
				dataBuffer.append("_");
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='251' class='leftline' align='center'>");
					rowBuffer.append("<div  class='leftlinepad'>");
					rowBuffer.append("");
					dataBuffer.append("");
					dataBuffer.append("");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][2].getPreviewText() != null) {
				rowBuffer.append("<TD width ='93' class='leftline' align='center'>");
				rowBuffer.append(rccells[cRow][2].getPreviewText());
				dataBuffer.append(rccells[cRow][2].getPreviewText());
				dataBuffer.append("_");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='93' class='leftline' align='center'>");
					rowBuffer.append("");
					dataBuffer.append("");
					dataBuffer.append("");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][3].getPreviewText() != null) {
				rowBuffer.append("<TD width ='71' class='leftline' align='center'>");
				rowBuffer.append("<div class='linepad11'>");
				rowBuffer.append(rccells[cRow][3].getPreviewText());
				dataBuffer.append(rccells[cRow][3].getPreviewText());
				dataBuffer.append("_");
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='71' class='leftline' align='center'>");
					rowBuffer.append("<div class='linepad11'>");
					rowBuffer.append("");
					dataBuffer.append("");
					dataBuffer.append("");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][4].getPreviewText() != null) {
				rowBuffer.append("<TD width ='93' class='leftline' align='center'>");
				rowBuffer.append(rccells[cRow][4].getPreviewText());
				dataBuffer.append(rccells[cRow][4].getPreviewText());
				dataBuffer.append("_");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='93' class='leftline' align='center'>");
					rowBuffer.append("");
					dataBuffer.append("");
					dataBuffer.append("");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][5].getPreviewText() != null) {
				rowBuffer.append("<TD width ='91' class='leftline' align='center'>");
				rowBuffer.append(rccells[cRow][5].getPreviewText());
				dataBuffer.append(rccells[cRow][5].getPreviewText());
				dataBuffer.append("_");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='91' class='leftline' align='center'>");
					rowBuffer.append("");
					dataBuffer.append("");
					dataBuffer.append("");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][6].getPreviewText() != null) {
				rowBuffer.append("<TD width ='92' class='leftline' align='center'>");
				rowBuffer.append("<div  class='linepad10'>");
				rowBuffer.append(rccells[cRow][6].getPreviewText());
				dataBuffer.append(rccells[cRow][6].getPreviewText());
				dataBuffer.append("_");
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='92' class='leftline' align='center'>");
					rowBuffer.append("<div  class='linepad10'>");
					rowBuffer.append("");
					dataBuffer.append("");
					dataBuffer.append("");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][7].getPreviewText() != null) {
				rowBuffer.append("<TD width ='73' class='leftline' align='center'>");
				rowBuffer.append("<div  class='linepad9'>");
				rowBuffer.append(rccells[cRow][7].getPreviewText());
				dataBuffer.append(rccells[cRow][7].getPreviewText());
				dataBuffer.append("_");
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='73' class='leftline' align='center'>");
					rowBuffer.append("<div  class='linepad9'>");
					rowBuffer.append("");
					dataBuffer.append("");
					dataBuffer.append("");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][8].getPreviewText() != null) {
				rowBuffer.append("<TD class='leftline' align='center'>");
				rowBuffer.append("<div  class='linepad8'>");
				rowBuffer.append(rccells[cRow][8].getPreviewText());
				dataBuffer.append(rccells[cRow][8].getPreviewText());
				dataBuffer.append("_");
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD class='leftline' align='center'>");
					rowBuffer.append("<div  class='linepad8'>");
					rowBuffer.append("");
					dataBuffer.append("");
					dataBuffer.append("");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][9].getPreviewText() != null) {
					rowBuffer.append("<TD class='leftline' align='center'>");
					rowBuffer.append("<div  class='linepad8'>");
					rowBuffer.append(rccells[cRow][9].getPreviewText());
					dataBuffer.append(rccells[cRow][9].getPreviewText());
					dataBuffer.append("_");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
					}
					else
					{
						rowBuffer.append("<TD class='leftline' align='center'>");
						rowBuffer.append("<div  class='linepad8'>");
						rowBuffer.append("");
						dataBuffer.append("");
						dataBuffer.append("");
						rowBuffer.append("</div>");
						rowBuffer.append("</TD>");
					}
				
				rowBuffer.append("</TR> ");
				buffer.append(rowBuffer);
				}

			}
    	}
		catch (Exception e) {
			System.out.println("Invoice Inquiry Exception = "+e.getMessage());
		}
		}
				
    	
    	
		buffer.append("</TBODY> ");
		
	
		buffer.append("</TABLE> ");
		
		buffer.append("<script> function doPage(myName){" + "alert('hello');"
				+ "document.getElementById('myName').value='X';" +
				// "checkBoxCheck(pageName);"+
				// "document.getElementById(pageName).value='20';"+
				"ms('[enter]', 'HATSForm');" + "}</script>");
		
URL url=this.getClass().getClassLoader().getResource("common");				
		
		String path1_1 = url.getPath().substring(0, url.getPath().lastIndexOf("common"))+"pdfFile\\";
		String path2_1 = url.getPath().substring(0, url.getPath().lastIndexOf("common"))+"excelFile\\";
		
		File pdfFile=new File(path1_1);
		File excelFile=new File(path2_1);
		
			
		pdfFile.mkdir();
		excelFile.mkdir();
		
		String path1 = pdfFile.getPath();
		String path2 = excelFile.getPath();
		
		System.out.println("path1"+path1);
		System.out.println("path2"+path2);
		
		try {
			doDelete(path1);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			doDelete(path2);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
			this.generatePdf(dataBuffer);
			try {
				this.generateExcel(dataBuffer);
			} catch (WriteException e) {
				e.printStackTrace();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		return (buffer);
	}
	
	
	
	public int getPropertyPageCount() {
		// TODO Auto-generated method stub
		return (1);
	}

	public Vector getCustomProperties(int iPageNumber, Properties properties,
			ResourceBundle bundle) {

		
		
		return (null);
	}

	public Properties getDefaultValues(int iPageNumber) {

		return (super.getDefaultValues(iPageNumber));
	}


	
	public void generateExcel(StringBuffer buffer) throws WriteException, IOException
	{
	
			WritableWorkbook workbook = null;
			try {
			
			URL url=this.getClass().getClassLoader().getResource("PdfFile");
			File file = new File(url.getPath()+"priceInqExcel.xls");
			//	File file = new File(path2+"priceInqExcel.xls");	
			workbook = Workbook.createWorkbook(file);
			WritableSheet sheet = workbook.createSheet("First Sheet", 0);
			WritableFont wfobj=new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,Colour.BLUE);
	        WritableCellFormat cfobj=new WritableCellFormat(wfobj);
	        cfobj.setBackground(Colour.YELLOW2);
	        sheet.addCell(new Label(0, 0, "Style Code  ",cfobj));
			sheet.addCell(new Label(1, 0,"Style Description",cfobj));
			sheet.addCell(new Label(2, 0,"Warehouse",cfobj ));
			sheet.addCell(new Label(3, 0,"Effect Date",cfobj ));
			sheet.addCell(new Label(4, 0, "Ending Date",cfobj));
			sheet.addCell(new Label(5, 0, "Min Qty",cfobj));
			sheet.addCell(new Label(6, 0,"Roll Price",cfobj ));
			sheet.addCell(new Label(7, 0, "Cut Price",cfobj));
			sheet.addCell(new Label(8, 0,"UM",cfobj));
			sheet.getSettings().setDefaultColumnWidth(18);
			StringTokenizer token=new StringTokenizer(buffer.toString(),"_");
			int totalTokens=token.countTokens();
			for(int row=1;row<totalTokens;row++){
				
				for(int col=0;col<9;col++){
					if(token.hasMoreElements()==true){
						String currentValue=token.nextToken();	
						sheet.addCell(new Label(col, row,currentValue));
					}
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		finally{

			workbook.write();
			workbook.close();
			
			
		}
		
		
	}
	
	
	public void generatePdf(StringBuffer buffer)
	{
			Document document=null;
		try{
		
				
				
				document=new Document();
				URL url=this.getClass().getClassLoader().getResource("PdfFile");
				File file = new File(url.getPath()+"priceInqPdf.pdf");
				//File file = new File(path1+"priceInqPdf.pdf");
				PdfWriter.getInstance(document,new FileOutputStream(file));
				document.open();
				
				int[] width={11,40,18,18,18,15,11,11,11};
				
				PdfPTable table = new PdfPTable(9);
				table.setWidths(width);
				table.setWidthPercentage(115f);
			    table.setHeaderRows(1);
		        
		        
		        PdfPCell cell1=new PdfPCell(new Phrase("Style Code",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Style Description",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Warehouse",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Effect Date",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Ending Date",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Min Qty",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Roll Price",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Cut Price",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("UM",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        table.getDefaultCell().setBackgroundColor(null);
		        
		        StringTokenizer st = new StringTokenizer(buffer.toString(),"_");
		        String currentVal="";
		   
		        int nor=st.countTokens();
		        
		        for(int i=0;i<nor;i++)
		        {
		        	currentVal =  st.nextToken();
		        	
		        	if(currentVal.equals("*"))
		        		currentVal=" ";
		        	table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		        	if(currentVal.contains("/")||currentVal.contains(".")){
		       
		        		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		        		
		        	      
		        	}
		        	if(currentVal.contains("M/DALTON") || currentVal.contains("A")||currentVal.contains("E") || currentVal.contains("I") ||currentVal.contains("O") ||currentVal.contains("U")){
		        		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		        		
		        	}
		        	table.addCell(new Phrase(currentVal, FontFactory.getFont(FontFactory.TIMES_ROMAN,8)));
		        	
		        }
		        document.add(table);
		      
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		finally {
			  
		        document.close(); 
		}
	}
	
	public static void doDelete(String path) throws IOException
	{		
		File directory = new File(path);
		
				
		System.out.println("Path is: "+path);
		File[] files = directory.listFiles();
		for (File file : files)
		{
			System.out.println( "Fil is: "+file );
			if (!file.delete())
			{
				System.out.println("Failed to delete "+file);
			}
		}		
	}

}
