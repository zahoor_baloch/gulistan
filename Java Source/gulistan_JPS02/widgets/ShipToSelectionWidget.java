package gulistan_JPS02.widgets;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;
import jxl.Workbook;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class ShipToSelectionWidget extends Widget implements HTMLRenderer {

	
	
	public ShipToSelectionWidget(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
		
	}
	

	public StringBuffer drawHTML() {
		
		StringBuffer buffer = new StringBuffer(256);
		StringBuffer dataBuffer=new StringBuffer();
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
		contextAttributes, settings);
		
		buffer.append("<TABLE ID='detailsTable' CLASS=\"HATSTABLE\" CELLSPACING=0 CELLPADDING=0 WIDTH=\"100%\" BORDER=0> ");
		buffer.append("<TBODY> ");
		int cRow = 0;
		int startPos = 0;
		int sid = 0;
		
		
		ComponentElement[] elements = this.getComponentElements();
		
    	int noOfComponents = elements.length;
    	for (int compNo=0; compNo<noOfComponents; compNo++){
    		TableComponentElement rcComp = (TableComponentElement)elements[compNo];
			TableCellComponentElement rccells[][] = rcComp.getCells();
			String oddEven = "oddRowd";
			
		try {
			for ( cRow=0; cRow<rccells.length; cRow+=2){
				
			if(!rccells[cRow][1].getPreviewText().trim().equals("")){
				
				if (oddEven.equalsIgnoreCase("evenRowd")) {
					oddEven="oddRowd";
				} else {
					oddEven="evenRowd";	
				}

				
				StringBuffer rowBuffer = new StringBuffer(256);
				sid 			= rccells[cRow][0].getScreenId();
				startPos 		= rccells[cRow][0].getStartPos();
				
				String evenRow = "";
				
				rowBuffer.append("<input type='hidden' name='in_"+startPos+"_1_"+sid+"' id='in_"+startPos+"_1_"+sid+"'>");
				rowBuffer.append("<TR id=\"row_"+cRow+"\" onclick =\"setCursorPosition("+startPos+", 'HATSForm');checkInput2('in_"+startPos+"_1_"+sid+"', '/','hidden');ms('[enter]', 'HATSForm')\" class=\""+oddEven+"\" onMouseOver=\"this.className='tableRowClickEffect1d'\" onMouseOut=\"this.className='"+oddEven+"'\">");
				
				if (rccells[cRow][0].getPreviewText() != null || rccells[cRow][0].getPreviewText() !="") {
				rowBuffer.append("<TD width ='175' align='right'>");
				rowBuffer.append("<div  class='leftpadbar1'>");
				rowBuffer.append(rccells[cRow][0].getPreviewText());
				dataBuffer.append(rccells[cRow][0].getPreviewText());
				dataBuffer.append("_");
				rowBuffer.append("</div>");
				rowBuffer.append("<div  class='leftpadbar1'>");
				rowBuffer.append(rccells[cRow+1][0].getPreviewText());
				//dataBuffer.append(rccells[cRow][0].getPreviewText());
				//dataBuffer.append("_");
				evenRow = evenRow+rccells[cRow+1][0].getPreviewText()+"_";
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='175' align='center'>");
					rowBuffer.append("");
					dataBuffer.append("");
					dataBuffer.append("_");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][1].getPreviewText() != null || rccells[cRow][2].getPreviewText() !="") {
				rowBuffer.append("<TD width ='285' class='leftline' align='center'>");
				rowBuffer.append("<div  class='leftlinepad'>");
				rowBuffer.append(rccells[cRow][1].getPreviewText());
				dataBuffer.append(rccells[cRow][1].getPreviewText());
				dataBuffer.append("_");
				rowBuffer.append("</div>");
				rowBuffer.append("<div  class='leftlinepad'>");
				rowBuffer.append(rccells[cRow+1][1].getPreviewText());
				//dataBuffer.append(rccells[cRow][1].getPreviewText());
				//dataBuffer.append("_");
				evenRow = evenRow+rccells[cRow+1][1].getPreviewText()+"_";
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='285' class='leftline' align='center'>");
					rowBuffer.append("<div  class='leftlinepad'>");
					rowBuffer.append("");
					dataBuffer.append("");
					dataBuffer.append("");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][2].getPreviewText() != null || rccells[cRow][2].getPreviewText() !="") {
				rowBuffer.append("<TD width ='300' class='leftline' align='left'>");
				rowBuffer.append("<div  class='leftpadbar2'>");
				rowBuffer.append(rccells[cRow][2].getPreviewText());
				dataBuffer.append(rccells[cRow][2].getPreviewText());
				dataBuffer.append("_");
				rowBuffer.append("</div>");
				rowBuffer.append("<div  class='leftpadbar2'>");
				rowBuffer.append(rccells[cRow+1][2].getPreviewText());
				//dataBuffer.append(rccells[cRow][2].getPreviewText());
				//dataBuffer.append("_");
				evenRow = evenRow+rccells[cRow+1][2].getPreviewText()+"_";
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='300' class='leftline' align='center'>");
					rowBuffer.append("");
					dataBuffer.append("");
					dataBuffer.append("");
					rowBuffer.append("</TD>");
				}
				dataBuffer.append(evenRow);
				rowBuffer.append("</TR> ");
				buffer.append(rowBuffer);
				
				}

			}
    	}
		catch (Exception e) {
			System.out.println("Invoice Inquiry Exception = "+e.getMessage());
		}
		}
				
    	
    	
		buffer.append("</TBODY> ");
		
	
		buffer.append("</TABLE> ");
		
		buffer.append("<script> function doPage(myName){" + "alert('hello');"
				+ "document.getElementById('myName').value='X';" +
				// "checkBoxCheck(pageName);"+
				// "document.getElementById(pageName).value='20';"+
				"ms('[enter]', 'HATSForm');" + "}</script>");
		
		/*URL url=this.getClass().getClassLoader().getResource("ShipToPdfFile");
		
		 String path1=url.getFile();
		 URL url2=this.getClass().getClassLoader().getResource("ShipToexcelFile");
		 String path2=url2.getFile();*/
		
		try {
			//doDelete(path1);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			//doDelete(path2);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
	//this.generatePdf(dataBuffer);
			/*try {
				//this.generateExcel(dataBuffer);
			} catch (WriteException e) {
				e.printStackTrace();
				
			} catch (IOException e) {
				e.printStackTrace();
			}*/
			
		return (buffer);
	}
	
	
	
	public int getPropertyPageCount() {
		// TODO Auto-generated method stub
		return (1);
	}

	public Vector getCustomProperties(int iPageNumber, Properties properties,
			ResourceBundle bundle) {

		
		
		return (null);
	}

	public Properties getDefaultValues(int iPageNumber) {

		return (super.getDefaultValues(iPageNumber));
	}


	public void generateExcel(StringBuffer buffer) throws WriteException, IOException
	{
	
			WritableWorkbook workbook = null;
			try {
			
			URL url=this.getClass().getClassLoader().getResource("ShipToexcelFile");
			/*System.out.println("url.getPath()"+url.getPath());
			File ShipToexcelFile = new File(url.getPath());
			if(ShipToexcelFile.exists()){
				System.out.println("ShipToexcelFile Folder exists..............");
			}else{
				boolean isShipToexcelFileCreated = ShipToexcelFile.mkdir();
				if(isShipToexcelFileCreated){
					System.out.println("ShipToexcelFile File is created..........");
				}else{
					System.out.println("ShipToexcelFile File is not created and something went wrong badly..........");
				}
			}*/
			File file = new File(url.getPath()+"priceInqExcel_"+System.currentTimeMillis()+".xls");	
			workbook = Workbook.createWorkbook(file);
			WritableSheet sheet = workbook.createSheet("First Sheet", 0);
			WritableFont wfobj=new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,Colour.BLUE);
	        WritableCellFormat cfobj=new WritableCellFormat(wfobj);
	        cfobj.setBackground(Colour.YELLOW2);
	        sheet.addCell(new Label(0, 0, "Customer Number ",cfobj));
			sheet.addCell(new Label(1, 0,"Customer Name",cfobj));
			sheet.addCell(new Label(2, 0,"City/State",cfobj ));
			sheet.getSettings().setDefaultColumnWidth(40);
			StringTokenizer token=new StringTokenizer(buffer.toString(),"_");
			int totalTokens=token.countTokens();
			for(int row=1;row<totalTokens;row++){
				
				for(int col=0;col<3;col++){
					if(token.hasMoreElements()==true){
						String currentValue=token.nextToken();	
						sheet.addCell(new Label(col, row,currentValue));
					}
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		finally{

			workbook.write();
			workbook.close();
			
			
		}
		
		
	}
	
	
	public void generatePdf(StringBuffer buffer)
	{
		
			Document document=null;
		try{
			
				
				
				document=new Document();
				URL url=this.getClass().getClassLoader().getResource("ShipToPdfFile");
				File file = new File(url.getPath()+"priceInqPdf_"+System.currentTimeMillis()+".pdf");
				PdfWriter.getInstance(document,new FileOutputStream(file));
				document.open();
				
				int[] width={11,16,22};
				
				PdfPTable table = new PdfPTable(3);
				table.setWidths(width);
				table.setWidthPercentage(115f);
			    table.setHeaderRows(1);
		        
		        
		        PdfPCell cell1=new PdfPCell(new Phrase("Customer Number",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Customer Name",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("City/State",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        		        
		        table.getDefaultCell().setBackgroundColor(null);
		        
		        StringTokenizer st = new StringTokenizer(buffer.toString(),"_");
		        String currentVal="";
		   
		        int nor=st.countTokens();
		        
		       for(int i=0;i<nor;i++)
		        {
		        	currentVal =  st.nextToken();
		        	
		        	/*if(currentVal.equals("*")){
		        		currentVal=" ";
		        		System.out.println("In PdfGenerate...First if");
		        	}
		        	table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		        	if(currentVal.contains("/")||currentVal.contains(".")){
		       
		        		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		        		System.out.println("In PdfGenerate...Second if");
		        		
		        	      
		        	}
		        	if(currentVal.contains("M/DALTON") || currentVal.contains("A")||currentVal.contains("E") || currentVal.contains("I") ||currentVal.contains("O") ||currentVal.contains("U")){*/
		        		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		        		//System.out.println("In PdfGenerate...Third if");
		        		
		        	//}
		        	table.addCell(new Phrase(currentVal, FontFactory.getFont(FontFactory.TIMES_ROMAN,8)));
		        	
		        }
		        document.add(table);
		      
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		finally {
			  
		       document.close(); 
		        
		}
	}
	
	public static void doDelete(String path) throws IOException
	{
		
		/*System.out.println("url.getPath()"+url.getPath());
		File ShipToexcelFile = new File(url.getPath());
		if(ShipToexcelFile.exists()){
			System.out.println("ShipToexcelFile Folder exists..............");
		}else{
			boolean isShipToexcelFileCreated = ShipToexcelFile.mkdir();
			if(isShipToexcelFileCreated){
				System.out.println("ShipToexcelFile File is created..........");
			}else{
				System.out.println("ShipToexcelFile File is not created and something went wrong badly..........");
			}
		}*/
		
		File directory = new File(path);
		
		if(directory.exists()){
			System.out.println("ShipToexcelFile Folder exists..............");
		}else{
			boolean isDirectoryCreated = directory.mkdir();
			if(isDirectoryCreated){
				System.out.println("ShipToexcelFile File is created..........");
			}else{
				System.out.println("ShipToexcelFile File is not created and something went wrong badly..........");
			}
		}
		
		System.out.println("Path is: "+path);
		File[] files = directory.listFiles();
		for (File file : files)
		{
			System.out.println( "Fil is: "+file );
			if (!file.delete())
			{
				System.out.println("Failed to delete "+file);
			}
		}
		
		
	}
	

	
	
	
	
	
	

}
