package gulistan_JPS02.widgets;

import java.util.Properties;

import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;

public class ClaimsInquiry extends Widget implements HTMLRenderer {

	public ClaimsInquiry(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
		// TODO Auto-generated constructor stub
	}

	public StringBuffer drawHTML() {
		// TODO Auto-generated method stub
		StringBuffer buffer = new StringBuffer(256);
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
				contextAttributes, settings);
buffer.append("<TABLE ID='detailsTable' CLASS=\"HATSTABLE\" CELLSPACING=0 CELLPADDING=0 WIDTH=\"100%\" BORDER=0> ");


		
		
		buffer.append("<TBODY> ");
		int cRow=0;
		int cCol=0;
		String styleDesc = "";
		String colorDesc = "";
		int len =0;
		int startPos =0;
		int sid =0;
		ComponentElement[] elements = this.getComponentElements();
		
    	int noOfComponents = elements.length;
    	
    	for (int compNo=0; compNo<noOfComponents; compNo++){
    		
    		
			TableComponentElement rcComp = (TableComponentElement)elements[compNo];

			
			TableCellComponentElement rccells[][] = rcComp.getCells();

			System.out.println("rccells= "+rccells.length);
			//rccells= 26
			String oddEven = "oddRowd";
			
		try {
			for ( cRow=0; cRow<rccells.length; cRow++){
				//System.out.println("rccells["+cRow+"][2] = X__" +rccells[cRow][2].getPreviewText().trim()+"__X");
			if(!rccells[cRow][2].getPreviewText().trim().equals("")){
				//System.out.println("rccells["+cRow+"][2] is non empty");
				if (oddEven.equalsIgnoreCase("evenRowd")) {
					oddEven="oddRowd";
				} else {
					oddEven="evenRowd";	
				}

			
				StringBuffer rowBuffer = new StringBuffer(256);
				len 			= rccells[cRow][0].getLength();
				sid 			= rccells[cRow][0].getScreenId();
				startPos 		= rccells[cRow][0].getStartPos();
				
				rowBuffer.append("<input type='hidden' name='in_"+startPos+"_1_"+sid+"' id='in_"+startPos+"_1_"+sid+"'>");
				rowBuffer.append("<TR id=\"row_"+cRow+"\" onclick =\"setCursorPosition("+startPos+", 'HATSForm');checkInput2('in_"+startPos+"_1_"+sid+"', '/','hidden');ms('[enter]', 'HATSForm')\" class=\""+oddEven+"\" onMouseOver=\"this.className='tableRowClickEffect1d'\" onMouseOut=\"this.className='"+oddEven+"'\">");
				
				
				if (rccells[cRow][1].getPreviewText() != null) {
				rowBuffer.append("<TD width ='71' class='leftline' align='center'>");
				rowBuffer.append(rccells[cRow][1].getPreviewText());
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='71' class='leftline' align='center'>");
					rowBuffer.append("");
					rowBuffer.append("</TD>");
				}
				
				if (rccells[cRow][2].getPreviewText() != null) {
				rowBuffer.append("<TD width ='101' class='leftline' align='center'>");
				rowBuffer.append(rccells[cRow][2].getPreviewText());
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='101' class='leftline' align='center'>");
					rowBuffer.append("");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][3].getPreviewText() != null) {
				rowBuffer.append("<TD width ='103' class='leftline' align='center'>");
				rowBuffer.append("<div class='linepad1'>");
				rowBuffer.append(rccells[cRow][3].getPreviewText());
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='103' class='leftline' align='center'>");
					rowBuffer.append("<div class='linepad1'>");
					rowBuffer.append("");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][4].getPreviewText() != null) {
				rowBuffer.append("<TD width ='102' class='leftline' align='center'>");
				rowBuffer.append("<div class='linepad2'>");
				rowBuffer.append(rccells[cRow][4].getPreviewText());
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='102' class='leftline' align='center'>");
					rowBuffer.append("<div class='linepad2'>");
					rowBuffer.append("");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][5].getPreviewText() != null) {
				rowBuffer.append("<TD width ='92' class='leftline' align='center'>");
				rowBuffer.append(rccells[cRow][5].getPreviewText());
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='92' class='leftline' align='center'>");
					rowBuffer.append("");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][6].getPreviewText() != null) {
				rowBuffer.append("<TD width ='111' class='leftline' align='center'>");
				rowBuffer.append("<div class='linepad3'>");
				rowBuffer.append(rccells[cRow][6].getPreviewText());
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='111' class='leftline' align='center'>");
					rowBuffer.append("<div class='linepad3'>");
					rowBuffer.append("");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][7].getPreviewText() != null) {
				rowBuffer.append("<TD width ='112' class='leftline' align='center'>");
				rowBuffer.append("<div class='linepad4'>");
				rowBuffer.append(rccells[cRow][7].getPreviewText());
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='112' class='leftline' align='center'>");
					rowBuffer.append("<div class='linepad4'>");
					rowBuffer.append("");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");	
				}
				if (rccells[cRow][8].getPreviewText() != null) {
				rowBuffer.append("<TD width ='103' class='leftline' align='center'>");
				rowBuffer.append("<div class='linepad5'>");
				rowBuffer.append(rccells[cRow][8].getPreviewText());
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD width ='103' class='leftline' align='center'>");
					rowBuffer.append("<div class='linepad5'>");
					rowBuffer.append("");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
				}
				if (rccells[cRow][9].getPreviewText() != null) {
				rowBuffer.append("<TD class='leftline' align='center'>");
				rowBuffer.append("<div class='leftlinepad10'>");
				if(rccells[cRow][9].getPreviewText().equalsIgnoreCase("  ISSUE + "))
					rowBuffer.append("ISSUE ");
				else
					rowBuffer.append(rccells[cRow][9].getPreviewText());
				rowBuffer.append("</div>");
				rowBuffer.append("</TD>");
				}
				else
				{
					rowBuffer.append("<TD class='leftline' align='center'>");
					rowBuffer.append("<div class='leftlinepad10'>");
					rowBuffer.append("");
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
				}
					rowBuffer.append("</TR> ");
					
					buffer.append(rowBuffer);
					}
			
			}
    	}
		catch (Exception e) {
			System.out.println("Invoice Inquiry Exception = "+e.getMessage());
		}
		}
				
		
    	
		buffer.append("</TBODY> ");
		
	
		buffer.append("</TABLE> ");
		buffer.append("<script> function doPage(myName){" +
				"alert('hello');"+
				"document.getElementById('myName').value='X';"+
				
				"ms('[enter]', 'HATSForm');"+
				"}</script>");
		return (buffer);
	}

}
