package gulistan_JPS02.widgets;

import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;

import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.regions.ScreenRegion;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;




public class AccountSummaryWidget extends Widget implements HTMLRenderer {

	public AccountSummaryWidget(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
		
	}

	public StringBuffer drawHTML() {
		
		StringBuffer buffer = new StringBuffer(256);
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
		contextAttributes, settings);
		
		buffer.append("<TABLE ID='detailsTable' CLASS=\"HATSTABLE\" CELLSPACING=0 CELLPADDING=0 WIDTH=\"100%\" BORDER=0> ");
		
		
		
		buffer.append("<TBODY> ");
		int cRow=0;
		int cCol=0;
		String styleDesc = "";
		String colorDesc = "";
		int len =0;
		int startPos =0;
		int sid =0;
		String cell0="";
		String cell1="";
		String cell2="";
		String cell3="";
		
		String cell4="";
		String cell5="";
		String cell6="";
		ComponentElement[] elements = this.getComponentElements();
		
		int noOfComponents = elements.length;
    	
    	for (int compNo=0; compNo<noOfComponents; compNo++){
    	
    		
		TableComponentElement rcComp = (TableComponentElement)elements[compNo];//the entire region in one Table

			TableCellComponentElement rccells[][] = rcComp.getCells();//all the rows & all the cells
			
			
			System.out.println("rccells= "+rccells.length);
			
			String oddEven = "oddRowd";
			
		try {
			for ( cRow=0; cRow<rccells.length; cRow++){
				
			if(!rccells[cRow][1].getPreviewText().trim().equals("") || !rccells[cRow][2].getPreviewText().trim().equals("") || !rccells[cRow][3].getPreviewText().trim().equals("") || !rccells[cRow][4].getPreviewText().trim().equals("") || !rccells[cRow][5].getPreviewText().trim().equals("") || !rccells[cRow][6].getPreviewText().trim().equals("") ){
			
				if (oddEven.equalsIgnoreCase("evenRowd")) {
					oddEven="oddRowd";
				} else {
					oddEven="evenRowd";	
				}

			
				StringBuffer rowBuffer = new StringBuffer(256);
				len 			= rccells[cRow][0].getLength();
				sid 			= rccells[cRow][0].getScreenId();
				startPos 		= rccells[cRow][0].getStartPos();
				
				rowBuffer.append("<input type='hidden' name='in_"+startPos+"_1_"+sid+"' id='in_"+startPos+"_1_"+sid+"'>");
				rowBuffer.append("<TR id=\"row_"+cRow+"\" checkInput2('in_"+startPos+"_1_"+sid+"', '/','hidden');ms('[enter]', 'HATSForm')\" class=\""+oddEven+"\" >");
				
				
				
				

				
				
				if(rccells[cRow].length <=5)
				{
				
				cell0=rccells[cRow][0].getPreviewText().substring(0, 12);
				
				cell1=rccells[cRow][1].getPreviewText().substring(0, 10);
				
				
				cell2=rccells[cRow][1].getPreviewText().substring(11);
				
				cell3=rccells[cRow][2].getPreviewText();
				
				 
				if(rccells[cRow].length >3){
					cell4=rccells[cRow][3].getPreviewText();
					cell5=rccells[cRow][4].getPreviewText();	
				}
				
				}
				
				
				else if(rccells[cRow].length >5){
					
					cell0=rccells[cRow][0].getPreviewText();
					cell1=rccells[cRow][1].getPreviewText();
					cell2=rccells[cRow][2].getPreviewText();
					cell3=rccells[cRow][3].getPreviewText();
					cell4=rccells[cRow][4].getPreviewText();
					
					
					cell5=rccells[cRow][5].getPreviewText();
					
				}
								if (cell0 != null) {
								rowBuffer.append("<TD width ='121' align='center'>");
								rowBuffer.append(cell0);
								rowBuffer.append("</TD>");
								}
								else
								{
									rowBuffer.append("<TD width ='121' align='center'>");
									rowBuffer.append("");
									rowBuffer.append("</TD>");
								}
								if (cell1 != null) {
								rowBuffer.append("<TD width ='122' align='center' class='leftline'>");
								rowBuffer.append(cell1);
								rowBuffer.append("</TD>");
								}
								else
								{
									rowBuffer.append("<TD width ='122' align='center' class='leftline'>");
									rowBuffer.append("");
									rowBuffer.append("</TD>");
								}
								if (cell2 != null) {
								rowBuffer.append("<TD width ='152' align='center' class='leftline'>");
								rowBuffer.append(cell2);
								rowBuffer.append("</TD>");
								}
								else
								{
									rowBuffer.append("<TD width ='152' align='center' class='leftline'>");
									rowBuffer.append("");
									rowBuffer.append("</TD>");
								}
								if (cell3 != null) {
								rowBuffer.append("<TD width ='122' align='center' class='leftline'>");
								rowBuffer.append(cell3);
								rowBuffer.append("</TD>");
								}
								else
								{
									rowBuffer.append("<TD width ='122' align='center' class='leftline'>");
									rowBuffer.append("");
									rowBuffer.append("</TD>");
								}
								if (cell4 != null) {
								rowBuffer.append("<TD width ='202' align='center' class='leftline'>");
								rowBuffer.append("<div class='leftlinepad18'>");
								rowBuffer.append(cell4);
								rowBuffer.append("</div>");
								rowBuffer.append("</TD>");
								}
								else
								{
									rowBuffer.append("<TD width ='202' align='center' class='leftline'>");
									rowBuffer.append("<div class='leftlinepad18'>");
									rowBuffer.append("");
									rowBuffer.append("</div>");
									rowBuffer.append("</TD>");
								}
								if (cell5 != null) {
								rowBuffer.append("<TD align='center' class='leftline'>");
								rowBuffer.append("<div class='leftlinepad17'>");
								rowBuffer.append(cell5);
								rowBuffer.append("</div>");
								rowBuffer.append("</TD>");
								}
								else
								{
									rowBuffer.append("<TD align='center' class='leftline'>");
									rowBuffer.append("<div class='leftlinepad17'>");
									rowBuffer.append("");
									rowBuffer.append("</div>");
									rowBuffer.append("</TD>");
								}
								
							
							
					
						
			
					
					rowBuffer.append("</TR> ");
					
					buffer.append(rowBuffer);
					}
			
			}
    	}
		catch (Exception e) {
			System.out.println("Account Summary Exception = "+e.getMessage());
		}
		}
				
		
    	
		buffer.append("</TBODY> ");
		
	
		buffer.append("</TABLE> ");
		buffer.append("<script> function doPage(myName){" +
				"alert('hello');"+
				"document.getElementById('myName').value='X';"+
				"ms('[enter]', 'HATSForm');"+
				"}</script>");
        
	
		
			System.out.println("This is account sum ");
		return (buffer);
	}
	public int getPropertyPageCount() {
		
		return (1);
	}

	public Vector getCustomProperties(int iPageNumber, Properties properties,
			ResourceBundle bundle) {
			return (null);
	}

	public Properties getDefaultValues(int iPageNumber) {
		// TODO Auto-generated method stub
		return (super.getDefaultValues(iPageNumber));
	}

}
