package gulistan_JPS02.widgets;

import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;

import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;

public class SpecInquiryWidget extends Widget implements HTMLRenderer {

	public SpecInquiryWidget(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
		
	}

	public StringBuffer drawHTML() {
		
		StringBuffer buffer = new StringBuffer(256);
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
		contextAttributes, settings);
		buffer.append("<TABLE ID='detailsTable' CLASS=\"HATSTABLE\" CELLSPACING=0 CELLPADDING=0 WIDTH=\"100%\" BORDER=0> ");
		buffer.append("<TBODY> ");
		int cRow=0;
		int cCol=0;
		String styleDesc = "";
		String colorDesc = "";
		int len =0;
		int startPos =0;
		int sid =0;
		ComponentElement[] elements = this.getComponentElements();
		
    	int noOfComponents = elements.length;
    	for (int compNo=0; compNo<noOfComponents; compNo++){
    		
    		
			TableComponentElement rcComp = (TableComponentElement)elements[compNo];

			
			TableCellComponentElement rccells[][] = rcComp.getCells();

			System.out.println("rccells= "+rccells.length);
			
			String oddEven = "oddRowd";
			
		try {
			for ( cRow=0; cRow<rccells.length; cRow++){
				
			if(!rccells[cRow][2].getPreviewText().trim().equals("")){
				
				if (oddEven.equalsIgnoreCase("evenRowd")) {
					oddEven="oddRowd";
				} else {
					oddEven="evenRowd";	
				}
					
			
				StringBuffer rowBuffer = new StringBuffer(256);
				len 			= rccells[cRow][0].getLength();
				sid 			= rccells[cRow][0].getScreenId();
				startPos 		= rccells[cRow][0].getStartPos();
				
				
				
				
				
				rowBuffer.append("<input type='hidden' name='in_"+startPos+"_1_"+sid+"' id='in_"+startPos+"_1_"+sid+"'>");
				rowBuffer.append("<TR id=\"row_"+cRow+"\" onclick =\"openPdf('"+rccells[cRow][1].getPreviewText().substring(0, 4).trim()+"');\" class=\""+oddEven+"\" onMouseOver=\"this.className='tableRowClickEffect1d'\" onMouseOut=\"this.className='"+oddEven+"'\">");
								
				
				if(rccells[cRow][3].getLength()==3 || rccells[cRow][3].getLength()==4)
				{
					String styleDescription=rccells[cRow][2].getPreviewText()+""+rccells[cRow][3].getPreviewText();
					rowBuffer.append("<TD width ='301' class='leftline' align='center'>");
					rowBuffer.append(rccells[cRow][1].getPreviewText());
					rowBuffer.append("</TD>");
					rowBuffer.append("<TD width ='352' class='leftline' align='center'>");
					rowBuffer.append("<div class='leftlinepad2'>");
					rowBuffer.append(styleDescription);
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
					rowBuffer.append("<TD class='leftline' align='center'>");
					rowBuffer.append("<div class='leftlinepad4'>");
					rowBuffer.append("<img title='Click here to view Invoice in PDF Format' border='0' src='../common/images/pdf_icon_16x16.png' width='20' height='20'></div>");
					rowBuffer.append("</TD>");
					
				}
				else{
						rowBuffer.append("<TD width ='301' class='leftline' align='center'>");
						rowBuffer.append(rccells[cRow][1].getPreviewText());
						rowBuffer.append("</TD>");
						rowBuffer.append("<TD width ='352' class='leftline' align='center'>");
						rowBuffer.append("<div class='leftlinepad2'>");
						rowBuffer.append(rccells[cRow][2].getPreviewText());
						rowBuffer.append("</div>");
						rowBuffer.append("</TD>");
						rowBuffer.append("<TD class='leftline' align='center'>");
						rowBuffer.append("<div class='leftlinepad4'>");
						rowBuffer.append("<img title='Click here to view Invoice in PDF Format' border='0' src='../common/images/pdf_icon_16x16.png' width='20' height='20'></div>");
						rowBuffer.append("</TD>");
								

				}	
								
								
								
							
							
					
						
			
					
					rowBuffer.append("</TR> ");
					
					buffer.append(rowBuffer);
					}
			
			}
    	}
		catch (Exception e) {
			System.out.println("Spec Inquiry Exception = "+e.getMessage());
		}
		}
				
		
    	
		buffer.append("</TBODY> ");
		
	
		buffer.append("</TABLE> ");
		buffer.append("<script> function doPage(myName){" +
				"alert('hello');"+
				"document.getElementById('myName').value='X';"+
				
				"ms('[enter]', 'HATSForm');"+
				"}</script>");
		return (buffer);
	}
	public int getPropertyPageCount() {
		
		return (1);
	}

	public Vector getCustomProperties(int iPageNumber, Properties properties,
			ResourceBundle bundle) {
				
		return (null);
	}

	public Properties getDefaultValues(int iPageNumber) {
	
		return (super.getDefaultValues(iPageNumber));
	}

}
