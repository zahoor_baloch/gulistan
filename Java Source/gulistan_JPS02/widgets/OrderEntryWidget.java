package gulistan_JPS02.widgets;


import java.util.ArrayList;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;

import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.FieldComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;









import com.ibm.hats.transform.elements.InputComponentElement;
import com.ibm.hats.transform.elements.FieldRowComponentElement;



import com.ibm.hats.transform.widgets.FieldWidget;

/**
 * @param args @Author Juzer 
 */
public class OrderEntryWidget extends Widget implements HTMLRenderer {

	//static Vector exceptionLabels = ExceptionLabelLoader.getExceptionLabels();
	public OrderEntryWidget(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
	}

	public StringBuffer drawHTML() {
		// TODO Auto-generated method stub
		StringBuffer buffer = new StringBuffer(256);
		
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
				contextAttributes, settings);
	
		
	
		//buffer.append("<DIV ID=\"scrollDiv1\" STYLE=\"overflow:auto; HEIGHT:310px; WIDTH:100% \"> ");
        
	
		buffer.append("<TABLE ID='detailsTable' CLASS=\"HATSTABLE\" CELLSPACING=0 CELLPADDING=0 WIDTH=\"100%\" BORDER=0> ");
				
		
	
		buffer.append("<TBODY> ");
		int cRow=0;
		int cCol=0;
		String styleDesc = "";
		String colorDesc = "";
		int len =0;
		int startPos =0;
		int sid =0;
		ComponentElement[] elements = this.getComponentElements();
    	int noOfComponents = elements.length;
    	//System.out.println("noOfComponents= "+noOfComponents);
    	//noOfComponents= 1
    	for (int compNo=0; compNo<noOfComponents; compNo++){
    		
    		
			TableComponentElement rcComp = (TableComponentElement)elements[compNo];

			
			TableCellComponentElement rccells[][] = rcComp.getCells();

			//System.out.println("rccells= "+rccells.length);
			//rccells= 96
			String oddEven = "oddRowd";
			
		try{
			for ( cRow=0; cRow<rccells.length; cRow++){
				//System.out.println("rccells["+cRow+"][2] = X__" +rccells[cRow][2].getPreviewText().trim()+"__X");
			if(!rccells[cRow][2].getPreviewText().trim().equals("")){
				//System.out.println("rccells["+cRow+"][2] is non empty");
				if (oddEven.equalsIgnoreCase("evenRowd")) {
					oddEven="oddRowd";
				} else {
					oddEven="evenRowd";	
				}

			
				StringBuffer rowBuffer = new StringBuffer(256);
				len 			= rccells[cRow][0].getLength();
				sid 			= rccells[cRow][0].getScreenId();
				startPos 		= rccells[cRow][0].getStartPos();
				
				
				
				rowBuffer.append("<input type='hidden' name='in_"+(startPos+1)+"_1_"+sid+"' id='in_"+(startPos+1)+"_1_"+sid+"'>");
				
				if(rccells[cRow][1].getPreviewText().contains("ORDERED")){
					rowBuffer.append("<TR id=\"record_"+cRow+"\" onclick =\"setCursorPosition("+(startPos+1)+", 'HATSForm');checkInput2('in_"+(startPos+1)+"_1_"+sid+"', '/','hidden');ms('[enter]', 'HATSForm')\" class=\"tableRowClickEffect1\" onMouseOver=\"this.className='tableRowClickEffect1d'\" onMouseOut=\"this.className='tableRowClickEffect1'\">");
				//System.out.println("The highlighted item is = "+rccells[cRow][1].getPreviewText());
				}
				else{
					rowBuffer.append("<TR id=\"record_"+cRow+"\" onclick =\"setCursorPosition("+(startPos+1)+", 'HATSForm');checkInput2('in_"+(startPos+1)+"_1_"+sid+"', '/','hidden');ms('[enter]', 'HATSForm')\" class=\""+oddEven+"\" onMouseOver=\"this.className='tableRowClickEffect1d'\" onMouseOut=\"this.className='"+oddEven+"'\">");	
					
				}
				
				
				
				//System.out.println("rccells[cRow]= "+rccells[cRow].length);
				//rccells[cRow]= 8 tableRowClickEffect1
		
				for(int j=1; j<rccells[cRow].length; j++){	
					
					
					//System.out.println("rccells["+cRow+"]["+j+"].getPreviewText() = "+rccells[cRow][j].getPreviewText());
				if(j==1){
					rowBuffer.append("<TD align='left' width='180' style='padding: 2px;'>");
				}
				else if(j==6){
					rowBuffer.append("<TD align='left' class='leftline'>");
				}
				else {
					rowBuffer.append("<TD align='left' class='leftline' style='padding: 2px;'>");
				}
				rowBuffer.append(rccells[cRow][j].getPreviewText());
				rowBuffer.append("</TD>");
				}			
							
				
					
						//rowBuffer.append(rccells[cRow][cCol].getPreviewText());
			
					
					rowBuffer.append("</TR> ");
					
					buffer.append(rowBuffer);
					}
			
			}}
  		catch (Exception e) {
  			System.out.println("Order Entry Exception = "+e.getMessage());
		}
		}
				
	
    	
		buffer.append("</TBODY> ");
		
	
		buffer.append("</TABLE> ");
		buffer.append("<script> function doPage(myName){" +
				"alert('hello');"+
				"document.getElementById('myName').value='X';"+
				//"checkBoxCheck(pageName);"+
				//"document.getElementById(pageName).value='20';"+
				"ms('[enter]', 'HATSForm');"+
				"}</script>");
        
	
		//buffer.append("</DIV>");
			
		return (buffer);
	}
	
	
	public int getPropertyPageCount() {
		// TODO Auto-generated method stub
		return (1);
	}

	public Vector getCustomProperties(int iPageNumber, Properties properties,
			ResourceBundle bundle) {
		// TODO Auto-generated method stub
		// Vector v = new Vector();
		// Fill vector with com.ibm.hats.common.HCustomProperty objects.
		// See the API reference for more information.
		// return(v);
		return (null);
	}

	public Properties getDefaultValues(int iPageNumber) {
		// TODO Auto-generated method stub
		return (super.getDefaultValues(iPageNumber));
	}

}