package gulistan_JPS02.widgets;

import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;

import com.ibm.hats.common.CommonFunctions;
import com.ibm.hats.common.HCustomProperty;
import com.ibm.hats.transform.TransformationFunctions;
import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.FieldComponentElement;
import com.ibm.hats.transform.elements.FieldRowComponentElement;
import com.ibm.hats.transform.html.HTMLElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.html.InputElement;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;

public class TextAreaWidget1 extends Widget implements HTMLRenderer {

	public TextAreaWidget1(ComponentElement[] arg0, Properties arg1) {
		super(arg0, arg1);
	}

	public StringBuffer drawHTML() {
		StringBuffer buffer = new StringBuffer(256);
		HTMLElementFactory factory = HTMLElementFactory.newInstance(contextAttributes, settings);

		String style = CommonFunctions.getSettingProperty_String(settings,"style", "");

		StringBuffer hiddenElements = new StringBuffer(256);

		if (componentElements != null && componentElements.length > 0) {
			int numCols = 0;
			int numRows = 0;
			String value = "";
			int startPosition = -1;
			int partGap = -1; // the number of characters between each part

			for (int i = 0; i < componentElements.length; i++) {
				if (componentElements[i] instanceof FieldRowComponentElement) {
					FieldRowComponentElement frce = (FieldRowComponentElement) componentElements[i];
					if (frce.getElementCount() == 1) {
						FieldComponentElement fce = (FieldComponentElement) frce.getElement(0);
						if (fce.isProtected()) {
							HTMLElement spanElement = new HTMLElement("span");
							spanElement.setStyle(style);
							spanElement.render(buffer, false);
							buffer.append(fce.getText() + "<br>");
							spanElement.renderEndTag();
						} else {
							if (startPosition == -1) {
								startPosition = fce.getStartPos();
							} else if (partGap == -1) {
								partGap = fce.getStartPos() - startPosition - fce.getLength();
							}

							numCols = fce.getLength();
							numRows++;

							String str = TransformationFunctions.rightTrimString(fce.getText());
							if (str.length() < numCols - 1) {
								if (str.trim().length() > 0) {
									value = value + TransformationFunctions.rightTrimString(fce.getText()) + "\n";
								}
							} else {
								value = value + fce.getText(); // + "\n";
							}

							InputElement hiddenElement = new InputElement();
							hiddenElement.setType("hidden");
							hiddenElement.setAttribute("size", numCols + "");
							hiddenElement.setName(HTMLElementFactory.generateElementName(fce));
							hiddenElement.setValue(fce.getText());

							hiddenElement.render(hiddenElements);
						}
					}
				}
			}

			if (numCols > 0 && numRows > 0) {
				if (TransformationFunctions.isInStudio(contextAttributes)) {
					buffer.append("<form name='HATSForm' method='post' action='/junk'>");
				}

				String textAreaName = "text" + ((int) (Math.random() * 10));
				buffer.append(createTextAreaElement(textAreaName, numRows,numCols, value, startPosition));
				//buffer.append("<br>");
				String msgLine ="";
				//buffer.append(createWarningMsg(msgLine));
				//buffer.append("<br>");
				//buffer.append(createLengthIndicatorField(textAreaName, numRows * numCols));
				buffer.append(hiddenElements.toString());
				buffer.append(createOnSubmitCall(startPosition, numCols,partGap, numRows, textAreaName));
			}
		}

		return (buffer);
	}

	protected String createTextAreaElement(String name, int numRows,
			int numCols, String value, int startPosition) {
		HTMLElement element = new HTMLElement("textarea");
		element.setAttribute("rows", numRows + "");
		element.setAttribute("cols", numCols + "");
		element.setAttribute("name", name);
		
		element.setAttribute("onKeyPress", "limitText(this, "+ (numRows * numCols) + ", " + numCols + ")");
		element.setAttribute("onFocus", "limitText(this, "+ (numRows * numCols) + ", " + numCols + ")");
		
		
		String style = CommonFunctions.getSettingProperty_String(settings,"style", "");
		element.setStyle(style);

		StringBuffer buffer = new StringBuffer();
		element.render(buffer, false);
		buffer.append(value);
		element.renderEndTag(buffer);

		return buffer.toString();
	}

	protected String createOnSubmitCall(int startPos, int partLength,	int partGap, int numParts, String textAreaName) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<script>");
		buffer.append(" document.HATSForm.onSubmit = \"processTextAreaField("+ startPos+ ","+ partLength	+ ","
				+ partGap	+ ","	+ numParts + ", '" + textAreaName + "')\";");
		buffer.append("</script>");
		return buffer.toString();
	}
	
	public int getPropertyPageCount() {
		return 1;
	}

	/*public Vector getCustomProperties(int iPageNumber, Properties properties,	ResourceBundle bundle) {
		Vector v = new Vector();
		v.add(HCustomProperty.new_Style("style", "Style:", false, "", null,	null));
		return v;
	}

	public Properties getDefaultValues(int iPageNumber) {
		Properties defaults = new Properties();
		defaults.put("style", "font-family: monospace; font-size: 10pt;");
		return defaults;
	}*/

}
