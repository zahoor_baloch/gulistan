package gulistan_JPS02.widgets;


import java.util.ArrayList;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;

import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.FieldComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;









import com.ibm.hats.transform.elements.InputComponentElement;
import com.ibm.hats.transform.elements.FieldRowComponentElement;



import com.ibm.hats.transform.widgets.FieldWidget;

/**
 * @param args @Author Juzer 
 */
public class SelectionComboWidget extends Widget implements HTMLRenderer {

	//static Vector exceptionLabels = ExceptionLabelLoader.getExceptionLabels();
	public SelectionComboWidget(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
	}

	public StringBuffer drawHTML() {
		// TODO Auto-generated method stub
		StringBuffer buffer = new StringBuffer(256);
		
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
				contextAttributes, settings);
	
		
	
		//buffer.append("<DIV ID=\"scrollDiv1\" STYLE=\"overflow:auto; HEIGHT:310px; WIDTH:100% \"> ");
        
	
		buffer.append("<TABLE ID='detailsTable' CLASS=\"HATSTABLE\" CELLSPACING=0 CELLPADDING=0 WIDTH=\"100%\" BORDER=0> ");
				
		
	
		buffer.append("<TBODY> ");
		int cRow=0;
		int cCol=0;
		String styleDesc = "";
		String colorDesc = "";
		int len =0;
		int startPos =0;
		int sid =0;
		ComponentElement[] elements = this.getComponentElements();
    	int noOfComponents = elements.length;
    	//System.out.println("noOfComponents= "+noOfComponents);
    	//noOfComponents= 1
    	for (int compNo=0; compNo<noOfComponents; compNo++){
    		
    		
			TableComponentElement rcComp = (TableComponentElement)elements[compNo];

			
			TableCellComponentElement rccells[][] = rcComp.getCells();

			System.out.println("rccells= "+rccells.length);
			//rccells= 96
			String oddEven = "oddRowd";
			
		try {
			for ( cRow=0; cRow<rccells.length; cRow++){
				System.out.println("rccells["+cRow+"][2] = X__" +rccells[cRow][2].getPreviewText().trim()+"__X");
			if(!rccells[cRow][2].getPreviewText().trim().equals("")){
				System.out.println("rccells["+cRow+"][2] is non empty");
				if (oddEven.equalsIgnoreCase("evenRowd")) {
					oddEven="oddRowd";
				} else {
					oddEven="evenRowd";	
				}

			
				StringBuffer rowBuffer = new StringBuffer(256);
				len 			= rccells[cRow][0].getLength();
				sid 			= rccells[cRow][0].getScreenId();
				startPos 		= rccells[cRow][0].getStartPos();
				
				rowBuffer.append("<input type='hidden' name='in_"+startPos+"_1_"+sid+"' id='in_"+startPos+"_1_"+sid+"'>");
				rowBuffer.append("<TR id=\"row_"+cRow+"\" onclick =\"setCursorPosition("+startPos+", 'HATSForm');checkInput2('in_"+startPos+"_1_"+sid+"', '/','hidden');ms('[enter]', 'HATSForm')\" class=\""+oddEven+"\" onMouseOver=\"this.className='tableRowClickEffect1d'\" onMouseOut=\"this.className='"+oddEven+"'\">");
				
				
				System.out.println("rccells[cRow]= "+rccells[cRow].length);
				//rccells[cRow]= 8
				//System.out.println("the length = "+rccells[cRow][2].getLength());//8 STYLE
				//System.out.println(rccells[cRow][2].getPreviewText());//50230
				//System.out.println(rccells[cRow][3].getLength());//9 COLOR
				//System.out.println(rccells[cRow][3].getPreviewText());//206
				//System.out.println(rccells[cRow][4].getLength());//4 BK
				//System.out.println(rccells[cRow][4].getPreviewText());//AB
				//System.out.println(rccells[cRow][5].getLength());//7 Style Description
				//System.out.println(rccells[cRow][5].getPreviewText());//ANGELS
				//System.out.println(rccells[cRow][6].getLength());//16 Style Description 2nd Part
				//System.out.println(rccells[cRow][6].getPreviewText());//WAY
				//System.out.println(rccells[cRow][7].getLength());//23 Color Description
				//System.out.println(rccells[cRow][7].getPreviewText());//WING WISP
							if(rccells[cRow].length > 7 && rccells[cRow].length != 9){
								if(rccells[cRow][7].getLength() == 23){
									styleDesc = rccells[cRow][5].getPreviewText() +" "+rccells[cRow][6].getPreviewText();
									colorDesc = rccells[cRow][7].getPreviewText();
							}
							else
							{
								styleDesc = rccells[cRow][5].getPreviewText();
								colorDesc = rccells[cRow][6].getPreviewText();
								
							}	
								
								rowBuffer.append("<TD width ='125' align='left'>");
								rowBuffer.append(rccells[cRow][2].getPreviewText());
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD width ='80' class='leftline' align='left'>");
								rowBuffer.append(rccells[cRow][3].getPreviewText());
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD width ='66' class='leftline' align='left'>");
								rowBuffer.append(rccells[cRow][4].getPreviewText());
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD width ='303' class='leftline' align='left'>");
								rowBuffer.append(styleDesc);
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD class='leftline' align='left'>");
								rowBuffer.append(colorDesc);
								rowBuffer.append("</TD>");
							}
							
							else if(rccells[cRow].length > 7 && rccells[cRow].length == 9){
								System.out.println("There are 9 cells in this row");
								System.out.println("Nine cell 2 = "+rccells[cRow][2].getPreviewText());
								System.out.println("Nine cell 3 = "+rccells[cRow][3].getPreviewText());
								System.out.println("Nine cell 4 = "+rccells[cRow][4].getPreviewText());
								System.out.println("Nine cell 5 = "+rccells[cRow][5].getPreviewText());
								System.out.println("Nine cell 6 = "+rccells[cRow][6].getPreviewText());
								System.out.println("Nine cell 7 = "+rccells[cRow][7].getPreviewText());
								System.out.println("Nine cell 8 = "+rccells[cRow][8].getPreviewText());
								
								
								if(!rccells[cRow][7].getPreviewText().trim().equalsIgnoreCase("") && rccells[cRow][8].getPreviewText().trim().equalsIgnoreCase("")){
									styleDesc = rccells[cRow][5].getPreviewText() +" "+ rccells[cRow][6].getPreviewText();
									
									colorDesc = rccells[cRow][7].getPreviewText();
								}
								
								else{
									
									styleDesc = rccells[cRow][5].getPreviewText() +" "+ rccells[cRow][6].getPreviewText()+" "
									+rccells[cRow][7].getPreviewText();
									colorDesc = rccells[cRow][8].getPreviewText();
								}
								
								
								
								
								rowBuffer.append("<TD width ='125' align='left'>");
								rowBuffer.append(rccells[cRow][2].getPreviewText());
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD width ='80' class='leftline' align='left'>");
								rowBuffer.append(rccells[cRow][3].getPreviewText());
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD width ='66' class='leftline' align='left'>");
								rowBuffer.append(rccells[cRow][4].getPreviewText());
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD width ='303' class='leftline' align='left'>");
								rowBuffer.append(styleDesc);
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD class='leftline' align='left'>");
								rowBuffer.append(colorDesc);
								rowBuffer.append("</TD>");
							}
							
							else{
								rowBuffer.append("<TD width ='255' align='left'>");
								rowBuffer.append(rccells[cRow][2].getPreviewText());
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD width ='80' class='leftline' align='left'>");
								rowBuffer.append(rccells[cRow][3].getPreviewText());
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD width ='66' class='leftline' align='left'>");
								rowBuffer.append(rccells[cRow][4].getPreviewText());
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD width ='303' class='leftline' align='left'>");
								rowBuffer.append(rccells[cRow][5].getPreviewText());
								rowBuffer.append("</TD>");
								rowBuffer.append("<TD class='leftline' align='left'>");
								rowBuffer.append(rccells[cRow][6].getPreviewText());
								rowBuffer.append("</TD>");
								
								
							}
							
							
							
				
					
						rowBuffer.append(rccells[cRow][cCol].getPreviewText());
			
					
					rowBuffer.append("</TR> ");
					
					buffer.append(rowBuffer);
					}
			
			}
    	}
		catch (Exception e) {
			System.out.println("Selection Combo Exception = "+e.getMessage());
		}
		}
				
		
    	
		buffer.append("</TBODY> ");
		
	
		buffer.append("</TABLE> ");
		buffer.append("<script> function doPage(myName){" +
				"alert('hello');"+
				"document.getElementById('myName').value='X';"+
				//"checkBoxCheck(pageName);"+
				//"document.getElementById(pageName).value='20';"+
				"ms('[enter]', 'HATSForm');"+
				"}</script>");
        
	
		//buffer.append("</DIV>");
			
		return (buffer);
	}
	
	
	public int getPropertyPageCount() {
		// TODO Auto-generated method stub
		return (1);
	}

	public Vector getCustomProperties(int iPageNumber, Properties properties,
			ResourceBundle bundle) {
		// TODO Auto-generated method stub
		// Vector v = new Vector();
		// Fill vector with com.ibm.hats.common.HCustomProperty objects.
		// See the API reference for more information.
		// return(v);
		return (null);
	}

	public Properties getDefaultValues(int iPageNumber) {
		// TODO Auto-generated method stub
		return (super.getDefaultValues(iPageNumber));
	}

}