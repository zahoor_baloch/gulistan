package gulistan_JPS02.widgets;

import java.util.Properties;

import sun.security.action.GetLongAction;

import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;

public class CustomerSearchCombo extends Widget implements HTMLRenderer {

	public CustomerSearchCombo(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
		// TODO Auto-generated constructor stub
	}

	public StringBuffer drawHTML() {
		// TODO Auto-generated method stub
		StringBuffer buffer = new StringBuffer(256);
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
				contextAttributes, settings);
		
buffer.append("<TABLE ID='detailsTable' CLASS=\"HATSTABLE\" CELLSPACING=0 CELLPADDING=0 WIDTH=\"100%\" BORDER=0> ");


		
String cell1="";
String cell2="";
String cell3="";

String cell4="";
		buffer.append("<TBODY> ");
		int cRow=0;
		int cCol=0;
		String styleDesc = "";
		String colorDesc = "";
		int len =0;
		int startPos =0;
		int sid =0;
		ComponentElement[] elements = this.getComponentElements();
		
    	int noOfComponents = elements.length;
    	//System.out.println("noOfComponents= "+noOfComponents);
    	//noOfComponents= 1
    	for (int compNo=0; compNo<noOfComponents; compNo++){
    		
    		
			TableComponentElement rcComp = (TableComponentElement)elements[compNo];

			
			TableCellComponentElement rccells[][] = rcComp.getCells();

			System.out.println("rccells= "+rccells.length);
			
			String oddEven = "oddRowd";
			
		try {
			for ( cRow=0; cRow<rccells.length; cRow++){
			//	System.out.println("rccells["+cRow+"][2] = X__" +rccells[cRow][2].getPreviewText().trim()+"__X");
			if(!rccells[cRow][2].getPreviewText().trim().equals("")){
				//System.out.println("rccells["+cRow+"][2] is non empty");
				if (oddEven.equalsIgnoreCase("evenRowd")) {
					oddEven="oddRowd";
				} else {
					oddEven="evenRowd";	
				}
					
			
				StringBuffer rowBuffer = new StringBuffer(256);
				len 			= rccells[cRow][0].getLength();
				sid 			= rccells[cRow][0].getScreenId();
				startPos 		= rccells[cRow][0].getStartPos();
				
				
				
				
				
				rowBuffer.append("<input type='hidden' name='in_"+startPos+"_1_"+sid+"' id='in_"+startPos+"_1_"+sid+"'>");
				rowBuffer.append("<TR id=\"row_"+cRow+"\" onclick =\"setCursorPosition("+startPos+", 'HATSForm');checkInput2('in_"+startPos+"_1_"+sid+"', '/','hidden');ms('[enter]', 'HATSForm')\" class=\""+oddEven+"\" onMouseOver=\"this.className='tableRowClickEffect1d'\" onMouseOut=\"this.className='"+oddEven+"'\">");
								
				
				//System.out.println("rccells[cRow]= "+rccells[cRow].length);
				
				
				
				
				
				
					
					
					
					if(rccells[cRow][2].getLength()==6 && rccells[cRow][1].getLength()==7){
						
						
						
						cell1=rccells[cRow][1].getPreviewText();
						cell2=rccells[cRow][2].getPreviewText()+""+rccells[cRow][3].getPreviewText();
						cell3=rccells[cRow][4].getPreviewText();
						
						
					}
					else if(rccells[cRow][2].getLength()==31){
						cell1=rccells[cRow][1].getPreviewText();
						cell2=rccells[cRow][2].getPreviewText();
						cell3=rccells[cRow][3].getPreviewText()+""+rccells[cRow][4].getPreviewText();
						
					}
					else if(rccells[cRow][2].getLength()==2){
						cell1=rccells[cRow][1].getPreviewText();
						cell2=rccells[cRow][2].getPreviewText()+""+rccells[cRow][3].getPreviewText();
						cell3=rccells[cRow][4].getPreviewText();
					}
					rowBuffer.append("<TD width ='301' class='leftline' align='center'>");
					rowBuffer.append("<div class='leftlinepad299'>");
					rowBuffer.append(cell1);
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
					rowBuffer.append("<TD width ='352' class='leftline' align='center'>");
					rowBuffer.append("<div class='leftlinepad299'>");
					rowBuffer.append(cell2);
					rowBuffer.append("</div>");
					rowBuffer.append("</TD>");
					rowBuffer.append("<TD class='leftline' align='center'>");
					rowBuffer.append("<div class='leftlinepad299'>");
					
					rowBuffer.append(cell3);
					rowBuffer.append("</TD>");
					
					
					

						
					

					
				
	
								
								
								
							
							
					
						
			
					
					rowBuffer.append("</TR> ");
					
					buffer.append(rowBuffer);
					}
			
			}
    	}
		catch (Exception e) {
			System.out.println("Spec Inquiry Exception = "+e.getMessage());
		}
		}
				
		
    	
		buffer.append("</TBODY> ");
		
	
		buffer.append("</TABLE> ");
		buffer.append("<script> function doPage(myName){" +
				"alert('hello');"+
				"document.getElementById('myName').value='X';"+
				//"checkBoxCheck(pageName);"+
				//"document.getElementById(pageName).value='20';"+
				"ms('[enter]', 'HATSForm');"+
				"}</script>");
		return (buffer);
	}

}
