package gulistan_JPS02.widgets;

import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;

import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;
import com.ibm.hats.common.IGlobalVariable; 
import com.ibm.hats.common.GlobalVariable;

public class InvoiceInquiryWidget extends Widget implements HTMLRenderer {

	public InvoiceInquiryWidget(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
		// TODO Auto-generated constructor stub
	}

	public StringBuffer drawHTML() {
		// TODO Auto-generated method stub
		StringBuffer buffer = new StringBuffer(256);
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
				contextAttributes, settings);
	
buffer.append("<TABLE ID='detailsTable' CLASS=\"HATSTABLE\" CELLSPACING=0 CELLPADDING=0 WIDTH=\"100%\" BORDER=0> ");


		
		
		buffer.append("<TBODY> ");
		int cRow=0;
		int cCol=0;
		String styleDesc = "";
		String colorDesc = "";
		int len =0;
		int startPos =0;
		int sid =0;
		ComponentElement[] elements = this.getComponentElements();
		
    	int noOfComponents = elements.length;
    
    	for (int compNo=0; compNo<noOfComponents; compNo++){
    	TableComponentElement rcComp = (TableComponentElement)elements[compNo];
		TableCellComponentElement rccells[][] = rcComp.getCells();

			//rccells= 26
			String oddEven = "oddRowd";
			
		try {
			for ( cRow=0; cRow<rccells.length; cRow++){
				
			if(!rccells[cRow][2].getPreviewText().trim().equals("")){
			
				if (oddEven.equalsIgnoreCase("evenRowd")) {
					oddEven="oddRowd";
				} else {
					oddEven="evenRowd";	
				}

			
				StringBuffer rowBuffer = new StringBuffer(256);
				len 			= rccells[cRow][0].getLength();
				sid 			= rccells[cRow][0].getScreenId();
				startPos 		= rccells[cRow][0].getStartPos();
				
				rowBuffer.append("<input type='hidden' name='in_"+startPos+"_1_"+sid+"' id='in_"+startPos+"_1_"+sid+"'>");
				rowBuffer.append("<TR id=\"row_"+cRow+"\" onclick =\"openPdf('"+rccells[cRow][0].getPreviewText().trim()+"');\" class=\""+oddEven+"\" onMouseOver=\"this.className='tableRowClickEffect1d'\" onMouseOut=\"this.className='"+oddEven+"'\">");
						
								
								if (rccells[cRow][0].getPreviewText() != null) {	
								rowBuffer.append("<TD width ='121' align='center'>");
								rowBuffer.append("<div class='leftlinepad15'>");
								rowBuffer.append(rccells[cRow][0].getPreviewText());
								rowBuffer.append("</div>");
								rowBuffer.append("</TD>");
								}
								else
								{
									rowBuffer.append("<TD width ='121' align='center'>");
									rowBuffer.append("<div class='leftlinepad15'>");
									rowBuffer.append("");
									rowBuffer.append("</div>");
									rowBuffer.append("</TD>");
									
								}
								if (rccells[cRow][1].getPreviewText() != null) {
								rowBuffer.append("<TD width ='122' class='leftline' align='center'>");
								rowBuffer.append("<div class='leftlinepad14'>");
								rowBuffer.append(rccells[cRow][1].getPreviewText());
								rowBuffer.append("</div>");
								rowBuffer.append("</TD>");
								}
								else
								{
									rowBuffer.append("<TD width ='122' class='leftline' align='center'>");
									rowBuffer.append("<div class='leftlinepad14'>");
									rowBuffer.append("");
									rowBuffer.append("</div>");
									rowBuffer.append("</TD>");
								}
								if (rccells[cRow][2].getPreviewText() != null) {
								rowBuffer.append("<TD width ='152' class='leftline' align='center'>");
								rowBuffer.append("<div class='leftlinepad13'>");
								rowBuffer.append(rccells[cRow][2].getPreviewText());
								rowBuffer.append("</div>");
								rowBuffer.append("</TD>");
								}
								else
								{
									rowBuffer.append("<TD width ='152' class='leftline' align='center'>");
									rowBuffer.append("<div class='leftlinepad13'>");
									rowBuffer.append("");
									rowBuffer.append("</div>");
									rowBuffer.append("</TD>");
								}
								if (rccells[cRow][3].getPreviewText() != null) {
								rowBuffer.append("<TD width ='122' class='leftline' align='center'>");
								rowBuffer.append("<div class='leftlinepad12'>");
								rowBuffer.append(rccells[cRow][3].getPreviewText());
								rowBuffer.append("</div>");
								rowBuffer.append("</TD>");
								}
								else
								{
									rowBuffer.append("<TD width ='122' class='leftline' align='center'>");
									rowBuffer.append("<div class='leftlinepad12'>");
									rowBuffer.append("");
									rowBuffer.append("</div>");
									rowBuffer.append("</TD>");
								}
								if (rccells[cRow][4].getPreviewText() != null) {
								rowBuffer.append("<TD width ='152' class='leftline' align='center'>");
								rowBuffer.append("<div class='leftlinepad11'>");
								rowBuffer.append(rccells[cRow][4].getPreviewText());
								rowBuffer.append("</div>");
								rowBuffer.append("</TD>");
								}
								else
								{
									rowBuffer.append("<TD width ='152' class='leftline' align='center'>");
									rowBuffer.append("<div class='leftlinepad11'>");
									rowBuffer.append("");
									rowBuffer.append("</div>");
									rowBuffer.append("</TD>");
								}
								if (rccells[cRow][5].getPreviewText() != null) {
								rowBuffer.append("<TD width ='152' class='leftline' align='center'>");
								rowBuffer.append("<div class='leftlinepad11'>");
								rowBuffer.append(rccells[cRow][5].getPreviewText());
								rowBuffer.append("</div>");
								rowBuffer.append("</TD>");
								}
								else
								{
									rowBuffer.append("<TD width ='152' class='leftline' align='center'>");
									rowBuffer.append("<div class='leftlinepad11'>");
									rowBuffer.append("");
									rowBuffer.append("</div>");
									rowBuffer.append("</TD>");
								}
								rowBuffer.append("<TD class='leftline' align='center'>");
								rowBuffer.append("<div class='leftlinepad3'><img title='Click here to view Invoice in PDF Format' border='0' src='../common/images/pdf_icon_16x16.png' width='20' height='20'></div>");
								rowBuffer.append("</TD>");
								
								
								
								
							
							
					
						
			
					
					rowBuffer.append("</TR> ");
					
					buffer.append(rowBuffer);
					}
			
			}
    	}
		catch(NullPointerException ne)
		{
			System.out.println("NullPointerException has been ignored in widget");
		}
		catch (Exception e) {
			System.out.println("Invoice Inquiry Exception = "+e.getMessage());
		}
		}
				
		
    	
		buffer.append("</TBODY> ");
		
	
		buffer.append("</TABLE> ");
		buffer.append("<script> function doPage(myName){" +
				"alert('hello');"+
				"document.getElementById('myName').value='X';"+
				"ms('[enter]', 'HATSForm');"+
				"}</script>");
		return (buffer);
	}
	public int getPropertyPageCount() {
		
		return (1);
	}

	public Vector getCustomProperties(int iPageNumber, Properties properties,
			ResourceBundle bundle) {
		
		
		return (null);
	}

	public Properties getDefaultValues(int iPageNumber) {
		
		return (super.getDefaultValues(iPageNumber));
	}

}
