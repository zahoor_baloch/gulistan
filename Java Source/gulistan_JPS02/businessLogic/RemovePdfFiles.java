package gulistan_JPS02.businessLogic;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;

import org.apache.openjpa.lib.util.Files;

import com.ibm.hats.common.IBusinessLogicInformation;
import com.ibm.hats.common.GlobalVariable;
import com.ibm.hats.common.IGlobalVariable;
import com.ibm.websphere.validation.base.config.level502.requestmetricsvalidation_502_NLS;

public class RemovePdfFiles {

	public static void execute(IBusinessLogicInformation blInfo) {
		System.out.println("In execute method of RemovePdfFiles");
		//Properties props = System.getProperties();
		//String userDir = System.getProperty("user.name");
		//System.out.println("userDir:......... "+userDir);
		// Enumerate all system properties


		
		
		
		String path1 = new String("C:\\Documents and Settings\\Royal Cyber\\IBM\\rationalsdp\\Gulistan_V01\\Gulistan_JPS02\\Web Content\\PdfFile");
		String path2 = new String("C:\\Documents and Settings\\Royal Cyber\\IBM\\rationalsdp\\Gulistan_V01\\Gulistan_JPS02\\Web Content\\excelFile");
		try {
			doDelete(path1);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			doDelete(path2);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	
	public static void doDelete(String path) throws IOException
	{
		
		
		
		File directory = new File(path);
		System.out.println("Path is: "+path);
		File[] files = directory.listFiles();
		for (File file : files)
		{
			System.out.println( "Fil is: "+file );
			if (!file.delete())
			{
				System.out.println("Failed to delete "+file);
			}
		}
		
		
	}
		
	
	
		
	

	public static void setGlobalVariable(IBusinessLogicInformation blInfo,
			String name, Object value) {
		IGlobalVariable gv = blInfo.getGlobalVariable(name);
		if (gv == null) {
			gv = new GlobalVariable(name, value);
		} else {
			gv.set(value);
		}
		blInfo.getGlobalVariables().put(name, gv);
	}

	public static void setSharedGlobalVariable(
			IBusinessLogicInformation blInfo, String name, Object value) {
		IGlobalVariable gv = blInfo.getSharedGlobalVariable(name);
		if (gv == null) {
			gv = new GlobalVariable(name, value);
		} else {
			gv.set(value);
		}
		blInfo.getSharedGlobalVariables().put(name, gv);
	}

	public static void removeGlobalVariable(IBusinessLogicInformation blInfo,
			String name) {
		IGlobalVariable gv = blInfo.getGlobalVariable(name);
		if (gv != null) {
			blInfo.getGlobalVariables().remove(name);
			gv.clear();
			gv = null;
		}
	}

	public static void removeSharedGlobalVariable(
			IBusinessLogicInformation blInfo, String name) {
		IGlobalVariable gv = blInfo.getSharedGlobalVariable(name);
		if (gv != null) {
			blInfo.getSharedGlobalVariables().remove(name);
			gv.clear();
			gv = null;
		}
	}

	public static IGlobalVariable getGlobalVariable(
			IBusinessLogicInformation blInfo, String name) {
		IGlobalVariable gv = blInfo.getGlobalVariable(name);
		return gv;
	}

	public static IGlobalVariable getSharedGlobalVariable(
			IBusinessLogicInformation blInfo, String name) {
		IGlobalVariable gv = blInfo.getSharedGlobalVariable(name);
		return gv;
	}

}
