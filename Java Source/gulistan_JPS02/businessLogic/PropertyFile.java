package gulistan_JPS02.businessLogic;


import com.ibm.hats.common.IBusinessLogicInformation;
import com.ibm.hats.common.GlobalVariable;
import com.ibm.hats.common.IGlobalVariable;
import java.util.*;
import java.io.*;
import com.ibm.hats.runtime.*;

/**
 * This class is used to perform business logic on a session 
 * It can be invoked by adding an Execute action to a screen customization 
 * The javadoc for the Business Logic functions is available at  
 * http://publib.boulder.ibm.com/infocenter/hatsv6/index.jsp
 */
public class PropertyFile
{

    /**
     * Method that executes business logic on a session
     * This is invoked by the runtime when an Execute Action is requested
     * in the Screen Customization Event
     * @param blInfo - Contains useful information about the current application
     */
    public static void execute(IBusinessLogicInformation blInfo)
    {
    	Properties property=new Properties();
    	IContext context=blInfo.getContext();
    	try{
    	
    	InputStream is=context.getClass().getClassLoader().getResourceAsStream("user.properties");
    	property.load(is);
    	
    	
    	
    	}catch(Exception e){
    	System.out.println(e.getMessage());
    	}
    	/*setGlobalVariable(blInfo,"userName",property.getProperty("username"));
    	setGlobalVariable(blInfo,"password",property.getProperty("password"));*/
    	String path = property.getProperty("path");
    	
    	File gulistan = new File(path);
		File excel = new File(path+"/excelFile");
			
			if(gulistan.isDirectory()){
				if(!(excel.isDirectory())){
					excel.mkdir();
					//path2 = excel.getPath();
				}
			}else{
				gulistan.mkdir();
				excel.mkdir();
				//path2 = excel.getPath();
			}		
		
    	
    	

    }

	/**
	 * Example method that sets a named global variable from the current session to a value
	 * @param blInfo - IBusinessLogicInformation from current session
	 * @param name - Name of the global variable
	 * @param value - Value of the global variable
	 */
	public static void setGlobalVariable(IBusinessLogicInformation blInfo, String name, Object value)
	{
		IGlobalVariable gv = blInfo.getGlobalVariable(name);
		if ( gv == null )
		{
			gv = new GlobalVariable(name,value);
		}
		else
		{
			gv.set(value);		
		}
		blInfo.getGlobalVariables().put(name,gv);
	}

	/**
	 * Example method that sets a named shared global variable from the current session to a value
	 * @param blInfo - IBusinessLogicInformation from current session
	 * @param name - Name of the shared global variable
	 * @param value - Value of the shared global variable
	 */	
	public static void setSharedGlobalVariable(IBusinessLogicInformation blInfo, String name, Object value)
	{
		IGlobalVariable gv = blInfo.getSharedGlobalVariable(name);
		if ( gv == null )
		{
			gv = new GlobalVariable(name,value);
		}
		else
		{
			gv.set(value);		
		}
		blInfo.getSharedGlobalVariables().put(name,gv);
	}
	
	/**
	 * Example method that removes a named global variable from the current session
	 * @param blInfo - IBusinessLogicInformation from current session
	 * @param name - Name of the global variable
	 */
	public static void removeGlobalVariable(IBusinessLogicInformation blInfo, String name)
	{
		IGlobalVariable gv = blInfo.getGlobalVariable(name);
		if ( gv != null )
		{
			blInfo.getGlobalVariables().remove(name);
			gv.clear();
			gv = null;
		}
	}
	
	/**
	 * Example method that removes a named shared global variable from the current session
	 * @param blInfo - IBusinessLogicInformation from current session
	 * @param name - Name of the shared global variable
	 */
	public static void removeSharedGlobalVariable(IBusinessLogicInformation blInfo, String name)
	{
		IGlobalVariable gv = blInfo.getSharedGlobalVariable(name);
		if ( gv != null )
		{
			blInfo.getSharedGlobalVariables().remove(name);
			gv.clear();
			gv = null;
		}
	}

    /**
     * Example method that retrieves a named global variable from the current session
     * @param blInfo - IBusinessLogicInformation from current session
     * @param name - Name of the global variable
     */
    public static IGlobalVariable getGlobalVariable(IBusinessLogicInformation blInfo, String name)
    {        
        IGlobalVariable gv = blInfo.getGlobalVariable(name);
        return gv;
    }

	/**
	 * Example method that retrieves a named shared global variable from the current session
	 * @param blInfo - IBusinessLogicInformation from current session
	 * @param name - Name of the shared global variable
	 */
	public static IGlobalVariable getSharedGlobalVariable(IBusinessLogicInformation blInfo, String name)
	{		
		IGlobalVariable gv = blInfo.getSharedGlobalVariable(name);
		return gv;
	}

}
