package gulistan_JPS02.businessLogic;

import com.ibm.hats.common.IBusinessLogicInformation;
import com.ibm.hats.common.GlobalVariable;
import com.ibm.hats.common.IGlobalVariable;

/**
 * This class is used to perform business logic on a session 
 * It can be invoked by adding an Execute action to a screen customization 
 * The javadoc for the Business Logic functions is available at  
 * http://publib.boulder.ibm.com/infocenter/hatsv6/index.jsp
 */
public class ArrangeMenuItems
{

    /**
     * Method that executes business logic on a session
     * This is invoked by the runtime when an Execute Action is requested
     * in the Screen Customization Event
     * @param blInfo - Contains useful information about the current application
     */
    public static void execute(IBusinessLogicInformation blInfo)
    {
        //add code here to perform your business logic
    	try {

        	//setGlobalVariable(blInfo, "ariq07Run", "false") ;
        	//System.out.println("Checking GV");
        	int mainMenuItems_size = blInfo.getGlobalVariable("mainMenuItems").size();
        	String categories = "mainMenuCategory";
        	String items = "mainMenuItemsList";
        	String page = "mainMenuPage";
        	String inputField = "mainMenuField";
        	String fieldNames[] = {"323","403","483","563","643","723","803","883","963","1043","1123","1203","1283","1363","1443","1523","1603","1683"};
        	int j = 0;
        	int totalPageNum = 0;
        	int k=0;
        	//System.out.println("yo, mainMenuItems_size is = "+mainMenuItems_size);
        	for(int i=0; i<mainMenuItems_size; i++){
           	 if(!blInfo.getGlobalVariable("mainMenuItems").getString(i).trim().equals("")){
           	 j++;
           	 }
        	}
        	
        	String[] mainMenuItemsList = new String[j];
        	String[] mainMenuCategory = new String[j];
        	String[] mainMenuPage = new String[j];
        	String[] mainMenuField = new String[j];
        	//System.out.println("menuItems_size = "+ j); 
        	
        	 for(int i=0; i<j; i++){
        	 if(!blInfo.getGlobalVariable("mainMenuItems").getString(i).trim().equals("")){
        	 mainMenuCategory[i] = blInfo.getGlobalVariable("mainMenuItems").getString(i).substring(0,10);
        	 mainMenuItemsList[i] = blInfo.getGlobalVariable("mainMenuItems").getString(i).substring(11);
        	 //System.out.println("The quotient of i/18 is ="+i/18);
        	 mainMenuPage[i] = Integer.toString(i/18);
        	 if(k == 18){
        		 k = 0;
        	 }
        	 mainMenuField[i] = fieldNames[k];
        	 k++;
         	  //System.out.println("mainMenuField = "+mainMenuField[i]);
        	 
        	 }
        	 }
        	 ////////////code for setting input field names start
//        	 {
//        		totalPageNum = j/18;
//        		System.out.println("totalPageNum"+totalPageNum);
//        		for(k=0;k<=totalPageNum;k++){
//        			System.out.println("Page Number = "+k);
//        		}
//        	 
//        	 }
        	 
        	 //////////code for setting input field names end
        	 IGlobalVariable gv1 = new GlobalVariable(categories, mainMenuCategory);
        	 blInfo.getGlobalVariables().put(categories, gv1);	
        	 IGlobalVariable gv2 = new GlobalVariable(items, mainMenuItemsList);
        	 blInfo.getGlobalVariables().put(items, gv2);
        	 IGlobalVariable gv3 = new GlobalVariable(page, mainMenuPage);
        	 blInfo.getGlobalVariables().put(page, gv3);
        	 IGlobalVariable gv4 = new GlobalVariable(inputField, mainMenuField);
        	 blInfo.getGlobalVariables().put(inputField, gv4);
		System.out.println("menuPageGV = "+blInfo.getGlobalVariable("menuPageGV").toString());  
		System.out.println("menuFieldGV = "+blInfo.getGlobalVariable("menuFieldGV").toString());
		
    	
    	} catch (Exception e) {
			// TODO: handle exception
		}

    }

	/**
	 * Example method that sets a named global variable from the current session to a value
	 * @param blInfo - IBusinessLogicInformation from current session
	 * @param name - Name of the global variable
	 * @param value - Value of the global variable
	 */
	public static void setGlobalVariable(IBusinessLogicInformation blInfo, String name, Object value)
	{
		IGlobalVariable gv = blInfo.getGlobalVariable(name);
		if ( gv == null )
		{
			gv = new GlobalVariable(name,value);
		}
		else
		{
			gv.set(value);		
		}
		blInfo.getGlobalVariables().put(name,gv);
	}

	/**
	 * Example method that sets a named shared global variable from the current session to a value
	 * @param blInfo - IBusinessLogicInformation from current session
	 * @param name - Name of the shared global variable
	 * @param value - Value of the shared global variable
	 */	
	public static void setSharedGlobalVariable(IBusinessLogicInformation blInfo, String name, Object value)
	{
		IGlobalVariable gv = blInfo.getSharedGlobalVariable(name);
		if ( gv == null )
		{
			gv = new GlobalVariable(name,value);
		}
		else
		{
			gv.set(value);		
		}
		blInfo.getSharedGlobalVariables().put(name,gv);
	}
	
	/**
	 * Example method that removes a named global variable from the current session
	 * @param blInfo - IBusinessLogicInformation from current session
	 * @param name - Name of the global variable
	 */
	public static void removeGlobalVariable(IBusinessLogicInformation blInfo, String name)
	{
		IGlobalVariable gv = blInfo.getGlobalVariable(name);
		if ( gv != null )
		{
			blInfo.getGlobalVariables().remove(name);
			gv.clear();
			gv = null;
		}
	}
	
	/**
	 * Example method that removes a named shared global variable from the current session
	 * @param blInfo - IBusinessLogicInformation from current session
	 * @param name - Name of the shared global variable
	 */
	public static void removeSharedGlobalVariable(IBusinessLogicInformation blInfo, String name)
	{
		IGlobalVariable gv = blInfo.getSharedGlobalVariable(name);
		if ( gv != null )
		{
			blInfo.getSharedGlobalVariables().remove(name);
			gv.clear();
			gv = null;
		}
	}

    /**
     * Example method that retrieves a named global variable from the current session
     * @param blInfo - IBusinessLogicInformation from current session
     * @param name - Name of the global variable
     */
    public static IGlobalVariable getGlobalVariable(IBusinessLogicInformation blInfo, String name)
    {        
        IGlobalVariable gv = blInfo.getGlobalVariable(name);
        return gv;
    }

	/**
	 * Example method that retrieves a named shared global variable from the current session
	 * @param blInfo - IBusinessLogicInformation from current session
	 * @param name - Name of the shared global variable
	 */
	public static IGlobalVariable getSharedGlobalVariable(IBusinessLogicInformation blInfo, String name)
	{		
		IGlobalVariable gv = blInfo.getSharedGlobalVariable(name);
		return gv;
	}

}
