


import java.io.*;
import java.util.StringTokenizer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.*;




/**
 * Servlet implementation class PdfExp
 */
public class PdfExp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PdfExp() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			
			ServletOutputStream op = response.getOutputStream();
			ServletContext      context  = getServletConfig().getServletContext();
			response.reset();
			response.setContentType("application/pdf" );

			response.setHeader( "Content-Disposition", "attachment; filename=\"ccc.pdf \"" );


			Document document=new Document();
			ByteArrayOutputStream buffer=new ByteArrayOutputStream(256);
			//FileOutputStream buffer=new FileOutputStream("PDF.pdf");
			PdfWriter.getInstance(document,buffer);
			
			document.open();
			System.out.println("opening document"+document.isOpen());
			String content=request.getParameter("pdfData");
			 int[] width={11,40,18,18,18,15,11,11,11};
			PdfPTable table = new PdfPTable(9);
			table.setWidths(width);
		        // the cell object
		        
		        table.setWidthPercentage(115f);

		        
		        
		       

		        table.setHeaderRows(1);
		        
		           /* table.addCell("Style Code");
		            table.addCell("Style Description");
		            table.addCell("Warehouse");
		            table.addCell(" Effect Date");
		            table.addCell("Ending Date");
		            table.addCell("Min Qty");
		            table.addCell("Roll Price");
		            table.addCell("Cut Price");
		            table.addCell("UM");
		            */
		        PdfPCell cell1=new PdfPCell(new Phrase("Style Code",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Style Description",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Warehouse",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Effect Date",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Ending Date",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Min Qty",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Roll Price",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("Cut Price",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        cell1=new PdfPCell(new Phrase("UM",FontFactory.getFont(FontFactory.HELVETICA, 8)));
		        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell1.setBackgroundColor(BaseColor.YELLOW);
		        table.addCell(cell1);
		        
		        
		        
		        
		        table.getDefaultCell().setBackgroundColor(null);
		        
		      
		       
		       
		        System.out.println("**** "+content);

		        
		        StringTokenizer st = new StringTokenizer(content,"_");
		        String currentVal="";
		   
		        
		        int nor=st.countTokens();
		        
		       
		        for(int i=0;i<nor;i++)
		        {
		        	currentVal =  st.nextToken();
		        	System.out.println("dffffffffffffffffffff"+currentVal);
		        		
		        	/*if (currentVal.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+")) {   
		                System.out.println("Is a number");   
		            } else {   
		                System.out.println("Is not a number");   
		            } */
		        		
		        	
		        	if(currentVal.equals("*"))
		        		currentVal=" ";
		        	table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		        	if(currentVal.contains("/")||currentVal.contains(".")){
		        		System.out.println("this is inside regex.............");
		        		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		        		
		        	      
		        	}
		        	if(currentVal.contains("M/DALTON") || currentVal.contains("A")||currentVal.contains("E") || currentVal.contains("I") ||currentVal.contains("O") ||currentVal.contains("U")){
		        		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		        		
		        	}
		        	table.addCell(new Phrase(currentVal, FontFactory.getFont(FontFactory.TIMES_ROMAN,8)));
		        	
		        	
		        }
		        
		        
		       
		        
		        document.add(table);

		    	
		        
			System.out.println("before closing document");
			document.close(); 
			System.out.println("closing document");
			//response.setContentType ( "application / pdf"); 

			//response.setContentLength (buffer.size()); 
			
		File file=new File(context.getRealPath("/PdfFile/test.pdf"));
		File excelfile=new File(context.getRealPath("C:exceltest.xls"));
		FileOutputStream foStream = new FileOutputStream(file);
		FileOutputStream foStreamexcel = new FileOutputStream(excelfile);
		buffer.writeTo(op);
		buffer.writeTo(foStream);
		buffer.writeTo(foStreamexcel);
		op.flush (); 
		buffer.close();	
		//this.generateExcel(request, response);
		
		
		File fileExcel=new File("C:\\testexcel.xls");
		
		WritableWorkbook workbook=Workbook.createWorkbook(fileExcel);
		WritableSheet sheet = workbook.createSheet("First Sheet", 0);
		Label label = new Label(0, 2, request.getParameter("pdfData"));
		sheet.addCell(label);
		workbook.write();
		
		FileOutputStream foStreamExc = new FileOutputStream(fileExcel);
		buffer.writeTo(foStreamExc);
		workbook.close(); 
		
		System.out.println("VINOD"+" **********  ");
		
		System.out.println(request.getParameter("pdfData"));
		
		
		
		}
		catch(Exception e){
			System.out.println("The error is "+ e);
		}

	}

	

	
	public void generateExcel(HttpServletRequest request, HttpServletResponse response)
	{
	
		try{
//			ServletContext context  = getServletConfig().getServletContext();
//			ByteArrayOutputStream buffer=new ByteArrayOutputStream(256);
//			
//			File file=new File(context.getRealPath("C:\\testexcel.xls"));
//			
//			WritableWorkbook workbook=Workbook.createWorkbook(file);
//			WritableSheet sheet = workbook.createSheet("First Sheet", 0);
//			Label label = new Label(0, 2, "R4R JExel API Example");
//			sheet.addCell(label);
//			workbook.write();
//			
//			FileOutputStream foStream = new FileOutputStream(file);
//			buffer.writeTo(foStream);
//			workbook.close(); 
			
			
		}catch (Exception e) {

		}
		
		
	}
	
	
	
	
	
}


